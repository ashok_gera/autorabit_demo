({
    //doInit function -- triggers when the component loading finish.
    //The objective of this function is to load onload events and on load calls to DB
    doInit : function(component, event, helper) {
        
        helper.showModal(component,'backdrop1','slds-backdrop--');
        helper.showModal(component,'confirmModal','slds-fade-in-');
              
    },
    //hideConfirmModal function -- triggers when the user tries to close the modal box.
    //The objective of this function is to hide the modal box
    approveConfirmModal : function(component, event,helper){
        //console.log("Hiding submit for approval modal");        
        var recId=component.get("v.recordId");
        var cmpTarget = component.find('confirmModal');
        var cmpBack = component.find('backdrop1');
        $A.util.removeClass(cmpBack,'slds-backdrop slds-backdrop--open');
        $A.util.addClass(cmpTarget, 'slds-hide');
        var navEvt = $A.get("e.force:navigateToSObject");
               navEvt.setParams({
               "recordId": recId,
               "slideDevName": "detail"
                });
               navEvt.fire();
    },
    
    
})