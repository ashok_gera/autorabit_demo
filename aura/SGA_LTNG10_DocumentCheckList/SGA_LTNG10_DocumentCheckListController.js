({
    doInit : function(component, event, helper) { 
        // Check Portal Login or Lightning
        var currentURL = window.location.pathname;
        if(currentURL.indexOf('one.app') > 0){
            component.set("v.isCommunityorLightning", 'Lightning');
            
        }
        else{
             component.set("v.isCommunityorLightning", 'Community');
        }
        //fetch list of DocumentCheckList
        var action = component.get("c.getDocumentCheckList");
        component.set("v.appId", component.get("v.recordId"));
        action.setParams({"applicationId":component.get('v.appId')});
        action.setCallback(this, function(actionResult) {
            component.set("v.documentCheckList", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action);
        
        //check current users profile
        var action1 = component.get("c.getCurrentUserProfile");
         action1.setCallback(this, function(actionResult) {
         component.set("v.profileName", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action1);
    },
    /* 
     * To open user Tab.
     */
    openUserRecord: function(component, event, helper){
        var userId = event.currentTarget.id;
        var myEvent = $A.get("e.c:RedirectToUrl");
        myEvent.setParams({"redirectRecId":userId,"SObject":"User"});
        myEvent.fire();
    },
    
    handleDeleteOperation : function(component, event, helper) { 
        var whichOne = event.getSource().getLocalId();
        var docid=event.getSource().get("v.class");
        //alert(docid);
        
        $A.createComponent(
            "c:SGA_LTNG10_DeleteDocumentCheckList",
            {
                "documentId": docid
            },
            function(newComponent){component.set("v.body",newComponent);}
        );                
        
    },
    
    handleEditOperation : function(component, event, helper) { 
        
        var whichOne = event.getSource().getLocalId();
        var docid=event.getSource().get("v.class");
        
        $A.createComponent(
            "c:SGA_LTNG10_EditDocumentCheckList",
            {
                "documentId": docid
            },
            function(newComponent){component.set("v.body",newComponent);}
        ); 
        
        
    },
})