({
	doInit : function(cmp, event, helper) {
        helper.initHelper(cmp);
    },
    //Vlocity Action Handler
    handleAction : function(cmp, event) {
        var item = event.getParam('item');
        var baseURL = cmp.get('v.baseURL');
        var isLEX;
        if (baseURL.includes('one.app')) {
            isLEX = true;
        }else {
            isLEX = false;
        }
        var customCommunityPage = cmp.get('v.customCommunityPage');
        var path = baseURL + item.url;
        
        //alert(':::Inside if :::path:::'+path);
        /*if (item.url.includes('/apex/')) {
            if (!isLEX) {
                path = baseURL + customCommunityPage + '?actionUrl=' + item.url;
           		//alert(':::Is Lex :::path:::'+path);
            }else {
                path = item.url;
                //alert(':::Inside Else :::path:::'+path);
            }
        }*/
        //alert(':::Final :::path:::'+path);
        // if (item.methodName !== null) HANDLE INVOKE METHOD
        //window.location.href = path;
        window.open(path, '_blank');
    }
})