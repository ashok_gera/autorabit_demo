({
    //doInit function -- triggers when the component loading finish.
    //The objective of this function is to load onload events and on load calls to DB
    doInit : function(component, event, helper) {
        helper.showModal(component,'backdrop1','slds-backdrop--');
        helper.showModal(component,'editConfirmModal','slds-fade-in-');
        var action = component.get("c.getdocumentDetails");
        var action1 = component.get("c.getDocumentStatus");
        action.setParams({"docId":component.get('v.documentId')});
        action1.setParams({"docId":component.get('v.documentId')});
        action.setCallback(this, function(actionResult) {
        
        component.set("v.documentDetails", actionResult.getReturnValue()); 
        
        });
        
        action1.setCallback(this, function(a) {
            component.find("InputSelectDocStatus").set("v.options", a.getReturnValue()); 
        });
        $A.enqueueAction(action);
        $A.enqueueAction(action1);
    },
    //hideEditConfirmModal function -- triggers when the user tries to close the modal box.
    //The objective of this function is to hide the modal box
    hideEditConfirmModal : function(component, event,helper){
        
        var cmpTarget = component.find('editConfirmModal');
        var cmpBack = component.find('backdrop1');
        $A.util.removeClass(cmpBack,'slds-backdrop slds-backdrop--open');
        $A.util.addClass(cmpTarget, 'slds-hide');
    },
    //confirmEdit function -- triggers when the user tries to submit/save modal box.
    //The objective of this function is to save the changes in DB.
    confirmEdit : function(component, event,helper){
        helper.hideModal(component,'editConfirmModal','slds-fade-in-'); 
        helper.hideModal(component,'backdrop1','slds-backdrop--');
      
    }
    
})