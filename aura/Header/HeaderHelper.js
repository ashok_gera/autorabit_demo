({
    //starts listner when the header mini-menus are opened
	startListener : function(component, orignalClick) {
		//allows for detection inside the iframe 
        window.iFrameMonitor = setInterval(function(){
            var elem = document.activeElement;
            //active element should become null if just detecting on the document
            if(elem == null || (elem && elem.tagName == "IFRAME")){
                clearInterval(window.iFrameMonitor);
                component.set("v.isIconMenuOpen", false);
               _this.killListener();
            }
        }, 100);
        //set the event listener for the rest of the parent DOM
        var _this = this;
        window.eventListener = _this.determineClose(_this, component, orignalClick);
        document.addEventListener('click', window.eventListener, true);
	},
    //function to remove all listeners in the scenario of a mini-listener closing
    killListener : function(){
        if (window.eventListener !== null) {
            document.removeEventListener('click', window.eventListener, true);
        }
        if (window.iFrameMonitor !== null) {
        	clearInterval(window.iFrameMonitor);
        }
	},
    //context is the instance of the helper to allow calling of the killListener function
    determineClose : function(context, component, originalClick) {
        var _this = context;
        return function(clickEvent) { 
            if (originalClick.target !== clickEvent.target && !document.getElementById("support-menu-container").contains(clickEvent.target)) { 
               component.set("v.isIconMenuOpen", false);
               _this.killListener();
            };
        }
	}
})