({
    doInit : function(component, event, helper) {  
    },
    togglePopUp : function(component, event, helper) {
        var isOpen = component.get("v.isIconMenuOpen");
        if (isOpen) {
            component.set("v.isIconMenuOpen", !isOpen);
            helper.killListener();
        } else { 
            component.set("v.isIconMenuOpen", !isOpen);
            helper.startListener(component, event);
        };
    }
})