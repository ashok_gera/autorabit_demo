/**************************************************************************
Controller Name  : BrokerSearch
Created Date     : 29-March-2017
Created By       : IDC Offshore
Description      : This javascript controller for BrokerSearch (SGA project)
***************************************************************************/
({
    doInit: function(component, event, helper) {
        
        var currentURL = window.location.pathname;
        //alert('currentURL'+currentURL);
         if(currentURL.indexOf('one.app') > 0 || currentURL.indexOf('broker') >0 ){
           component.set("v.isCommunityorLightning", true);
         }else {
             component.set("v.isCommunityorLightning", false);
         }
          
        var action = component.get("c.getUserDetails"); 
        action.setCallback(this, function(a) {
            var userrec = a.getReturnValue();
            var quoteActionDiv = component.find("quoteAction");
            var enrollActionDiv = component.find("enrollAction");
            component.set("v.portalUserContactId", userrec.ContactId);
            if(userrec.Profile.Name.indexOf('Non Quoting') === -1) {
        		$A.util.removeClass(quoteActionDiv, "toggle");
        		$A.util.addClass(quoteActionDiv, "slds-show");
            } else {
                $A.util.removeClass(enrollActionDiv, "toggle");
        		$A.util.addClass(enrollActionDiv, "slds-show");
            }
        });
        $A.enqueueAction(action);
    },
    scrollToResults : function(component, event, helper) {
    	alert("Hi");
    },
	getAccQuoteApplicationRec : function(component, event, helper) {
		var groupName = component.get("v.groupName");
        var groupNumber = component.get("v.groupNumber");
        var employerTaxId = component.get("v.employerTaxId");
        
        if(groupName == null && groupNumber == null && employerTaxId == null){
            alert('Please input atleast one field to search..')
        }else{
            var searchAccount = component.get("c.getAccountSearchResults");
            searchAccount.setParams({
                "groupName" : groupName,
                "groupNumber" : groupNumber,
                "employerTaxId" : employerTaxId
            });
            searchAccount.setCallback(this,function(data){
               if(data.getReturnValue() == null || data.getReturnValue() === undefined || data.getReturnValue() == ''){
                   component.set("v.noresults",$A.get("$Label.c.SG35_NoGroupResults"));
                   component.set("v.accountList",null);
                   window.scrollTo(0, 500);
                   var noResultsDiv = document.getElementById('noResults');
                    if(noResultsDiv != null){
                        var scrollToResults = document.getElementById('noResults').offsetTop;
                        window.scrollTo(0, scrollToResults);
                    } else {
                        setTimeout(function(){ 
                            var scrollToResults = document.getElementById('noResults').offsetTop;
                            window.scrollTo(0, scrollToResults);
                        }, 500);
                    }
               }else{
                    component.set("v.accountList",data.getReturnValue());
                   	component.set("v.noresults","");
                    var accountsDiv = document.getElementById('accountList');
                    if(accountsDiv != null){
                        var top = document.getElementById('accountList').offsetTop;
                        window.scrollTo(0, top);
                    } else {
                        setTimeout(function(){ 
                            var top = document.getElementById('accountList').offsetTop;
                            window.scrollTo(0, top);
                        }, 500);
                    }
               }
                              
                
            })
           
            $A.enqueueAction(searchAccount);
        }
	},
    
    navigateToRollCall : function(component, event, helper) {
        var selectedAnsIdx = event.target.id;
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/quote?quoteId='+selectedAnsIdx+'&',
            "isredirect" :false
        });
        urlEvent.fire();
    },
    /*Lauching Quote Omniscript Function*/
    gotoURL : function(component, event, helper) {
        //window.location.reload( true );
        var userContactId = component.get("v.portalUserContactId");
        var urlEvent = $A.get("e.force:navigateToURL");
        var currentURL = window.location.pathname;
        //alert('currentURL::->'+currentURL);
        if(currentURL.indexOf('one.app') > 0){
            urlEvent.setParams({
                 "url": "../apex/AnthemSG#/"
            });
        }else{
            urlEvent.setParams({
                /*"url": "/getaquote"*/
                "url": "/getaquote?actionUrl=apex/AnthemSG?ActionButton=quote"
            });
        }
        
        urlEvent.fire();
	},
     gotoClassicURL : function(component, event, helper) {
            var currentURL = window.location.pathname;
            // alert('Classic URL>>'+currentURL);
             window.location.href= "/apex/AnthemSG#/";
    },
    
    /*Lauching L&C Enroll Omniscript Function*/
    gotoEnrollURL : function(component, event, helper) {
        //window.location.reload( true );
        var userContactId = component.get("v.portalUserContactId");
        var urlEvent = $A.get("e.force:navigateToURL");
        var currentURL = window.location.pathname;
        //alert('currentURL::->'+currentURL);
        if(currentURL.indexOf('one.app') > 0){
            urlEvent.setParams({
                 "url": "../apex/AnthemSG_NonQuoting#/"
            });
        }else{
            urlEvent.setParams({
                "url": "/getaquote?actionUrl=apex/AnthemSG_NonQuoting?ActionButton=enroll"
            });
        }
        
        urlEvent.fire();
	},
     gotoClassicEnrollURL : function(component, event, helper) {
            var currentURL = window.location.pathname;
            window.location.href= "/apex/AnthemSG_NonQuoting#/";
    }
})