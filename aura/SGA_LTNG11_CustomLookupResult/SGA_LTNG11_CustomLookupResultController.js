({
	 selectAnObject : function(component, event, helper){      
    // get the selected Account from list  
      var getSelectobj = component.get("v.oSobject");
     
    // call the event   
      var compEvent = component.getEvent("oSelectedObjEvent");
    // set the Selected Account to the event attribute.  
         compEvent.setParams({"objByEvent" : getSelectobj });  
    // fire the event  
         compEvent.fire();
    },
})