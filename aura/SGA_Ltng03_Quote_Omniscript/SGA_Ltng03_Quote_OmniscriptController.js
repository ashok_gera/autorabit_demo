/*****************************************************************************************
Controller Name  : SGA_Ltng03_Quote_Omniscript
Created Date     : 11-April-2017
Created By       : IDC Offshore
Description      : This javascript controller for SGA_Ltng03_Quote_Omniscript (SGA project)
*******************************************************************************************/
({
	gotoURL : function(component, event, helper) {
        //window.location.reload(false); 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          /*"url": "/getaquote"*/
          "url": "/getaquote?actionUrl=apex/AnthemSG?ActionButton=quote"
        });
        urlEvent.fire();
	}
})