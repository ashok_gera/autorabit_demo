({
    doInit : function(component, event, helper) { 
        
        // Check Portal Login or Lightning
        var currentURL = window.location.pathname;
        if(currentURL.indexOf('one.app') > 0){
            component.set("v.isCommunityorLightning", 'Lightning');
        }
        else{
             component.set("v.isCommunityorLightning", 'Community');
        }
        
        var action = component.get("c.completedApplicationList");
         component.set("v.accId", component.get("v.recordId"));
         action.setParams({"accId":component.get('v.accId')});
         action.setCallback(this, function(actionResult) {
         component.set("v.wrpApplicationList", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action);
    }
})