/*****************************************************************************************
Controller Name  : SGA_LTNG14_NonQuotingEnrollment
Created Date     : 31-Jan-2018
Created By       : IDC Offshore
Description      : This javascript controller for SGA_LTNG14_NonQuotingEnrollment (SGA project)
*******************************************************************************************/
({
	gotoURL : function(component, event, helper) {
        //window.location.reload(false); 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          /*"url": "/getaquote"*/
          "url": "/getaquote?actionUrl=apex/AnthemSG_NonQuoting?ActionButton=enroll"
        });
        urlEvent.fire();
	}
})