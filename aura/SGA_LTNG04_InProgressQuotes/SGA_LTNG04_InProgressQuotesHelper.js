({
    getUserList: function(component) {
        var action = component.get("c.InProcessQuotesList");
        action.setParams({"accId":component.get('v.accId')});
        action.setCallback(this, function(actionResult) {
            component.set("v.savedOmniscripts", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action);
    }
})