({
    //doInit function -- triggers when the component loading finish.
    //The objective of this function is to load onload events and on load calls to DB
    doInit : function(component, event, helper) {
        
        helper.showModal(component,'backdrop1','slds-backdrop--');
        helper.showModal(component,'deleteConfirmModal','slds-fade-in-');
       // alert("documentId "+component.get("v.documentId"));        
    },
    //hideDeleteConfirmModal function -- triggers when the user tries to close the modal box.
    //The objective of this function is to hide the modal box
    hideDeleteConfirmModal : function(component, event,helper){
        //console.log("Hiding submit for approval modal");        
        //helper.hideModal(component,'deleteConfirmModal','slds-fade-in-'); 
        //helper.hideModal(component,'backdrop1','slds-backdrop--');
        var cmpTarget = component.find('deleteConfirmModal');
        var cmpBack = component.find('backdrop1');
        $A.util.removeClass(cmpBack,'slds-backdrop slds-backdrop--open');
        $A.util.addClass(cmpTarget, 'slds-hide');
    },
    //confirmDelete function -- triggers when the user tries to submit/save modal box.
    //The objective of this function is to save the changes in DB.
    confirmDelete : function(component, event,helper){
        helper.hideModal(component,'deleteConfirmModal','slds-fade-in-'); 
        helper.hideModal(component,'backdrop1','slds-backdrop--');
      }
    
})