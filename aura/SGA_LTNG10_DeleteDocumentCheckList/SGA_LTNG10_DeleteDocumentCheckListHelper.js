({
    //showDeleteConfirmModal function -- triggers when the user wants to open the modal box.
    //The objective of this function is to open the modal box
    showDeleteConfirmModal: function(component, event,helper) {
        console.log("Showing submit for approval modal");        
        this.showModal(component,'backdrop1','slds-backdrop--');
        this.showModal(component,'deleteConfirmModal','slds-fade-in-');
        console.log("Showing submit");
    },  
    //showModal function -- triggers when the user wants to open the modal box.
    //The objective of this function is to open the modal box
    showModal : function(component,componentId,className) {
        console.log("Showing submit  showModal");
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    //hideModal function -- triggers when the user wants to hide the modal box.
    //The objective of this function is to hide the modal box
    hideModal : function(component,componentId,className) {
        
        var modal = component.find(componentId);
        
       
        var action = component.get("c.getDeletedlist");
        action.setParams({
            "docId" : component.get("v.documentId") 
        });
        action.setCallback(this,function(a){
            var state = a.getState();
            if(state === 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            }
            
        });
        $A.enqueueAction(action);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    }
    
})