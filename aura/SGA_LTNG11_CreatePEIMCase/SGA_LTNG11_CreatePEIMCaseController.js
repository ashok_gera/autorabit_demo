({
	//doInit function -- triggers when the component loading finish.
    //The objective of this function is to load onload events
    doInit : function(component, event, helper) {
        // populate state Picklists
       var action1 = component.get("c.getCaseSates");
       action1.setCallback(this, function(a) {
            component.find("InputSelectStates").set("v.options", a.getReturnValue()); 
        });
        $A.enqueueAction(action1);
        
        // populate Agent Details
        var action2 = component.get("c.getAgent");
        action2.setCallback(this, function(actionResult) {
         component.set("v.writingAgent", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action2);
 
    },
	create : function(component, event, helper) {
		console.log('Create Case');
        var csData = component.get("v.cs");
        var action = component.get("c.createCase");
        //fetch account ID
        var accountId = component.get('v.objNew').Id;
        var caseDesc= component.get('v.caseDescription');
		csData.Description = caseDesc;
        // validation check
        var agentName= component.get('v.writingAgent').writingAgentName;
        var agentEtin= component.get('v.writingAgent').writingAgentETIN;
        var InputSelectStates = component.find("InputSelectStates");
        
        var input_agentName = component.find("input_agentName");
        var input_etin = component.find("input_etin");
        //var input_phone = component.find("input_phone");
        //var input_email = component.find("input_email");
        if($A.util.isEmpty(accountId) || $A.util.isUndefined(accountId)){
            alert('Account is Required');
            return;
        }
        if($A.util.isEmpty(agentName) || $A.util.isUndefined(agentName)){
             input_agentName.set("v.errors", [{message:"Broker/Writing Agent Name is Required" }]);
            return;
            
        } else{
            input_agentName.set("v.errors",null);
        }
        
         if($A.util.isEmpty(agentEtin) || $A.util.isUndefined(agentEtin)){
             input_etin.set("v.errors", [{message:"Broker/Writing Agent ETIN is Required" }]);
            return;
            
        } else{
            input_etin.set("v.errors",null);
        }
       
		if($A.util.isEmpty(csData.BR_State__c) || $A.util.isUndefined(csData.BR_State__c)){
            
            InputSelectStates.set("v.errors", [{message:"Select a state value" }]);
            return;
            
        } else{
            InputSelectStates.set("v.errors",null);
        }
        
        /* if($A.util.isEmpty(csData.ContactPhone) || $A.util.isUndefined(csData.ContactPhone)){
              input_phone.set("v.errors", [{message:"Enter Contact Phone" }]);
             return;
        }else{
            input_phone.set("v.errors",null);
        }
        
        if($A.util.isEmpty(csData.ContactEmail) || $A.util.isUndefined(csData.ContactEmail)){
              input_email.set("v.errors", [{message:"Enter Contact Email" }]);
              return;
        }else{
            input_email.set("v.errors",null);
        } */
        
        if($A.util.isEmpty(csData.BR_Category__c) || $A.util.isUndefined(csData.BR_Category__c)){
            alert('Category is Required');
            return;
        }
        
        
        action.setParams({
            cs : csData,
            accId : accountId   
        });
        
        action.setCallback(this,function(a,component){
            var state = a.getState();
            ;
            if(state == "SUCCESS"){
             var recId=a.getReturnValue().Id;
              var action3 = component.get("c.getCaseNumber");
              action3.setParams({
              insertedCaseId : recId   
             });
             action3.setCallback(this, function(ca) {
               component.set("v.caseNum", ca.getReturnValue()); 
                   var caseNumberCreated = ca.getReturnValue();
                  console.log('Created Case'+ca.getReturnValue());
                 //alert('Thank you for submitting your inquiry to Anthem. Your Request '+caseNumberCreated+' has been received and you will be contacted shortly.') ; 
                   $A.createComponent(
                 "c:SGA_LTNG11_ConfirmPEIMCase",
                  {
                   "recordNumber": caseNumberCreated,
				   "recordId": recId
                   },
                   function(newComponent){component.set("v.body",newComponent);}
                   );
             });
             $A.enqueueAction(action3);
            
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
        });
       $A.enqueueAction(action);
	},
       
})