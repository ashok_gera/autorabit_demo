({
	fetchCaseNum : function(component,caseId) {
		   var action3 = component.get("c.getCaseNumber");
             
              action3.setParams({
              insertedCaseId : caseId   
             });
             
             action3.setCallback(this, function(a) {
               component.set("v.caseNum", a.getReturnValue()); 
                   var caseNumberCreated = a.getReturnValue().CaseNumber;
                   console.log('Created Case'+a.getReturnValue());
                 alert('Thank you for submitting'+caseNumberCreated) ; 
             });
              $A.enqueueAction(action3);
          
	},
    
     // automatically call when the component is done waiting for a response to a server request.  
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : false });
        evt.fire();    
    },
 // automatically call when the component is waiting for a response to a server request.
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        var evt = spinner.get("e.toggle");
        evt.setParams({ isVisible : true });
        evt.fire();    
    },

    
    
})