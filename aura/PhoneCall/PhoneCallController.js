({
        
    myAction : function(component, event, helper) {
        
		var cmp=component.find('container1');
        $A.util.toggleClass(cmp,'container1Active');
        var cmp1=component.find('mainContainer');
        $A.util.toggleClass(cmp1,'transformActive');
        var cmp2=component.find('logoDiv');
        $A.util.toggleClass(cmp2,'hideDiv');
	},
    showpopup : function(cmp, evt, helper) {
      /* previous logic... */
      var myPopover = cmp.find('onclickPopover');
      $A.util.toggleClass(myPopover, 'slds-hide');
     
    },
    
    hidepopover : function(cmp, evt, helper){
         var myPopover = cmp.find('onclickPopover');
        $A.util.removeClass(myPopover, 'slds-hide');
      
        
    }
})