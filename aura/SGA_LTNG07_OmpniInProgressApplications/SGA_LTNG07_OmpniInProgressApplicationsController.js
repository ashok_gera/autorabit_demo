({
    doInit : function(component, event, helper) { 
        //Fetch the in Progress Applications from the Apex controller
        var toggleText = component.find("popupId");
        $A.util.addClass(toggleText, "slds-hide");   
        var action = component.get("c.inProgressApplications");
        component.set("v.accId", component.get("v.recordId"));
        action.setParams({"accId":component.get('v.accId')});
        action.setCallback(this, function(actionResult) {
        component.set("v.savedOmniscripts", actionResult.getReturnValue());            
        });
        $A.enqueueAction(action);
        //check current users profile
        var action1 = component.get("c.getCurrentUserProfile");
         action1.setCallback(this, function(actionResult) {
         component.set("v.profileName", actionResult.getReturnValue()); 
        });
        $A.enqueueAction(action1);
        
    },
     /* 
     * To toggle the classname for action button onclick.
     */
     toggleVisibility: function(cmp, event) {
        console.log('event.target.parentElement.className',event.target.parentElement.className);
        if( event.target.parentElement.className =="slds-dropdown-trigger slds-dropdown-trigger_click"){
            event.target.parentElement.className = "slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open";
        }
        else if(event.target.parentElement.className=="slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open"){
            event.target.parentElement.className= "slds-dropdown-trigger slds-dropdown-trigger_click" ;  
        }
    },
    /* 
     * To Cancel the saved omniscript.
     */
     Cancel : function(component, event, helper){
        var Rec = event.currentTarget;
        var Index = Rec.dataset.index;
        console.log('Rowindex',Index);
        var Omni = component.get('v.savedOmniscripts');
        var OmniRec = Omni[Index];
        console.log('record',OmniRec);
        if(confirm("Are you sure you want to cancel the Application?")== true){
            var action = component.get("c.cancelInProgressApplications");
            action.setParams({"Rec":OmniRec,"accId":component.get("v.recordId")});
            action.setCallback(this, function(response) 
                               { 
                                   component.set("v.savedOmniscripts", response.getReturnValue()); 
                                  /* var state = response.getState();
                                   if (state === "SUCCESS") 
                                   {
                                       console.log('response',response.getReturnValue());
                                       component.set("v.savedOmniscripts", response.getReturnValue()); 
                                  
                                   } 
                                   else if (state === "INCOMPLETE") {alert('Incomeplte') } 
                                       else if (state === "ERROR") 
                                       { var errors = response.getError(); 
                                        if (errors) 
                                        { if (errors[0] && errors[0].message) 
                                        { console.log("Error message: " + errors[0].message); 
                                        } } else { console.log("Unknown error"); } } */
                               });
            
            $A.enqueueAction(action);
        }    
    },
    /* 
     * To open user Tab.
     */
    openUserRecord: function(component, event, helper){
        var userId = event.currentTarget.id;
        var myEvent = $A.get("e.c:RedirectToUrl");
        myEvent.setParams({"redirectRecId":userId,"SObject":"User"});
        myEvent.fire();
    },
    
    testOpenSubtab: function(component, event, helper){
        helper.validateLicense(component,event);        
	},
    
    closeError: function(component, event, helper){
        helper.closePopup(component);        
    }
})