({
	validateLicense : function(component,event) {
		var parentId = event.target.attributes['data-Id'].value;
        var action = component.get("c.inProgressApplicationLicense");
        var licStat;
        action.setParams({"parentId":parentId});
        action.setCallback(this, function(actionResult) {                        
            licStat = actionResult.getReturnValue();            
            if(licStat != undefined && licStat.indexOf('not actively licensed') == -1){
                if (sforce.console.isInConsole()) {
                    sforce.console.getEnclosingPrimaryTabId(function(result) {
                        var primaryTabId = result.id;
                        sforce.console.openSubtab(primaryTabId , event.target.attributes['data-link'].value, true, 'Create SG Enrollment', null, null, 'Create SG Enrollment');
                    });
                } 
                else{                    
                    window.open(event.target.attributes['data-link'].value, '_blank');
                }
            }
            else{
                //alert('You are not actively licensed and contracted in this state');
                var toggleText = component.find("popupId");
                $A.util.removeClass(toggleText, "slds-hide");
        		$A.util.addClass(toggleText, "slds-show");
            }            
        });
        $A.enqueueAction(action);
	},
    closePopup:function(component) {
        var toggleText = component.find("popupId");
        $A.util.removeClass(toggleText, "slds-show");
		$A.util.addClass(toggleText, "slds-hide");
    }
})