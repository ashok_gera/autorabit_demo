({
	doInit : function(component, event, helper) {
		
        var recId = component.get("v.recordId");
        component.set("v.recID", recId);
       
	},
    
    create : function(component, event, helper){
        
        var commentField = component.find("comment");		
		var commentValue = commentField.get("v.value");
		
		if($A.util.isEmpty(commentValue) || $A.util.isUndefined(commentValue)){
            commentField.set("v.errors", [{message:"Enter the Comment"}]);
			return;
        }         
        else {
            commentField.set("v.errors",null);
        }
    
		var cc =component.get("v.cc");
        var parIdSend =component.get("v.recID");
        var action = component.get("c.createCaseComment");
        action.setParams({
            cc : cc,
            caseParnt : parIdSend
        });
        
        action.setCallback(this, function(a){
            
            var state =a.getState();
            if(state == "SUCCESS"){
                var newCaseComment = { 'sobjectType': 'CaseComment',
                                       'IsPublic': '', 
                                   	   'CommentBody': '',
                         		       'ParentID': '' 
                                   };
                	component.set("v.cc", newCaseComment);
                	alert('Comment Added Successfully!');
                	window.top.close();
            	}  
            else if(state == "ERROR"){
                alert('Error Creating Case Comment. Please contact the Administrator');
            }                        
        });
        $A.enqueueAction(action);
    },
    
	cancel : function(component, event, helper){
        window.top.close();
	},
	
	handleError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.addClass(comp, "error");   
    },

    handleClearError:function(cmp,event,helper){
        var comp = event.getSource();
        $A.util.removeClass(comp, "error");   
    }
	
})