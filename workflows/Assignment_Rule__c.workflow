<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Assignment_Rule_ID_Name_Geography</fullName>
        <description>Updates the Assignment Rule Name for a new Geography Assignment</description>
        <field>Name</field>
        <formula>IF( RecordType.Name = &quot;Geography Record Type&quot;, State__r.Name, (Campaign__r.Name + Campaign__r.Event_Code1__c ) )</formula>
        <name>Anthem Opps: Assignment Rule Name - Geo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Anthem Opps%3A Assignment Rule ID Name - Geography</fullName>
        <actions>
            <name>Assignment_Rule_ID_Name_Geography</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Assignment Rule Name for a new Geography Assignment</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
