<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>SFDC_Campaign_RTKeys</fullName>
        <apiVersion>36.0</apiVersion>
        <endpointUrl>https://app2.informaticacloud.com/saas/api/1/salesforceoutboundmessage/p7DZfJMte22oVheMDIbt9o7SItEbmwTb</endpointUrl>
        <fields>Event_Code1__c</fields>
        <fields>Event_Code__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>ad64108@wellpoint.com.isg</integrationUser>
        <name>SFDC_Campaign_RTKeys</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>SFDC_Campaign_RTKeys</fullName>
        <actions>
            <name>SFDC_Campaign_RTKeys</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Campaign.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Campaign.CreatedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
