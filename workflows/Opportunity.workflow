<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Anthem_Broker_Expired_Lead_Notification</fullName>
        <description>Anthem Opps: Broker - Expired Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Broker__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_Expired_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Broker_Inactive_Lead_Notification</fullName>
        <description>Anthem Opps: Broker - Inactive Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_Inactive_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Broker_New_Lead_Day_2_Reminder</fullName>
        <description>Anthem Opps: Broker - New Lead Day 2 Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_New_Opportunity_Day_2_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Broker_New_Lead_Day_3_Reminder</fullName>
        <description>Anthem Opps: Broker - New Lead Day 3 Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_New_Opportunity_Day_3_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Lead_Owner_Inactive_Lead_for_60_days</fullName>
        <description>Anthem Opps: Lead Owner - Inactive Lead for 60 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_Inactive_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Declined_Opp_Notice_for_Old_Owner</fullName>
        <description>Anthem Opps: Declined Opp Notice for Old Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Sales_Rep_Emails/Anthem_Opps_Declined_Opportunity_Notification_for_Old_Owner</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Future_Opportunity_Task_Reminder</fullName>
        <description>Anthem Opps: Future Opportunity - Task Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Future_Opportunity_Task_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Non_Broker_Change_of_Owner_Email_Alert</fullName>
        <description>Anthem Opps: Non-Broker Change of Owner Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_InternalAnthem_New_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Non_Broker_New_Opportunity_Alert</fullName>
        <description>Anthem Opps: Non-Broker New Opportunity Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_InternalAnthem_New_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Opportunity_Update_Alert</fullName>
        <description>Anthem Opps: Opportunity Update Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ad64108@wellpoint.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Opportunity_Update</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Opps_Sales_Admin_New_Opportunity_Assignment</fullName>
        <description>Anthem Opps: Sales Admin - New Opportunity Assignment</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Sales_Admin_Emails/Anthem_Opps_Sales_Admin_New_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Anthem_Sales_Rep_New_Lead_Notification</fullName>
        <description>Anthem Opps: Sales Rep - New Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Sales_Rep_Emails/Anthem_Opps_Sales_Rep_New_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_to_Sales_Rep_that_lead_expired</fullName>
        <description>Anthem Opps: Sales Rep - Expired Owned Lead Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Sales_Rep_Emails/Anthem_Sales_Rep_Expired_Opportunity_Notification</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Lead_assignment_notification_email</fullName>
        <description>Anthem Opps: Broker - New Lead Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/Anthem_Opps_Broker_New_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>Sales_Rep_notifying_them_that_their_Broker_has_been_assigned_a_lead</fullName>
        <description>Anthem Opps: Sales Rep - Broker assigned to lead</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Rep__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Sales_Rep_Emails/Anthem_Opps_Sales_Rep_Broker_Assigned_New_Opportunity_Alert</template>
    </alerts>
    <alerts>
        <fullName>TESTINGANTHEMOPPSUPDATES</fullName>
        <description>TESTINGANTHEMOPPSUPDATES</description>
        <protected>false</protected>
        <recipients>
            <type>partnerUser</type>
        </recipients>
        <recipients>
            <type>partnerUser</type>
        </recipients>
        <recipients>
            <recipient>ad64108@wellpoint.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>anthemleads@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Anthem_Broker_Emails/TESTINGAnthem_Opps_Opportunity_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>Anthem_Leads_Opp_Date_Time_Assigned</fullName>
        <description>When the lead is created or owner is changed, update the Date Assigned to reflect the date the new owner was assigned the lead</description>
        <field>Date_Time_Assigned__c</field>
        <formula>NOW()</formula>
        <name>Anthem Opps: Opp - Date/Time Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Anthem_Opps_Language_Preference</fullName>
        <description>Anthem Opps Language Preference, 2-Spanish</description>
        <field>Language_Preference__c</field>
        <literalValue>Spanish</literalValue>
        <name>Anthem Opps: Language Preference (Span)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Anthem_Opps_Language_Preference_English</fullName>
        <description>Anthem Opps: If 1-English</description>
        <field>Language_Preference__c</field>
        <literalValue>English</literalValue>
        <name>Anthem Opps:Language Preference(English)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Anthem_Opps_New_Opp_Initial_Assignment</fullName>
        <description>Checks the checkbox on create when a new Opp is assigned to a non-broker user.</description>
        <field>Anthem_Opps_New_Initial_Owner_Assigned__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: New Opp Initial Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Anthem_Opps_Populate_Close_Date</fullName>
        <description>Populates Close Date when Opp marked as Closed Won</description>
        <field>CloseDate</field>
        <formula>NOW()</formula>
        <name>Anthem Opps: Populate Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Anthem_Opps_Primary_Campaign_Source2_Up</fullName>
        <description>Updates the Primary Campaign Source2 with the Campaign Description</description>
        <field>Primary_Campaign_Source2__c</field>
        <formula>Campaign.Description</formula>
        <name>Anthem Opps: Primary Campaign Source2 Up</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_assignment_to_SalesRep</fullName>
        <description>Anthem Opps - Marks the Auto-Assignement field to true after 72 hours</description>
        <field>Auto_assignment__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Auto-assignment to SalesRep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_Campaign_Description</fullName>
        <field>Campaign_Description__c</field>
        <formula>Campaign.Description</formula>
        <name>Fill Campaign Description</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>First_Name</fullName>
        <description>Patching for Apex not fired in this case.</description>
        <field>First_Name__c</field>
        <formula>Primary_Account_Contact__r.FirstName</formula>
        <name>OP - SG Direct - FName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Increment_Push_Counter_Field</fullName>
        <description>Increment the Push Counter by 1</description>
        <field>Push_Counter__c</field>
        <formula>IF( 
ISNULL( Push_Counter__c ), 
1, 
Push_Counter__c + 1 
)</formula>
        <name>Increment Push Counter Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Expire_to_Sales_Rep</fullName>
        <description>Expire the Sales Rep upon Expiring from the Broker</description>
        <field>Broker_Expired__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Opp: Expire to Sales Rep</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU01_OppStage</fullName>
        <field>StageName</field>
        <literalValue>Opportunity</literalValue>
        <name>OPP_FU01_OppStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU01_SGStage</fullName>
        <field>StageName</field>
        <literalValue>Waiting for Disposition</literalValue>
        <name>OPP_FU01_SGStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU02_WEM_Status_Followup_Check</fullName>
        <field>Application_Status_Follow_Up__c</field>
        <literalValue>1</literalValue>
        <name>OPP_FU02_WEM Status Followup Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU03_UpdateStage</fullName>
        <description>update Customer Stage to &quot;Application Submitted&quot; when Status changes</description>
        <field>Customer_Stage__c</field>
        <literalValue>Application Submitted</literalValue>
        <name>OPP_FU03_UpdateStageOnStatusChange</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU04_RecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Won_Lost_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OPP_FU04_RecordTypeUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU05_UpdateStageOnStatusEnrolled</fullName>
        <field>Customer_Stage__c</field>
        <literalValue>Customer Enrolled</literalValue>
        <name>OPP_FU05_UpdateStageOnStatusEnrolled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU06_WEM_Status_Followup_UnCheck</fullName>
        <field>Application_Status_Follow_Up__c</field>
        <literalValue>0</literalValue>
        <name>OPP_FU06_WEM Status Followup UnCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OP_SG_Direct_Company_Name</fullName>
        <description>Patching missing flow from Apex not firing in this business case.</description>
        <field>Company_Name__c</field>
        <formula>Account.Name</formula>
        <name>OP - SG Direct - Company Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OP_SG_Direct_LName</fullName>
        <description>Patch, Apex, etc.</description>
        <field>Last_Name__c</field>
        <formula>Primary_Account_Contact__r.LastName</formula>
        <name>OP - SG Direct - LName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Close_Date</fullName>
        <description>Captures the Date it was first assigned.  When the opportunity is closed won this date will change to that new date.</description>
        <field>CloseDate</field>
        <formula>NOW()</formula>
        <name>Anthem Opps: Opp - Close Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Date_Time_Assigned</fullName>
        <description>When the owner is changed, update the Date Assigned to reflect the date the new owner was assigned the lead</description>
        <field>Date_Time_Assigned__c</field>
        <formula>NOW()</formula>
        <name>Anthem Opps: Opp - Date/Time Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Declined</fullName>
        <description>Indicates that the lead was declined and should go back through the round robin process or be assigned to the Sales Rep</description>
        <field>Declined__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Opp - Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Declined_to_Admin</fullName>
        <description>Marks the Declined to Admin field as True</description>
        <field>Declined_to_Admin__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Opp - Declined to Admin</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Expired</fullName>
        <description>Marks the opportunity as expired</description>
        <field>StageName</field>
        <literalValue>Expired</literalValue>
        <name>Anthem Opps: Opp - Expired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Reassignment_after_Declined</fullName>
        <description>Unchecks the &quot;Declined&quot; checkbox once the opportunity owner is changed</description>
        <field>Declined__c</field>
        <literalValue>0</literalValue>
        <name>Anthem Opps: Opp - Reassign if Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU48_UpdateOppName</fullName>
        <description>update the opportunity name based on Effective Date and LoB</description>
        <field>Name</field>
        <formula>Account.Name +&apos; &apos;+IF((MONTH(Effective_Date__c)) &gt;9, 
TEXT(MONTH(Effective_Date__c)), &apos;0&apos; &amp; TEXT(MONTH(Effective_Date__c)))+&apos;/&apos;+ TEXT(DAY(Effective_Date__c))+&apos;/&apos;+TEXT(YEAR(Effective_Date__c))+&apos; &apos;+(RIGHT(Name , LEN(Name) - FIND(&quot;Opp -&quot;, Name) + 1) )</formula>
        <name>SGA_FU48_UpdateOppName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU51_UpdateRenewalDate</fullName>
        <field>Renewal_Date__c</field>
        <formula>DATE(YEAR( Effective_Date__c ) + 1, Month(Effective_Date__c ),Day(Effective_Date__c ))</formula>
        <name>SGA_FU51_UpdateRenewalDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field</fullName>
        <field>Opty_ETL_External_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>WF_Upd_Primary_Source</fullName>
        <field>Primary_Campaign_Source2__c</field>
        <formula>IF(ISBLANK(Campaign.Parent_Campaign_Name__c),&quot;&quot;,Campaign.Parent_Campaign_Name__c &amp; &quot; - &quot;) &amp; IF(ISNULL(Campaign.Child_Campaign_Name__c),&quot;&quot;,Campaign.Child_Campaign_Name__c)</formula>
        <name>**낓韣껜防왫떰辇킎䷲呛説꽅\ 鮃킭鋲秏蹙䷜&amp;\50p!</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_auto_assignment</fullName>
        <field>Auto_assignment__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Update Auto Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SFDC_Opportunity_RTKeys</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://app2.informaticacloud.com/saas/api/1/salesforceoutboundmessage/m2FoMtsjfNP8uG0UKub7tOsVuG0yoyPZ</endpointUrl>
        <fields>AccountId</fields>
        <fields>Converted_Lead_ID__c</fields>
        <fields>CreatedDate</fields>
        <fields>Id</fields>
        <fields>LastModifiedDate</fields>
        <fields>Lead_ID__c</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>ad64108@wellpoint.com.isg</integrationUser>
        <name>SFDC_Opportunity_RTKeys</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>**既畷윀樔쫉욭銺볰丛긔嶿錵%5C 濖뵡矦馚茈下ꈋ쒮冡廑觷癴%5C 颅뿴争%26%5C8480p%21</fullName>
        <actions>
            <name>WF_Upd_Primary_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <description>Looks to see when the Primary Campaign Sources has changed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A  Opportunity Closed Won</fullName>
        <actions>
            <name>Anthem_Opps_Populate_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Leads</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Evaluates when an opportunity is marked as Closed Won</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Auto-assignment</fullName>
        <active>false</active>
        <description>Reassigns the opportunity once it expires after 72 hours from &quot;Waiting for Disposition&quot;</description>
        <formula>AND( Stage_changed__c==false,  Auto_assignment__c==false, RecordType.DeveloperName = &quot;Anthem_Opps&quot;,  ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Auto_assignment_to_SalesRep</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Opportunity_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Date_Time_Assigned__c</offsetFromField>
            <timeLength>72</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Auto-assignment 1 hour</fullName>
        <active>false</active>
        <description>For TESTING Auto-assignment. Should remain off except for testing.&quot;</description>
        <formula>AND ( Stage_changed__c==false, Auto_assignment__c==false, ISPICKVAL(Tech_Businesstrack__c ,&apos;ANTHEMOPPS&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunity_Expired</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>update_auto_assignment</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Date_Time_Assigned__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Broker - Disposition Not Updated Email Reminder</fullName>
        <active>false</active>
        <description>Notifies Brokers of the amount of time they have left to disposition their opportunity. If they do not disposition in 72 hours, the opportunity will be reassigned to Sales Rep</description>
        <formula>AND(  RecordType.DeveloperName = &quot;Anthem_Opps&quot;, Stage_changed__c == false, ISPICKVAL(StageName, &quot;Waiting for Disposition&quot;), Owner.Profile.Name = &quot;Anthem Opps Brokers&quot;,  ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Anthem_Broker_New_Lead_Day_2_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Date_Time_Assigned__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Anthem_Broker_New_Lead_Day_3_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Date_Time_Assigned__c</offsetFromField>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Broker - New Opp - Owner Changed</fullName>
        <actions>
            <name>Opportunity_Lead_assignment_notification_email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Sales_Rep_notifying_them_that_their_Broker_has_been_assigned_a_lead</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Anthem_Opps_New_Opp_Initial_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the owner is changed, notify Broker and Sales Rep of new opportunity</description>
        <formula>AND( ISCHANGED(OwnerId), Owner.Profile.Name = &quot;Anthem Opps Brokers&quot;, RecordType.DeveloperName = &quot;Anthem_Opps&quot;,  ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Broker%2FSales Rep - Expired Opportunity Notification</fullName>
        <actions>
            <name>Anthem_Broker_Expired_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Anthem_Sales_Rep_New_Lead_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Sales_rep_expired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Broker_Expired__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>After 72 hours of a status of &quot;Waiting for Disposition&quot; an email will be sent to the broker and Sales Rep notifying them of the expired opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Existing Opportunity - Date%2FAssigned</fullName>
        <actions>
            <name>Anthem_Leads_Opp_Date_Time_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Capture the date an existing opportunity was reassigned to a new owner</description>
        <formula>AND( ISCHANGED(OwnerId), RecordType.DeveloperName = &quot;Anthem_Opps&quot;, ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Future Opportunity - Create Follow Up Task</fullName>
        <actions>
            <name>Anthem_Opps_Future_Opportunity_Follow_Up</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Future Opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Future_Opportunity_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>When the Disposition is Future Opportunity and the Future Opportunity date is filled in, create a task with an email reminder for the Broker to follow-up on this opportunity.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Anthem_Opps_Future_Opportunity_Task_Reminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.Future_Opportunity_Date__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Inactive Opportunity</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Accept</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Notifies Opportunity Owner that their Opportunity has not been modified for 60 days</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Anthem_Broker_Inactive_Lead_Notification</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.LastModifiedDate</offsetFromField>
            <timeLength>60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Language Preference %28English%29</fullName>
        <actions>
            <name>Anthem_Opps_Language_Preference_English</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Language_Preference_Number__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Anthem Opps Language Preference 1 - English</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Language Preference %28Span%29</fullName>
        <actions>
            <name>Anthem_Opps_Language_Preference</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Language_Preference_Number__c</field>
            <operation>equals</operation>
            <value>2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Anthem Opps Language Preference 2 - Spanish</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A New Opportunity - Date Assigned</fullName>
        <actions>
            <name>Opportunity_Close_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Opportunity_Date_Time_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Captures the date the new opportunity was assigned to the owner</description>
        <formula>AND( OwnerId &lt;&gt; null, RecordType.DeveloperName = &quot;Anthem_Opps&quot;, ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Non-Broker - Change Owner Alert</fullName>
        <actions>
            <name>Anthem_Opps_Non_Broker_Change_of_Owner_Email_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Alerts non-Broker team members if a new Opportunity has changed owners and has been assigned to them.</description>
        <formula>AND( ISCHANGED(OwnerId), Owner.Profile.Name &lt;&gt; &quot;Anthem Opps Brokers&quot;, Anthem_Opps_New_Initial_Owner_Assigned__c=TRUE, NOT(ISPICKVAL(StageName,&quot;Expired&quot;)), RecordType.DeveloperName = &quot;Anthem_Opps&quot;, ISPICKVAL(Tech_Businesstrack__c, &quot;ANTHEMOPPS&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Non-Broker New Opportunity Owner Alert</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Owner_Role__c</field>
            <operation>notEqual</operation>
            <value>Broker</value>
        </criteriaItems>
        <description>Alerts a non-broker opportunity owner that they have an new Opportunity assigned to them.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Anthem_Opps_Non_Broker_New_Opportunity_Alert</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Anthem_Opps_New_Opp_Initial_Assignment</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Opportunity.Anthem_Opps_New_Opp_Time_Alert__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Opportunity - Declined to Admin</fullName>
        <actions>
            <name>Opportunity_Declined_to_Admin</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Decline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Secondary_Disposition_new__c</field>
            <operation>equals</operation>
            <value>Invalid contact information,Not a qualified opportunity,Other</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Indicates that the opportunity was declined and should return to the System Admin</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Opportunity - Declined to RR%2FSales Rep</fullName>
        <actions>
            <name>Opportunity_Declined</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Decline</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Secondary_Disposition_new__c</field>
            <operation>equals</operation>
            <value>Language preference,Not interested in working this opportunity</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>Indicates that the opportunity was declined and should go back through the round robin process or be assigned to the Sales Rep</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Opportunity Update Alert</fullName>
        <actions>
            <name>Anthem_Opps_Opportunity_Update_Alert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Used to update the Opportunity owner that someone other than themselves altered the Call Agent Notes on the opportunity.  The use case is an Alta Agent updating the opp.</description>
        <formula>AND(  RecordType.DeveloperName = &quot;Anthem_Opps&quot;, $User.ProfileId  &lt;&gt;  Owner.Profile.Id,  ISCHANGED( Call_Agent_Notes__c ),ISPICKVAL(Tech_Businesstrack__c ,&apos;ANTHEMOPPS&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Primary Campaign Source Description</fullName>
        <actions>
            <name>Anthem_Opps_Primary_Campaign_Source2_Up</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CampaignId</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Looks to see when the Primary Campaign Sources has changed</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Sales Rep%2FSales Admin - Expired Opportunity Notification</fullName>
        <actions>
            <name>Anthem_Opps_Sales_Admin_New_Opportunity_Assignment</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Email_to_Sales_Rep_that_lead_expired</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Lead_Controller_Expired__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_rep_expired__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Anthem Opps</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>After 72 hours of a status of &quot;Waiting for Disposition&quot; an email will be sent to the Sales Rep notifying them of the expired Opportunity and will notify Sales Admin of new Opportunity</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Update Campaign Description</fullName>
        <actions>
            <name>Fill_Campaign_Description</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Give Campaign Description to Opportunity</description>
        <formula>AND( 
CONTAINS(RecordType.DeveloperName, &apos;Anthem&apos;), 
OR(
ISCHANGED(CampaignId),
ISNEW()
 ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP01_SGDirect_OppStage</fullName>
        <actions>
            <name>First_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OPP_FU01_SGStage</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OP_SG_Direct_Company_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>OP_SG_Direct_LName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($User.BypassWF__c) &amp;&amp; (ISPICKVAL(Tech_Businesstrack__c,&quot;SGDIRECT&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP01_StageUpdateOnLeadConversion</fullName>
        <actions>
            <name>OPP_FU01_OppStage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This workflow rule will automatically update Customer Stage from New Lead to Opportunity when the Lead is converted</description>
        <formula>NOT($User.BypassWF__c) &amp;&amp; (ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;)) &amp;&amp; AND(CreatedDate = LastModifiedDate, Tech_Opportunity_ConvertedLead__c ==true )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>OPP02_UpdateWEMStatusFollowup</fullName>
        <actions>
            <name>OPP_FU02_WEM_Status_Followup_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Medical_Application_Status__c</field>
            <operation>equals</operation>
            <value>Application Closed,Application Denied,Application Dis-Enrolled,Application Incomplete,Application Pending,Application Appealed,Application Cancelled,Application Payment Missing,Application Payment Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>TELESALES</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP03_UpdateStageOnStatusChange</fullName>
        <actions>
            <name>OPP_FU03_UpdateStage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Medical_Application_Status__c</field>
            <operation>equals</operation>
            <value>Application Cancelled,Application Closed,Application Denied,Application Dis-Enrolled,Application Incomplete,Application Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Medical_Application_Status__c</field>
            <operation>equals</operation>
            <value>Application Appealed,Application Payment Missing,Application Payment Received,Application Received,Application Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>TELESALES</value>
        </criteriaItems>
        <description>update Customer Stage to &quot;Application Submitted&quot; when Status changes</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP04_ClosedWon%2FLost Read Only</fullName>
        <actions>
            <name>OPP_FU04_RecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Lost,Sold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>TELESALES</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP05_UpdateStageOnStatusEnrolled</fullName>
        <actions>
            <name>OPP_FU05_UpdateStageOnStatusEnrolled</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>update Customer Stage to &quot;Customer Enrolled&quot; when Status is Enrolled</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp; OR(AND(ISNEW(),ISPICKVAL(Medical_Application_Status__c ,&quot;Enrolled&quot;), ISPICKVAL(Tech_Businesstrack__c ,&quot;TELESALES&quot;)), 
AND( ISCHANGED( Medical_Application_Status__c ) , ISPICKVAL(Medical_Application_Status__c ,&quot;Enrolled&quot;), ISPICKVAL(Tech_Businesstrack__c ,&quot;TELESALES&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OPP05_UpdateWEMStatusFollowup</fullName>
        <actions>
            <name>OPP_FU06_WEM_Status_Followup_UnCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Medical_Application_Status__c</field>
            <operation>notEqual</operation>
            <value>Application Cancelled,Application Closed,Application Denied,Application Dis-Enrolled,Application Incomplete,Application Pending,Application Appealed,Application Payment Missing,Application Payment Received</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>TELESALES</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Push Counter</fullName>
        <actions>
            <name>Increment_Push_Counter_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Increment the Push Counter field by 1</description>
        <formula>IF( CloseDate &gt; PRIORVALUE( CloseDate ) &amp;&amp; ISPICKVAL(Tech_Businesstrack__c, &apos;ANTHEMOPPS&apos;) , IF (MONTH(CloseDate) &lt;&gt; MONTH(PRIORVALUE( CloseDate )) , TRUE, FALSE), FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SFDC_Opportunity_RTKeys</fullName>
        <actions>
            <name>SFDC_Opportunity_RTKeys</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR48_PopulateOppName</fullName>
        <actions>
            <name>SGA_FU48_UpdateOppName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( $User.BypassWF__c =FALSE, ISCHANGED(Effective_Date__c), CONTAINS(RecordType.Name, &apos;SG Quoting&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR51_UpdateRenewalDate</fullName>
        <actions>
            <name>SGA_FU51_UpdateRenewalDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to update the Renewal Date based on the Effective Date for Renewal Opportunities</description>
        <formula>AND(ISPICKVAL(Tech_Businesstrack__c ,&apos;SGQUOTING&apos;),NOT(ISBLANK( Effective_Date__c )),ISPICKVAL(Type,&apos;Renewal&apos;),OR(ISNEW(),ISCHANGED(Effective_Date__c )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TESTAnthem Opps%3A Opportunity UpdateTEST</fullName>
        <actions>
            <name>TESTINGANTHEMOPPSUPDATES</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.CreatedById</field>
            <operation>contains</operation>
            <value>Onye</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Opty ETL External ID</fullName>
        <actions>
            <name>Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is meant for Informatica usage only.</description>
        <formula>AccountId &lt;&gt; null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Anthem_Opps_Future_Opportunity_Follow_Up</fullName>
        <assignedToType>owner</assignedToType>
        <description>This is a task reminder that you dispositioned this lead as a Future Opportunity.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Future_Opportunity_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Anthem Opps: Future Opportunity Follow Up</subject>
    </tasks>
</Workflow>
