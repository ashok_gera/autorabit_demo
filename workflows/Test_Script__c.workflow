<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>TestScriptNotification</fullName>
        <description>TestScriptNotification</description>
        <protected>false</protected>
        <recipients>
            <recipient>mamatha.bg@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>suvendu.prusty@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/TestScriptUpdateTemplate</template>
    </alerts>
    <rules>
        <fullName>Test Script Notification</fullName>
        <actions>
            <name>TestScriptNotification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(OR(Release__c =&apos;Sprint 1 - Telesales&apos;,Release__c =&apos;Sprint 2 - Telesales&apos;,
Release__c =&apos;Sprint 3 - Telesales&apos;,Release__c =&apos;Sprint 4 - Telesales&apos;,Release__c =&apos;Sprint 5 - Telesales&apos;,Release__c =&apos;End to End Integration - Telesales&apos;),
OR(
ISNEW(),ISCHANGED( Execution_Steps__c )
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
