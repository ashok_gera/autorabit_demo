<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>ETL_Reference_Update</fullName>
        <field>ETL_External_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>ETL Reference Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Update ETL External ID</fullName>
        <actions>
            <name>ETL_Reference_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field is purely for Informatica usage only</description>
        <formula>ISNUMBER( Household_Member_ID__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
