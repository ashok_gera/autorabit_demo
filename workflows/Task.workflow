<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BR_TASK_FU01_UpdateCompletedDate</fullName>
        <description>Used to populate the competed date field.</description>
        <field>BR_Completed_Date__c</field>
        <formula>TODAY()</formula>
        <name>BR_TASK_FU01_UpdateCompletedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_TASK_FU02_UpdateTechManagerId</fullName>
        <field>BR_Tech_TaskManagerId__c</field>
        <formula>Owner:User.ManagerId</formula>
        <name>BR_TASK_FU02_UpdateTechManagerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_TASK_FU03_UpdateTechSuperManagerId</fullName>
        <field>BR_Tech_TaskSuperManagerId__c</field>
        <formula>Owner:User.Manager.ManagerId</formula>
        <name>BR_TASK_FU03_UpdateTechSuperManagerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_TASK_FU05_UpdateEmailActivityRecType</fullName>
        <description>Used to updated email activity record type</description>
        <field>RecordTypeId</field>
        <lookupValue>BR_Schedule_Followup</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BR_TASK_FU05_UpdateEmailActivityRecType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Source_External_ID</fullName>
        <field>Source_External_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Update Source External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>BR_TASK_WF01_UpdateCompletedDate</fullName>
        <actions>
            <name>BR_TASK_FU01_UpdateCompletedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to populate task completed date</description>
        <formula>AND( NOT($User.BypassWF__c ),   AND( OR( NOT(ISPICKVAL( PRIORVALUE(Status) , &apos;Completed&apos;)), NOT(ISPICKVAL( PRIORVALUE(Status) , &apos;Created In Error&apos;)) ), OR( ISPICKVAL( Status , &apos;Completed&apos;),  ISPICKVAL( Status , &apos;Created In Error&apos;) ) ),   ISPICKVAL( Tech_Businesstrack__c , &apos;BROKER&apos;)   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_TASK_WF02_UpdateEmailCaseRecType</fullName>
        <actions>
            <name>BR_TASK_FU01_UpdateCompletedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BR_TASK_FU05_UpdateEmailActivityRecType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to update email to case activity record type.</description>
        <formula>AND(NOT($User.BypassWF__c ),  ISPICKVAL( Status , &apos;Completed&apos;), CONTAINS( Subject , &apos;Email:&apos;),  ISPICKVAL( Tech_Businesstrack__c , &apos;BROKER&apos;)  )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BR_TASK_WF03_TechManagerandSuperManagerId</fullName>
        <actions>
            <name>BR_TASK_FU02_UpdateTechManagerId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BR_TASK_FU03_UpdateTechSuperManagerId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($User.BypassWF__c ), OR(ISNEW(), ISCHANGED(OwnerId)), ISPICKVAL( Tech_Businesstrack__c , &apos;BROKER&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Task Source External ID</fullName>
        <actions>
            <name>Update_Source_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Task.RecordTypeId</field>
            <operation>equals</operation>
            <value>Schedule Followup,Send Fulfillment,Siebel Activity</value>
        </criteriaItems>
        <description>This is for ETL purpose only.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
