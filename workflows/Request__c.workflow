<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Broker_Request_Create_Change_Notification</fullName>
        <description>Broker_Request Create/Change Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>joseph.ravas@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kishore.jonnadula@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>nagarjuna.kaipu@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rajendra.aravala@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>venkatesulu.meka@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>vijay.krishnakumar@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/BROKER_New_Request_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_notification_to_development_team</fullName>
        <description>Email notification to development team</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhaskar.bellapu@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>franklin.a.williams@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rajendra.aravala@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>santosh.mohanty@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>venkatesulu.meka@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_Clarification_Template</template>
    </alerts>
    <alerts>
        <fullName>NotifyTestTeam</fullName>
        <ccEmails>justin.proctor@accenture.com</ccEmails>
        <description>NotifyTestTeam</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhaskar.bellapu@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>franklin.a.williams@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>rajendra.aravala@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>santosh.mohanty@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>venkatesulu.meka@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/NotificationToTestTeam</template>
    </alerts>
    <alerts>
        <fullName>Send_notification_to_development_team</fullName>
        <description>Send notification to development team</description>
        <protected>false</protected>
        <recipients>
            <recipient>bhaskar.bellapu@accenture.com.pwm</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>eric.adu-gyasi@anthem.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kishore.jonnadula@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>s.jayamaruthyraman@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>venkatesulu.meka@accenture.com.isg</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/New_Request_Email_Template</template>
    </alerts>
    <rules>
        <fullName>BROKER_Email alert on Request Creation%2FChange</fullName>
        <actions>
            <name>Broker_Request_Create_Change_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(Planned_Release__r.Name == &apos;Sprint 1 - Broker Services&apos;||  Planned_Release__r.Name == &apos;Sprint 2 - Broker Services&apos; ||
Planned_Release__r.Name == &apos;Sprint 3 - Broker Services&apos; ||Planned_Release__r.Name == &apos;Sprint 4 - Broker Services&apos;|| Planned_Release__r.Name == &apos;Sprint 5 - Broker Services&apos;)&amp;&amp;
(IsNew() || ISCHANGED( Request_Description__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email alert on Request Creation%2FChange</fullName>
        <actions>
            <name>Send_notification_to_development_team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>(Planned_Release__r.Name == &apos;Sprint 1 - Small Group Automation&apos;||  Planned_Release__r.Name == &apos;Sprint 2 - Small Group Automation&apos; ||
Planned_Release__r.Name == &apos;Sprint 3 - Sprint 1 - Small Group Automation&apos; ||Planned_Release__r.Name == &apos;Sprint 4 - Sprint 1 - Small Group Automation&apos;|| Planned_Release__r.Name == &apos;Sprint 5 - Sprint 1 - Small Group Automation&apos;)&amp;&amp;
(IsNew() || ISCHANGED( Request_Description__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>REQUEST01_NotifyTestTeam</fullName>
        <actions>
            <name>NotifyTestTeam</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Request Email Notification on status change
Recipients : Offshore Team and Frank</description>
        <formula>AND(
OR(Planned_Release__r.Name == &apos;Sprint 1 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 2 - Telesales&apos;,
Planned_Release__r.Name == &apos;Sprint 3 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 4 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 5 - Telesales&apos;),
ISPICKVAL(Status__c,&apos;Release: Test&apos;),ISCHANGED(Status__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Request Email Notification on Clarification Requested%2FProvided</fullName>
        <actions>
            <name>Email_notification_to_development_team</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Request Email Notification on Clarification Requested/Provided
Recipients : Offshore Team and Frank</description>
        <formula>AND(
OR(Planned_Release__r.Name == &apos;Sprint 1 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 2 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 3 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 4 - Telesales&apos;,Planned_Release__r.Name == &apos;Sprint 5 - Telesales&apos;),
OR(AND(ISPICKVAL(Status__c,&apos;Clarification Requested&apos;),ISCHANGED(Status__c)) ,AND(ISPICKVAL(Status__c,&apos;Clarification Provided&apos;),ISCHANGED(Status__c))
)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
