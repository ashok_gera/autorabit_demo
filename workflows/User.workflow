<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>All_Other_States_Anthem_Blue_Cross_and</fullName>
        <field>Regional_Brand__c</field>
        <literalValue>Anthem Blue Cross and Blue Shield</literalValue>
        <name>All Other States = Anthem Blue Cross and</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CA_Anthem_Blue_Cross</fullName>
        <field>Regional_Brand__c</field>
        <literalValue>Anthem Blue Cross</literalValue>
        <name>CA = Anthem Blue Cross</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Check_IND</fullName>
        <field>IND__c</field>
        <literalValue>1</literalValue>
        <name>Anthem Opps: Check IND</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GA_Blue_Cross_and_Blue_Shield_of_Geo</fullName>
        <field>Regional_Brand__c</field>
        <literalValue>Blue Cross and Blue Shield of Georgia</literalValue>
        <name>GA = Blue Cross and Blue Shield of Geo</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NY_Empire_Blue_Cross_Blue_Shield</fullName>
        <field>Regional_Brand__c</field>
        <literalValue>Empire BlueCross Blue Shield</literalValue>
        <name>NY = Empire Blue Cross Blue Shield</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>All Other States %3D Anthem Blue Cross and Blue Shield</fullName>
        <actions>
            <name>All_Other_States_Anthem_Blue_Cross_and</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.User_State__c</field>
            <operation>notEqual</operation>
            <value>New York,California,Georgia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Anthem Opps%3A Check IND</fullName>
        <actions>
            <name>Check_IND</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.IND_Only__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <description>If the field IND Only is check, the field IND will be checked too.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CA %3D Anthem Blue Cross</fullName>
        <actions>
            <name>CA_Anthem_Blue_Cross</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.User_State__c</field>
            <operation>equals</operation>
            <value>California</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GA %3D Blue Cross and Blue Shield of Georgia</fullName>
        <actions>
            <name>GA_Blue_Cross_and_Blue_Shield_of_Geo</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.User_State__c</field>
            <operation>equals</operation>
            <value>Georgia</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NY %3D Empire Blue Cross Blue Shield</fullName>
        <actions>
            <name>NY_Empire_Blue_Cross_Blue_Shield</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.User_State__c</field>
            <operation>equals</operation>
            <value>New York</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
