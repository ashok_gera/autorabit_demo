<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>BR_Update_Call_End_Date</fullName>
        <description>Update the end date to the current datetime</description>
        <field>BR_End_Date__c</field>
        <formula>NOW()</formula>
        <name>BR_Update_Call_End_Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BR_Update_Call_End_Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Call_Info__c.BR_End_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Update the end date on a call info record if still null after x hrs of creation.</description>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>BR_Update_Call_End_Date</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Call_Info__c.CreatedDate</offsetFromField>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
