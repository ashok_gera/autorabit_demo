<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>BR_CASE_EA01_NotificationEmail</fullName>
        <description>BR_CASE_EA01_NotificationEmail</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Broker_Services_Templates/BrokerEmailtoCaseemailresponse</template>
    </alerts>
    <alerts>
        <fullName>BR_CASE_EA02_CustomNotificationEmail</fullName>
        <description>BR_CASE_EA02_CustomNotificationEmail</description>
        <protected>false</protected>
        <recipients>
            <field>BR_Notification_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Broker_Services_Templates/Broker_Case_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>BR_CASE_EA03_CaseCloseBTSEmailSurveyLink</fullName>
        <description>BR_CASE_EA03_CaseCloseBTSEmailSurveyLink</description>
        <protected>false</protected>
        <recipients>
            <field>Tech_Case_Survey_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>software.support@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Broker_Services_Templates/Broker_Case_BTS_Survey_Notification_Email</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA10_Group_Submitted_Under_Review</fullName>
        <description>SGA EA10 Group Submitted, Under Review</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>docusign-apps@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA09_Group_Submitted_Under_Review</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA13_Case_Enrollment_Complete</fullName>
        <description>SGA EA13 Case Enrollment Complete</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>docusign-apps@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA12_Case_Enrollment_Complete</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA14_Case_Group_Returned</fullName>
        <description>SGA EA14 Case Group Returned</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>docusign-apps@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA13_Case_Group_Returned</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA17_Case_Group_Denied</fullName>
        <description>SGA EA17 Case Group Denied</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>docusign-apps@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA16_Case_Group_Denied</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA18_Case_Group_Expired</fullName>
        <description>SGA EA18 Case Group Expired</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>docusign-apps@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA17_Case_Group_Expired</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA23_CaseComment_EmailAlert</fullName>
        <description>SGA EA23 CaseComment EmailAlert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>webcontent@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA_21_CaseComment_Alert</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA24_Notify_Owner_Response_PEIM</fullName>
        <description>SGA_EA24_Notify_Owner_Response_PEIM</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>webcontent@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA24_PEIM_Notiication_To_Owner_For_Response</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA25_Notify_Owner_Response_Inquiry</fullName>
        <description>SGA_EA25_Notify_Owner_Response_Inquiry</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>webcontent@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA25_Notiication_To_Owner_For_Response_Inquiry</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA26_GroupApproved_LoadDDS</fullName>
        <ccEmails>SGimplementation@anthemdentaladmin.com</ccEmails>
        <description>SGA_EA26_GroupApproved_LoadDDS</description>
        <protected>false</protected>
        <recipients>
            <recipient>connectteamlead@accenture.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA26_LoadGroupintoDDS</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA27_Internal_Open</fullName>
        <description>SGA_EA27_Internal_Open</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA27_InternalOpen</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA28_Internal_Closed</fullName>
        <description>SGA_EA28_Internal_Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA28_InternalClosed</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA29_LC_Internal_Open</fullName>
        <description>SGA_EA29_LC_Internal_Open</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA29_LC_Internal_Open</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA30_LC_Internal_Closed</fullName>
        <description>SGA_EA30_LC_Internal_Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA30_LC_Internal_Closed</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA31_LC_External_Open</fullName>
        <description>SGA_EA31_LC_External_Open</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA31_LC_External_Open</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA32_LC_External_Closed</fullName>
        <description>SGA_EA32_LC_External_Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA32_LC_External_Closed</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA33_East_West_Internal_Open</fullName>
        <description>SGA_EA33_East_West_Internal_Open</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA33_East_West_Internal_Open</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA34_East_West_Internal_Closed</fullName>
        <description>SGA_EA34_East_West_Internal_Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA34_East_West_Internal_Closed</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA35_East_West_External_Open</fullName>
        <description>SGA_EA35_East_West_External_Open</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA35_East_West_External_Open</template>
    </alerts>
    <alerts>
        <fullName>SGA_EA36_East_West_External_Closed</fullName>
        <description>SGA_EA36_East_West_External_Closed</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>SGA_Email_Templates/SGA_EA36_East_West_External_Closed</template>
    </alerts>
    <fieldUpdates>
        <fullName>BR_CASE_EA03_Upate_Recipient_Email</fullName>
        <field>Tech_Case_Survey_Email__c</field>
        <formula>Account.PersonContact.Email</formula>
        <name>BR_CASE_EA03 Upate Recipient Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU01_TechLockDescription</fullName>
        <field>BR_Tech_Lock_Subject_Description__c</field>
        <literalValue>1</literalValue>
        <name>BR_CASE_FU01_TechLockDescription</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU02_ReadOnlyRecordTypeUpdate</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Case_Read_Only</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BR_CASE_FU02_ReadOnlyRecordTypeUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU03_UnCheckSendNotification</fullName>
        <description>Used to uncheck the send notification check-box once mail sent.</description>
        <field>BR_Send_notification_email__c</field>
        <literalValue>0</literalValue>
        <name>BR_CASE_FU03_UnCheckSendNotification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU04_UpdateTechManagerId</fullName>
        <field>BR_Tech_CaseManagerId__c</field>
        <formula>Owner:User.ManagerId</formula>
        <name>BR_CASE_FU04_UpdateTechManagerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU05_UpdateTechSuperManagerId</fullName>
        <field>BR_Tech_CaseSuperManagerId__c</field>
        <formula>Owner:User.Manager.ManagerId</formula>
        <name>BR_CASE_FU05_UpdateTechSuperManagerId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU08_UpdateBTSCaseRecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>BTS_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BR_CASE_FU08_UpdateBTSCaseRecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BR_CASE_FU09_UpdateCaseRecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Broker_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>BR_CASE_FU09_UpdateCaseRecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CASE_Fill_E_B_Start</fullName>
        <field>E_B_Start_Date__c</field>
        <formula>TODAY()</formula>
        <name>CASE - Fill E&amp;B Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Carve_Out_Dates</fullName>
        <field>GroupReturnVendorReturnCarveOut__c</field>
        <formula>IF(ISBLANK(GroupReturnVendorReturnCarveOut__c),PRIORVALUE(GroupReturnVendorReturnCarveOut__c)+  Vendor_Return_Date__c -  Date_Group_Returned__c, Vendor_Return_Date__c -  Date_Group_Returned__c)</formula>
        <name>Case - Carve Out Dates</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Date_Canceled_Denied_or_Closed</fullName>
        <field>Date_Closed__c</field>
        <formula>TODAY()</formula>
        <name>Case - Date Canceled, Denied or Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Date_Group_Returned</fullName>
        <field>Date_Group_Returned__c</field>
        <formula>TODAY()</formula>
        <name>Case - Date Group Returned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Enrollment_Complete_Date</fullName>
        <field>Enrollment_Complete_Date__c</field>
        <formula>TODAY()</formula>
        <name>Case - Enrollment Complete Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Vendor_Returned</fullName>
        <field>Vendor_Return_Date__c</field>
        <formula>TODAY()</formula>
        <name>Case - Vendor Returned Resolved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Owner_Changed</fullName>
        <field>First_Time_Ownership_Changed__c</field>
        <formula>NOW()</formula>
        <name>Owner Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Paperwork_Received_Date_on_Case</fullName>
        <field>Paperwork_Received__c</field>
        <formula>Now()</formula>
        <name>Populate Paperwork Received Date on Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU17_UpdateTechProductsLoaded</fullName>
        <description>This is used for report formula.</description>
        <field>SGA_Tech_Stage_Change_to_Products_Loaded__c</field>
        <literalValue>1</literalValue>
        <name>SGA_FU17_UpdateTechProductsLoaded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU18_UpdateStatus</fullName>
        <description>Update the case status to closed</description>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>SGA_FU18_UpdateStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU19_UpdateRecordType_closed_Instal</fullName>
        <description>Update the record type to closed.</description>
        <field>RecordTypeId</field>
        <lookupValue>SG_Closed_Case_Installation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SGA_FU19_UpdateRecordType_closed_Instal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU19_UpdateTechVendorReturnDate</fullName>
        <description>This is used to update vendor return date.</description>
        <field>Tech_Vendor_Returned_Date__c</field>
        <formula>NOW()</formula>
        <name>SGA_FU19_UpdateTechVendorReturnDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU20_UpdateVendorReturnDateToBlank</fullName>
        <description>This is used to update tech vendor returned date to blank if the stage is changed from vendor return.</description>
        <field>Tech_Vendor_Returned_Date__c</field>
        <name>SGA_FU20_UpdateVendorReturnDateToBlank</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU21_UpdateTechBorkerReuploadedDocs</fullName>
        <description>This is used to update tech broker re uploaded documents to false.</description>
        <field>Tech_Broker_Re_Uploaded_Documents__c</field>
        <literalValue>0</literalValue>
        <name>SGA_FU21_UpdateTechBorkerReuploadedDocs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU22_Update_RecordType_Installation</fullName>
        <description>Update the Record type of &apos;SGA Case Installation&apos; to &apos;SG Closed - Case Installation&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>SG_Closed_Case_Installation</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SGA_FU22_Update_RecordType_Installation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU23_Update_RecordType_PEIM</fullName>
        <description>Update the Record type of &apos;Small Group Pre-Enrollment Inquiry&apos; to &apos;SG Closed - PEIM Case&apos;</description>
        <field>RecordTypeId</field>
        <lookupValue>SG_Closed_PEIM_Case</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SGA_FU23_Update_RecordType_PEIM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU27_Case_Status_To_Assigned</fullName>
        <field>Status</field>
        <literalValue>Assigned</literalValue>
        <name>SGA_FU27_Case Status To Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU28_Case_Tech_Owner_Field_Update</fullName>
        <description>This field is set to true if the Owner is changed</description>
        <field>Tech_Owner_Change_field__c</field>
        <literalValue>1</literalValue>
        <name>SGA_FU28_Case Tech Owner Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU29_Case_Status_To_Inprogress</fullName>
        <description>Update case status to inprogress if record is edited</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>SGA_FU29_Case Status To Inprogress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU30_Case_Record_Update_Field</fullName>
        <description>This field is checked once record is edited</description>
        <field>Tech_record_update__c</field>
        <literalValue>1</literalValue>
        <name>SGA_FU30_Case Record Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU31_Tech_ReOpened_time</fullName>
        <field>Tech_ReOpened_Date__c</field>
        <formula>NOW()</formula>
        <name>SGA_FU31_Tech ReOpened time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU32_Case_Priority_Update</fullName>
        <description>Update the Priority as &apos;High&apos;</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>SGA_FU32_Case Priority Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU33_AssignCase_To_Queue</fullName>
        <description>This is to update the case owner to Queue if  the stage is set to &quot;Scrub Completed&quot;.</description>
        <field>OwnerId</field>
        <lookupValue>Connect_Case_Installation</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>SGA_FU33_ AssignCase_To_Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU36_Update_Case_DateDue</fullName>
        <description>Update case Date Due to 5 days after created date.</description>
        <field>Date_Due__c</field>
        <formula>CreatedDate + 3</formula>
        <name>SGA_FU36_Update_Case_DateDue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU52_UpdateCheckStatusToPaymentRetur</fullName>
        <field>Check_Status__c</field>
        <literalValue>Payment Return Requested</literalValue>
        <name>SGA_FU52_UpdateCheckStatusToPaymentRetur</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU53_UpdateCheckStatusToPayReadyProc</fullName>
        <field>Check_Status__c</field>
        <literalValue>Payment Ready for Processing</literalValue>
        <name>SGA_FU53_UpdateCheckStatusToPayReadyProc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU56_UpdateCaseRecordType</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Close_SG_SCLC_Inquiry</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>SGA_FU56_UpdateCaseRecordType</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Case_Origin_to_Email</fullName>
        <field>Case_Origin_Custom__c</field>
        <literalValue>Email</literalValue>
        <name>Set Case Origin to Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Store_Previous_Case_Record_Type</fullName>
        <field>Case_Record_Type_before_Closing__c</field>
        <formula>RecordType.Id</formula>
        <name>Store Previous Case Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Uncheck_Case_Status_Checkbox</fullName>
        <field>Update_Status_from_Closed_to_Reopened__c</field>
        <literalValue>0</literalValue>
        <name>Uncheck Case Status Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Effective_Date</fullName>
        <field>Effective_Date__c</field>
        <formula>Application_Name__r.Group_Coverage_Date__c</formula>
        <name>Update Effective Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Effective_date_in_case</fullName>
        <field>Effective_Date__c</field>
        <formula>Application_Name__r.Group_Coverage_Date__c</formula>
        <name>Update Effective date in case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Effective_date_on_case</fullName>
        <description>update effective date from the application</description>
        <field>Effective_Date__c</field>
        <formula>Application_Name__r.Group_Coverage_Date__c</formula>
        <name>Update Effective date on case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Group_Returned_Date_Time</fullName>
        <field>Group_Returned_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Group Returned Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Time_Case_Stage_Changed</fullName>
        <field>Case_Stage_Changed_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Time Case Stage Changed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Vendor_Returned_Date_Time</fullName>
        <field>Vendor_Returned_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Vendor Returned Date/Time</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Vendor_Returned_Resolved_Date_Tim</fullName>
        <field>Vendor_Returned_Resolved_Date_Time__c</field>
        <formula>NOW()</formula>
        <name>Update Vendor Returned/Resolved Date/Tim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BR_CASE_WF01_TechRestrictSubjectandDescription</fullName>
        <actions>
            <name>BR_CASE_FU01_TechLockDescription</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule will update the Tech_Locking_Subject_Description__c field whenever the ownership of a case is changed.
This field data will be used to through validation error when user tries to edit Decription</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;),ISCHANGED(OwnerId ), NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF02_BrokerNotificationEmail</fullName>
        <actions>
            <name>BR_CASE_EA01_NotificationEmail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BR_CASE_FU03_UnCheckSendNotification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to send the notification to Contact email address.</description>
        <formula>AND(NOT($User.BypassWF__c ),ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;), BR_Send_notification_email__c = True,      ISBLANK(BR_Notification_Email__c )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF03_BrokerCustomNotificationEmail</fullName>
        <actions>
            <name>BR_CASE_EA02_CustomNotificationEmail</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BR_CASE_FU03_UnCheckSendNotification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to send the notification to custom email address.</description>
        <formula>AND(NOT($User.BypassWF__c ),ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;),BR_Send_notification_email__c  = True,     NOT( ISBLANK(BR_Notification_Email__c ) )     )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF04_CaseCloseRecordTypeUpdate</fullName>
        <actions>
            <name>BR_CASE_FU02_ReadOnlyRecordTypeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to assign read only record type when the case is closed.</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Status, &apos;Closed&apos;), ISPICKVAL(Tech_Businesstrack__c, &apos;BROKER&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF05_CaseCloseBTSSurveyEmail</fullName>
        <actions>
            <name>BR_CASE_EA03_CaseCloseBTSEmailSurveyLink</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>BR_CASE_EA03_Upate_Recipient_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Department__c</field>
            <operation>equals</operation>
            <value>BTS</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Broker: Technical Support</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>BROKER</value>
        </criteriaItems>
        <description>Used to send the survey monkey email once the Email to case is closed by BTS representative.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF06_UpdateTechManagerSuperManagerId</fullName>
        <actions>
            <name>BR_CASE_FU04_UpdateTechManagerId</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>BR_CASE_FU05_UpdateTechSuperManagerId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($User.BypassWF__c) &amp;&amp; ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;) &amp;&amp; (ISNEW()||ISCHANGED(OwnerId))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF07_UpdateCaseRecordType</fullName>
        <actions>
            <name>BR_CASE_FU09_UpdateCaseRecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(NOT(ISPICKVAL(Status, &apos;Closed&apos;)), ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;),(RecordType.Name == &apos;Case Read Only&apos;), Owner:User.Profile.Name  != &apos;Broker: Technical Support&apos;  ), true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>BR_CASE_WF08_UpdateBTSCaseRecordType</fullName>
        <actions>
            <name>BR_CASE_FU08_UpdateBTSCaseRecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF(AND(NOT(ISPICKVAL(Status, &apos;Closed&apos;)),ISPICKVAL(Tech_Businesstrack__c, &quot;BROKER&quot;), (RecordType.Name == &apos;Case Read Only&apos;), Owner:User.Profile.Name  == &apos;Broker: Technical Support&apos; ), true, false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case - Closed %26 Denied%2C Expired or Cancelled</fullName>
        <actions>
            <name>Case_Date_Canceled_Denied_or_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Denied,Group Expired,Group Cancelled</value>
        </criteriaItems>
        <description>SS-2228 - Date Group Denied, Group Expired, or Group Cancelled</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Enrollment Complete Date</fullName>
        <actions>
            <name>Case_Enrollment_Complete_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Enrollment Complete</value>
        </criteriaItems>
        <description>Case Stage = Scrub Completed, Products Loaded or Members Loaded</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Fill E%26B Start Date</fullName>
        <actions>
            <name>CASE_Fill_E_B_Start</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Scrub Completed,Products Loaded,Members Loaded</value>
        </criteriaItems>
        <description>Case Stage = Scrub Completed, Products Loaded or Members Loaded</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Group Returned</fullName>
        <actions>
            <name>Case_Date_Group_Returned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Returned</value>
        </criteriaItems>
        <description>SS-2227, SS-2229 - Start = Case Stage changes to Group Returned (Date/Time)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Vendor Return Carve Out</fullName>
        <actions>
            <name>Case_Carve_Out_Dates</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( Vendor_Return_Date__c != Null &amp;&amp;  Vendor_Return_Date__c &gt;  Date_Group_Returned__c,TRUE,FALSE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Vendor Returned</fullName>
        <actions>
            <name>Case_Vendor_Returned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Vendor Return Resolved</value>
        </criteriaItems>
        <description>SS-2227, SS-2229 - Start = Case Stage changes to Vendor Returned (Date)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Origin Set to Email</fullName>
        <actions>
            <name>Set_Case_Origin_to_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>equals</operation>
            <value>Email - NationalBrokerSG</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create Connect Activity</fullName>
        <actions>
            <name>SGA_EA26_GroupApproved_LoadDDS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Group_Approved_Load_Group_into_DDS</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Status is updated to &apos;Scrub Completed&apos; OR &apos;Products Loaded&apos; and Dental-Other = true than create Connect Activity</description>
        <formula>AND(Application_Name__r.Dental__c == True , Application_Name__r.Dental_Other__c == True, OR(ISPICKVAL( Stage__c ,&apos;Scrub Completed&apos;),ISPICKVAL( Stage__c ,&apos;Products Loaded&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MTM TAT %23 of Days w%2FCarve Out</fullName>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND (3 or 4) AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>SG</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>contains</operation>
            <value>Small Group</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>Paper</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>contains</operation>
            <value>SALES</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.BR_Sub_Category__c</field>
            <operation>equals</operation>
            <value>Real-Time,Census,OME,Paper,Automated Bulk Upload</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Enrollment Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Owner Changed</fullName>
        <actions>
            <name>Owner_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Case_Owner__c  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Paperwork Received Date on Case</fullName>
        <actions>
            <name>Populate_Paperwork_Received_Date_on_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>SMGR-2008--The Paperwork Receieved date field should be automatically populated onto the Case Installation case detail page with the date the case was created by the system.</description>
        <formula>RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR14_CasePriorityFieldUpdate</fullName>
        <actions>
            <name>SGA_FU32_Case_Priority_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Vendor Return</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Small Group Case Installation</value>
        </criteriaItems>
        <description>This rule is to update the update priority as &apos;High&apos;, when case stage is changed to &apos;Vendor Returned&apos;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR18_Case_Group_Submitted%2CUnder _Review</fullName>
        <actions>
            <name>SGA_EA10_Group_Submitted_Under_Review</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>&quot;Group Submitted, Under Review&quot;</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;Group  Submitted, Under Review &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR20_Case_Enrollment_Complete</fullName>
        <actions>
            <name>SGA_EA13_Case_Enrollment_Complete</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Enrollment Complete</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;Enrollment Complete &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR21_Case_Group_Returned</fullName>
        <actions>
            <name>SGA_EA14_Case_Group_Returned</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Returned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;Group Returned  &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR23_Case_Payment_Failed</fullName>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Payment Failed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;Payment Failed &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR24_Case_Group_Denied</fullName>
        <actions>
            <name>SGA_EA17_Case_Group_Denied</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Denied</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;Group Denied &quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR25_Case_Group_Expired</fullName>
        <actions>
            <name>SGA_EA18_Case_Group_Expired</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Expired</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Email_ON_OFF_By_State__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <description>This rule is used to send email, if the case stage  is changed to &quot;group Expired&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR29_CaseScrub Completed</fullName>
        <actions>
            <name>SGA_FU33_AssignCase_To_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule is used to create an activity once the case stage is set to &quot;Scrub Completed&quot;.</description>
        <formula>AND(NOT($User.BypassWF__c),  ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.Name =&apos;Small Group Case Installation&apos;,  ISPICKVAL(Stage__c, &apos;Scrub Completed&apos;), NOT(AND(Medical__c = True , Dental__c = False, Vision__c = False, Optional_Supp_Vol_Life_ADD_Dep_Life__c = False, Life_ADD_Dep_Life__c = False, STD_VSTD__c = False, LTD_VLTD__c = False, EAP__c = False)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR30_CaseStatusOpen</fullName>
        <actions>
            <name>SGA_FU31_Tech_ReOpened_time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is to update a field if status is Closed-Reopened</description>
        <formula>AND(NOT($User.BypassWF__c), (ISPICKVAL(Status, &quot;Closed - Reopen&quot;)), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR34_Case_Close_Case_by_Stage_Installation</fullName>
        <actions>
            <name>SGA_FU18_UpdateStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SGA_FU19_UpdateRecordType_closed_Instal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the case status to closed when the case stage is Group Cancelled or Group Denied or Group Expired or Enrollment Complete. For Installation Case</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.Name =&apos;Small Group Case Installation&apos;, OR(ISPICKVAL(Stage__c, &apos;Group Denied&apos;), ISPICKVAL(Stage__c, &apos;Group Expired&apos;), ISPICKVAL(Stage__c, &apos;Group Cancelled&apos;), ISPICKVAL(Stage__c, &apos;Enrollment Complete&apos;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR34_UpdateGroupProductsLoaded</fullName>
        <actions>
            <name>SGA_FU17_UpdateTechProductsLoaded</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This is used to update SGA_Tech_Stage_Change_to_Products_Loaded check box when Stage is Products Loaded</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), OR(AND(ISNEW(), ISPICKVAL(Stage__c,&apos;Products Loaded&apos;), RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;) ,AND(ISCHANGED(Stage__c),ISPICKVAL(Stage__c,&apos;Products Loaded&apos;), RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;,SGA_Tech_Stage_Change_to_Products_Loaded__c &lt;&gt; TRUE)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR35_UpdateVendorReturnedDateToSystemDate</fullName>
        <actions>
            <name>SGA_FU19_UpdateTechVendorReturnDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to update vendor returned with system date when the stage is vendor return.</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), ISPICKVAL(Stage__c,&quot;Vendor Return&quot;),RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR36_UpdateVendorReturnedDateToBlank</fullName>
        <actions>
            <name>SGA_FU20_UpdateVendorReturnDateToBlank</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule is used to update the vendor returned date to blank if the status is changed from vendor returned date</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), ISCHANGED(Stage__c),NOT(ISPICKVAL(Stage__c,&quot;Vendor Return&quot;)), ISPICKVAL(PRIORVALUE(Stage__c),&quot;Vendor Return&quot;), RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR37_UpdateTechBrokerReUploadCheckbox</fullName>
        <actions>
            <name>SGA_FU21_UpdateTechBorkerReuploadedDocs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is used to update tech broker re uploaded document to false.</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), Tech_Broker_Re_Uploaded_Documents__c = true,OwnerId =  $User.Id )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR38_Case_Update_InstallationClosed_RecordType</fullName>
        <actions>
            <name>SGA_FU22_Update_RecordType_Installation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the case closed record type for SG Installation cases</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), ISPICKVAL(Status, &apos;Closed&apos;), RecordType.Name = &apos;Small Group Case Installation&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR39_Case_Update_PEIMClosed_RecordType</fullName>
        <actions>
            <name>SGA_FU23_Update_RecordType_PEIM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the case closed record type for SG Pre Enrollment Inquiry cases</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), ISPICKVAL(Status, &apos;Closed&apos;), RecordType.Name = &apos;Small Group Pre-Enrollment Inquiry&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR40_Case_CaseCommentExternalAlert</fullName>
        <actions>
            <name>SGA_EA23_CaseComment_EmailAlert</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <description>Workflow Rule for Email alert on case when the Tech_CaseCommentExternalAlert__c field is changed</description>
        <formula>AND(ISCHANGED(Tech_CaseCommentExternalAlert__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR42_Case_Owner_Change_field_Update</fullName>
        <actions>
            <name>SGA_FU27_Case_Status_To_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SGA_FU28_Case_Tech_Owner_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is to update status field to Assigned once case is assigned to user</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.DeveloperName =&apos;Small_Group_Case_Installation&apos;,LEFT(OwnerId, 3) = &apos;005&apos; ,LEFT(PRIORVALUE(OwnerId), 3) =&apos;00G&apos;,Tech_Owner_Change_field__c = False )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR43_Case_Record_Edit_Field Update</fullName>
        <actions>
            <name>SGA_FU29_Case_Status_To_Inprogress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SGA_FU30_Case_Record_Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule to update status field to Inprogress if record is edited</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.DeveloperName =&apos;Small_Group_Case_Installation&apos;,  ISPICKVAL( Status , &apos;Assigned&apos;) , NOT(ISNEW()),Tech_record_update__c = false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR45_Case_Notification_To_Owner_PEIM</fullName>
        <actions>
            <name>SGA_EA24_Notify_Owner_Response_PEIM</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is to send a Email notification  to owner</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.DeveloperName =&apos;Small_Group_Pre_Enrollment_Inquiry&apos;,ISCHANGED(Latest_Inbound_Email__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR46_Case_Notification_To_Owner_Inquiry</fullName>
        <actions>
            <name>SGA_EA25_Notify_Owner_Response_Inquiry</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This is to send a Email notification  to owner</description>
        <formula>AND(NOT($User.BypassWF__c),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.DeveloperName =&apos;Small_Group_Case_Installation&apos;,ISCHANGED(Latest_Inbound_Email__c))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR47_Case_Task_to_ConnectTL</fullName>
        <actions>
            <name>SGA_EA26_GroupApproved_LoadDDS</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Group_Approved_Load_Group_into_DDS</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>SGA_WR46_Case_Notification_To_Owner_Inquiry</description>
        <formula>AND(NOT($User.BypassWF__c),OR(ISCHANGED(Stage__c),ISCHANGED(BR_State__c)),ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), OR(AND(ISPICKVAL(Stage__c,&apos;Scrub Completed&apos;), OR(ISPICKVAL(BR_State__c,&apos;NY&apos;), ISPICKVAL(BR_State__c,&apos;ME&apos;))), AND( ISPICKVAL(Stage__c,&apos;Products Loaded&apos;), OR( ISPICKVAL(BR_State__c,&apos;CA&apos;), ISPICKVAL(BR_State__c,&apos;CO&apos;)))),    Dental__c = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR48_Case_Task_Paper_Check</fullName>
        <actions>
            <name>Group_Approved_Process_Paper_Payment</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <description>SGA_WR48_Case_Task_Paper_Check</description>
        <formula>AND(AND(ISPICKVAL(Check_Status__c,&quot;Received By Cash Ops&quot;),NOT(ISBLANK(Check_Number__c)),NOT(ISBLANK(Check_Amount__c)),NOT(ISBLANK(BR_Group_Number__c))),OR(AND(OR(ISPICKVAL(BR_State__c,&quot;NY&quot;),ISPICKVAL(BR_State__c,&quot;ME&quot;)),ISPICKVAL(Stage__c,&quot;Scrub Completed&quot;)),AND(OR(ISPICKVAL(BR_State__c,&quot;CA&quot;),ISPICKVAL(BR_State__c,&quot;CO&quot;),OR(ISPICKVAL(BR_Sub_Category__c,&quot;Paper&quot;),ISPICKVAL(BR_Sub_Category__c,&quot;Census&quot;),ISPICKVAL(BR_Sub_Category__c,&quot;Automated Bulk Load&quot;)),ISPICKVAL(Stage__c,&quot;Products Loaded&quot;)),AND(OR(ISPICKVAL(BR_State__c,&quot;CA&quot;),ISPICKVAL(BR_State__c,&quot;CO&quot;)),OR(ISPICKVAL(BR_Sub_Category__c,&quot;Real-Time&quot;), ISPICKVAL(BR_Sub_Category__c,&quot;OME&quot;)),ISPICKVAL(Stage__c,&quot;Members Loaded&quot;)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR52_Case_DateDue_Update</fullName>
        <actions>
            <name>SGA_FU36_Update_Case_DateDue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Date due field to 5 days after created date</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;), RecordType.Name = &apos;SG SCLC Inquiry&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR53_UpdateCheckStatusToPaymentReturn</fullName>
        <actions>
            <name>SGA_FU52_UpdateCheckStatusToPaymentRetur</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the check status when
Case Stage set to “Group Denied” or “Group Cancelled”
Check Status ≠ Sent to Cash Ops”</description>
        <formula>AND(NOT($User.BypassWF__c),NOT(ISNEW()), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;),  RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;,  OR(ISCHANGED(Check_Status__c),ISCHANGED(Stage__c)),OR(ISPICKVAL(Stage__c,&quot;Group Denied&quot;),ISPICKVAL(Stage__c,&quot;Group Cancelled&quot;)),OR(ISPICKVAL(Check_Status__c,&quot;Sent to Cash Ops&quot;),ISPICKVAL(Check_Status__c,&quot;Received by Cash Ops&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR54_UpdateCheckStatusToPaymentReadyforProcessing</fullName>
        <actions>
            <name>SGA_FU53_UpdateCheckStatusToPayReadyProc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT($User.BypassWF__c),NOT(ISNEW()), ISPICKVAL(Tech_Businesstrack__c , &apos;SGQUOTING&apos;),  RecordType.DeveloperName = &quot;Small_Group_Case_Installation&quot;, ISPICKVAL(Check_Status__c,&quot;Received by Cash Ops&quot;), NOT(ISBLANK(Tech_GroupNumber__c)), OR(ISCHANGED(BR_State__c),ISCHANGED(BR_Sub_Category__c),ISCHANGED(Stage__c),ISCHANGED(Check_Status__c)), OR( AND(OR(ISPICKVAL(BR_State__c,&quot;NY&quot;),ISPICKVAL(BR_State__c,&quot;ME&quot;)),ISPICKVAL(Stage__c,&quot;Scrub Completed&quot;)), AND(OR(ISPICKVAL(BR_State__c,&quot;CA&quot;),ISPICKVAL(BR_State__c,&quot;CO&quot;)),OR(ISPICKVAL(BR_Sub_Category__c,&quot;Paper&quot;),ISPICKVAL(BR_Sub_Category__c,&quot;Census&quot;),ISPICKVAL(BR_Sub_Category__c,&quot;Automated Bulk Upload&quot;)),ISPICKVAL(Stage__c,&quot;Products Loaded&quot;)), AND(OR(ISPICKVAL(BR_State__c,&quot;CA&quot;),ISPICKVAL(BR_State__c,&quot;CO&quot;)),OR(ISPICKVAL(BR_Sub_Category__c,&quot;Real-Time&quot;),ISPICKVAL(BR_Sub_Category__c,&quot;OME&quot;)),ISPICKVAL(Stage__c,&quot;Members Loaded&quot;)) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR56_UpdateCaseRecordTypeWhenCaseClosed</fullName>
        <actions>
            <name>SGA_FU56_UpdateCaseRecordType</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>SGQUOTING</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>SCLC Inquiry</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Store Case Record Type Before Closing Case</fullName>
        <actions>
            <name>Store_Previous_Case_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Closed,Closed - Reopen</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>BROKER</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Time Case Stage Changed</fullName>
        <actions>
            <name>Update_Time_Case_Stage_Changed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(
ISCHANGED(   Case_Owner__c    ),
OR(
 ISPICKVAL( Stage__c , &quot;Scrub Completed&quot;) ,
ISPICKVAL( Stage__c , &quot;Products Loaded&quot;) ,
ISPICKVAL( Stage__c , &quot;Members Loaded&quot;) ,
ISPICKVAL( Stage__c , &quot;Group Denied&quot;) ,
ISPICKVAL( Stage__c , &quot;Group Expired&quot;) ,
ISPICKVAL( Stage__c , &quot;Group Cancelled&quot;) 

))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Uncheck Case Status Flag for Reopen Update</fullName>
        <actions>
            <name>Uncheck_Case_Status_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Update_Status_from_Closed_to_Reopened__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Reopen</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>BROKER</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Effective Date</fullName>
        <actions>
            <name>Update_Effective_date_in_case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>NOT(ISNULL( Application_Name__r.Group_Coverage_Date__c ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Time of Group Returned</fullName>
        <actions>
            <name>Update_Group_Returned_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Group Returned</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Time of Vendor Returned</fullName>
        <actions>
            <name>Update_Vendor_Returned_Date_Time</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Vendor Return</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Vendor Returned%2FResolved Date%2FTime</fullName>
        <actions>
            <name>Update_Vendor_Returned_Resolved_Date_Tim</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Stage__c</field>
            <operation>equals</operation>
            <value>Vendor Return Resolved</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Create_Connect_Activity_Dental_Checkboxes</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Date_Due__c</offsetFromField>
        <priority>Low</priority>
        <protected>false</protected>
        <status>Awaiting Response</status>
        <subject>Create Connect Activity Dental Checkboxes</subject>
    </tasks>
    <tasks>
        <fullName>Group_Approved_Load_Group_into_DDS</fullName>
        <assignedTo>connectteamlead@accenture.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Effective_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Group Approved. Load Group into DDS</subject>
    </tasks>
    <tasks>
        <fullName>Group_Approved_Process_Paper_Payment</fullName>
        <assignedTo>bill.harrison@anthem.com</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Case.Date_Due__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Group Approved, Process Paper Payment</subject>
    </tasks>
</Workflow>
