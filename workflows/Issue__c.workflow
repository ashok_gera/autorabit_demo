<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Issue_Thank_You_of_Identifier</fullName>
        <description>Issue: Thank You of Identifier</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Issue_Notification_of_User_Indentifying</template>
    </alerts>
    <fieldUpdates>
        <fullName>Issue_update_status_field</fullName>
        <field>Status__c</field>
        <literalValue>Assigned</literalValue>
        <name>Issue: update status field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Issue Identifier Notification</fullName>
        <actions>
            <name>Issue_Thank_You_of_Identifier</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Issue__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Thank you for identifying a possible issue</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
