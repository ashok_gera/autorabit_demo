<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Anthem_Leads_Zip_Code_Assignment_Name</fullName>
        <description>Updates the Name of the User by Zip-City-County record</description>
        <field>Name</field>
        <formula>State_Abbreviated__c &amp;&quot;-&quot;&amp; Zip_Code__r.Name</formula>
        <name>Anthem Opps: Zip Code Assignment Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Anthem Opps%3A Zip Code Assignment Name</fullName>
        <actions>
            <name>Anthem_Leads_Zip_Code_Assignment_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User_by_Zip_City_County__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updates the name of the User by Zip-City-County Record name</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
