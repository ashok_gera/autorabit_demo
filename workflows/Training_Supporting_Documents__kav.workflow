<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Article_Rejection_Email_Aler</fullName>
        <description>Article Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Article_Rejection_Email_Alert</fullName>
        <description>Article Rejection Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Rejection_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Final_Article_Approval_Email_To_Creator</fullName>
        <description>Final Article Approval Email To Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Final_Email_Alert_for_Publishing_the_Article</fullName>
        <description>Final Email Alert for Publishing the Article</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Final_Approval_Email</template>
    </alerts>
    <alerts>
        <fullName>HIX_Approval_Email_Alert</fullName>
        <description>HIX Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>HIX</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>ISG_Strategy_Approval_Email_Alert</fullName>
        <description>ISG Strategy Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>ISG_Strategy</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Legal_Approval_Email_Alert</fullName>
        <description>Legal Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Legal</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Marketing_PR_Approval_Email_Alert</fullName>
        <description>Marketing &amp; PR Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Marketing_Public_Relations</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Regulatory_Approval_Email_Alert</fullName>
        <description>Regulatory Approval Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>Regulatory</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <recipient>system.admin@anthem.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Knowledge_Approval_Email_Template</template>
    </alerts>
</Workflow>
