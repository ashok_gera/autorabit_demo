<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SGA_FU34_Case_Latest_Inbound_Email</fullName>
        <field>Latest_Inbound_Email__c</field>
        <formula>TextBody</formula>
        <name>SGA_FU34_Case_Latest_Inbound_Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU35_Update_LastModifiedDate</fullName>
        <field>New_Email_Timestamp__c</field>
        <formula>NOW()</formula>
        <name>SGA_FU35_Update_LastModifiedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>SGA_WR44_Case_Latest_Inbound_Email _FiledUpdate</fullName>
        <actions>
            <name>SGA_FU34_Case_Latest_Inbound_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>SGA_FU35_Update_LastModifiedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This rule is to send Notification to Owner Response for Pre-Enrollment case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
