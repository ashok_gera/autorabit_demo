<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SGA_EA19_Documents_ReUploaded</fullName>
        <description>SGA EA19 Documents ReUploaded</description>
        <protected>false</protected>
        <recipients>
            <field>Tech_CaseOwner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>webcontent@anthem.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SGA_Email_Templates/SGA19_Document_Re_Uploaded</template>
    </alerts>
    <fieldUpdates>
        <fullName>SGA_FU16_UpdateRequiredCheckBox</fullName>
        <field>Required__c</field>
        <literalValue>1</literalValue>
        <name>SGA_FU16_UpdateRequiredCheckBox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SGA_FU25_UpdateDateTimeSubmitted</fullName>
        <field>Date_Time_Submitted__c</field>
        <formula>NOW()</formula>
        <name>SGA_FU25_UpdateDateTimeSubmitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SGFO_OM_DocChecklist_Informatica</fullName>
        <apiVersion>40.0</apiVersion>
        <description>SGFO Outbound Message for Document Checklist</description>
        <endpointUrl>https://ps1w2-obm.rt.informaticacloud.com/active-bpel/services/REST/SalesforceNotificationRESTPort?processName=SGCDocUploadServiceSFDCByChkListId</endpointUrl>
        <fields>Application__c</fields>
        <fields>Id</fields>
        <fields>Tech_Content_Document_Id__c</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>etl_ind_sales@anthem.com.isg.prod</integrationUser>
        <name>SGFO_OM_DocChecklist_Informatica</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>SGA_WR05_DocumentChecklist_Is_Updated</fullName>
        <actions>
            <name>SGFO_OM_DocChecklist_Informatica</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Application_Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>Re-Uploaded,Uploaded,Document Signed</value>
        </criteriaItems>
        <description>This rule is to send an Outbound Message to Informatica if the Document Check List has been updated and &apos;Status&apos; = &apos;Uploaded&quot; or &apos;Re-Uploaded&apos;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR26_Documents_Required _Missing</fullName>
        <actions>
            <name>SGA_FU16_UpdateRequiredCheckBox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>Missing Information</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.BypassWF__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Automatically update the Required check field box on a Document record in the Document Checklist if the status of the Document record has been changed to &quot;Missing Information&quot;.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SGA_WR28_Documents_ReUploaded</fullName>
        <actions>
            <name>SGA_EA19_Documents_ReUploaded</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule is used to send email notification to a Case Owner that the Broker is &quot;Re-Uploaded&quot; a document which is previously marked as &quot;Missing Information&quot;.</description>
        <formula>AND(NOT($User.BypassWF__c), ISPICKVAL(PRIORVALUE(Status__c), &apos;Missing Information&apos;), ISPICKVAL(Status__c, &apos;Re-Uploaded&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SG_WR38_UpdateDateTimeSubmitted</fullName>
        <actions>
            <name>SGA_FU25_UpdateDateTimeSubmitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Application_Document_Checklist__c.Status__c</field>
            <operation>equals</operation>
            <value>Uploaded</value>
        </criteriaItems>
        <criteriaItems>
            <field>Application_Document_Checklist__c.Date_Time_Submitted__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>This workflow rule is used to update Date Time Submitted field on uploading the file.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
