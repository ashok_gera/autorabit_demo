<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>EA02_Account_Confidential_Notification_Email</fullName>
        <description>EA02_Account Confidential Notification Email</description>
        <protected>false</protected>
        <recipients>
            <field>Tech_Account_Owner_s_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Telesales_Email_Templates/Account_Confidential_Address_Notification_to_Team_Lead</template>
    </alerts>
    <fieldUpdates>
        <fullName>ACC_FU01_AccountStage</fullName>
        <description>Update customer stage to Opportunity</description>
        <field>Customer_Stage__c</field>
        <literalValue>Opportunity</literalValue>
        <name>ACC_FU01_AccountStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU02_EligibilityDesc</fullName>
        <description>Update Eligibility Description to blank when qualifying event is not Other</description>
        <field>Eligibility_Description__c</field>
        <name>ACC_FU02_EligibilityDesc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU03_CityUpdate</fullName>
        <field>BillingCity</field>
        <formula>Zip_Code__r.City__c</formula>
        <name>ACC_FU03_CityUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU04_CountyUpdate</fullName>
        <field>County__c</field>
        <formula>Zip_Code__r.County__c</formula>
        <name>ACC_FU04_CountyUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU04_WorngChannelDesc</fullName>
        <field>Wrong_Channel_Description__c</field>
        <name>ACC_FU04_WorngChannelDesc</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU05_StateUpdate</fullName>
        <field>State__c</field>
        <formula>Zip_Code__r.State__c</formula>
        <name>ACC_FU05_StateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU06_StandardStateUpdate</fullName>
        <field>BillingState</field>
        <formula>Zip_Code__r.State__c</formula>
        <name>ACC_FU06_StandardStateUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU07_StandardPostalCodeUpdate</fullName>
        <field>BillingPostalCode</field>
        <formula>Zip_Code__r.Name</formula>
        <name>ACC_FU07_StandardPostalCodeUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU08_DefaultCountryUpdate</fullName>
        <field>BillingCountry</field>
        <formula>Zip_Code__r.Country__c</formula>
        <name>ACC_FU08_DefaultCountryUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU09_Suppressed_Email</fullName>
        <field>Suppressed_Email__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU09_Suppressed:Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU11_Suppressed_Mail</fullName>
        <field>Suppressed_Mail__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU11_Suppressed:Mail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU13_Suppressed_MobilePhone</fullName>
        <field>Suppressed_Phone__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU13_Suppressed:MobilePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU14_Suppressed_HomePhone</fullName>
        <field>Suppressed_HomePhone__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU14_Suppressed:HomePhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU15_AccountOwner_sEmailIDUpdate</fullName>
        <field>Tech_Account_Owner_s_Manager_Email__c</field>
        <formula>Tech_Agent_s_Manager_Email__c</formula>
        <name>ACC_FU15_AccountOwner&apos;sEmailIDUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU15_Suppressed_WorkPhone</fullName>
        <field>Suppressed_WorkPhone__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU15_Suppressed:WorkPhone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU16_Suppress</fullName>
        <field>Suppress__c</field>
        <literalValue>1</literalValue>
        <name>ACC_FU16_Suppress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU16_Suppressed_unCheck</fullName>
        <field>Suppress__c</field>
        <literalValue>0</literalValue>
        <name>ACC_FU16_Suppressed unCheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ACC_FU17_MarketingCampaignUpdate</fullName>
        <field>Marketing_Campaign__c</field>
        <formula>Marketing_Event__r.Description</formula>
        <name>ACC_FU17_MarketingCampaignUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Acc_FU18_VendorIdUpdate</fullName>
        <field>Vendor_Customer_ID__c</field>
        <formula>&apos;SFDC&apos; &amp; Tech_Acc_SFDCX__c</formula>
        <name>Acc_FU18_VendorIdUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account</fullName>
        <field>Contact_Phone__pc</field>
        <formula>Phone</formula>
        <name>Account - Phone to Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Contact_Phone</fullName>
        <field>Contact_Phone__c</field>
        <formula>Phone</formula>
        <name>Update Contact Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field</fullName>
        <field>Account_ETL_External_ID__c</field>
        <formula>CASESAFEID(Id)</formula>
        <name>Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <outboundMessages>
        <fullName>SFDC_Account_RTKeys</fullName>
        <apiVersion>34.0</apiVersion>
        <endpointUrl>https://app2.informaticacloud.com/saas/api/1/salesforceoutboundmessage/PFaCf5cU4kJ0GC2VCcOGWbWC0V0wWZrU</endpointUrl>
        <fields>CreatedDate</fields>
        <fields>Id</fields>
        <fields>LastModifiedDate</fields>
        <fields>Lead_ID__c</fields>
        <fields>Lead_ID__pc</fields>
        <fields>RecordTypeId</fields>
        <fields>SystemModstamp</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>ad64108@wellpoint.com.isg</integrationUser>
        <name>SFDC_Account_RTKeys</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <outboundMessages>
        <fullName>SGA_Account_Test</fullName>
        <apiVersion>40.0</apiVersion>
        <endpointUrl>https://ps1w2-obm.rt.informaticacloud.com/active-bpel/services/REST/SalesforceNotificationRESTPort?processName=SGCDocUploadServiceSFDCByChkListId</endpointUrl>
        <fields>Application_ID__c</fields>
        <fields>Id</fields>
        <includeSessionId>true</includeSessionId>
        <integrationUser>darren.sherman@anthem.isg</integrationUser>
        <name>SGA_Account_Test</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>ACC01_StageUpdateOnLeadConversion</fullName>
        <actions>
            <name>ACC_FU01_AccountStage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will automatically update Customer Stage from New Lead to New Opportunity when the Lead is converted to Household (Person Account)</description>
        <formula>NOT($User.BypassWF__c) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c ,&quot;TELESALES&quot;) ) &amp;&amp; AND(CreatedDate = LastModifiedDate, Tech_Account_ConvertedLead__c ==true )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ACC02_EligibilityDescription</fullName>
        <actions>
            <name>ACC_FU02_EligibilityDesc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This field will clear the eligibility description field values if the qualifying event is not other</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;)) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;))&amp;&amp; (NOT(ISPICKVAL(Upcoming_Qualifying_Event__c ,&quot;Other&quot;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC03_PostalCodeCountyUpdate</fullName>
        <actions>
            <name>ACC_FU03_CityUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU04_CountyUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU05_StateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU06_StandardStateUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU07_StandardPostalCodeUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU08_DefaultCountryUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to populate the address information from Postal Code County object</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;))&amp;&amp;(ISNEW() || ISCHANGED( Zip_Code__c )) &amp;&amp; Zip_Code__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC04_CustomerStage</fullName>
        <actions>
            <name>ACC_FU04_WorngChannelDesc</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow rule will clear the wrong channel description field when the stage is not wrong channel and reason is not other</description>
        <formula>NOT( $User.BypassWF__c ) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;))&amp;&amp; (OR(NOT(ISPICKVAL(Customer_Stage__c,&quot;Wrong Channel&quot;)),AND(ISPICKVAL(Customer_Stage__c,&quot;Wrong Channel&quot;),NOT(ISPICKVAL(Reason__c,&quot;Other&quot;)))))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC05_Suppress Fields Check</fullName>
        <actions>
            <name>ACC_FU09_Suppressed_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU11_Suppressed_Mail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU13_Suppressed_MobilePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU14_Suppressed_HomePhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU15_Suppressed_WorkPhone</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ACC_FU16_Suppress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is used to update the Suppressed: Email,Suppressed: Mail,Suppressed: Phone check boxes when Suppress field value selected &quot;Yes&quot;</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp; (ISPICKVAL(Tech_Businesstrack__c , &quot;TELESALES&quot;)) &amp;&amp; AND(Suppress__c == true,Suppressed_Email__c==false,Suppressed_Mail__c==false,Suppressed_HomePhone__c==false,Suppressed_Phone__c==false,Suppressed_WorkPhone__c==false )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC06_Suppress Fields Uncheck</fullName>
        <actions>
            <name>ACC_FU16_Suppressed_unCheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This rule is used to uncheck the Suppressed: Email,Suppressed: Mail,Suppressed: Phone check boxes when Suppress field value selected &quot;No&quot;</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c , &quot;TELESALES&quot;)) &amp;&amp; OR(Suppressed_Email__c==false,Suppressed_Mail__c==false,Suppressed_HomePhone__c==false,Suppressed_Phone__c==false,Suppressed_WorkPhone__c==false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC07_UserManagerEmailUpdate</fullName>
        <actions>
            <name>ACC_FU15_AccountOwner_sEmailIDUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT( $User.BypassWF__c )&amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;))&amp;&amp;(Tech_Agent_s_Manager_Email__c&lt;&gt;NULL)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC08_Account Confidential Notification Rule</fullName>
        <actions>
            <name>EA02_Account_Confidential_Notification_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>This rule is used to send email alert to Agent&apos;s Team Lead when an Agent select confidential check box</description>
        <formula>NOT( $User.BypassWF__c )&amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;)) &amp;&amp;  OR( AND(ISCHANGED( Confidential__c ) , Confidential__c == True), AND(ISCHANGED( Confidential_Billing_Address__c) , Confidential_Billing_Address__c == True) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>ACC09_MarketingCampaignUpdate</fullName>
        <actions>
            <name>ACC_FU17_MarketingCampaignUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This workflow is used to populate the address information from Postal Code County object</description>
        <formula>NOT($User.BypassWF__c ) &amp;&amp;(ISPICKVAL(Tech_Businesstrack__c,&quot;TELESALES&quot;))&amp;&amp;(ISNEW() || ISCHANGED( Marketing_Event__c)) &amp;&amp; Marketing_Event__c != NULL</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Acc10_VendorIDSFDCX</fullName>
        <actions>
            <name>Acc_FU18_VendorIdUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT($User.BypassWF__c )&amp;&amp;(ISPICKVAL(Tech_Businesstrack__c , &quot;TELESALES&quot;))&amp;&amp;(ISBLANK(Vendor_Customer_ID__c))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account - Copy Phones</fullName>
        <actions>
            <name>Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND( OR( NOT(ISBLANK(Mobile_Phone__c)), NOT(ISBLANK(Phone)) ), OR( RecordType.Name =&apos;Anthem Opps&apos;, RecordType.Name =&apos;SG Direct&apos; ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SFDC_Account_RTKeys</fullName>
        <actions>
            <name>SFDC_Account_RTKeys</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Tech_Businesstrack__c</field>
            <operation>equals</operation>
            <value>ANTHEMOPPS</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SGA_Account_Test</fullName>
        <actions>
            <name>SGA_Account_Test</name>
            <type>OutboundMessage</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>TestAcct</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Account ETL External ID</fullName>
        <actions>
            <name>Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Person Account</value>
        </criteriaItems>
        <description>This rule is meant for Informatica usage only</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contact Phone</fullName>
        <actions>
            <name>Update_Contact_Phone</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates Contact Phone with Home phone.

Jira: SS-1220</description>
        <formula>ISCHANGED(   Phone   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
