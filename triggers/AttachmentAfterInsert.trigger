/*****************************************************************************************
 * Trigger Name   : AttachmentAfterInsert
 * Created By     : IDC Offshore
 * Created Date   : 29-Jun-2017
 * Description    : To update the Document CheckList records based on the docusign status
 					record attachments.
 * Change History : 
 ****************************************************************************************/
trigger AttachmentAfterInsert on Attachment (after insert) {
	Set<ID> docuSignStatusIds = new Set<ID>();
    Schema.DescribeSObjectResult r = dsfs__Docusign_Status__c.sObjectType.getDescribe();
    String keyPrefix = r.getKeyPrefix();
    for(Attachment attachObj : Trigger.New){
        if(attachObj.ParentId != NULL && String.valueOf(attachObj.ParentId).startsWith(keyPrefix)){
            docuSignStatusIds.add(attachObj.ParentId);
        }
    }

    if(docuSignStatusIds != NULL && !docuSignStatusIds.isEmpty()){
        SGA_AP17_UpdateDocCheckListDSStatus.updateDCList(docuSignStatusIds);
    }
}