trigger AccountAfterInsert on Account (after insert) 
{
    /*
    // Begin - Daryl - Telesales WDE changes - 4/27/16
    if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue) || 'ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue))
    {
        try
        {
            system.debug('currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
            if(Trigger.new[0].Inbound_Activity_Id__c != NULL  && AssociateActivityToLeadAndAccount.currObjId == null)
            {
                AssociateActivityToLeadAndAccount.currObjId = Trigger.new[0].Id;
                AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();
                assocAct.AssociateActivity('Account', Trigger.new[0].Id);
            }
        }
        catch(Exception e)
        {
           System.Debug(e.getMessage());
        } 
    }
    // End - Daryl - Telesales WDE changes - 4/27/16
    */
    //START*******SG QUOTING CODING **********************************//
         if(System.Label.SG_BusinessTrack.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            // System.debug('AccountTriggerSGHandler Enter for Insert::');
            AccountTriggerSGHandler.onAfterInsert(Trigger.newMap);    
             
         }
        //********SG QUOTING CODING END****************************************//
    
}