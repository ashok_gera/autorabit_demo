/***************************************************************************
Trigger Name   : NoteBeforeUpdate
Date Created : 09/11/2017 (MM/DD/YYYY)
Created By   : IDC Offshore
Description  :1. Trigger to invoke SGA_AP43_PreventNotesOnOpp if an Note is updted for Opporurtunity
              2. Prevents updation of Note records under Opporutunity of "SG Quoting" type.
********************************************************************************/
trigger NoteBeforeUpdate on Note (before update) {
   // call handler method to prevent note updation for SGQOUTING opportunites
      NoteTriggerHandler.onBeforeUpdate(Trigger.newMap);  
}