trigger ResetCallId on Call_Info__c (after update) 
{
    try
    {
        for (Call_Info__c ci : Trigger.new) 
        {
            DateTime currCallEndDate = ci.BR_End_Date__c;
            //     If call end date is populated, then nullify the current call id field on the user record.
            if (currCallEndDate != null)
            {
                User usr = [select Id, Broker_Current_Call_Id__c from user where Id =:UserInfo.getUserId()];
                if (usr.Broker_Current_Call_Id__c == ci.BR_Call_Id__c)
                {
                    usr.Broker_Current_Call_Id__c = null;
                    update usr;
                }
            }
        }
    }
    catch(Exception e)
    {
        System.Debug(e.getMessage());
    }
}