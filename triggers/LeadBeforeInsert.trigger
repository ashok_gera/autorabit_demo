/*
Date Created : 16-April-2015
Created By   : Bhaskar/Santosh/Venkat
Modified By  : Nagarjuna
Description  : This Trigger will auto populate the customer score on Lead based on the Zip code entered by an Agent upon save.
Trigger Event : Before Insert
*/
trigger LeadBeforeInsert on Lead (before Insert) { 
    //**************************TELESALES CODING START****************************************//
     Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
     if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            List<Lead> leadList = New List<Lead>();
 
            Util01_RetrieveZipCodeScore zipCodeScoreHelper = new Util01_RetrieveZipCodeScore();
            
            //Recursion Protection 
            if(Util01_RetrieveZipCodeScore.isFirstRunLeadInsert== true)
            {
                for(Lead l : Trigger.New){
                    if(l.Customer_Score__c == NULL){
                        leadList.add(l);
                    }
                }
                
                if(!leadList.IsEmpty()){
                    zipCodeScoreHelper.CustomerScoreAutoPopulate(leadList,Util03_HardCodingConstants.TECHZIP_CODE,Util03_HardCodingConstants.DOB,Util03_HardCodingConstants.CUSTOMER_SCORE);
                }
                Util01_RetrieveZipCodeScore.isFirstRunLeadInsert = false;
            }
            if(AP19_PopulateTechStreetValue.isLeadInsertFirstRun == true)
            {
                AP19_PopulateTechStreetValue pts = new AP19_PopulateTechStreetValue();
                pts.populateTechStreetVal(Trigger.New);
                AP19_PopulateTechStreetValue.isLeadInsertFirstRun = false;
            }
            /* Call the class method to perform the actual marketing campaign population business logic */
          /*AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
            //Recursion protection
             if(AP09_PopulateMarketingCampaign.isLeadInsertFirstRun == true){
                 populateMC.populateLeadMarketingCampaign(trigger.new,false);
                 AP09_PopulateMarketingCampaign.isLeadInsertFirstRun = false;
            }*/
    }
    //**************************TELESALES CODING END****************************************//
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        LeadTriggerDispatcher.onBeforeInsert( Trigger.new );
    }
    //**************************ANTHEMOPPS CODING END****************************************//
}