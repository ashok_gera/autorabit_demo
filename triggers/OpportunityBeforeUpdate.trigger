/********************************************************
Trigger Name : OpportunityBeforeUpdate 
Date Created : 11-June-2015
Created By   : Bhaskar
Modified By  : 
Description  :  This Trigger will restrict creation of multiple opportunities with the same stage name except
                closed won / closed lost for an Account.
Trigger Event : Before Update
********************************************************/
trigger OpportunityBeforeUpdate on Opportunity(before update) {
    //**************************TELESALES CODING START****************************************//
     Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
     if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        try{
            List<Opportunity> oppList = new List<Opportunity>();
            AP08_RestrictMultipleOpenOpportunities restrictOpp =new AP08_RestrictMultipleOpenOpportunities();
            //Recursion Protection 
            if(AP08_RestrictMultipleOpenOpportunities.isFirstRunOPPBeforeUpdate==true){
                /* Call the class method to perform the actual restriction logic */
                 for(Opportunity aa : Trigger.New){
                            if(aa.Open_Enrollment__c != trigger.oldMap.get(aa.Id).Open_Enrollment__c){
                                    oppList.add(aa);    
                            }
                        }
                        if(!oppList.isEmpty()){
                            restrictOpp.restrictMultipleOpenOppUpdate(oppList);
                        }
                
                AP08_RestrictMultipleOpenOpportunities.isFirstRunOPPBeforeUpdate = false;
                 }
        }catch(Exception e){          
                System.debug('### Exception OpportunityBeforeUpdate Trigger ###'+e);
        }
    }
    //**************************TELESALES CODING END****************************************//
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onBeforeUpdate( Trigger.newMap, Trigger.oldMap );
    }
    //**************************ANTHEMOPPS CODING END****************************************//
}