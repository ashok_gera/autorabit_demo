/***********************************************************************************
 * Trigger Name : DocusignStatusAfterUpdate
 * Created Date : 23-June-2017
 * Created By   : IDC Offshore
 * Description  : This Trigger is used to update the file_size__c,File_URL__c,
 *              Tech_Content_Document_ID__c,Status__c on DocumentChecklist
 *              from taking the attachments from DocuSignStatus object. 
 **********************************************************************************/
trigger DocusignStatusAfterUpdate on dsfs__DocuSign_Status__c (After Update) {
    system.debug('inaide upda');
    DocusignStatusTriggerHandler.onAfterUpdate(Trigger.NewMap,Trigger.OldMap);
}