trigger LicenseAppointmentAfterInsertUpdate on License_Appointment__c (After Insert, After Update) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            License_Appointment__c lap = Trigger.New[0];
            if(System.Label.SG_BusinessTrack.equalsIgnoreCase(lap.Tech_Businesstrack__c)){
                SGA_AP58_LicenseAppt_AccUpdate.insertAccTechAppointment(Trigger.New);
            }
        }
        
        if(Trigger.isUpdate){
        
        }
    }

}