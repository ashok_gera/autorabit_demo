trigger CopyCallId on Call_Info__c (after insert) 
{
    try
    {
        String currCallId = trigger.new[0]. BR_Call_Id__c;
        User usr = [select Id, Broker_Current_Call_Id__c from user where Id =:UserInfo.getUserId()];
        usr.Broker_Current_Call_Id__c = currCallId;
        update usr;
    }
    catch(Exception e)
    {
        System.Debug(e.getMessage());
    }
}