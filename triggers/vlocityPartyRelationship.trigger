trigger vlocityPartyRelationship on vlocity_ins__PartyRelationship__c (after insert, after update, after delete) {
    
    vlocityPartyRelationshipTriggerHandler handler = new vlocityPartyRelationshipTriggerHandler();

    if (Trigger.isAfter && Trigger.isInsert) {
       // handler.onAfterInsert(Trigger.new);
       vlocityPartyRelationshipTriggerHandler.onAfterInsert(Trigger.new);

    } else if (Trigger.isAfter && Trigger.isUpdate) {
        //handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
        vlocityPartyRelationshipTriggerHandler.onAfterUpdate(Trigger.new, Trigger.oldMap);

    } else if (Trigger.isAfter && Trigger.isDelete) {
        //handler.onAfterDelete(Trigger.old);
        vlocityPartyRelationshipTriggerHandler.onAfterDelete(Trigger.old);

    }
}