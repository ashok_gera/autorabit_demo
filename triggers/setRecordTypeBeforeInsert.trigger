trigger setRecordTypeBeforeInsert on vlocity_ins__Application__c (before insert) {
    map <String,String> rtList = new Map <String,String>();
    
    for (RecordType rt: [select Id,Name from RecordType where SobjectType = 'vlocity_ins__Application__c']){
        rtList.put(rt.Name,rt.Id);
    }
    List <String> Apps = new List <String>();
    for (vlocity_ins__Application__c via :trigger.new){
        Apps.add(via.vlocity_ins__AccountId__c);
    
    }

    map <String,String> accList = new Map <String,String>();
    
    for (Account acc: [select Company_State__c from Account where id in: Apps]){
        accList.put(acc.Id,acc.Company_State__c);
    }
    
    for (vlocity_ins__Application__c via :trigger.new){
        
        if (accList.get(via.vlocity_ins__AccountId__c)!=null){
            if (rtList.containsKey(accList.get(via.vlocity_ins__AccountId__c))){
                via.RecordTypeId = rtList.get(accList.get(via.vlocity_ins__AccountId__c));
            }
        }

    
    }
  

}