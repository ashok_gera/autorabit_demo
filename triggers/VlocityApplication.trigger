/*****************************************************************************************
Trigger Name   : VlocityApplication
Date Created : 6/01/2017
Created By   : IDC Offshore
Description  : 1. This trigger class create document checklist records when application record
is created.
2. Creates records Application share Object.
Change History : 
******************************************************************************************/
trigger VlocityApplication on vlocity_ins__Application__c (after update) {
    VlocityApplicationTriggerHandler handler = new VlocityApplicationTriggerHandler();
              
        if (Trigger.isAfter && Trigger.isUpdate) {
        
        if(VlocityApplicationTriggerHandler.isFirstRunAfterUpdate)
        {
            handler.onAfterInsert(Trigger.oldMap, Trigger.new);
            handler.onAfterUpdate(Trigger.oldMap,Trigger.newMap);
            handler.effectivemethod(trigger.new, trigger.oldmap);
            VlocityApplicationTriggerHandler.isFirstRunAfterUpdate = false;
        }
        VlocityApplicationTriggerHandler.onAfterUpdateStatus(Trigger.NewMap,Trigger.OldMap);
        
    }
}