trigger OpportunityBeforeDelete on Opportunity (before delete) {
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onBeforeDelete( Trigger.oldMap );
    }
    //**************************ANTHEMOPPS CODING END****************************************//
}