/**********************************************************************
trigger Name : TaskAfterDelete 
Date Created : 01-Jun-2015
Created By   : Nagarjuna
Description  : Used to populate next follow up days on account
**********************************************************************/
trigger TaskAfterDelete on Task (After Delete) {

 //**************************TELESALES CODING START****************************************//
   // if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        AP05_ActivityTimeInteraction aa=new AP05_ActivityTimeInteraction(Trigger.old);      
          
          if(AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun==true){       
             aa.populateNotstartedInProgressAcitivity();           
             AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun= false;   
               
            }
          if(AP05_ActivityTimeInteraction.islastactivityFirstRun==true ){          
             aa.populateTimeSinceLastActivity();           
             AP05_ActivityTimeInteraction.islastactivityFirstRun = false;
          
            }  
            
          if(AP05_ActivityTimeInteraction.isInteractedFirstRun==true ){          
             aa.populateAgentInteraction();           
             AP05_ActivityTimeInteraction.isInteractedFirstRun= false;
          
            }  
    

    AP06_UpdateNextFollowUpDaysOnAccount clsPopulate = new AP06_UpdateNextFollowUpDaysOnAccount();
    if(AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterDelete == true)
    {
        clsPopulate.PopulateNExtFollowUpDays(Trigger.Old);
        AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterDelete = false;
    }
    if(AP22_GenerateTaskXml.isXmlGeneratorAfterDelete == true){
                id sendCommrecordtypeId =[select Id,Name from Recordtype where Name = 'Send Fulfillment' and SobjectType = 'Task'].Id;
                Task t = Trigger.Old[0];
                if(t.recordTypeid == sendCommrecordtypeId && t.WhatId!=NULL){
                    String uId = UserInfo.getUserId();
                    AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                    generateXml.insertXmlFields(t,uId,System.Label.isDelete);
                }else if(t.recordTypeid == sendCommrecordtypeId && t.WhoId!=NULL){
                    String uId = UserInfo.getUserId();
                    AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                    generateXml.insertLeadXmlFields(t,uId,System.Label.isDelete);
                }
                AP22_GenerateTaskXml.isXmlGeneratorAfterDelete = false;
    }
  //}
   //**************************TELESALES CODING END****************************************//
}