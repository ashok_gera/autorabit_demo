/********************************************************
Trigger Name : HouseholdMemberAfterUpdate
Date Created : 19-May-2015
Created By   : Santosh/Amanjot
Modified By  : 
Description  :  This Trigger will restrict insert/update of more than 1 spouses or domestic partners
Trigger Event : Before Insert
********************************************************/
trigger HouseholdMemberAfterUpdate on Household_Member__c (after update) {
        //**************************TELESALES CODING START****************************************//
            AP03_RestrictMultipleSpouseDP hhmInstance2=new AP03_RestrictMultipleSpouseDP();
        
            //Recursion Protection 
            
            if(AP03_RestrictMultipleSpouseDP.isFirstRunHHMbeforeUpdate==true){
                /* Call the class method to perform the actual restriction logic */
                hhmInstance2.restrictMultipleSpouceDP(Trigger.New);
                AP03_RestrictMultipleSpouseDP.isFirstRunHHMbeforeUpdate = false;
            }
        
   //**************************TELESALES CODING END****************************************//
}