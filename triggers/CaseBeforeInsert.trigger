/*************************************************************
Trigger Name : CaseeBeforeInsert 
Date Created : 15-JUN-2016
Created By   : Rayson Landeta
Description  : Trigger before the case to be insert.
**************************************************************/
trigger CaseBeforeInsert on Case (before insert) {
    Case cs = Trigger.new[0];
    if(System.Label.BR_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c)){
        //Get custom setting for emails to assign per department
        Map<String,Department_Email__c> deptMap = Department_Email__c.getall();
        
        for(Case c : Trigger.New){   
            
            
            
            //Set Custom Case Origin based on Standard if Email
            if(c.Origin != null && c.Origin.contains('Email')){
                c.Case_Origin_Custom__c = 'Email';
            }
            
            //Set Standard Case Origin based on Custom if non-Email
            if(c.Case_Origin_Custom__c != null && !c.Case_Origin_Custom__c.contains('Email')){
                c.Origin = c.Case_Origin_Custom__c;
            }
            
            //Populate Original Subject and Description
            c.OriginalSubject__c = c.Subject;
            c.OriginalDescription__c = c.Description;
            
            //Populate Routing Address based on department
            if(c.Origin == 'Phone'){
                if(deptMap.containsKey(c.BR_Department__c)){
                    c.BR_Tech_Case_RoutingAddress__c = deptMap.get(c.BR_Department__c).Email__c;
                }
                
            }
            
        }
    }
}