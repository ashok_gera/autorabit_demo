/*****************************************************************
Trigger Name : TaskBeforeDelete 
Date Created : 23-Sep-2015
Created By   : Nagarjuna Kaipu
Description  :1. This trigger is used to restrict broker task deletion
*****************************************************************/
Trigger TaskBeforeDelete on Task (before Delete) {
/**************************BROKER CODING START****************************************/
    String ProfileId = UserInfo.getProfileId();  
    List<Profile> profiles=[SELECT Id FROM PROFILE WHERE Name = 'System Administrator' LIMIT 1];
    
    for(Task ObjTask : Trigger.old){
        if('BROKER'.EqualsIgnoreCase(ObjTask.Tech_Businesstrack__c) && ProfileId != profiles[0].Id)
        ObjTask.adderror('You do not have privileges to delete task.');
    }
/**************************BROKER CODING START****************************************/
}