trigger BRPSGCSendApplicationData on vlocity_ins__Application__c (after insert, after update) 
{
    if(system.isFuture()) 
    return;
    
    if ((Trigger.isInsert || Trigger.isUpdate) && (Trigger.isAfter))
    {
    	boolean includeAppnStatus = false;
    	boolean includeCaseStage = false;
    	
        for (vlocity_ins__Application__c applObj : Trigger.new) 
        {
        	vlocity_ins__Application__c oldApplObj = new vlocity_ins__Application__c();
        	
            //SGC changes 09/11/2017
        	if (Trigger.isUpdate && Trigger.isAfter)
        	{  	
	        	oldApplObj = Trigger.oldMap.get(applObj.Id);
	 	        System.debug('INTEGRATION TESTING******************************NEW ApplicationStatus: ' + applObj.vlocity_ins__Status__c);
		        System.debug('INTEGRATION TESTING******************************OLD ApplicationStatus: ' + oldApplObj.vlocity_ins__Status__c);
		        
		        if (oldApplObj.vlocity_ins__Status__c != applObj.vlocity_ins__Status__c)
	        	{  
		            includeAppnStatus = true;
	        	}		        	        
        	} 
        	else        	
        	{
        		includeAppnStatus = true;
        	}
        	
        	//// End of SGC changes 09/11/2017
        	
        	System.debug('INTEGRATION TESTING****************************** NEW Application Object: ' + applObj);
        	
        	BRPSGCUploadGroupQuoteData bRPSGCUploadGroupQuoteData = new BRPSGCUploadGroupQuoteData();
        	bRPSGCUploadGroupQuoteData.sendDataToSGC(applObj.Id, includeAppnStatus, includeCaseStage, oldApplObj);        	
        }
    }
}