/*************************************************************
Trigger Name : EmailMessageBeforeDelete 
Date Created : 12-JUN-2016
Created By   : Wendy Kelley
Description  : Before Delete trigger for EmailMessage records
Change History: 12-JUN-2016 Created.
*****************************************************************/
trigger EmailMessageBeforeDelete on EmailMessage (before delete) {
    
    /****************************************************************
    * Check 'Bypass Validation Rules' field on current user's record
    * Allow delete if field is set to true
    *****************************************************************/
    User currUser = [Select Id, BypassVR__c from User where Id =: UserInfo.getUserId()];
    for(EmailMessage e : Trigger.old) {
        if(!currUser.BypassVR__c) {
            e.addError(Label.E2C_EmailDeleteError);
        }
    }
    
}