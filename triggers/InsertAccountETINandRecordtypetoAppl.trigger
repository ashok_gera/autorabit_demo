trigger InsertAccountETINandRecordtypetoAppl  on vlocity_ins__Application__c (before insert, before update) {

    ETINandRecordtypetoApplHandler helpclass = new ETINandRecordtypetoApplHandler();
    
    helpclass.etinandrecordtype(trigger.new);           

}