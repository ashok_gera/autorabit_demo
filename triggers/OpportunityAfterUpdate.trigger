trigger OpportunityAfterUpdate on Opportunity (after update) {
    
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onAfterUpdate( Trigger.newMap, Trigger.oldMap );
    }
    //**************************ANTHEMOPPS CODING END*****************************************//
    
    //**************************SGQUOTING CODING START****************************************//
    if('SGQUOTING'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerSGHelper.doAfterUpdate(trigger.oldMap, trigger.newMap);
    }
    //**************************SGQUOTING CODING END*****************************************//
        
    // Added by nmishra@vocity.com
    // Calls a handler class, which checks if there are any existing Application for the Opportunity, 
    // and if there is one, the class re-evalutes Application Visibility based on General Agency, Paid Agency and Broker of the Opportunity
    if(Util03_HardCodingConstants.recursionOpportunity == false){
        OpportunityApplicationSharingHandler handler = new OpportunityApplicationSharingHandler();
        handler.evaluateApplicationSharing(trigger.oldMap, trigger.newMap);
        Util03_HardCodingConstants.recursionOpportunity = true;
    }
}