trigger BRPSendDocumentToSGC on Attachment (after insert) {
String pId;
String Name;
    for(Attachment att: Trigger.new){
        pId=att.ParentId;
        Name = att.Name;
        System.debug('Name - '+Name);
        System.debug('Pid - '+pId);
        if((pId != null) && ((Name.containsIgnoreCase('Application')) || (Name.containsIgnoreCase('Payment')) || (Name.containsIgnoreCase('HRA')))){
            List<vlocity_ins__Application__c> appList =[select Id from vlocity_ins__Application__c  where Id=:pId];
            if(appList.size() >0)
            { 
                    System.debug('List AFTER Query- '+appList);
                BRPSGCSendDocumentsBackend bRPSGCSendDocumentsBackend= new BRPSGCSendDocumentsBackend();
                bRPSGCSendDocumentsBackend.SendDocumentToSGC(appList[0].Id,Name); 
            }
         }
     }
        
}