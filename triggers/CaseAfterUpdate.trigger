/*************************************************************
Trigger Name : CaseAfterUpdate 
Date Created : 05-Aug-2015
Created By   : Nagarjuna Kaipu
Description  : Used to create CTI task on case
**************************************************************/
trigger CaseAfterUpdate on Case (after update) 
{
    //SGA Implementation 
    Case cs = Trigger.new[0];
    if(System.Label.SG_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c))           
    {
        CaseTriggerHandler.onAfterUpdate(Trigger.NewMap, Trigger.OldMap);
    }
    if(System.Label.BR_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c)){
        //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
        CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
        if(!userSettings.Bypass_Validation__c) {  //E2C exclude --- START
            try
            {
                if(BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseAfterUpdate == true)
                {
                    List<Id> cIds= new List<Id>();
                    for(Case c: Trigger.New)
                    {
                        if (c.BR_Tech_Case_RoutingAddress__c  == Trigger.oldMap.get(c.Id).BR_Tech_Case_RoutingAddress__c) {
                            cIds.add(c.Id);
                        }
                        
                    }
                    BR_AP02_CreateCTIActivityOnCase clsTask = new BR_AP02_CreateCTIActivityOnCase();
                    if(cIds.size() > 0){
                        clsTask.CreateCTITaskOnCase(cIds);
                    }
                    BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseAfterUpdate = false;
                }
            }
            catch(Exception e)
            {
                System.Debug(e.getMessage());
            }
            
        } //E2C exclude --- END
    }
}