/**************************************************************************
 * Trigger Name : CaseCommentBeforeDelete
 * Created Date : 8/28/2017
 * Created By   : IDC Offshore
 * Description  : Used to restrict the user should to not delete the case 
 * 				  installation related case comment
 **************************************************************************/
trigger CaseCommentBeforeDelete on CaseComment (before delete) {
	SGA_AP26_CaseCommentHandler.restrictCaseCommentDelete(Trigger.Old);
}