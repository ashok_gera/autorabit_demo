trigger BRPSGCSendCaseStage on Case (after insert, after update) 
{	
	if(system.isFuture()) 
       return;
	
    if ((Trigger.isAfter))
    {  
    	boolean includeAppnStatus = false;
    	boolean includeCaseStage = true;
    	  	   	    	
        for (Case caseObj : Trigger.new) 
        {  
	        System.debug('INTEGRATION TESTING******************************Tech_Businesstrack__c: ' + System.Label.SG_BusinessTrack);
	                	
        	if(System.Label.SG_BusinessTrack.equalsIgnoreCase(caseObj.Tech_Businesstrack__c))           
            {              
        	   if (Trigger.isUpdate)
        	   {
        	       Case oldCaseObj = Trigger.oldMap.get(caseObj.Id);
        	       
 	               System.debug('INTEGRATION TESTING******************************NEW CaseStage: ' + caseObj.Stage__c);
	               System.debug('INTEGRATION TESTING******************************OLD CaseStage: ' + oldCaseObj.Stage__c);
	               
	               //Stage__c value has changed validation check.  	
        	       if (oldCaseObj.Stage__c == caseObj.Stage__c)
        	       {
        	       	   includeCaseStage = false;	     		      	    
        	       }        	
           	   }
           	   
           	   //Instaniate BRPSGCUploadGroupQuoteData class
           	   System.debug('INTEGRATION TESTING******************************includeAppnStatus: ' + includeAppnStatus);
	           System.debug('INTEGRATION TESTING******************************includeCaseStage: ' + includeCaseStage);
           	          	               	               	               	        	      	         	         		     		      	
               BRPSGCUploadGroupQuoteData bRPSGCUploadGroupQuoteData = new BRPSGCUploadGroupQuoteData();
        	   bRPSGCUploadGroupQuoteData.sendDataToSGC(caseObj.Id, includeAppnStatus, includeCaseStage, null);        	                	           	           	               	               	       
        	}
        }
    }
}