/*************************************************************
Trigger Name : EmailMessageBeforeInsert 
Date Created : 25-Aug-2015
Created By   : Nagarjuna Kaipu
Description  : Broker Services: This trigger is used to capture the Email to Case Email Routing Address to run the assignment rules based on the routing address.
Change History: 10-JUN-2016 Rayson Landeta 
                Add the update of status
                Collect all the email address for assignment rule.
*****************************************************************/
trigger EmailMessageBeforeInsert on EmailMessage (before insert) {

   
    

    List<Case> casesToUpdate = new List<Case>();
    List<Id> cIds= new List<Id>();
    
    //Set DML Options to run assignment rules
    AssignmentRule ar = [SELECT Id FROM AssignmentRule WHERE SObjectType = 'Case' AND Active = true LIMIT 1];
    Database.DMLOptions dmlOpts = new Database.DMLOptions();
    
    dmlOpts.EmailHeader.triggerAutoResponseEmail = true;

    //Get parent cases of incoming emails
    for(EmailMessage e : Trigger.new) {
        if(e.Incoming) {
            cIds.add(e.ParentId);
        }
    }
    
    //Map parent cases
    Map<Id,Case> lCases  =  new Map<Id,Case>([SELECT Id, BR_Tech_Case_RoutingAddress__c, Status, Update_Status_from_Closed_to_Reopened__c, Case_Record_Type_before_Closing__c, BR_Department__c, Origin, Case_Origin_Custom__c FROM CASE WHERE Id IN :cIds]);
    
    if(!lCases.isEmpty()){
        for(EmailMessage e : Trigger.new){
            if(e.Incoming && lCases.containsKey(e.ParentId)){
            
                //Get parent case from the map
                Case c = lCases.get(e.ParentId);
                
                
          if(c.BR_Tech_Case_RoutingAddress__c == NULL){
                
                    //To address
                  c.BR_Tech_Case_RoutingAddress__c = e.ToAddress;
                    
                    //Consolidated email addresses
                    if(c.Case_Origin_Custom__c == 'Email'){
                        String emails_string = '';
                        if (e.ToAddress != null){
                            emails_string += e.ToAddress;
                        }
                        if (e.CcAddress != null){
                            emails_string += e.CcAddress;
                        }
                        if (e.BccAddress != null){
                            emails_string += e.BccAddress;
                        }
                       c.BR_Tech_Case_RoutingEmailAddress__c = emails_string;
                    }
            }
                
                //For closed Case with new email, reopen and run assignment rules
                if(c.Status == 'Closed'){
                    
                    //Change status
                    c.Status = 'Closed - Reopen';
                    
                    //Re-run assignment rules
                    dmlOpts.AssignmentRuleHeader.AssignmentRuleId = ar.Id;
                    c.setOptions(dmlOpts);
                }
                    //temp to test auto reply
                    c.setOptions(dmlOpts);
                casesToUpdate.add(c);
                
            }//end-if
        }//end-for
    }//end-if
    
    if(!casesToUpdate.isEmpty()){
        // to bypass validation in CaseBeforeUpdate trigger
        CaseHandler.isEmailMessageInsert = true;
        
        // update Cases
        update casesToUpdate;        
    }
}