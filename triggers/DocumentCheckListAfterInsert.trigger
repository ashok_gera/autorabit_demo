/***************************************************************************************************
Trigger Name : DocumentCheckListAfterInsert
Date Created : 15-June-2017
Created By   : IDC Offshore
Description  : This trigger is used to update Quote Proposal Document Check list 
			   record with File_Name__c, File_Size__c, File_URL__c, Tech_Content_Document_Id__c 
Change History: 
*************************************************************************************************/
trigger DocumentCheckListAfterInsert on Application_Document_Checklist__c (after insert) {
    DocumentCheckListTriggerHandler.afterInsert(Trigger.NewMap.KeySet());
}