/*************************************************************
Trigger Name : CaseAfterInsert
Date Created : 05-Aug-2015
Created By   : Nagarjuna Kaipu
Description  : Used to create CTI task on case
**************************************************************/
trigger CaseAfterInsert on Case (after insert) {

    try
    {      
       if(BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseAfterInsert == true)
       {
           List<Id> cIds= new List<Id>();
           for(Case c: Trigger.New)
           {
              if (c.BR_Category__c != null) {
                cIds.add(c.Id);
                }
           }
           BR_AP02_CreateCTIActivityOnCase clsTask = new BR_AP02_CreateCTIActivityOnCase();
           if(cIds.size() > 0)
           clsTask.CreateCTITaskOnCase(cIds); 
           BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseAfterInsert = false;
       }
    }
    catch(Exception e)
    {
       System.Debug(e.getMessage());
    }
}