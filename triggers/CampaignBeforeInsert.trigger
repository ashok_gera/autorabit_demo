/*************************************************************
Trigger Name : CampaignBeforeInsert 
Date Created : 05-Aug-2015
Created By   : Bhaskar Bellapu
Description  : 
**************************************************************/
trigger CampaignBeforeInsert on Campaign (Before insert) {
     //**************************TELESALES CODING START****************************************//
         Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
         if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
         }
     //**************************TELESALES CODING END****************************************//
}