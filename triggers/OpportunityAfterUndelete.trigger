trigger OpportunityAfterUndelete on Opportunity (after undelete) {
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onAfterUnDelete( Trigger.oldMap );
     }
    //**************************ANTHEMOPPS CODING END****************************************//
}