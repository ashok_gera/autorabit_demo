/***************************************************************************************************************
* Trigger Name : InsertAccountETINtoAppl 
* Created By   : Srikanth Thummeti
* Created Date : 1/31/2018
* Description  : This trigger is used to autopopulate ETIN Value on Application Object from Related Account
*****************************************************************************************************************/


trigger InsertAccountETINtoAppl on vlocity_ins__Application__c (before insert, before update) {

        set<id> accntids = new set<id>();
        
        for(vlocity_ins__Application__c acid: trigger.new){
        
            if(acid.Broker_Agent_Name_1__c != null){
        
                accntids.add(acid.Broker_Agent_Name_1__c);
                
            }
            
            if(acid.General_Agency_Name1__c != null){
            
                accntids.add(acid.General_Agency_Name1__c);
            
            }
            
            if(acid.Paid_Agency_Name1__c != null){
            
                accntids.add(acid.Paid_Agency_Name1__c);
                
            }
        
        }
 
        Map<ID, Account> accmap = new Map<ID, Account>([SELECT Id, Name, BR_Encrypted_TIN__c FROM Account where id in : accntids]); 
        
        for(vlocity_ins__Application__c ap: trigger.new){
        
         if(ap.Broker_Agent_Name_1__c != null && accmap.get(ap.Broker_Agent_Name_1__c).BR_Encrypted_TIN__c != null){
         
             ap.WritingAgentETIN__c = accmap.get(ap.Broker_Agent_Name_1__c).BR_Encrypted_TIN__c ;
             
            }else{
            
            ap.WritingAgentETIN__c = '';
            
             }
            
            if(ap.General_Agency_Name1__c != null && accmap.get(ap.General_Agency_Name1__c).BR_Encrypted_TIN__c != null){
            
                ap.Parent_Agency_ETIN__c = accmap.get(ap.General_Agency_Name1__c).BR_Encrypted_TIN__c;
            
            }else{
            
                 ap.Parent_Agency_ETIN__c = '';
            
            }
            
            if(ap.Paid_Agency_Name1__c != null && accmap.get(ap.Paid_Agency_Name1__c).BR_Encrypted_TIN__c != null){
            
                ap.Paid_Agency_ETIN__c = accmap.get(ap.Paid_Agency_Name1__c).BR_Encrypted_TIN__c ; 
            
            }else{
            
                 ap.Paid_Agency_ETIN__c = '';
            
            }
        
        }
        
}