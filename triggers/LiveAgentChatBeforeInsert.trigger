trigger LiveAgentChatBeforeInsert on Live_Agent_Chat_Survey__c (Before Insert) {
    if(AP20_AttachSurveyToChatScript.isBeforeIsertFirstRun == true){
        AP20_AttachSurveyToChatScript attachSurvey = new AP20_AttachSurveyToChatScript();
        attachSurvey.attachSurveyTranscript(Trigger.New);
    }
}