/*************************************************************
Trigger Name : CaseCommentAfterUpdate 
Date Created : 05-Aug-2015
Created By   : Nagarjuna Kaipu
Description  : Used to create CTI task on case
**************************************************************/
trigger CaseCommentAfterUpdate on CaseComment (after update) {
    CaseComment caseCommentObj = trigger.New[0];
    Case caseObj = [select id, Tech_Businesstrack__c from case where id =: caseCommentObj.parentid LIMIT 1];
    if(System.Label.BR_BusinessTrack.equalsIgnoreCase(caseObj.Tech_BusinessTrack__c)){
        try
        {
            if(BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseCommentAfterUpdate == true)
            {
                List<Id> cIds= new List<Id>();
                for(CaseComment cc: Trigger.New)
                {
                    cIds.add(cc.ParentId);
                }
                BR_AP02_CreateCTIActivityOnCase clsTask = new BR_AP02_CreateCTIActivityOnCase();
                clsTask.CreateCTITaskOnCase(cIds); 
                BR_AP02_CreateCTIActivityOnCase.isFirstRunCaseCommentAfterUpdate = false;
            }
        }
        catch(Exception e)
        {
            System.Debug(e.getMessage());
        }    
    }
}