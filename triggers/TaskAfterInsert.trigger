/*************************************************************
Trigger Name : TaskAfterInsert 
Date Created : 26-May-2015
Created By   : Bhaskar/Shivakanth
Description  :1. This trigger is used to update Outbound Call Count on Lead and used to update Customer Stage and Reason
                when the outbound call count reached to 10.
              2.This trigger is used to populate "Tech_Task_time__c" field on Account and Lead with the ActivityDate.  
*****************************************************************/
trigger TaskAfterInsert on Task (after insert) {
  
    //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
    CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
    if(!userSettings.Bypass_Validation__c) {  //E2C exclude --- START
    
          //**************************TELESALES CODING START****************************************//
            if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            try
            {
                List<Task> taskList = new List<Task>();

              // Begin - Daryl - WDE changes - Store the incoming activty Id on the User record
                String currActId = trigger.new[0].Id;
                User usr = [select Id,  Current_Activity_Id__c from user where Id =:UserInfo.getUserId()];
                usr.Current_Activity_Id__c = currActId;
                update usr;
              // End - Daryl - WDE changes - Store the incoming activty Id on the User record
                        
                if(AP04_LeadStageReasonUpdate.isFirstRun==true){
                    AP04_LeadStageReasonUpdate.updateStageReason(Trigger.New);
                    AP04_LeadStageReasonUpdate.isFirstRun = false;
                }
                
                AP05_ActivityTimeInteraction aa=new AP05_ActivityTimeInteraction(Trigger.New);      
                
                if(AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun==true){       
                    aa.populateNotstartedInProgressAcitivity();           
                    AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun= false;   
                
                }
                if(AP05_ActivityTimeInteraction.islastactivityFirstRun==true ){          
                    aa.populateTimeSinceLastActivity();           
                    AP05_ActivityTimeInteraction.islastactivityFirstRun = false;
                
                }  
                if(AP05_ActivityTimeInteraction.isInteractedFirstRun==true ){          
                    aa.populateAgentInteraction();           
                    AP05_ActivityTimeInteraction.isInteractedFirstRun= false;
                
                }   
                AP06_UpdateNextFollowUpDaysOnAccount clsPopulate = new AP06_UpdateNextFollowUpDaysOnAccount();
                if(AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterInsert == true)
                {
                    clsPopulate.PopulateNExtFollowUpDays(Trigger.New);
                    AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterInsert = false;
                }
                if(AP17_PopulateMrktngIDONLeadORAccount.isTaskAfterInsertFirstRun == true){ 
                            AP17_PopulateMrktngIDONLeadORAccount pmrktngId = new AP17_PopulateMrktngIDONLeadORAccount();
                            pmrktngId.populateMarketingIDONLeadORAccount(Trigger.New);
                            AP17_PopulateMrktngIDONLeadORAccount.isTaskAfterInsertFirstRun = false;
                }
                if(AP22_GenerateTaskXml.isXmlGeneratorAfterInsert == true){
                    id sendCommrecordtypeId =[select Id,Name from Recordtype where Name = 'Send Fulfillment' and SobjectType = 'Task'].Id;
                    Task t = Trigger.New[0];
                    if(t.recordTypeid == sendCommrecordtypeId && t.whatId!=NULL){
                        String uId = UserInfo.getUserId();
                        AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                        generateXml.insertXmlFields(t,uId,System.Label.isInsert);
                    }else if(t.recordTypeid == sendCommrecordtypeId && t.whoId!=NULL){
                        String uId = UserInfo.getUserId();
                        AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                        generateXml.insertLeadXmlFields(t,uId,System.Label.isInsert);
                    }
                    AP22_GenerateTaskXml.isXmlGeneratorAfterInsert = false;
                }
            }catch(Exception e){  
                system.debug('### exception ###'+e);
                     for(Task t : Trigger.New){
                        t.addError(System.Label.ContactSystemAdmin);
                    }
            }  
            }
             //**************************TELESALES CODING END****************************************//
    
    } //E2C Exclude --- END
}