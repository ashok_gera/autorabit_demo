/*************************************************************
Trigger Name : LeadAfterUpdate 
Date Created : 29-Jun-2015
Created By   : Bhaskar
Description  : 1. This trigger is used to update task owner based on the Lead owner change
Change History: 
*****************************************************************/
trigger LeadAfterUpdate on Lead (after update) {
    //**************************TELESALES CODING START****************************************//
    if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue) || 'ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue))
    {
        List<Lead> leadList = new List<Lead>();
        
    // Begin - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Lead is updated
        system.debug('currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
        system.debug('trigger Util04_BusinessTrack.businessTrackValue = '+Util04_BusinessTrack.businessTrackValue);
        system.debug('isEntityFirstUpdate  = '+AssociateActivityToLeadAndAccount.isEntityFirstUpdate);
        AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
        try
        {
          for (Lead updLead : Trigger.new)  
            {
                system.debug('updLead.Inbound_Activity_Id__c = '+updLead.Inbound_Activity_Id__c);
//                if (updLead.Inbound_Activity_Id__c == null && AssociateActivityToLeadAndAccount.currObjId == null)
                if (AssociateActivityToLeadAndAccount.isEntityFirstUpdate == true)                
                {
                    AssociateActivityToLeadAndAccount.isEntityFirstUpdate = false;
                    system.debug('after associating activity -  setting isEntityFirstUpdate  to false');  
                    AssociateActivityToLeadAndAccount.currObjId = updLead.Id;
//                    AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
                    system.debug('updated lead id = '+updLead.Id);  
                    assocAct.AssociateActivity('Lead', updLead.Id);
                }
            }
        }
        catch(Exception e)
        {
               System.Debug(e.getMessage());
        }
  
       // End - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Lead is updated
                     
        if(AP13_UpdateAcitivityOwner.isLeadAfterUpdate == true){
                    for(Lead aa : Trigger.New){
                        if(aa.OwnerId != trigger.oldMap.get(aa.Id).OwnerId){
                                leadList.add(aa);    
                            }
                    }
                    AP13_UpdateAcitivityOwner activityOwner = new AP13_UpdateAcitivityOwner();
                    if(!leadList.isEmpty()){
                        activityOwner.changeLeadAcitivityOwner(leadList);
                    }
                    AP13_UpdateAcitivityOwner.isLeadAfterUpdate = false;
         }
    }
    //**************************TELESALES CODING END****************************************//
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        LeadTriggerDispatcher.onAfterUpdate( Trigger.newMap, Trigger.oldMap );
    }
    //**************************ANTHEMOPPS CODING END****************************************//
    
    //*************************Copy Call ID and SF ID from newly created Lead Records to Call ID Keys****//
    
    List <Call_ID_Keys__c> callids = new List <Call_ID_Keys__c>();
    List <Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE id=:userInfo.getProfileId() LIMIT 1];
    String p = PROFILE[0].Name;
    
    for (Lead l : Trigger.new) {
    
    // check to see if Call ID is null and the Profile is Representative
    if (l.Call_ID__c != null && p == 'Representative') {
    
    Call_ID_Keys__c keys = new Call_ID_Keys__c ();
    
    keys.Call_ID__c = l.Call_ID__c;
    keys.SFID__c = l.Lead_ID__c;
    
    callids.add(keys);
    
    
    } //end if
    
    } //end l
    
    try {
    insert callids;
    } catch (system.Dmlexception e) {
    system.debug (e);
    }
    //*******************************CALL ID convert END********************************//
}