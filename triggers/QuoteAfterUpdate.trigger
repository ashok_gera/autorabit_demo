/*****************************************************************************************
Trigger Name  : QuoteAfterUpdate
Created Date  : 8-Aug-2017
Created By    : IDC Offshore
Description   : Trigger is used to update the quote created/modified date on Account.
Change History:
******************************************************************************************/
trigger QuoteAfterUpdate on Quote (after Update) {
    QuoteTriggerHandler.onAfterUpdate(Trigger.New);
}