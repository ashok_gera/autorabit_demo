/*************************************************************
Trigger Name : TaskBeforeUpdate
Date Created : 10-June-2015
Created By   : Bhaskar
Description  : This trigger is used to update the Markeing Campaign on task
*****************************************************************/
trigger TaskBeforeUpdate on Task (before update) {

    //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
    CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
    if(!userSettings.Bypass_Validation__c) {

     //**************************TELESALES CODING START****************************************//
     Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
     if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            try{
            
               AP21_PopulateDueDate duedate=new AP21_PopulateDueDate();
                
                if(AP21_PopulateDueDate.isBeforeUpdateFirstRun==true)
                {
                   duedate.copyDueDate(Trigger.New);
                   AP21_PopulateDueDate.isBeforeUpdateFirstRun=false;
                }
                
                List<Task> taskList = new List<Task>();
                if(AP07_PopulateKitIDOnTask.isBeforeUpdateFirstRun ==true){
                    for(Task aa : Trigger.New){
                        if(aa.communication_type__c != trigger.oldMap.get(aa.Id).communication_type__c || 
                            aa.language__c!= trigger.oldMap.get(aa.Id).language__c || 
                            aa.Email_Address__c != trigger.oldMap.get(aa.Id).Email_Address__c){
                                taskList.add(aa);
                            }
                    }
                        if(!taskList.isEmpty()){
                            AP07_PopulateKitIDOnTask.populateKitIds(taskList,System.Label.isUpdate);
                        }
                        AP07_PopulateKitIDOnTask.isBeforeUpdateFirstRun = false;
                }
                
                //Recursion protection
                if(AP09_PopulateMarketingCampaign.isTaskUpdateFirstRun== true){
                    AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
                    for(Task a : Trigger.New){
                            if(a.Marketing_Id__c != trigger.oldMap.get(a.Id).Marketing_Id__c ){
                                taskList.add(a);    
                            }
                    }
                    if(!taskList.isEmpty()){
                        populateMC.populateTaskMarketingCampaign(taskList);
                    }
                    AP09_PopulateMarketingCampaign.isTaskUpdateFirstRun= false;
                }
                AP10_ValidateSuppressOnTask validateSuppress = new AP10_ValidateSuppressOnTask();
                 if(AP10_ValidateSuppressOnTask.isBeforeUpdateFirstRun == true){
                     validateSuppress.checkSuppress(Trigger.New); 
                     AP10_ValidateSuppressOnTask.isBeforeUpdateFirstRun =false;
                }
                if(AP18_RestrictResidenceStateChange.isTaskBeforeUpdateFirstRun == true){
                     User u = [select id,profile.name from user where id =:UserInfo.getUserId()];
                     if(u.Profile.Name==System.Label.LicensedAgent || u.Profile.Name==System.Label.Representative){
                         for(Task t : Trigger.New){
                             if(t.residence_state__c != trigger.oldMap.get(t.Id).residence_state__c){
                                 taskList.add(t);
                             }
                         }
                         AP18_RestrictResidenceStateChange rrsObj = new AP18_RestrictResidenceStateChange();
                         if(!taskList.isEmpty()){
                             rrsObj.checkTaskRStateChanged(taskList);
                         }
                     }
                     AP18_RestrictResidenceStateChange.isTaskBeforeUpdateFirstRun = false;
                 }
                 if(AP14_RestrictPhoneNumber.isBeforeUpdateFirstRun ==true){
                     for(Task aaa : Trigger.New){
                        if(aaa.Phone_Number__c != trigger.oldMap.get(aaa.Id).Phone_Number__c ){
                                taskList.add(aaa);
                            }
                    }
                     AP14_RestrictPhoneNumber restrictPhone = new AP14_RestrictPhoneNumber();
                     if(!taskList.isEmpty()){
                         restrictPhone.checkPhoneNumber(taskList);
                     }
                     AP14_RestrictPhoneNumber.isBeforeUpdateFirstRun = false;
                 }
            }catch(CustomException e){
              // for(Task t: Trigger.new){
               //     t.addError(e.getMessage());
               // }
                System.debug('### Exception in TaskBeforeUpdate ###'+e);
            }catch(Exception e){
                System.debug('### Exception in Task Before Update ###'+e);
            }
      }
      //**************************TELESALES CODING END****************************************//
      //**************************BROKER CODING START****************************************//
        if('BROKER'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            try{
                List<Task> brTaskList = new List<Task>();
                List<Task> comTask = new List<Task>();
                BR_AP03_CheckOwnerAndUpdateOnTask upTask = new BR_AP03_CheckOwnerAndUpdateOnTask();
                if(BR_AP03_CheckOwnerAndUpdateOnTask.isBrokerTaskBeforeUpdateFirstRun == true)
                {
                    User userObj = [select id, UserRole.name from user where id =:UserInfo.getUserId()];
                    for(Task t: Trigger.New)
                    {
                     if(t.OwnerId  != Trigger.oldMap.get(t.Id).OwnerId && (userObj.UserRole.Name == System.Label.BrokerRepresentative || userObj.UserRole.Name == System.Label.BrokerTempUser))
                     {
                         t.addError('User has no permission to Change Owner.');
                     }
                        if(t.Status == 'Completed')
                        {
                            brTaskList.Add(t); 
                        }
                    }
                    
                    if(!brTaskList.isEmpty())
                    {
                        upTask.UpdateCompletedDateOnTask(brTaskList);
                    }
                    
                BR_AP03_CheckOwnerAndUpdateOnTask.isBrokerTaskBeforeUpdateFirstRun = false;
                }
            }catch(CustomException e){
                for(Task t: Trigger.new){
                    t.addError(e.getMessage());
                }
                System.debug('### Exception in TaskBeforeUpdate ###'+e);
            }catch(Exception e){
                System.debug('### Exception in Task Before Update ###'+e);
            }
        }
      //**************************BROKER CODING END****************************************//

    }
}