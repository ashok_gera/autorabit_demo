trigger ApplEffectiveDateOnCase on Case (before insert) {

    set<id> appid = new set<id>();
    
    for(case ca: trigger.new){
    
        appid.add(ca.Application_Name__c);
    
    }
    
    if(Trigger.isbefore){
    
        if(trigger.isInsert){

            if(appid.size()>0){
            
                map<id,vlocity_ins__Application__c> applist = new map<id,vlocity_ins__Application__c>([select id, name, Group_Coverage_Date__c from vlocity_ins__Application__c where id in: appid ]);                
                
                for(case cc: trigger.new){
                
                    if(cc.Application_Name__c != null && applist.get(cc.Application_Name__c).Group_Coverage_Date__c  != null){
                    
                        cc.Effective_Date__c = applist.get(cc.Application_Name__c).Group_Coverage_Date__c;
                    
                    }else{
                    
                         cc.Effective_Date__c = null;
                    
                    }
                
                } 
                       
            }
            
        }
    
    }

}