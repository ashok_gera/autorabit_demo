/*****************************************************************
Trigger Name : HouseholdMemberAfterInsert
Date Created : 19-May-2015
Created By   : Santosh/Amanjot
Modified By  : 
Description  :  This Trigger will restrict Insert/update of more than 1 spouses or domestic partners
Trigger Event : Before Insert
*********************************************************************/
trigger HouseholdMemberAfterInsert on Household_Member__c (after Insert) {
        //**************************TELESALES CODING START****************************************//
            AP03_RestrictMultipleSpouseDP hhmInstance1=new AP03_RestrictMultipleSpouseDP();
    
            //Recursion Protection 
        
            if(AP03_RestrictMultipleSpouseDP.isFirstRunHHMbeforeInsert==true){
                /* Call the class method to perform the actual restriction logic */
                hhmInstance1.restrictMultipleSpouceDP(Trigger.New);
                AP03_RestrictMultipleSpouseDP.isFirstRunHHMbeforeInsert= false;
             }
      
      //**************************TELESALES CODING END****************************************//
}