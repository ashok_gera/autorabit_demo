/***************************************************************************************************
Trigger Name : DocumentCheckListAfterUpdate
Date Created : 8-Aug-2017
Created By   : IDC Offshore
Description  : This trigger is update reupload time stamp on related case record. 
Change History: 
*************************************************************************************************/
trigger DocumentCheckListAfterUpdate on Application_Document_Checklist__c (after update) {
    DocumentCheckListTriggerHandler.afterUpdate(Trigger.NewMap,Trigger.OldMap);
}