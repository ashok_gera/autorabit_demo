/*************************************************************
Trigger Name : CampaignBeforeUpdate
Date Created : 05-Aug-2015
Created By   : Bhaskar Bellapu
Description  : 
**************************************************************/
trigger CampaignBeforeUpdate on Campaign (Before update) {
     //**************************TELESALES CODING START****************************************//
         Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
         if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
         }
     //**************************TELESALES CODING END****************************************//
}