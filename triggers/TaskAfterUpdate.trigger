/**********************************************************************
trigger Name   : TaskAfterUpdate 
Date Created : 27-May-2015
Created By   : Nagarjuna/SHIVAKANTH
Description  : 1.Used to populate next follow up days on account
               2.This trigger is used to populate "Tech_Task_time__c" field on Account and Lead with the ActivityDate. 
**********************************************************************/
trigger TaskAfterUpdate on Task (After Update) {

  //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
  CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
  if(!userSettings.Bypass_Validation__c) {  //E2C exclude --- START

    //**************************TELESALES CODING START****************************************//
      if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){    
          try{ 
               List<Task> taskList = new List<Task>();
               AP05_ActivityTimeInteraction aa=new AP05_ActivityTimeInteraction(Trigger.New);       
          
                if(AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun==true){        
                   aa.populateNotstartedInProgressAcitivity();           
                   AP05_ActivityTimeInteraction.isNotstartedInprogressFirstRun= false;   
                     
                  }
                if(AP05_ActivityTimeInteraction.islastactivityFirstRun==true ){
               
                   aa.populateTimeSinceLastActivity();           
                   AP05_ActivityTimeInteraction.islastactivityFirstRun = false;
                
                  }  
                  
                if(AP05_ActivityTimeInteraction.isInteractedFirstRun==true ){
                
                   aa.populateAgentInteraction();           
                   AP05_ActivityTimeInteraction.isInteractedFirstRun= false;
                
                  }  
                  if(AP17_PopulateMrktngIDONLeadORAccount.isTaskAfterUpdateFirstRun == true){ 
                  for(Task tsk : Trigger.New){
                      if(tsk.Marketing_ID__c != Trigger.OldMap.get(tsk.id).Marketing_ID__c || 
                      tsk.DNIS__c != Trigger.OldMap.get(tsk.id).DNIS__c){
                          taskList.add(tsk);
                      }
                  }    
                  if(!taskList.isEmpty()){
                      AP17_PopulateMrktngIDONLeadORAccount pmrktngId = new AP17_PopulateMrktngIDONLeadORAccount();
                      pmrktngId.populateMarketingIDONLeadORAccount(taskList);
                  }         
                  AP17_PopulateMrktngIDONLeadORAccount.isTaskAfterUpdateFirstRun = false;
                  }
                  if(AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterUpdate == true)
                  {
                      AP06_UpdateNextFollowUpDaysOnAccount clsPopulate = new AP06_UpdateNextFollowUpDaysOnAccount();
                      clsPopulate.PopulateNExtFollowUpDays(Trigger.New);
                      AP06_UpdateNextFollowUpDaysOnAccount.isFirstRunTaskAfterUpdate = false;
                  }
                  if(AP22_GenerateTaskXml.isXmlGeneratorAfterUpdate == true){
                      id sendCommrecordtypeId =[select Id,Name from Recordtype where Name = 'Send Fulfillment' and SobjectType = 'Task'].Id;
                      Task t = Trigger.New[0];
                      if(t.recordTypeid == sendCommrecordtypeId && Trigger.newMap.get(t.id).Source_External_ID__c == Trigger.OldMap.get(t.id).Source_External_ID__c 
                      && t.WhatId!=NULL){
                          String uId = UserInfo.getUserId();
                          AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                          generateXml.insertXmlFields(t,uId,System.Label.isUpdate);
                      }else if(t.recordTypeid == sendCommrecordtypeId && Trigger.newMap.get(t.id).Source_External_ID__c == Trigger.OldMap.get(t.id).Source_External_ID__c 
                      && t.WhoId!=NULL){
                          String uId = UserInfo.getUserId();
                          AP22_GenerateTaskXml generateXml = new AP22_GenerateTaskXml();
                          generateXml.insertLeadXmlFields(t,uId,System.Label.isUpdate);
                      }
                      AP22_GenerateTaskXml.isXmlGeneratorAfterUpdate = false;
                  }
          }catch(Exception e){
                  for(Task t : Trigger.New){
                      t.addError(System.Label.ContactSystemAdmin);
                 }
          }
      }
       //**************************TELESALES CODING END****************************************//

  } //E2C exclude --- END
}