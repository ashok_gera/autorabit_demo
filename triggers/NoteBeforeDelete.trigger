/***************************************************************************
Trigger Name   : NoteBeforeDelete
Date Created : 09/11/2017 (MM/DD/YYYY)
Created By   : IDC Offshore
Description  :1. Trigger to invoke SGA_AP44_PreventNotesDeletion if an Note is deleted for Opporurtunity
              2. Prevents deletion of Note records under Opportunity of "SG Quoting" type.
********************************************************************************/
trigger NoteBeforeDelete on Note (before delete) {
   // call handler method to prevent note deletion
    NoteTriggerHandler.onBeforeDelete(Trigger.oldMap);  
}