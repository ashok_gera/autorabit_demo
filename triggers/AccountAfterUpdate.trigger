/*************************************************************
Trigger Name : AccountAfterUpdate
Date Created : 20-April-2015
Created By   : Bhaskar/Santosh
Description  : This trigger is used to update opportunity stage on update of account customer stage.
Change History: 
*****************************************************************/
trigger AccountAfterUpdate on Account (after update) { 
        //**************************TELESALES CODING START****************************************//
        if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            List<Account> accList = new List<Account>();
                    if(AP01_AccountOppStageUpdate.isFirstRun==true)
                    {
                     /****************************Changes Made By Sunnish 05/03/2016 Starts***********************************/
                     
                       AP01_AccountOppStageUpdate.getAccountswithOpportunities(Trigger.Newmap,Trigger.Oldmap);
                       
                     /****************************Changes Made By Sunnish 05/03/2016 Starts***********************************/
                     
                       AP01_AccountOppStageUpdate.isFirstRun = false;
                    }
                    if(AP13_UpdateAcitivityOwner.isAccAfterUpdate == true){
                        for(Account aa : Trigger.New){
                            if(aa.OwnerId != trigger.oldMap.get(aa.Id).OwnerId){
                                    accList.add(aa);    
                                }
                        }
                        AP13_UpdateAcitivityOwner activityOwner = new AP13_UpdateAcitivityOwner();
                        if(!accList.isEmpty()){
                            activityOwner.changeAccountAcitivityOwner(accList);
                        }
                        AP13_UpdateAcitivityOwner.isAccAfterUpdate = false;
                    }
            
                // Begin - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Account is updated
                system.debug('Account currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
                system.debug('AssociateActivityToLeadAndAccount.isEntityFirstUpdate = '+AssociateActivityToLeadAndAccount.isEntityFirstUpdate);  
                 AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
                try
                {
                  for (Account updAcc: Trigger.new) 
                    {
                        system.debug('updAcc.Inbound_Activity_Id__c = '+updAcc.Inbound_Activity_Id__c);
//                        if (updAcc.Inbound_Activity_Id__c == null && AssociateActivityToLeadAndAccount.currObjId == null)
                          if (AssociateActivityToLeadAndAccount.isEntityFirstUpdate == true)       
                          {
                          AssociateActivityToLeadAndAccount.isEntityFirstUpdate = false;
                            AssociateActivityToLeadAndAccount.currObjId = updAcc.Id;
//                            AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
                            system.debug('updated Account id = '+updAcc.Id);  
                            assocAct.AssociateActivity('Account', updAcc.Id);
                            
                            // Begin - Daryl - WDE changes - Store the incoming activty Id on the User record
                            User usr = [select Id,  Current_Activity_Id__c from user where Id =:UserInfo.getUserId()];
                            usr.Current_Activity_Id__c = null;
                            update usr;
                            // End - Daryl - WDE changes - Store the incoming activty Id on the User record
                          }
                    }
                }
                catch(Exception e)
                {
                       System.Debug(e.getMessage());
                }
          
                // End - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Account is updated
          }
          //**************************TELESALES CODING END****************************************//
           
          if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue))
          {
                // Begin - Daryl - Telesales WDE changes - 8/1/16 - Associate Activity after Account is updated
                AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();  
                system.debug('AssociateActivityToLeadAndAccount.isEntityFirstUpdate = '+AssociateActivityToLeadAndAccount.isEntityFirstUpdate);  
                system.debug('Account currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
                try
                {
                  for (Account updAcc: Trigger.new) 
                    {
                        system.debug('updAcc.Inbound_Activity_Id__c = '+updAcc.Inbound_Activity_Id__c);
//                        if (updAcc.Inbound_Activity_Id__c == null && AssociateActivityToLeadAndAccount.currObjId == null)
                        if (AssociateActivityToLeadAndAccount.isEntityFirstUpdate == true)       
                        {
                            AssociateActivityToLeadAndAccount.isEntityFirstUpdate = false;
                            AssociateActivityToLeadAndAccount.currObjId = updAcc.Id;
//                            AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
                            system.debug('updated Account id = '+updAcc.Id);  
                            assocAct.AssociateActivity('Account', updAcc.Id);
                            
                            // Begin - Daryl - WDE changes - Store the incoming activty Id on the User record
                            User usr = [select Id,  Current_Activity_Id__c from user where Id =:UserInfo.getUserId()];
                            usr.Current_Activity_Id__c = null;
                            update usr;
                            // End - Daryl - WDE changes - Store the incoming activty Id on the User record
                        }
                    }
                }
                catch(Exception e)
                {
                       System.Debug(e.getMessage());
                }
          
                // End - Daryl - Telesales WDE changes - 8/1/16 - Associate Activity after Account is updated
          }
    
      //START*******SG QUOTING CODING **********************************//
         if(System.Label.SG_BusinessTrack.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
             
            AccountTriggerSGHandler.onAfterUpdate(Trigger.newMap,Trigger.oldMap);    
             
         }
        //********SG QUOTING CODING END****************************************//
}