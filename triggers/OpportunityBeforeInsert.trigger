/********************************************************
Trigger Name : OpportunityBeforeInsert 
Date Created : 11-June-2015
Created By   : Bhaskar
Modified By  : 
Description  :  This Trigger will restrict creation of multiple opportunities with the same stage name except
                closed won / closed lost for an Account.
Trigger Event : before Insert
********************************************************/
trigger OpportunityBeforeInsert on Opportunity(before insert) {
    for(Opportunity a : Trigger.New){
            if(a.Tech_BusinessTrack__c != NULL && a.Tech_BusinessTrack__c !=''){
                CS002_RTypeAssignOnLeadConvert__c csr = CS002_RTypeAssignOnLeadConvert__c.getValues('Opportunity'+'_'+a.Tech_Businesstrack__c);
                if(csr != NULL && csr.RecordTypeID__c != NULL && csr.RecordTypeID__c != '')
                a.RecordTypeId = csr.RecordTypeID__c;
            }
     }
     Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
    //**************************TELESALES CODING START****************************************//
     if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
     try{
            AP08_RestrictMultipleOpenOpportunities restrictOpp = new AP08_RestrictMultipleOpenOpportunities();
               //Recursion Protection 
               if(AP08_RestrictMultipleOpenOpportunities.isFirstRunOPPBeforeInsert==true){
                    /* Call the class method to perform the actual restriction logic */
                    restrictOpp.restrictMultipleOpenOpp(Trigger.New);
                     AP08_RestrictMultipleOpenOpportunities.isFirstRunOPPBeforeInsert = false;
               }
        }catch(Exception e){          
                System.debug('### Exception OpportunityBeforeInsert Trigger ###'+e);
        }
   }
   //**************************TELESALES CODING END****************************************//
   //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onBeforeInsert( Trigger.new );
    }
   //**************************ANTHEMOPPS CODING END****************************************//
   
   
   //**************************SGQUOTING CODING START****************************************//
    if('SGQUOTING'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerSGAHandler.doBeforInsert(Trigger.New);
    }
    //**************************SGQUOTING CODING END*****************************************//
}