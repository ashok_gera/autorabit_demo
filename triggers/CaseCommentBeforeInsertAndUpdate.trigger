/*
Date Created : 21-July-2015
Created By   : Nagarjuna Kaipu
Description  : This Trigger is used to prevent public checkbox.
Trigger Event : before insert & before update
*/
trigger CaseCommentBeforeInsertAndUpdate on CaseComment (before insert, before update) {
    
    CaseComment caseCommentObj = trigger.New[0];
    Case caseObj = [select id, Tech_Businesstrack__c from case where id =: caseCommentObj.parentid LIMIT 1];
    if(System.Label.BR_BusinessTrack.equalsIgnoreCase(caseObj.Tech_BusinessTrack__c)){
        for(CaseComment cc: trigger.New)
        {
            if(cc.IsPublished == true)
            {
                cc.AddError('Please uncheck \'Public\' checkbox.');
            }
        }
    }
    
/*Developer Name : Akash Naik
* Created Date  7-24-2017
* Description -  Throws validation Rule if the case comments is edited.
*/
    if(System.Label.SG_BusinessTrack.equalsIgnoreCase(caseObj.Tech_BusinessTrack__c)){
        if(Trigger.isBefore)
        {
            if(Trigger.isInsert)
            {
                //Add before Insert code 
            }
            if(Trigger.isUpdate)
            {
                SGA_AP26_CaseCommentHandler.doNotEditComment(Trigger.New);
            }
        }
    }
}