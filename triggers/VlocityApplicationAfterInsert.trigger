/******************************************************************************************************
* Trigger Name : VlocityApplicationAfterInsert
* Created By   : IDC Offshore
* Created Date : 7/26/2017
* Description  : This trigger is used to create the document check list records for Paper Application.
*******************************************************************************************************/

trigger VlocityApplicationAfterInsert on vlocity_ins__Application__c (after insert) {
        VlocityApplicationTriggerHandler handlerObj = new VlocityApplicationTriggerHandler();
        handlerObj.onAfterInsert(Trigger.NewMap);
}