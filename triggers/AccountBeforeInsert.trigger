/**************************
Date Created : 07-May-2015
Created By   : Nagarjuna
Modified By  : 
Description  : This Trigger will auto populate Customer Score on Account based on the Zip code entered by an Agent upon save.
Trigger Event : Before Update
*****************************/
trigger AccountBeforeInsert on Account (before Insert) {
        for(Account a : Trigger.New){
            if(a.Tech_BusinessTrack__c != NULL){
                CS002_RTypeAssignOnLeadConvert__c csr = CS002_RTypeAssignOnLeadConvert__c.getValues('Account'+'_'+a.Tech_Businesstrack__c);
                if(csr != NULL && csr.RecordTypeID__c != NULL && csr.RecordTypeID__c != '')
                a.RecordTypeId = csr.RecordTypeID__c;
            }
        }
        Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
        //**************************TELESALES CODING START****************************************//
         if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            List<Account> accList = New List<Account>();
            /* Call the util class method to perform the actual zip code customer score business logic */
            Util01_RetrieveZipCodeScore zipCodeScoreHelper = new Util01_RetrieveZipCodeScore();
            
            //Recursion Protection 
            if(Util01_RetrieveZipCodeScore.isFirstRunAccInsert==true)
            {
                    for(Account a : Trigger.New){
                    if(a.Customer_Score__c == NULL){
                        accList.add(a);
                    }
                }
                if(!accList.IsEmpty()){
                zipCodeScoreHelper.CustomerScoreAutoPopulate(accList, 'Tech_ZipCode__c', 'Date_of_Birth__c', 'Customer_Score__c');
                }
                Util01_RetrieveZipCodeScore.isFirstRunAccInsert = false;
            }
        /* Call the class method to perform the actual marketing campaign population business logic */
       /*     AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
            //Recursion protection
             if(AP09_PopulateMarketingCampaign.isAccInsertFirstRun == true){
                 populateMC.populateAccMarketingCampaign(trigger.new,false);
                 AP09_PopulateMarketingCampaign.isAccInsertFirstRun = false;
             }*/
        }
        //**************************TELESALES CODING END****************************************//
        //**************************ANTHEMOPPS CODING START****************************************//
        if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            AccountTriggerDispatcher.onBeforeInsert( Trigger.new );
        }
        //**************************ANTHEMOPPS CODING END****************************************//
        
        //**************************SG QUOTING CODING START****************************************//
         if(System.Label.SG_BusinessTrack.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
             // call method to populate Rating Area on insert

             AccountTriggerSGHandler.onBeforeInsert(Trigger.New);
             
         }
        //**************************SG QUOTING CODING END****************************************//
}