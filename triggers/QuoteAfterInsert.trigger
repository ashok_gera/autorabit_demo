/**
Trigger Name  : QuoteAfterInsert
Created Date  : 21-June-2017
Created By    : IDC Offshore
Description   : Inserting QuoteStatusDashboard record whenever the quote record is created
Change History:
*/
trigger QuoteAfterInsert on Quote (after insert) {
    QuoteTriggerHandler.onAfterInsert(Trigger.New);
}