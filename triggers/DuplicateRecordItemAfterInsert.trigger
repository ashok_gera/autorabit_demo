/*************************************************************
Trigger Name : DuplicateRecordItemAfterInsert 
Date Created : 22-July-2015
Created By   : Bhaskar
Description  : This trigger is used to get the duplicate records which are ignored the alert
                And update those records that the record with Allowed Duplicate check box
*****************************************************************/
trigger DuplicateRecordItemAfterInsert on DuplicateRecordItem (after insert) {
    List<DuplicateRecordItem> duplicateList = Trigger.New;
    List<ID> leadIds = new List<ID>();
    for(DuplicateRecordItem dri : duplicateList ){
        String strRecordID = dri.RecordId;
        if(strRecordID.startsWith('00Q')){
            leadIds.add(dri.RecordId);
        }
    }
    if(!leadIds.isEmpty()){
        AP15_AllowedDuplicateRecords.updateDuplicateLeadRecords(leadIds);
    }
}