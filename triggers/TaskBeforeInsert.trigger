/*************************************************************
Trigger Name : TaskBeforeInsert 
Date Created : 05-June-2015
Created By   : Bhaskar
Description  :1. This trigger is used to update the Kit Ids on task
*****************************************************************/
trigger TaskBeforeInsert on Task (Before insert) {
    Task newTask = Trigger.New[0];  
    //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
    CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
    if(!userSettings.Bypass_Validation__c) {    //E2C Exclude --- START
    
        //**************************TELESALES CODING START****************************************//
        Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
 
        if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        try{

                AP21_PopulateDueDate duedate=new AP21_PopulateDueDate();
                
                if(AP21_PopulateDueDate.isBeforeInsertFirstRun==true)
                {
                   duedate.copyDueDate(Trigger.New);
                   AP21_PopulateDueDate.isBeforeInsertFirstRun=false;
                }
               if(AP07_PopulateKitIDOnTask.isBeforeInsertFirstRun ==true){
                    AP07_PopulateKitIDOnTask.populateKitIds(Trigger.New,System.Label.isInsert);
                    AP07_PopulateKitIDOnTask.isBeforeInsertFirstRun = false;
               }
             /* Call the class method to perform the actual marketing campaign population business logic */
             AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
             //Recursion protection
             if(AP09_PopulateMarketingCampaign.isTaskInsertFirstRun == true){
                 populateMC.populateTaskMarketingCampaign(trigger.new);
                 AP09_PopulateMarketingCampaign.isTaskInsertFirstRun = false;
             }
             AP10_ValidateSuppressOnTask validateSuppress = new AP10_ValidateSuppressOnTask();
             List <Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE id=:userInfo.getProfileId() LIMIT 1];
             String p = PROFILE[0].Name;
             if(AP10_ValidateSuppressOnTask.isBeforeInsertFirstRun ==true && p != 'System Administrator'){
                 validateSuppress.checkSuppress(Trigger.New);
                 AP10_ValidateSuppressOnTask.isBeforeInsertFirstRun =false;
             }
             if(AP18_RestrictResidenceStateChange.isTaskBeforeInsertFirstRun == true){
                 User u = [select id,profile.name from user where id =:UserInfo.getUserId()];
                 if(u.Profile.Name==System.Label.LicensedAgent || u.Profile.Name==System.Label.Representative){
                     AP18_RestrictResidenceStateChange rrsObj = new AP18_RestrictResidenceStateChange();
                     rrsObj.checkTaskRStateChanged(Trigger.New);
                 }
                 AP18_RestrictResidenceStateChange.isTaskBeforeInsertFirstRun = false;
             }
             if(AP14_RestrictPhoneNumber.isBeforeInsertFirstRun == true){
                 AP14_RestrictPhoneNumber restrictPhone = new AP14_RestrictPhoneNumber();
                 restrictPhone.checkPhoneNumber(Trigger.New);
                 AP14_RestrictPhoneNumber.isBeforeInsertFirstRun = false;
             }
        }catch(Exception e){
            for(Task t: Trigger.new){
                t.addError(e.getMessage());
            }
        }
     }
     //**************************TELESALES CODING END****************************************//
     //**************************BROKER CODING START****************************************//
        if('BROKER'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            try{
                List<Task> brTaskList = new List<Task>();

                BR_AP03_CheckOwnerAndUpdateOnTask upTask = new BR_AP03_CheckOwnerAndUpdateOnTask();
                if(BR_AP03_CheckOwnerAndUpdateOnTask.isBrokerTaskBeforeInsertFirstRun == true)
                {
                
                    for(Task t: Trigger.New)
                    {
                        if(t.Status == 'Completed' || (t.subject.Contains('Email:') && t.subject.Contains('ref:')))
                        {
                            brTaskList.Add(t); 
                        }
                    }
                     
                    //if(!userSettings.Bypass_Validation__c) {
                        upTask.CheckTaskOwnerWithCase(Trigger.New);
                    //}
                   
                    if(!brTaskList.isEmpty())
                    {
                        upTask.UpdateCompletedDateOnTask(brTaskList);
                    }
                
                BR_AP03_CheckOwnerAndUpdateOnTask.isBrokerTaskBeforeInsertFirstRun = false;
                }
            }catch(CustomException e){
                for(Task t: Trigger.new){
                    t.addError(e.getMessage());
                }
                System.debug('### Exception in TaskBefore Insert ###'+e);
            }catch(Exception e){
                System.debug('### Exception in Task Before Insert ###'+e);
            }
        }
      //**************************BROKER CODING END****************************************//
      //**************************SGQUOTING CODING START****************************************//
        if('SGQUOTING'.equalsIgnoreCase(newTask.Tech_Businesstrack__c)){
             SGA_AP61_RestrictAuditTask.restrictAuditforOpenCase(Trigger.New);
        }
        //**************************SGQUOTING CODING END****************************************//
    } //E2C Exclude --- END
    
    
}