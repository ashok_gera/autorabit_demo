/*
Date Created : 23-July-2015
Created By   : Kishore Jonnadula
Description  : This Trigger will prevent Case close, if there is any open tasks under the case.
Trigger Event : before update
Change History :
1. Modified by Rayson Landeta - Added changing of record type to Case record's previous record type when Case status is changed to 'Closed-Reopen'
2. Modified by Cathy Evangelista - Added Validation rule BR_CASE_VR08_CheckOwnerShipOnSave condition formula
*/
trigger CaseBeforeUpdate on Case (before Update) {
    
        
    //**************************BROKER SERVICES CODING START****************************************//
    Case cs = Trigger.new[0];
    //SGA Implementation 
    if(System.Label.SG_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c))           
    {
        CaseTriggerHandler.onBeforeUpdate(Trigger.NewMap, Trigger.OldMap);
    }
    if(System.Label.BR_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c)){
        try
        {   
            //Changing back record typre from closed to reopen status
            Set<Id> rtIds = new Set<Id>();
            for(Case c: Trigger.New){
                
                //Retrieve previous record types of all Case records
                if(!String.isBlank(c.Case_Record_Type_before_Closing__c)){
                    rtIds.add(c.Case_Record_Type_before_Closing__c);
                }
            }
            //Get record type ID of 'BTS Case' record type
            Id btsCaseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('BTS Case').getRecordTypeId();
            //Get record type ID of 'Broker Case' record type
            Id brokerCaseId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Broker Case').getRecordTypeId();
            User usr = [SELECT Id, BypassVR__c FROM User WHERE Id = :UserInfo.getUserId() ];
            
            CS003_BrokerEmail2CaseDefaultOwner__c setting = CS003_BrokerEmail2CaseDefaultOwner__c.getInstance();
            Id defaultOwnerId = setting != null ? setting.BR_CaseDefaultOwnerId__c : null;
            
            //Retrieve all Case record types
            Map<Id, RecordType> rtMap = new Map<Id, RecordType>([SELECT DeveloperName,Id,SobjectType FROM RecordType WHERE SobjectType = 'Case' AND Id IN: rtIds]);
            
            for(Case c : Trigger.New){
                
                //Change record type back after reopening
                if(c.Status == 'Closed - Reopen' && Trigger.oldMap.get(c.Id).Status == 'Closed'){
                    
                    //To bypass validation rule BR_CASE_VR02_RestrictClosedStatusChange
                    //Will be reset back to false through workflow rule 'Uncheck Case Status Flag for Reopen Update'
                    c.Update_Status_from_Closed_to_Reopened__c = true;
                    
                    if(!String.isBlank(c.Case_Record_Type_before_Closing__c)){
                        c.RecordTypeId = rtMap.get(c.Case_Record_Type_before_Closing__c).Id;
                    } else {
                        // if Case has no previous record type, set record type based on BR_Department__c field
                        if(c.BR_Department__c == 'BTS'){
                            //Change record type to 'BTS Case'
                            c.RecordTypeId = btsCaseId;
                        } else {
                            //Change record type to 'Broker Case'
                            c.RecordTypeId = brokerCaseId;
                        }
                    }  
                }
                
                //Validation rule BR_CASE_VR08_CheckOwnerShipOnSave
                //Used to restrict to the case save without taking the ownership to rep and temp users.
                if(!CaseHandler.isEmailMessageInsert){
                    //Validation rule logic
                    if( !c.BR_Is_Spam__c && !usr.BypassVR__c 
                       && c.OwnerId != usr.Id && c.OwnerId == Trigger.oldMap.get(c.Id).OwnerId 
                       && !(Trigger.oldMap.get(c.Id).Status == 'Closed' && c.Status == 'Closed - Reopen')
                       && usr.Id != defaultOwnerId){
                           c.addError('Please take ownership and save.');
                       }
                }else{
                    //Populate email timestamp if updated from Email Message
                    c.New_Email_Timestamp__c = DateTime.now();
                }
            }   
        }catch(CustomException e){for(Case c: Trigger.new){c.addError(e.getMessage());}
        }
    }        
    //**************************BROKER SERVICES CODING END****************************************//
    
    //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
    try
    {
        CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
        if(!userSettings.Bypass_Validation__c) {  //E2C exclude --- START
            Case cs = Trigger.new[0];
            Case caseObj = [select id, Tech_Businesstrack__c, RecordType.Name from case where id =: cs.id LIMIT 1];
            if(BR_AP01_RetrieveOpenTasksOnCase.isFirstRunCaseBeforeUpdate == true &&(System.Label.BR_BusinessTrack.equalsIgnoreCase(cs.Tech_BusinessTrack__c) || caseObj.RecordType.Name == SG01_Constants.CASE_RECORDTYPENAME_ENROLL || caseObj.RecordType.Name == system.label.SG129_SCLC_RT ) )
            {
                BR_AP01_RetrieveOpenTasksOnCase openCase = new BR_AP01_RetrieveOpenTasksOnCase();
                openCase.retrieveOpenTasksOnCase(Trigger.New);
                BR_AP01_RetrieveOpenTasksOnCase.isFirstRunCaseBeforeUpdate = false;
            }
        } // E2C Exclude --- END
    }catch(CustomException e){for(Case c: Trigger.new){c.addError(e.getMessage());}
    }
  
}