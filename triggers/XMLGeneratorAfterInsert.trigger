/*************************************************************
Trigger Name : XMLGeneratorAfterInsert 
Date Created : 20-Oct-2015
Created By   : Bhaskar
Description  : 1. This trigger is used to update final SMC XML file in XML Generator object
*****************************************************************/
trigger XMLGeneratorAfterInsert on XML_Generator__c (after insert) {
    if(AP22_GenerateTaskXml.isSMCXmlGeneratorAfterInsert == true){
        AP22_GenerateTaskXml gtx = new AP22_GenerateTaskXml();
        XML_Generator__c xg = trigger.new[0];
        gtx.generateSMCXMLFile(xg.id);
        AP22_GenerateTaskXml.isSMCXmlGeneratorAfterInsert = false;
    }
}