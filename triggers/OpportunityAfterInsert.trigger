trigger OpportunityAfterInsert on Opportunity (after insert) {
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onAfterInsert( Trigger.newMap );
    }
    //**************************ANTHEMOPPS CODING END****************************************//

    //**************************SGQUOTING CODING START****************************************//
    if('SGQUOTING'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
      OpportunityTriggerSGAHandler.doAfterInsert( Trigger.newMap );
    }
    //**************************SGQUOTING CODING END*****************************************//

}