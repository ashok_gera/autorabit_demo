/*
Date Created : 16-April-2015
Created By   : Bhaskar/Santosh/Venkat
Modified By  : Nagarjuna/Bhaskar
Description  : This Trigger will auto populate the customer score on Lead based on the Zip code entered by an Agent upon save.
Trigger Event : Before Update
*/

trigger LeadBeforeUpdate on Lead (before Update) {
    //**************************TELESALES CODING START****************************************//
     Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
     if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            List<Lead> leadList = New List<Lead>();
            List<Lead> leadListCampaignIDs = New List<Lead>();
            /* Call the util class method to perform the actual zip code customer score business logic */
            
            Util01_RetrieveZipCodeScore zipCodeScoreHelper = new Util01_RetrieveZipCodeScore();
            
            //Recursion Protection 
            
            if(Util01_RetrieveZipCodeScore.isFirstRunLeadUpdate==true)
            {
                for(Lead l : trigger.New){
                    if(trigger.NewMap.get(l.Id).Customer_Score__c == trigger.oldMap.get(l.Id).Customer_Score__c && (trigger.NewMap.get(l.Id).Date_of_Birth__c != trigger.oldMap.get(l.Id).Date_of_Birth__c || trigger.NewMap.get(l.Id).Zip_Code__c != trigger.oldMap.get(l.Id).Zip_Code__c)){
                        leadList.add(l);    
                    }
                    else if(trigger.NewMap.get(l.Id).Customer_Score__c != trigger.oldMap.get(l.Id).Customer_Score__c ){
                           l.Customer_Score_Source__c = 'Agent';
                        } 
                }
                if(!leadList.IsEmpty()){
                    
                    zipCodeScoreHelper.CustomerScoreAutoPopulate(leadList, 'Tech_ZipCode__c', 'Date_of_Birth__c', 'Customer_Score__c');
                }
              Util01_RetrieveZipCodeScore.isFirstRunLeadUpdate = false;
           } 
             if(AP19_PopulateTechStreetValue.isLeadUpdateFirstRun == true)
            {
                AP19_PopulateTechStreetValue pts = new AP19_PopulateTechStreetValue();
                for(Lead a :Trigger.New)
                  {
                      if(a.street != Trigger.OldMap.get(a.id).street){
                            leadList.add(a);
                       }
                  }
                  pts.populateTechStreetVal(leadList);
                  AP19_PopulateTechStreetValue.isLeadUpdateFirstRun=false;
            }
         /* if(AP09_PopulateMarketingCampaign.isLeadUpdateFirstRun == true){
                /* Call the class method to perform the actual marketing campaign population business logic */
             /*   AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
                for(Lead a : Trigger.New){
                        if(a.Marketing_Event__c != trigger.oldMap.get(a.Id).Marketing_Event__c){
                            leadListCampaignIDs.add(a);    
                        }
                }*/
                //Recursion protection
             /*   if(!leadListCampaignIDs.isEmpty()){
                    populateMC.populateLeadMarketingCampaign(leadListCampaignIDs,true);
                }
                AP09_PopulateMarketingCampaign.isLeadUpdateFirstRun = false;
        }*/
             try{
                
                if(AP12_RestrictOwnerChangeStateBase.leadFirstRun == true){
                    AP12_RestrictOwnerChangeStateBase restrictlead = new AP12_RestrictOwnerChangeStateBase();
                    for(Lead a : Trigger.New){
                        if(a.OwnerId != trigger.oldMap.get(a.Id).OwnerId){
                            leadList.add(a);    
                        }
                    }
                    if(!leadList.isEmpty()){
                        restrictlead.restrictLeadOwnerChange(leadList);
                    }
                    AP12_RestrictOwnerChangeStateBase.leadFirstRun=false;
                }
            }catch(CustomException e){
                
            }
     }
     //**************************TELESALES CODING END****************************************//
     //**************************ANTHEMOPPS CODING START****************************************//
     if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
         LeadTriggerDispatcher.onBeforeUpdate( Trigger.newMap, Trigger.oldMap );
     }
     //**************************ANTHEMOPPS CODING END****************************************//
}