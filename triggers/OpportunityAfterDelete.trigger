trigger OpportunityAfterDelete on Opportunity (after delete) {
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        OpportunityTriggerDispatcher.onAfterDelete( Trigger.oldMap );
    }
     //**************************ANTHEMOPPS CODING END****************************************//
}