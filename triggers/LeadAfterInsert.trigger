trigger LeadAfterInsert on Lead (after insert) 
{
    //**************************ANTHEMOPPS CODING START****************************************//
    if('ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
        LeadTriggerDispatcher.onAfterInsert( Trigger.newMap );
    }
    //**************************ANTHEMOPPS CODING END****************************************//
    
    // Begin - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Lead is inserted
    
    if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue) || 'ANTHEMOPPS'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue))
    {
        try
        {
            system.debug('currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
            system.debug('isEntityFirstUpdate  = '+AssociateActivityToLeadAndAccount.isEntityFirstUpdate);
            AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();   
        
            for (Lead newLead : Trigger.new)  
            {
//                if(newLead.Inbound_Activity_Id__c != NULL && AssociateActivityToLeadAndAccount.currObjId == null)
                if (AssociateActivityToLeadAndAccount.isEntityFirstUpdate == true)            
                {
                    AssociateActivityToLeadAndAccount.isEntityFirstUpdate = false;    
                    system.debug('after associating activity -  setting isEntityFirstUpdate  to false');  
                    AssociateActivityToLeadAndAccount.currObjId = newLead.Id;
//                    AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();
                    assocAct.AssociateActivity('Lead', newLead.Id);
                    
                    // Begin - Daryl - WDE changes - Store the incoming activty Id on the User record
                    User usr = [select Id,  Current_Activity_Id__c from user where Id =:UserInfo.getUserId()];
                    usr.Current_Activity_Id__c = null;
                    update usr;
                  // End - Daryl - WDE changes - Store the incoming activty Id on the User record
              
                }
            }
        }
        catch(Exception e)
        {
           System.Debug(e.getMessage());
        }
    }
    // End - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Lead is inserted
}