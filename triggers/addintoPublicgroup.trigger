trigger addintoPublicgroup on User (after insert,after update,before delete) {
    Map<Id,User> usrMap = new  Map<Id,User> ();
    Map<Id,Id> newUsrProfiles = new Map<Id,Id>();
    for(User usr:Trigger.New){
        if(usr.ProfileId != null){
            usrMap.put(usr.Id,usr);
        }
    }
    if( Trigger.isUpdate){
          for(User usrold:Trigger.oldMap.values()){
            if(usrold.ProfileId != null){
              if(usrold.ProfileId != Trigger.newMap.get(usrold.Id).ProfileId){
                   usrMap.put(usrold.Id,Trigger.newMap.get(usrold.Id));
                   newUsrProfiles.put(usrold.Id,Trigger.newMap.get(usrold.Id).ProfileId);
              }
        }
    }
    }
   SGA_AP66_AddUsersToPublicGroups.AddToGroups(trigger.newMap.keySet());
   
    
    SGA_AP66_AddUsersToPublicGroups.DeteleFromGroups(trigger.oldMap.keySet());
    

}