/******************************************************************************************************
* Trigger Name : VlocityApplicationBeforeInsert
* Created By   : IDC Offshore
* Created Date : 8/25/2017
* Description  : This trigger is used to update the party id on Application if it is empty
*******************************************************************************************************/
trigger VlocityApplicationBeforeInsert on vlocity_ins__Application__c (before insert,before update) {
    
    
    if(Trigger.isInsert && Trigger.isBefore){
        VlocityApplicationTriggerHandler.onBeforeInsert(Trigger.New);
        VlocityApplicationTriggerHandler.ETIN_Recordtype_isinsert(Trigger.new);
    }
    
    if(Trigger.isUpdate && Trigger.isBefore){    
        VlocityApplicationTriggerHandler.ETIN_Recordtype_isinsert(Trigger.new);    
    }
}