/*************************************************************
Trigger Name  : ContactBeforeInsertUpdate
Date Created  : 15-Sep-2016
Created By    : Rakesh Muppiri
Description   : Used to update all contact records Agency type field value with it associated Account.AgencyType
Change History:
**************************************************************
Note:   If trigger events are seperated, please make sure that below functionality should be there in 
        both before insert & update Triggers
*************************************************************/
trigger ContactBeforeInsertUpdate on Contact (before Insert, before update) {
    
    Set<Id> accIdsList = new Set<Id>();
    Map<Id, Account> accountIdMap;
    
    for(Contact con : Trigger.new){
        accIdsList.add(con.AccountId);
    }
    if(!accIdsList.isEmpty())
        accountIdMap = new Map<Id, Account>([SELECT Id, AgencyType__c FROM Account WHERE Id IN : accIdsList]);
    
    //Updating Agency type field value with associated Account's AgencyType value
    for(Contact con : Trigger.new)
        if(accountIdMap.get(con.AccountId) != NULL)
            con.AgencyType__c = accountIdMap.get(con.AccountId).AgencyType__c;   
}