trigger UserTrigger on User (before insert, before update) {

	if(trigger.isBefore){
		
		if(trigger.isInsert){
			UserTriggerHandler.beforeInsert(trigger.new);
		}else if(trigger.isUpdate){
			UserTriggerHandler.beforeUpdate(trigger.new, trigger.old);
		}
		
	}

}