/* 
Date Created : 07-May-2015
Created By   : Nagarjuna
Modified By  : Bhaskar
Description  : This Trigger will auto populate Customer Score on Account based on the Zip code entered by an Agent upon save.
Trigger Event : Before Update
Modified Components: Added AP12_RestrictOwnerChangeStateBase class related stuff
*/
trigger AccountBeforeUpdate on Account (before Update) {
        //**************************TELESALES CODING START****************************************//
         for(Account a : trigger.New){
             System.debug ('::Old isPartner ::' +trigger.OldMap.get(a.Id).IsPartner );
             System.debug ('::New isPartner ::' +trigger.NewMap.get(a.Id).IsPartner ); 
         }
         Util04_BusinessTrack.retrieveBusinessTrackValue(Trigger.New);
         if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
            List<Account> accList = New List<Account>();
            /* Call the util class method to perform the actual zip code customer score business logic */
            Util01_RetrieveZipCodeScore zipCodeScoreHelper = new Util01_RetrieveZipCodeScore();
            
            //Recursion Protection 
            if(Util01_RetrieveZipCodeScore.isFirstRunAccUpdate==true)
            {
             for(Account a : trigger.New){
                 
                 System.debug ('NewMap Customer Score =' +trigger.NewMap.get(a.Id).Customer_Score__c );
                 System.debug ('OldMap Customer Score =' +trigger.OldMap.get(a.Id).Customer_Score__c );
                 System.debug ('NewMap DOB =' +trigger.NewMap.get(a.Id).Date_of_Birth__c );
                 System.debug ('OldMap DOB =' +trigger.OldMap.get(a.Id).Date_of_Birth__c );
                 System.debug ('NewMap ZipCode =' +trigger.NewMap.get(a.Id).Zip_Code__c );
                 System.debug ('OldMap isPartner =' +trigger.OldMap.get(a.Id).IsPartner );       
                 System.debug ('NewMap isPartner =' +trigger.NewMap.get(a.Id).IsPartner ); 
                    if(trigger.NewMap.get(a.Id).Customer_Score__c == trigger.oldMap.get(a.Id).Customer_Score__c  && (trigger.NewMap.get(a.Id).Date_of_Birth__c != trigger.oldMap.get(a.Id).Date_of_Birth__c || trigger.NewMap.get(a.Id).Zip_Code__c != trigger.oldMap.get(a.Id).Zip_Code__c)){
                        accList.add(a);    
                    }
                    
                    else if(trigger.NewMap.get(a.Id).Customer_Score__c != null && trigger.NewMap.get(a.Id).Customer_Score__c != trigger.oldMap.get(a.Id).Customer_Score__c){
                           System.Debug('Trigger.New ***********'+trigger.NewMap.get(a.Id).Customer_Score__c);
                           a.Customer_Score_Source__c = 'Agent';
                        }
                    else if(trigger.NewMap.get(a.Id).Customer_Score__c == null && trigger.NewMap.get(a.Id).Customer_Score__c != trigger.oldMap.get(a.Id).Customer_Score__c){
                           a.Customer_Score_Source__c = '';
                         }  
                           
                }
                if(!accList.IsEmpty()){
                    zipCodeScoreHelper.CustomerScoreAutoPopulate(accList, 'Tech_ZipCode__c', 'Date_of_Birth__c', 'Customer_Score__c');
                }
         
                Util01_RetrieveZipCodeScore.isFirstRunAccUpdate = false;
            }
                 if(AP12_RestrictOwnerChangeStateBase.accFirstRun == true){
                                AP12_RestrictOwnerChangeStateBase restrictAccount = new AP12_RestrictOwnerChangeStateBase();
                                for(Account a : Trigger.New){
                                    if(a.OwnerId != trigger.oldMap.get(a.Id).OwnerId){
                                        accList.add(a);    
                                    }
                                }
                                if(!accList.isEmpty()){
                                    restrictAccount.restrictAccountOwnerChange(accList);
                                }
                                AP12_RestrictOwnerChangeStateBase.accFirstRun=false;
                }
                
           /* if(AP09_PopulateMarketingCampaign.isAccUpdateFirstRun == true){
                /* Call the class method to perform the actual marketing campaign population business logic */
                  /*  AP09_PopulateMarketingCampaign populateMC = new AP09_PopulateMarketingCampaign();
                    for(Account a : Trigger.New){
                            if(a.Marketing_Event__c != trigger.oldMap.get(a.Id).Marketing_Event__c){
                                accList.add(a);    
                            }
                    }*/
                //Recursion protection
                 /*   if(!accList.isEmpty())
                    populateMC.populateAccMarketingCampaign(accList,true);
                    AP09_PopulateMarketingCampaign.isAccUpdateFirstRun = false;
             }*/
       }
       //**************************TELESALES CODING END****************************************//
       //**************************SG QUOTING CODING START****************************************//
         if(System.Label.SG_BusinessTrack.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue)){
              
             AccountTriggerSGHandler.onBeforeUpdate(Trigger.newMap,Trigger.oldMap);        
             
         }
        //**************************SG QUOTING CODING END****************************************//
}