trigger EffectiveDateonCase on vlocity_ins__Application__c (after update) {
        
    if(trigger.isafter && trigger.isUpdate){  
        
            EffectiveDateonCaseclass helpclass =  new EffectiveDateonCaseclass();
            
            helpclass.effectivemethod(trigger.new, trigger.oldmap);
   
    }

}