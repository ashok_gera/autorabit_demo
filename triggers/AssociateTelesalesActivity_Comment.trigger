trigger AssociateTelesalesActivity_Comment on FeedComment (after insert) {
        system.debug('inside feedcomment trigger - '+Util04_BusinessTrack.businessTrackValue);
//  if('TELESALES'.equalsIgnoreCase(Util04_BusinessTrack.businessTrackValue))
  //  {
        try
        {
            system.debug('feedcomment currObjId = '+AssociateActivityToLeadAndAccount.currObjId);
            if(AssociateActivityToLeadAndAccount.currObjId == null)
            {
                String parId = Trigger.new[0].ParentId;
                String entity;
                //    Search Lead
                List<RecordType> ldRecType = [SELECT Id, name FROM RECORDTYPE WHERE Name = 'TS MASTER' and sObjectType = 'Lead'];                       
                Lead[] ld = [SELECT Id from Lead where Id =:parId AND RecordTypeId =:ldRecType[0].Id];
                if(ld != null && !ld.isEmpty())
                {
                    system.debug('lead id = '+ld[0].Id);
                    entity = 'Lead'; 
                }
                else    // Search Account
                {
                    Account[] acc = [SELECT Id from Account where Id =:parId];
                    if(acc != null && !acc.isEmpty())
                    {
                        system.debug('acc id = '+acc[0].Id);
                        entity = 'Account';
                    }
                }
                if (entity != null)
                {
                    AssociateActivityToLeadAndAccount.currObjId = parId;
                    AssociateActivityToLeadAndAccount assocAct = new AssociateActivityToLeadAndAccount();
                    system.debug('passing Id = '+parId+ ' and entity = '+entity);
                    assocAct.AssociateActivity(entity, parId);
                }
            }
        }
        catch(Exception e)
        {
           System.Debug('Error occured in AssociateTelesalesActivity Feedcomment trigger: '+e.getMessage());
        }
   // }
    // End - Daryl - Telesales WDE changes - 4/27/16 - Associate Activity after Lead is inserted    
}