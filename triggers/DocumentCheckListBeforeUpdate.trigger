trigger DocumentCheckListBeforeUpdate on Application_Document_Checklist__c (before update) {
    Map<ID,Application_Document_Checklist__c> appDocMap = new Map<ID,Application_Document_Checklist__c>([select Id,Case__r.Owner.Email,Status__c from Application_Document_Checklist__c where ID IN :Trigger.NewMap.keySet()]);

    for(Application_Document_CheckList__c appDocListObj : trigger.new){
        if(System.Label.SG14_Re_Uploaded.equalsIgnoreCase(appDocListObj.Status__c) && appDocListObj.Status__c != Trigger.OldMap.get(appDocListObj.Id).status__c){
           appDocListObj.Tech_CaseOwner_Email__c = appDocMap.get(appDocListObj.Id).Case__r.Owner.email;
        }
    }
}