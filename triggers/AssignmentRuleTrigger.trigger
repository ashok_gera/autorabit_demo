trigger AssignmentRuleTrigger on Assignment_Rule__c (before insert, before update) {
	
	AssignmentRuleHandler.updateOwner(trigger.new);

}