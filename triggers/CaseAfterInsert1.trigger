trigger CaseAfterInsert1 on Case (after insert) {
    
    //Added to exclude E2C Case/Auto-response e-mail creation-related Task inserts/updates
    CS006_TaskTrigger_Settings__c userSettings = CS006_TaskTrigger_Settings__c.getInstance(UserInfo.getUserId());
    if(!userSettings.Bypass_Validation__c) {  //E2C exclude --- START  
        //SGA Implementation
        Case cs = Trigger.new[0];
        if(System.Label.SG_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c))           
        {
            CaseTriggerHandler.onAfterInsert(Trigger.NewMap);
        }
        if(System.Label.BR_BusinessTrack.equalsIgnoreCase(cs.Tech_Businesstrack__c)){
            try
            {      
                if(BR_AP02_CreateCTIActivityOnNewCase.isFirstRunCaseAfterInsert == true)
                {
                    List<Id> cIds= new List<Id>();
                    for(Case c: Trigger.New)
                    {
                        system.debug('RoutingEmail after insert ::: - '+c.BR_Tech_Case_RoutingEmailAddress__c );
                        if (c.BR_Category__c != null) {
                            cIds.add(c.Id);
                            system.debug('adding case with Id - '+c.Id);
                        }
                    }
                    BR_AP02_CreateCTIActivityOnNewCase clsTask = new BR_AP02_CreateCTIActivityOnNewCase();
                    if(cIds.size() > 0)
                        clsTask.CreateCTITaskOnCase(cIds); 
                    BR_AP02_CreateCTIActivityOnNewCase.isFirstRunCaseAfterInsert = false;
                }
            }
            catch(Exception e)
            {
                System.Debug(e.getMessage());
            }
        }
        
    } //E2C Exclude --- END
}