<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ap_Dental_NY</label>
    <protected>false</protected>
    <values>
        <field>APS_Attribute_Display_Field__c</field>
        <value xsi:type="xsd:string">{&quot;Card&quot;:[{&quot;attribute_name&quot;:&quot;Deductible&quot;,&quot;display_name&quot;:&quot;Deductible&quot;}],&quot;Detail&quot;:[{&quot;attribute_name&quot;:&quot;Deductible&quot;,&quot;display_name&quot;:&quot;Deductible&quot;},{&quot;attribute_name&quot;:&quot;Annual Maximum&quot;,&quot;display_name&quot;:&quot;Annual Maximum&quot;},{&quot;attribute_name&quot;:&quot;Endo/Perio/Oral Surgery&quot;,&quot;display_name&quot;:&quot;Endodontics/Periodontics/Oral Surgery&quot;},{&quot;attribute_name&quot;:&quot;Child Orthodontics&quot;,&quot;display_name&quot;:&quot;Child Orthodontics&quot;},{&quot;attribute_name&quot;:&quot;Dental Implants&quot;,&quot;display_name&quot;:&quot;Dental Implants&quot;},{&quot;attribute_name&quot;:&quot;Posterior Composites&quot;,&quot;display_name&quot;:&quot;Posterior Composites&quot;},{&quot;attribute_name&quot;:&quot;OON Reimbursement&quot;,&quot;display_name&quot;:&quot;Out-of-Network Reimbursement&quot;}]}</value>
    </values>
</CustomMetadata>
