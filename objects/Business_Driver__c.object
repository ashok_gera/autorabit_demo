<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>TOOLKIT: An object to capture business drivers related to a release and a capability.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ReadWrite</externalSharingModel>
    <fields>
        <fullName>Business_Driver__c</fullName>
        <description>Business Drivers and their details</description>
        <encrypted>false</encrypted>
        <externalId>false</externalId>
        <label>Business Driver</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Capability__c</fullName>
        <description>Capabilities which will help enable the Business Driver</description>
        <externalId>false</externalId>
        <inlineHelpText>Select the Capability or Capabilities that will help enable the Business Driver.</inlineHelpText>
        <label>Capability</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Account Management</fullName>
                    <default>false</default>
                    <label>Account Management</label>
                </value>
                <value>
                    <fullName>Campaign Management</fullName>
                    <default>false</default>
                    <label>Campaign Management</label>
                </value>
                <value>
                    <fullName>Contact Management</fullName>
                    <default>false</default>
                    <label>Contact Management</label>
                </value>
                <value>
                    <fullName>Contract Management</fullName>
                    <default>false</default>
                    <label>Contract Management</label>
                </value>
                <value>
                    <fullName>Lead Management</fullName>
                    <default>false</default>
                    <label>Lead Management</label>
                </value>
                <value>
                    <fullName>Opportunity Management</fullName>
                    <default>false</default>
                    <label>Opportunity Management</label>
                </value>
                <value>
                    <fullName>Product Management</fullName>
                    <default>false</default>
                    <label>Product Management</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Release__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Related release to the business driver</description>
        <externalId>false</externalId>
        <label>Release</label>
        <referenceTo>Release__c</referenceTo>
        <relationshipLabel>Business Driver</relationshipLabel>
        <relationshipName>Business_Driver</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Business Driver</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Business_Driver__c</columns>
        <columns>Capability__c</columns>
        <columns>Release__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Business Driver Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Business Drivers</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Business_Driver__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Release__c</customTabListAdditionalFields>
        <searchResultsAdditionalFields>Business_Driver__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Release__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
