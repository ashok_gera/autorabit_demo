<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <tab>standard-Chatter</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-Idea</tab>
    <tab>standard-IdeaTheme</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Questions</tab>
    <tab>Keep_It_Local</tab>
    <tab>vlocity_ins__OmniScriptInstance__c</tab>
    <tab>vlocity_ins__AttributeAssignment__c</tab>
    <tab>vlocity_ins__Party__c</tab>
</CustomApplication>
