<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Homebase</label>
    <logo>ButtonsLogosImages/Homebase_Logo_2.png</logo>
    <tab>standard-Chatter</tab>
    <tab>standard-Knowledge</tab>
    <tab>standard-KnowledgePublishing</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Producer_Toolbox</tab>
    <tab>Subsidy_Calculator</tab>
    <tab>Request_Log</tab>
    <tab>Keep_It_Local</tab>
    <tab>TASKRAY__TaskRay</tab>
    <tab>TASKRAY__Project_Task__c</tab>
    <tab>TASKRAY__Project__c</tab>
    <tab>XML_Generator__c</tab>
    <tab>Support_Request__c</tab>
    <tab>vlocity_ins__OmniScriptInstance__c</tab>
    <tab>vlocity_ins__AttributeAssignment__c</tab>
    <tab>vlocity_ins__Party__c</tab>
</CustomApplication>
