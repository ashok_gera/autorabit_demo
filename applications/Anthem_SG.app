<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Search_Broker_tab</defaultLandingTab>
    <description>App for Anthem SG</description>
    <formFactors>Large</formFactors>
    <label>Anthem SG</label>
    <tab>SGA_Ltng_Broker_Search</tab>
    <tab>SG_Broker_Search</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Quote</tab>
    <tab>standard-Opportunity</tab>
    <tab>vlocity_ins__Application__c</tab>
    <tab>standard-File</tab>
    <tab>Quote_Status_Dashboard__c</tab>
    <tab>Application_Document_Config__c</tab>
    <tab>vlocity_ins__OmniScriptInstance__c</tab>
    <tab>vlocity_ins__AttributeAssignment__c</tab>
    <tab>vlocity_ins__Party__c</tab>
</CustomApplication>
