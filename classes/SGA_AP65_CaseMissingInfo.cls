/************************************************************************
 * Page Name   : SGA_AP65_CaseMissingInfo
 * Created By  : IDC Offshore
 * Created Date: 02/02/2018
 * Description : It is used to perform the evaluation for Missing information 
 * and generate a Error message with only the offending fields that need to be updated by the use
 **********************************************************************/
global class SGA_AP65_CaseMissingInfo {  

    private static Final String MSG = 'Following field values are required to initiate Group Shell Generation:';
    private static String GlobalSMsg= 'GLOBAL';
	private static Final String ERR_ETIN ='Employer ETIN';
	private static Final String ERR_STATE ='Account State';
	private static Final String ERR_ACCNAME ='Account Name';
	private static Final String ERR_EFFDATE ='Effective Date';
	private static Final String ERR_PHYSTREET ='Physical Street';
	private static Final String ERR_PHYSTATE='Physical State';
	private static Final String ERR_PHYCITY ='Physical City';
	private static Final String ERR_PHYZIP ='Physical Zipcode';
	private static Final String ERR_COUNTY ='County';
	private static Final String ERR_BILL_Street ='Billing Street ';
    private static Final String ERR_BILL_State ='Billing State';
    private static Final String ERR_BILL_City ='Billing City';
    private static Final String ERR_BILL_COUNTY ='Billing County';
    private static Final String ERR_BILL_Zipcode ='Billing Zipcode';
	private static Final String ERR_GRPADMIN ='Group Admin';
	private static Final String ERR_OFFPHONE ='Office Phone';
	private static Final String ERR_APPOWNER ='Application Owner';
	private static Final String ERR_APPSTATUS ='Application Status';
	private static Final String COMMA =',';
	private static Final String SPACE =' ';

/************************************************************************************
    Method Name : evaluateMissingInfo
    Parameters  : Id 
    Return type : String
    Description : Method to query and generate the Error Message with Offending fields
*************************************************************************************/

    webservice static String evaluateMissingInfo(ID caseId){ 
        String missingFieldsErrMsg='';
        Case caseInfo=new Case();
		System.debug('Current Case is::'+caseId );
		if(caseId != NULL ){
		caseInfo = [Select Id,Tech_Required_Field_Check__c,Tech_ObtainGroupNumber__c,Account.Employer_EIN__c, Application_Name__r.Account_State__c, Account.Name, Application_Name__r.Group_Coverage_Date__c, Account.Company_Street__c,Account.Company_City__c,Account.Company_State__c,Account.Company_Zip__c,Account.County__c,Account.Billing_City__c,Account.Billing_State__c,Account.Billing_Street__c,Account.Billing_PostalCode__c,Account.Billing_County__c,Account.Group_Admin__c,Account.Phone_Number__c,Application_Name__r.OwnerId,Application_Name__r.vlocity_ins__Status__c From Case Where Id =: caseId limit 1];
        }
        System.debug('Current Case is::'+caseInfo ); 
       if(caseInfo.Tech_Required_Field_Check__c != TRUE){
       if(String.isBlank(caseInfo.Account.Employer_EIN__c)){
        missingFieldsErrMsg = missingFieldsErrMsg + ERR_ETIN + COMMA + SPACE;
        }
       if(String.isBlank(caseInfo.Application_Name__r.Account_State__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_STATE+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.Name)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_ACCNAME+ COMMA + SPACE;
       }
	   if(caseInfo.Application_Name__r.Group_Coverage_Date__c == NULL){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_EFFDATE+ COMMA + SPACE;
       }
	   if(String.isBlank(caseInfo.Account.Company_Street__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_PHYSTREET+ COMMA + SPACE;
       }
	    if(String.isBlank(caseInfo.Account.Company_City__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_PHYCITY+ COMMA + SPACE;
       }
	    if(String.isBlank(caseInfo.Account.Company_State__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_PHYSTATE+ COMMA + SPACE;
       }
	    if(String.isBlank(caseInfo.Account.Company_Zip__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_PHYZIP+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.County__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_COUNTY+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.Billing_State__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_BILL_State+ COMMA + SPACE;
       }      
       if(String.isBlank(caseInfo.Account.Billing_City__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_BILL_City+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.Billing_Street__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_BILL_Street+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.Billing_PostalCode__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_BILL_Zipcode+ COMMA + SPACE;
       }
       if(String.isBlank(caseInfo.Account.Billing_County__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_BILL_COUNTY+ COMMA + SPACE;
       } 
	   if(String.isBlank(caseInfo.Account.Group_Admin__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_GRPADMIN + COMMA + SPACE;
       }
	   if(String.isBlank(caseInfo.Account.Phone_Number__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_OFFPHONE+ COMMA + SPACE;
       }
	   if(String.isBlank(caseInfo.Application_Name__r.OwnerId)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_APPOWNER+ COMMA + SPACE;
       }
	   if(String.isBlank(caseInfo.Application_Name__r.vlocity_ins__Status__c)){
       missingFieldsErrMsg = missingFieldsErrMsg + ERR_APPSTATUS;
       }
         if(missingFieldsErrMsg!=null && missingFieldsErrMsg.length()>0)
         {
         GlobalSMsg = MSG + SPACE +missingFieldsErrMsg;
         GlobalSMsg = GlobalSMsg.trim(); 
         if (GlobalSMsg.endsWith(COMMA)) {
         GlobalSMsg = GlobalSMsg.substring(0, GlobalSMsg.length() - 1);
         }         
        }
     }        
        return GlobalSMsg;
       }
}