/**********************************************************************************
Class Name :   SGA_AP54_CensusValidator_Test
Date Created : 29-November-2017
Created By   : IDC Offshore
Description  : 1. This class is the test class for SGA_AP54_CensusValidator
*************************************************************************************/
@isTest
private class SGA_AP54_CensusValidator_Test { 
/*This method creates a list of census wrapper*/
    public static List<SGA_AP53_CensusMemberCtrl.censusWrapper> createCensusEmployeeWrapperList(Id censusId){
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp ;
        List<SGA_AP53_CensusMemberCtrl.censusWrapper> testcnsEmpList = new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
    	List<SGA_AP53_CensusMemberCtrl.censusWrapper> childList;    
        for(Integer i=1; i<10; i++)
        {
            childList = new List<SGA_AP53_CensusMemberCtrl.censusWrapper>(); 
            cnemp = new SGA_AP53_CensusMemberCtrl.censusWrapper(i,childList,censusId,null,'TestCensusMember'+i,'Employee',Date.ValueOf('1985-07-01'),null,'Male','No','Employee','Employee','Employee','1',12000,'Annually');
            
      
      for(Integer j=1;j<10;j++){
      
      cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(i+1,null,censusId,null,'TestCensusDep'+j,'Child',null,27,'Female','No','Employee','Employee','Employee',null,0,null));
      }
            testcnsEmpList.add(cnemp);
        }
        return testcnsEmpList;
  }
/************************************************************************************
    Method Name : testvalidateEmp
    Parameters  : None
    Return type : void
    Description : This is  testmethod for validate Method 
*************************************************************************************/
    private static testMethod void testvalidateEmp(){
        map<String,Object> dateMap;
        User testUser = Util02_TestData.createUser();
        List<SGA_AP53_CensusMemberCtrl.censusWrapper> censusMemList;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            censusMemList= createCensusEmployeeWrapperList(grpCensus.Id); 
            dateMap = (Map<String, Object>) JSON.deserializeUntyped('{"1-0":"1985-06-07"}');
            Test.startTest();
            SGA_AP54_CensusValidator.validate(censusMemList,dateMap);
            SGA_AP54_CensusValidator.rerenderWrapper(censusMemList,dateMap);
            Test.stopTest();
        }
    } 
/************************************************************************************
    Method Name : testDOBAgeValidations
    Parameters  : None
    Return type : void
    Description : This is  testmethod for validate Method 
*************************************************************************************/
    private static testMethod void testDOBAgeValidations(){
        map<String,Object> dateMap;
        User testUser = Util02_TestData.createUser();
        List<SGA_AP53_CensusMemberCtrl.censusWrapper> censusMemList;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            censusMemList= createCensusEmployeeWrapperList(grpCensus.Id);
            censusMemList[0].birthDate=Date.valueOf('1850-07-07'); 
            censusMemList[1].birthDate=Date.Today();
            censusMemList[2].memberAge=-3;
            censusMemList[3].birthDate=null;
            censusMemList[3].memberAge=0; 
            censusMemList[4].birthDate=Date.valueOf('2000-07-07');
            censusMemList[4].memberAge=14;
            censusMemList[5].birthDate=Date.valueOf('2007-07-07');
            censusMemList[5].memberAge=0;
            censusMemList[6].birthDate=Date.valueOf('2007-07-07');
            censusMemList[6].memberAge=14;
            censusMemList[7].birthDate=null;
            censusMemList[7].memberAge=13;
            dateMap = (Map<String, Object>) JSON.deserializeUntyped('{"1-1":"1850-07-07"}');
            Test.startTest();
            SGA_AP54_CensusValidator.validate(censusMemList,dateMap);
            Test.stopTest();
        }
    }   
/************************************************************************************
    Method Name : testCoverageTypeValidations
    Parameters  : None
    Return type : void
    Description : This is  testmethod for validate Method 
*************************************************************************************/
    private static testMethod void testCoverageTypeValidations(){
        map<String,Object> dateMap;
        User testUser = Util02_TestData.createUser();
        List<SGA_AP53_CensusMemberCtrl.censusWrapper> censusMemList;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            censusMemList= createCensusEmployeeWrapperList(grpCensus.Id);
            censusMemList[0].medicalCoverageType=null; 
            censusMemList[0].dentalCoverageType=null;
            censusMemList[0].visionCoverageType=null;
            censusMemList[0].cobra=null;
            censusMemList[0].gender=null;
            censusMemList[0].lifeClass=null;
            censusMemList[0].salary=1252000;
            censusMemList[1].dependentIndicator=null;
            censusMemList[2].childList=new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
            censusMemList[2].medicalCoverageType='Family';           
            dateMap = (Map<String, Object>) JSON.deserializeUntyped('{"1-1":"1850-07-07"}');
            Test.startTest();
            SGA_AP54_CensusValidator.validate(censusMemList,dateMap);
            Test.stopTest();
        }
    }    
}