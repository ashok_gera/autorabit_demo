/**********************************************************************************
Class Name :   SGA_AP54_CensusValidator
Date Created : 23-Nov-2017
Created By   : IDC Offshore
Description  : This class is the to perform the validation for Census Members
*************************************************************************************/
public without sharing class  SGA_AP54_CensusValidator{
    private static final String EMPTY = '';
    private static final String COLONSYM=': ';
    private static final String NEXTLINE='\r\n';
    private static final String DOTSYM='.';
    private static final String DOTSPLIT='\\.';
    private static final String SPACE=' ';
    private static final String LINE='Line ';
    private static final String YES='Yes';
    private static final String EMPLOYEEVAL='Employee';
    private static final String WAIVEDVAL='Waived';
    private static final String CHILDVAL='Child';
    private static final String MINDATE='1900-01-01';
    private static final String ZEROSTR = '-0';
    private static final String SPECIALSTR = '-';
    private static final String COBRAVALMSG= System.Label.SG100_COBRAVAL_MSG;
    private static final String DEPINDICATORMSG=System.Label.SG112_DEPINDICATOR_MSG;
    private static final String MEDVALMSG= System.Label.SG101_MEDVAL_MSG;
    private static final String DENVALMSG= System.Label.SG102_DENVAL_MSG;
    private static final String VSNVALMSG= System.Label.SG103_VSNVAL_MSG;
    private static final String SALVALMSG= System.Label.SG104_SALVAL_MSG;
    private static final String DOBORAGEVALMSG= System.Label.SG105_DOBORAGEVAL_MSG;
    private static final String GRTRDOBVALMSG= System.Label.SG106_GRTRDOBVAL_MSG;
    private static final String LESSERDOBVALMSG= System.Label.SG107_LESSERDOBVAL_MSG;
    private static final String NEGAGEVALMSG= System.Label.SG108_NEGAGEVAL_MSG  ;
    private static final String AGECALVALMSG= System.Label.SG109_AGECALVAL_MSG;
    private static final String GENDERREQDMSG= System.Label.SG110_GENDERREQD_MSG;
    private static final String LIFECLSREQDMSG= System.Label.SG111_LIFECLSREQD_MSG;
    private static final String EMPAGE15MSG=System.Label.SG113_EMPAGE15_MSG;
    private static final String CHILDAGE26MSG=System.Label.SG114_CHILDAGE26_MSG;
    private static final String MISMATCH_COVERAGE =System.Label.SG115_MISMATCH_COVERAGE;
    private static final String MISMATCH_CLASS =System.Label.SG116_MISMATCH_CLASS;
    private static final String ONE='1';
    private static boolean ageFailed;
    private static boolean birth1DateFail;
    public static String validationMsgGlobal;
    private static String validationRowMsg;
    private static Integer rowIndex;
    private static Integer formulaAge;
    private static set<String> selectedClass = new Set<String>();
    private static Integer empCount;
    private static List<string> dateList;
    private static set<string> coverageType;       
    /************************************************************************************
    Method Name : validate
    Parameters  : List<vlocity_ins__GroupCensusMember__c> 
    Return type : List<SGA_AP53_CensusMemberCtrl.censusWrapper>
    Description : Used to perform the validation logic based on Census Member Data
    *************************************************************************************/
    public static List<SGA_AP53_CensusMemberCtrl.censusWrapper> validate(List<SGA_AP53_CensusMemberCtrl.censusWrapper>
     censusMemWrapper,
    map<String,Object> dateMap) { 
        formulaAge=0;
        rowIndex=0;
        validationMsgGlobal=EMPTY;
        coverageType = new set<string>{EMPLOYEEVAL,WAIVEDVAL,null};
        if(!censusMemWrapper.isEmpty()){
            empCount = censusMemWrapper.size();
            for(SGA_AP53_CensusMemberCtrl.censusWrapper employee: censusMemWrapper)
            {
                nullifyParams();
                if(dateMap.containsKey(employee.rowIndex+ZEROSTR)){
                    parseDOB(employee,(string)dateMap.get(employee.rowIndex+ZEROSTR));
                }
                if(employee.birthDate!=null){                    
                    formulaAge=calculateAge(employee.birthDate);
                }
                //Parent Validation
                validateEmployee(employee);
                //Dependent Validation
                if(!employee.childList.isEmpty()){
                    for(SGA_AP53_CensusMemberCtrl.censusWrapper dependent:employee.childList){
                        if(dateMap.containsKey(employee.rowIndex+SPECIALSTR+dependent.rowIndex)){
                            parseDOB(dependent,(string)dateMap.get(employee.rowIndex+SPECIALSTR+dependent.rowIndex));
                        }
                        if(dependent.birthDate!=null){                    
                            formulaAge=calculateAge(dependent.birthDate);
                        }
                        validateDependent(dependent);   
                    }
                }
                selectedClass.add(employee.lifeClass);
            }          
        }
        return censusMemWrapper;
    }       
    /************************************************************************************
    Method Name : Rerender
    Parameters  : List<vlocity_ins__GroupCensusMember__c> 
    Return type : List<SGA_AP53_CensusMemberCtrl.censusWrapper>
    Description : Used to maintain the DOB field state when rerender happens from VF page.
    *************************************************************************************/
    public static List<SGA_AP53_CensusMemberCtrl.censusWrapper> rerenderWrapper(List<SGA_AP53_CensusMemberCtrl.censusWrapper>
     censusMemWrapper,
    map<String,Object> dateMap) { 
        if(!censusMemWrapper.isEmpty()){
            for(SGA_AP53_CensusMemberCtrl.censusWrapper employee: censusMemWrapper)
            {
                //Employee Rerender
                if(dateMap.containsKey(employee.rowIndex+ZEROSTR)){
                    parseDOB(employee,(string)dateMap.get(employee.rowIndex+ZEROSTR));
                }
                //Dependent Rerender
                if(!employee.childList.isEmpty()){
                    for(SGA_AP53_CensusMemberCtrl.censusWrapper dependent:employee.childList){
                        if(dateMap.containsKey(employee.rowIndex+SPECIALSTR+dependent.rowIndex)){
                            parseDOB(dependent,(string)dateMap.get(employee.rowIndex+SPECIALSTR+dependent.rowIndex));
                        }
                    }
                }
            }          
        }
        return censusMemWrapper;
    }    
    /************************************************************************************
    Method Name : validateEmployee
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate the Employee. This method maintains the Employee validation rules.
    *************************************************************************************/
    private static void validateEmployee(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        wrapper.censusMemberError = new List<String>();  
        dependentIndicator_Cobra_Rule(wrapper);
        coverageType_Validations(wrapper);
        birthDate_Age_ValidationRules(wrapper);
        gender_ValidationRule(wrapper);
        salary_ValidationRule(wrapper);
        lifeClass_ValidationRule(wrapper);
        nullifyErrorMessages(wrapper);      
        
    }        
    /************************************************************************************
    Method Name : validateDependent
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate the Dependent. This method maintains the Dependent validation rules.
    *************************************************************************************/
    private static void validateDependent(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        wrapper.censusMemberError = new List<String>();
        nullifyParams();
        dependentIndicator_Cobra_Rule(wrapper);
        BirthDate_Age_ValidationRules(wrapper);
        nullifyErrorMessages(wrapper);
    }   
    /************************************************************************************
    Method Name : nullifyParams
    Parameters  : void
    Return type : void
    Description : Used to nullify Error booleans
    *************************************************************************************/
    private static void nullifyParams(){
        validationRowMsg=EMPTY;
        rowIndex += 1;
        birth1DateFail = false;
        ageFailed = false;
    }    
    /************************************************************************************
    Method Name : checkCoverageTypeforEmp
    Parameters  : censusWrapper
    Return type : Boolean
    Description : Used to check if all the CoverageTypes all Employee or not.
    *************************************************************************************/
    private static boolean checkCoverageTypeforEmp(SGA_AP53_CensusMemberCtrl.censusWrapper empData){
        if(!coverageType.contains(empData.medicalCoverageType) || !coverageType.contains(empData.dentalCoverageType) 
        || !coverageType.contains(empData.visionCoverageType)){
            return true;
        }
        return false;       
    }    
    /************************************************************************************
    Method Name : parseDOB
    Parameters  : censusWrapper & sDate String
    Return type : void
    Description : Used to Parse DOB as dob value is String coming from page.
    *************************************************************************************/
    private static void parseDOB(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper,String sDate){
        if(sDate!=EMPTY){
            dateList = sDate.split(SPECIALSTR);
            wrapper.htmlDate = sDate;
            wrapper.birthDate = date.newinstance(Integer.valueOf(dateList[0]), Integer.valueOf(dateList[1]),
             Integer.valueOf(dateList[2]));
        }
        else{
            wrapper.htmlDate = EMPTY;
            wrapper.birthDate = null;
        }
    }   
    /************************************************************************************
    Method Name : BirthDate_Age_ValidationRules
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate given BirthDate and Age.
    *************************************************************************************/
    private static void birthDate_Age_ValidationRules(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if(wrapper.birthDate==null && wrapper.memberAge==0){
             wrapper.birthDateError=DOBORAGEVALMSG;
             wrapper.memberAgeError=DOBORAGEVALMSG;
             validationRowMsg+=DOBORAGEVALMSG+SPACE;
             birth1DateFail =  true;
             ageFailed = true;
        }
        if(Date.valueOf(wrapper.birthDate)>=Date.Today()){
            wrapper.birthDateError= GRTRDOBVALMSG;
            validationRowMsg+=GRTRDOBVALMSG+SPACE;
             birth1DateFail =  true;            
        }
        if(Date.valueOf(wrapper.birthDate)< Date.valueOf(MINDATE)){
            wrapper.birthDateError=LESSERDOBVALMSG;
            validationRowMsg+=LESSERDOBVALMSG+SPACE;
             birth1DateFail =  true;          
        }
        if(Integer.valueOf(wrapper.memberAge)<0){  
            wrapper.memberAgeError=NEGAGEVALMSG;
            validationRowMsg+=NEGAGEVALMSG+SPACE;
            ageFailed = true;
        }        
        if(wrapper.memberAge !=0 && wrapper.birthDate!=null && Integer.valueOf(wrapper.memberAge)!=formulaAge){            
            wrapper.memberAgeError=AGECALVALMSG;
            validationRowMsg+=AGECALVALMSG+SPACE;
            ageFailed = true;          
        }
        if(wrapper.dependentIndicator==EMPLOYEEVAL){
            if(wrapper.birthDate==null && Integer.valueOf(wrapper.memberAge)>0 && Integer.valueOf(wrapper.memberAge)<15){ 
                wrapper.memberAgeError=EMPAGE15MSG;
                validationRowMsg+=EMPAGE15MSG+SPACE;
                ageFailed = true;
            }
            else if(wrapper.birthDate != null && Integer.valueOf(wrapper.memberAge)==0 && formulaAge<15){
                wrapper.birthDateError=EMPAGE15MSG;
                validationRowMsg+=EMPAGE15MSG+SPACE;
                birth1DateFail = true;
            }
            else if(wrapper.birthDate != null && Integer.valueOf(wrapper.memberAge)>0 && Integer.valueOf(wrapper.memberAge)<15 
            && formulaAge<15){
                wrapper.memberAgeError=EMPAGE15MSG;
                wrapper.birthDateError=EMPAGE15MSG;
                validationRowMsg+=EMPAGE15MSG+SPACE;
                ageFailed = true;
                birth1DateFail = true;
            }else{}
        }        
        if(wrapper.dependentIndicator==CHILDVAL && (Integer.valueOf(wrapper.memberAge)>26 || formulaAge>26)){  
            wrapper.memberAgeError=CHILDAGE26MSG;
            validationRowMsg+=CHILDAGE26MSG+SPACE;
            ageFailed = true;
        system.debug('##childage26');
        }        
        formulaAge =0; //Clear calculated Age.
    }   
    /************************************************************************************
    Method Name : CoverageType_Validations
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate all CovergeTypes for Employee.
    *************************************************************************************/
    private static void coverageType_Validations(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if(wrapper.dependentIndicator==EMPLOYEEVAL && wrapper.medicalCoverageType==null){
            wrapper.medicalCoverageTypeError=MEDVALMSG;
            validationRowMsg+=MEDVALMSG+SPACE;
            
        }
        else{
            wrapper.medicalCoverageTypeError =null;
        }
        
        if(wrapper.dependentIndicator==EMPLOYEEVAL && wrapper.dentalCoverageType==null){
            wrapper.dentalCoverageTypeError=DENVALMSG;
            validationRowMsg+=DENVALMSG+SPACE;
        }
        else{
            wrapper.dentalCoverageTypeError =null;
        }
        if(wrapper.dependentIndicator==EMPLOYEEVAL && wrapper.visionCoverageType==null){
            wrapper.visionCoverageTypeError=VSNVALMSG;
            validationRowMsg+=VSNVALMSG+SPACE;
            
        }
        else{
            wrapper.visionCoverageTypeError =null;
        }
        if(wrapper.dependentIndicator == EMPLOYEEVAL && wrapper.childList.isEmpty()){
            if(checkCoverageTypeforEmp(wrapper)){
                 validationRowMsg+=MISMATCH_COVERAGE+SPACE;
            }
        }
    }   
    /************************************************************************************
    Method Name : gender_ValidationRule
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate Gender.
    *************************************************************************************/
    private static void gender_ValidationRule(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if (wrapper.gender==null){
            validationRowMsg+=GENDERREQDMSG+SPACE;
            wrapper.genderError = GENDERREQDMSG;
        }
        else{
            wrapper.genderError = EMPTY;
        }
    }    
    /************************************************************************************
    Method Name : dependentIndicator_Cobra_Rule
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate Dependent Indicator and Cobra.
    *************************************************************************************/
    private static void dependentIndicator_Cobra_Rule(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if(wrapper.dependentIndicator == null){
            wrapper.dependentIndicatorError =DEPINDICATORMSG;
            validationRowMsg+=DEPINDICATORMSG+SPACE;
        }
        else{
            wrapper.dependentIndicatorError =null;
        }
        if(wrapper.dependentIndicator == EMPLOYEEVAL && wrapper.cobra == null ){
            wrapper.cobraError=COBRAVALMSG;
            validationRowMsg+=COBRAVALMSG+SPACE;
        }
        else{
            wrapper.cobraError =null;
        }
    }   
    /************************************************************************************
    Method Name : salary_ValidationRule
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate Salary.
    *************************************************************************************/
    private static void salary_ValidationRule(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if(wrapper.salary>999999.99){
            wrapper.salaryError=SALVALMSG ;
            validationRowMsg+=SALVALMSG+SPACE;
        }  
        else{
            wrapper.salaryError =null;
        } 
    }           
    /************************************************************************************
    Method Name : lifeClass_ValidationRule
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to validate LifeClass if Employee count is less than 10.
    *************************************************************************************/
    private static void lifeClass_ValidationRule(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
        if (wrapper.lifeClass ==null){
            validationRowMsg+=LIFECLSREQDMSG+SPACE;
            wrapper.lifeClassError  = LIFECLSREQDMSG;
        }
        else{
            wrapper.lifeClassError  = null;
        }        
        if(empCount < 10 &&  wrapper.lifeClass != ONE && wrapper.lifeClass != null){
             wrapper.lifeClassError =MISMATCH_CLASS;
             validationRowMsg+=MISMATCH_CLASS+SPACE;
        }
    }    
    /************************************************************************************
    Method Name : nullifyErrorMessages
    Parameters  : censusWrapper 
    Return type : void
    Description : Used to nullifyErrorMessages and Append errorMessage to GloablErrorMessage.
    *************************************************************************************/
    private static void nullifyErrorMessages(SGA_AP53_CensusMemberCtrl.censusWrapper wrapper){
         if(!ageFailed){
            wrapper.memberAgeError = null;
        }
        if(!birth1DateFail){
            wrapper.birthDateError =  null;
        }
        
        if(String.isNotBlank(validationRowMsg)){
            wrapper.censusMemberError.add(validationRowMsg);   
            validationMsgGlobal += NEXTLINE+LINE+ rowIndex+COLONSYM+validationRowMsg;
        } else {
            wrapper.censusMemberError = null;
        } 
    }
    /************************************************************************************
    Method Name : calculateAge
    Parameters  : Date 
    Return type : Integer
    Description : Used to caculate the age based on entered DOB value.
    *************************************************************************************/
    public static Integer calculateAge(Date DOB){
        Integer age = Date.Today().year()-Date.valueOf(DOB).year();
        if(Date.today().month()>Date.valueOf(DOB).month()){
            age=Date.today().year()-Date.valueOf(DOB).year();
        }
        else if((Date.today().month()==Date.valueOf(DOB).month()) && (Date.today().day()>=Date.valueOf(DOB).day())){
            age=Date.today().year()-Date.valueOf(DOB).year();
        }
        else {
            age=(Date.today().year()-Date.valueOf(DOB).year())-1;
        }
        return age;
    }    
}