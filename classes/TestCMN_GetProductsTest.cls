global with sharing class TestCMN_GetProductsTest implements vlocity_ins.VlocityOpenInterface{

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;

        try{
            if(methodName == 'getProducts') {
                getProducts(inputMap, outMap, options);
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        
        return success;
    }
    
    public void getProducts(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
    
        String searchSpec = (String) options.get('productSearchCriteria');
        
        Map<String, Object> m1 = (Map<String, Object>)inputMap.get('TypeAheadZip-Block'); 
       
        Map<String, Object> plantype = (Map<String, Object>)inputmap.get('planTypePage');
        
        string brand = string.valueof(m1.get('Brand'));
        
        string statefield = string.valueof(m1.get('MinState'));
        
        Map<String, Object> plans = (Map<String, Object>)plantype.get('planTypeBlock');
        
        string medical = string.valueof(plans.get('medicalInsuranceType'));
        
        string dental =  string.valueof(plans.get('dentalInsuranceType'));
        
        string vision =  string.valueof(plans.get('visionInsuranceType'));  
        
        string prodquery = 'SELECT Id, ProductCode, Name';
        
        prodquery +=' from Product2 where ';
        
        if(medical == 'true'){
        
            prodquery += ' vlocity_ins__Type__c = \'' +'Medical'+ '\'';
        
        }else if(dental == 'true'){
        
            prodquery += ' vlocity_ins__Type__c = \'' +'dental'+ '\'';
        
        }else if(vision == 'true'){
        
            prodquery += ' vlocity_ins__Type__c = \'' +'vision '+ '\'';
        
        }
        
        prodquery += ' AND IsActive=true AND ' ;
        
        if(brand != null){
            
            prodquery += ' Brand__c = \'' + brand + '\'';
        
        }
        
        if(statefield != null){
        
            prodquery += ' AND vlocity_ins__Availability__c= \'' +statefield + '\' AND vlocity_ins__EndDate__c=2017-12-31 ' ;
        
        }
        
        system.debug(prodquery );
        
        //searchSpec = 'vlocity_ins__Type__c= ' + planType + 'vlocity_ins__Availability__c = ' + stateField;
        //searchSpec = 'vlocity_ins__Type__c='Medical' AND IsActive=true AND Brand__c='EBC' AND vlocity_ins__Availability__c='NY' AND vlocity_ins__EndDate__c=2017-12-31';
        
        
       /* if(searchSpec !=null && searchSpec!=''){
            if(!searchSpec.contains('vlocity_ins__EndDate__c')){
                Map<String,Object> CategorySelectionField = (Map<String,Object>) inputMap.get('CategorySelection');
                String MONTHYEARField = (String)CategorySelectionField.get('ReqEffDtFormula-Monthly');
                String YEARField = (String)CategorySelectionField.get('ReqEffDtFormula-Year');

                System.debug('The YEARField is' + YEARField);
                if(YEARField == null){
                    System.debug('The YEARField is null');
                    CategorySelectionField.put('ReqEffDtFormula-Year', MONTHYEARField.split('_')[1]);
                    outMap.put('CategorySelection', CategorySelectionField);
                }
                else{
                    System.debug('The YEARField is not null');
                    CategorySelectionField.put('ReqEffDtFormula-Year', MONTHYEARField.split('_')[1]);
                }

                prodquery +=' where '+ searchSpec + '  AND vlocity_ins__EndDate__c='+MONTHYEARField.split('_')[1]+'-12-31';
                System.debug('The inputMap is' + inputMap);
            }
            else{
                prodquery +=' where '+searchSpec;
            }

            System.debug('The searchSpec is' + searchSpec);
            System.debug('The prodquery is' + prodquery);
        } */

        prodquery +=' LIMIT 50000';
        
        Map<Id, Product2> productMap;

        try{
            productMap = new Map<Id, Product2>((List<Product2>)database.query(prodquery));
        }catch(Exception e){
            System.debug(Logginglevel.ERROR, +e);
            throw e;
        }
        

        Map<Product2,List<vlocity_ins__AttributeAssignment__c>> productAttributeMap=new Map<Product2,List<vlocity_ins__AttributeAssignment__c>>();
        if(productMap!=null&&productMap.size()>0)
        {
            System.debug(Logginglevel.ERROR,' product list '  +productMap);
            Map<Id,List<vlocity_ins__AttributeAssignment__c>> attributeMap=new Map<Id,List<vlocity_ins__AttributeAssignment__c>>();
            List<vlocity_ins__AttributeAssignment__c> assignmentList = [SELECT Id, Name, vlocity_ins__ObjectId__c,vlocity_ins__ValueDescription__c, vlocity_ins__Value__c, CategoryName__c, AttributeDisplayName__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c,vlocity_ins__ValueDataType__c,vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c, vlocity_ins__AttributeId__r.IsKey__c from vlocity_ins__AttributeAssignment__c where vlocity_ins__ObjectId__c IN :productMap.keySet() Order by vlocity_ins__ObjectId__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c, vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c,AttributeDisplayName__c LIMIT 50000];
            System.debug(Logginglevel.ERROR,' attribute list '  +assignmentList);
            for(vlocity_ins__AttributeAssignment__c attribute:[SELECT Id, Name, vlocity_ins__ObjectId__c,vlocity_ins__ValueDescription__c, vlocity_ins__Value__c, 
                CategoryName__c, AttributeDisplayName__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c,vlocity_ins__ValueDataType__c,
                vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c, vlocity_ins__AttributeId__r.IsKey__c from vlocity_ins__AttributeAssignment__c where 
                vlocity_ins__ObjectId__c IN :productMap.keySet() Order by vlocity_ins__ObjectId__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c, 
                vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c,AttributeDisplayName__c LIMIT 50000]){
                
                if(attributeMap.containsKey(attribute.vlocity_ins__ObjectId__c))
                    attributeMap.get(attribute.vlocity_ins__ObjectId__c).add(attribute);
                else
                    attributeMap.put(attribute.vlocity_ins__ObjectId__c,new List<vlocity_ins__AttributeAssignment__c>{attribute});          
                
            }    
            for(Id prodId: productMap.keyset()){
                if(attributeMap.containsKey(prodId))
                    productAttributeMap.put(productMap.get(prodId), attributeMap.get(prodId));
                else
                    productAttributeMap.put(productMap.get(prodId), null);
            }
        }
        outMap.put('output', productAttributeMap);
 
    }
}