/*@author       Accenture IDC
@date           7/2/2018
@name           SGA_AP63_BrokerRelatedAgencies_Test
@description    Test class to test the SGA_AP63_BrokerRelatedAgencies class.
*/
@isTest
private class SGA_AP63_BrokerRelatedAgencies_Test {
  
/************************************************************************************
Method Name : TestGetBroker
Parameters  : None
Return type : void
Description : This is testmethod for getting BrokerId as per the agency type and Zipcode 
*************************************************************************************/
    private static testMethod void TestGetBroker(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            Contact contactProd = Util02_TestData.createAccData();
            
            Geographical_Info__c geo = Util02_TestData.createGeoRecord();
            Database.insert(geo);
            vlocity_ins__Party__c party = Util02_TestData.CreatePartydata();
            party.vlocity_ins__AccountId__c = contactProd.AccountId;
            party.vlocity_ins__ContactId__c = contactProd.id;
            Database.insert(party);
            vlocity_ins__PartyRelationshipType__c PrtyRType = Util02_TestData.CreatePartyRltnTypedata();
            PrtyRType.vlocity_ins__TargetRole__c = 'GENERAL AGENCY';
            Database.insert(PrtyRType);
            vlocity_ins__PartyRelationship__c partyRelation = Util02_TestData.CreatePartyRltndata(party);
            partyRelation.vlocity_ins__RelationshipTypeId__c = PrtyRType.id;
            Database.insert(partyRelation);
            Test.startTest();
            Map<String,Object> inputmap=new Map<String,Object>();
            Map<String,Object> options=new Map<String,Object>();
            Map<String,Object> outputmap=new Map<String,Object>();
            String methodName='getAgencyList';                     
            inputmap.put('ZipCode3','12766');
            options.put('AgencyType','General Agency');                                 
            SGA_AP63_BrokerRelatedAgencies brage=new SGA_AP63_BrokerRelatedAgencies();
            brage.blockMap = new map<string,Object>();
            brage.blockMap.put('BrokerId',contactProd.id);
            Boolean brokr= brage.invokeMethod(methodName,inputmap,outputmap,options); 
            Test.stopTest();
            system.assertEquals(True, brokr);
        }
    }   
/************************************************************************************
Method Name : TestMyexception
Parameters  : None
Return type : void
Description : This is testmethod for testing Negative Scenario
*************************************************************************************/           
private static testMethod void TestMyexception(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            Contact contactProd = Util02_TestData.createAccData();
            Geographical_Info__c geo = Util02_TestData.createGeoRecord();
            Database.insert(geo);
            vlocity_ins__Party__c party = Util02_TestData.CreatePartydata();
            party.vlocity_ins__AccountId__c = contactProd.AccountId;
            party.vlocity_ins__ContactId__c = contactProd.id;
            Database.insert(party);
            vlocity_ins__PartyRelationshipType__c PrtyRType = Util02_TestData.CreatePartyRltnTypedata();
            PrtyRType.vlocity_ins__TargetRole__c = 'GENERAL AGENCY';
            Database.insert(PrtyRType);
            vlocity_ins__PartyRelationship__c partyRelation = Util02_TestData.CreatePartyRltndata(party);
            partyRelation.vlocity_ins__RelationshipTypeId__c = PrtyRType.id;
            Database.insert(partyRelation);
            Test.startTest();
            Map<String,Object> inputmap=new Map<String,Object>();
            Map<String,Object> options=new Map<String,Object>();
            Map<String,Object> outputmap=new Map<String,Object>();
            String methodName='getAgencyList';                     
            inputmap.put('ZipCode3','12766');
            options.put('AgencyType','General Agency');                                 
            SGA_AP63_BrokerRelatedAgencies brage=new SGA_AP63_BrokerRelatedAgencies();
            brage.blockMap = new map<string,Object>();
            brage.blockMap.put('BrokerId',null);  
            Boolean brokr= brage.invokeMethod(methodName,inputmap,outputmap,options); 
            Test.stopTest();
            system.assertEquals(True, brokr);
        }
    }   
}