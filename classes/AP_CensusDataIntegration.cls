/********************************************************************************
Class Name :   AP_CensusDataIntegration implements vlocity_ins.VlocityOpenInterface2
Date Created : 19-February-2018 
Created By   : Muthu Rajagopalan
Description  : Creating the account data, census data, product data JSON for Integration Procedures.
Usage: 
Change History : 
Draft: Initial design
*************************************************************************************/
global with sharing class AP_CensusDataIntegration implements vlocity_ins.VlocityOpenInterface2{
    
    /**********************************************************************************
    Private Method Name : invokeMethod
    Description : Gets Census Header Record Information
    *************************************************************************************/
    global Boolean invokeMethod(String methodName, Map<String, Object> inputMap, Map<String, Object> outMap, Map<String, Object> optns) {
        if(methodName == 'getCensusDataForAccount') {
            //getCMRecords(inputMap, optns);
            System.debug ('inputMap: ' + String.valueOf(inputMap));
            System.debug ('outMap1: ' + String.valueOf(outMap));
            AP_CensusDataUtils cenData = new AP_CensusDataUtils(inputMap);
            map <String, AP_CensusDataUtils> calculateRateRequest = new map<String, AP_CensusDataUtils>();
            calculateRateRequest.put('calculateRateRequest', cenData);
            outMap.put('OutData', JSON.serialize(calculateRateRequest));
            System.debug ('outMap2: ' + String.valueOf(outMap));
        }
        return true;
    }

}