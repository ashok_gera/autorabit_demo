/**********************************************************************************
Class Name :  SGA_AP64_ValidateNSaveAccAddr_Test
Date Created : 01/30/2018
Created By   : IDC Offshore
Description  : 1. This class is the test class for SGA_AP64_ValidateNSaveAccAddr
*************************************************************************************/
@isTest
private class SGA_AP64_ValidateNSaveAccAddr_Test {
/************************************************************************************


/************************************************************************************
    Method Name : validateAddressInfo
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for  validateAddressInfo
*************************************************************************************/

private static testMethod void testgetAvailableCountiesNeg(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
        Geographical_Info__c geoInfo= Util02_TestData.createGeoRecord();
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfo);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
         SGA_AP64_ValidateNSaveAccAddr.getAvailableCounties('10001','Test','PHY');
         Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.physicalCounty);
		 Test.stopTest();
        
}
}

private static testMethod void testPopulateCountyListPositive1(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
        Geographical_Info__c geoInfo= Util02_TestData.createGeoRecord();
		geoInfo.County__c='Test County';
		geoInfo.Zip_Code__c='10001';
        testAcc.Company_Zip__c='10001';
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfo);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
         testAccCtrl.cancelEdit();
		 Test.startTest();
		 System.assertNotEquals(NULL,testAccCtrl.wrp.physicalCounty);
		 Test.stopTest();
        
}
}
private static testMethod void testPopulateCountyListPositive2(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Company_Zip__c='10001';
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
		 Test.startTest();
		 System.assertEquals(3,testAccCtrl.wrp.physicalCounty.size());
		 Test.stopTest();
        
}
}
 private static testMethod void testpopulateCountyListOnChange(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Company_Zip__c='10001';
        
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
         testAccCtrl.populateCountyListOnChange();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.physicalCounty);
		 Test.stopTest();
        
}
}

private static testMethod void testsaveAddressinfo(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Company_Zip__c='10001';
        
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
         testAccCtrl.populateCountyListOnChange();     
         testAccCtrl.saveAddressInfo();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.physicalCounty);
		 Test.stopTest();
        
}
}
private static testMethod void testsaveAddressinfo2(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Company_Zip__c='10001';
        
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
         testAcc.Company_Zip__c='10441';
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
         testAccCtrl.populateCountyListOnChange();
         testAccCtrl.wrp.phyCountyErr ='test';      
         testAccCtrl.saveAddressInfo();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.physicalCounty);
		 Test.stopTest();
        
}
}
private static testMethod void testPopulateCountyListBill(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
        Geographical_Info__c geoInfo= Util02_TestData.createGeoRecord();
		geoInfo.County__c='Test County';
		geoInfo.Zip_Code__c='10001';
        testAcc.Billing_PostalCode__c='10001';
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfo);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList(); 
		 Test.startTest();
		 System.assertNotEquals(NULL,testAccCtrl.wrp.billingCounty);
		 Test.stopTest();
        
}
}


 private static testMethod void testpopulateCountyListOnChangeBill(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Billing_PostalCode__c='10001'; 
		
	System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.populateCountyListOnChange();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.billingCounty);
		 Test.stopTest();
        
 }
}
  private static testMethod void testsaveAddressBillinfo(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Billing_PostalCode__c='10001';
        
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.populateCountyListOnChange();
         testAccCtrl.saveAddressInfo();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.billingCounty);
		 Test.stopTest();
        
 }
}
    
 private static testMethod void testsaveAddressBillinfo2(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Billing_PostalCode__c='10001';
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
         testAcc.Billing_PostalCode__c='10041';
		 testAccCtrl.populateCountyList12();
         testAccCtrl.populateCountyListOnChange();
         testAccCtrl.wrp.billCountyErr='err';
         testAccCtrl.saveAddressInfo();
		 Test.startTest();
		 System.assertEquals(NULL,testAccCtrl.wrp.billingCounty);
		 Test.stopTest();
        
 }
}
 
    /*
private static testMethod void testsaveAddressBillinfo3(){

  User testUser = Util02_TestData.createUser();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        Account testAcc = Util02_TestData.createGroupAccount();
		List<Geographical_Info__c> geoInfoList = new List<Geographical_Info__c>();
        Geographical_Info__c geoInfo1= Util02_TestData.createGeoRecord();
        Geographical_Info__c geoInfo2= Util02_TestData.createGeoRecord();
		geoInfo1.County__c='Test County1';
		geoInfo2.County__c='Test County2';
		geoInfo1.Zip_Code__c='10001';
		geoInfo2.Zip_Code__c='10001';
		geoInfoList.add(geoInfo1);
		geoInfoList.add(geoInfo2);
        testAcc.Billing_PostalCode__c='10001';
		System.runAs(testUser){
       
		Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc);
        Database.insert(geoInfoList);
		Test.setCurrentPageReference(new PageReference('Page.SGA_VFP15_CopyAddressInfo')); 
         System.currentPageReference().getParameters().put('id', testAcc.Id); 
		 ApexPages.StandardController  stdController = new ApexPages.StandardController(testAcc);
        SGA_AP64_ValidateNSaveAccAddr testAccCtrl= new SGA_AP64_ValidateNSaveAccAddr(stdController);
         testAccCtrl.setMode();
          System.debug('test account'+testAccctrl.accData);
		  List<selectOption> countyReceived= new List<selectOption>();
         testAccCtrl.populateCountyList();
		 testAccCtrl.populateCountyList12();
         testAccCtrl.clearBilling();
         SelectOption  optGot=testAccCtrl.wrp.physicalCounty[2];
         testAccCtrl.accData.Billing_County__c= optGot.getValue();
         testAccCtrl.saveAddressInfo();
		 Test.startTest();
		 System.assertEquals(3,testAccCtrl.wrp.physicalCounty.size());
		 Test.stopTest();
        
}
}*/
    
}