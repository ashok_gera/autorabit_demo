global class useraccountcreation implements vlocity_ins.VlocityOpenInterface {

        public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        boolean success = true;
        if (methodName == 'accountSync') {
            accountSync(inputMap,outMap,options);
        }else{
            success = false;
        }
        return success;
    }
    
    public void accountSync(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        map<string,object> accountPagedata = (map<string,object>)inputmap.get('accountPage');
        map<string,object> createAccountdata = (map<string,object>)accountPagedata.get('createAccount');
       // list<account> acclist = new list<account>();
            account ac = new account();
            ac.name = string.valueof(createAccountdata.get('ca_businessName'));
            ac.phone = string.valueof(createAccountdata.get('ca_phoneNumber'));
            //ac.PersonEmail = string.valueof(createAccountdata.get('ca_emailAddress'));
          //  acclist.add(ac);
            insert ac;
            
            outMap.put('ContextId',ac.id);
            
            system.debug('This is outmap'+outMap);
    }
    
}