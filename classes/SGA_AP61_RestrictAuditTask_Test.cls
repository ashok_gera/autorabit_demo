/*****************************************************************************
* Class Name  : SGA_AP61_RestrictAuditTask_Test
* Created By  : IDC Offshore
* Description : This class is the test class for SGA_AP61_RestrictAuditTask
* ****************************************************************************/
@isTest
private class SGA_AP61_RestrictAuditTask_Test {
    private static final string CLOSED = 'Closed';
    /*****************************************************************************
     * Method Name  : auditTaskPositiveTest
     * Deescription : This is for audit task create test
     *                Checking audit task is able to create for SCLC closed case
     * ***************************************************************************/
    private testMethod static void auditTaskPositiveTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            insert cs001List; 
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
            insert cs002List;
            Case sclcCase = Util02_TestData.insertSCLCase();
            Database.insert(sclcCase);
            sclcCase.Status = CLOSED;
            Database.update(sclcCase);
            List<Task> taskList = new List<Task>();
            for(Integer i=0;i<200;i++){
                Task taskObj = Util02_TestData.createAuditTask();
                taskObj.whatId = sclcCase.Id;
                taskList.add(taskObj);
            }
            Test.startTest();
            Database.insert(taskList);
            Test.stopTest();
            //checking the task is created successfully or not
            system.assertNotEquals(null, taskList[0].Id);
        }
    }
    /*****************************************************************************
     * Method Name  : sgaTaskPositiveTest
     * Deescription : This is for sga task create test
     *                Checking sga task is not able to create for closed case
     * ***************************************************************************/
    private testMethod static void sgaTaskPositiveTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            insert cs001List; 
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
            insert cs002List;
            Case sclcCase = Util02_TestData.insertSCLCase();
            Database.insert(sclcCase);
            sclcCase.Status = CLOSED;
            Database.update(sclcCase);
            List<Task> taskList = new List<Task>();
            for(Integer i=0;i<200;i++){
                Task taskObj = Util02_TestData.createSGATask();
                taskObj.whatId = sclcCase.Id;
                taskList.add(taskObj);
            }
            try{
                Test.startTest();
                Database.insert(taskList);
                Test.stopTest();
            }catch(Exception e){
            }
            //checking the task is not created
            system.assertEquals(null, taskList[0].Id);
        }
    }
    /*****************************************************************************
     * Method Name  : auditTaskNegativeTest
     * Deescription : This is for audit task create test
     *                Checking aduit task is not able to create for other cases
     *                except SCLC closed case
     * ***************************************************************************/
    private testMethod static void auditTaskNegativeTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            insert cs001List; 
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
            insert cs002List;
            //Account data creation
            Account testAccount = Util02_TestData.createGroupAccount();
            //Application record creation
            vlocity_ins__Application__c testApplication = Util02_TestData.createApplication();
            testApplication.Group_Coverage_Date__c = system.Today();
            database.insert(testAccount);
            database.insert(testApplication);
            Case testCaseIns = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            database.insert(testCaseIns);
            testCaseIns.Status = CLOSED;
            Database.update(testCaseIns);
            List<Task> taskList = new List<Task>();
            for(Integer i=0;i<200;i++){
                Task taskObj = Util02_TestData.createAuditTask();
                taskObj.whatId = testCaseIns.Id;
                taskList.add(taskObj);
            }
            try{
                Test.startTest();
                Database.insert(taskList);
                Test.stopTest();
            }catch(Exception e){}
            //checking the audit task is not created
            system.assertEquals(null, taskList[0].Id);
        }
    }
    /*****************************************************************************
     * Method Name  : auditTaskforOpenCaseNegTest
     * Deescription : This is for audit task create test
     *                Checking aduit task is not able to create for open SCLC case
     * ***************************************************************************/
    private testMethod static void auditTaskforOpenCaseNegTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            insert cs001List; 
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
            insert cs002List;
            Case sclcCase = Util02_TestData.insertSCLCase();
            Database.insert(sclcCase);
            List<Task> taskList = new List<Task>();
            for(Integer i=0;i<200;i++){
                Task taskObj = Util02_TestData.createAuditTask();
                taskObj.whatId = sclcCase.Id;
                taskList.add(taskObj);
            }
            try{
                Test.startTest();
                Database.insert(taskList);
                Test.stopTest();
            }catch(Exception e){}
            //Checking the task is not created
            system.assertEquals(null, taskList[0].Id);
        }
    }
}