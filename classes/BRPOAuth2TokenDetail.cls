public class BRPOAuth2TokenDetail{
 public String issued_at{get;set;}
 public String application_name{get;set;}
 public String status{get;set;}
 public String expires_in{get;set;}
 public String client_id{get;set;}    
 public String access_token{get;set;}
}