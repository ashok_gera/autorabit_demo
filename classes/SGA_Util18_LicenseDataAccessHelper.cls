/*****************************************************************************************
Class Name   : SGA_Util18_LicenseDataAccessHelper
Date Created : 01/02/2018
Created By   : IDC Offshore
Description  : This is the util data access class for License_Appointment__c object. Which is used to fetch 
the details from License_Appointment__c based on some parameters
******************************************************************************************/
public without sharing class SGA_Util18_LicenseDataAccessHelper{
public static Id contactId;
public static Id brokerId;
public static  String businessTrack;
public static  String state;
public static  String licenseVal;
public static  String contractVal;
public static  String contactTypeVal;
public static Set<String> idSet;
public static Set<String> idSet1;
/****************************************************************************************************
    Method Name : fetchLicenseList
    Parameters  : String selectQuery,String whereClause
    Return type : List<License_Appointment__c>
    Description : This method is used to fetch the License & Appointment records based on parameters passed.
    ******************************************************************************************************/
    public static List<License_Appointment__c > fetchLicenseList(String selectQuery,String whereClause){
    String dynaQuery = SG01_Constants.BLANK;
    List<License_Appointment__c> licenseList = NULL;
    dynaQuery = String.isNotBlank(selectQuery) ? selectQuery : SG01_Constants.BLANK;
    dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(whereClause) ? dynaQuery+=whereClause : dynaQuery;
    if(String.isNotBlank(dynaQuery))
        {
            licenseList = new List<License_Appointment__c>((List<License_Appointment__c >)Database.query(dynaQuery));
        }
        return licenseList;
    }
 }