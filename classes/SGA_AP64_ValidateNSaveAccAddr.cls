/************************************************************************
* Class Name  : SGA_AP64_ValidateNSaveAccAddr
* Created By  : IDC Offshore
* Created Date: 02/05/2018
* Description : It is used to Update and Validate Physical and Billing Address present in Account
* 
**********************************************************************/
public class SGA_AP64_ValidateNSaveAccAddr {
    public static final string FRWD_SLASH = '/';
    public static final string MODE_EDIT = 'Edit';
    public static final string MODE_READ = 'MainDetail';
    public boolean readMode{get;set;}
    public Account accData {get;set;}
    private List<Account> acListToUpdate;
    private static final String CLS_SGA_AP64 ='SGA_AP64_ValidateNSaveAccAddr';
    private static final String SGA_AP64_ACCINFO ='fetchExistingAccountInfo';
    public String pageBlockMode{get;set;}
    public Id accountId{get;set;}
    public String errMsg{get;set;}
    public Boolean errOnZip_County{get;set;}
    public static final String ZIP_ERR ='County not found for given Postal Code';
    public static final String BLANK_COUNTY_ERR ='Please select a county';
    public static final String INVALID_ZIP_ERR ='Please input a valid Zip Code';
    public Map<String,String> zipCodeMap = new Map<String,String>();
    public Static Map<String,String> countyErrMap = new Map<String,String>();
    private static final String PHY ='PHY';
    private static final String BILL ='BILL';
    public String changedZipCodeType{get;set;}
    public String selectedPhyCountyVar{get;set;}
    public String selectedPhyCountyValue{get;set;}
    public String selectedBillCountyValue{get;set;}
    public String selectedBillCounty{get;set;}
    public wrpCounties wrp{get;set;} 
    
    /**************************************
    *Constructor for the class.
    ************************************/

    public SGA_AP64_ValidateNSaveAccAddr(ApexPages.StandardController controller){
        accData = new Account();
        //acListToUpdate = new List<Account>();
        accountId = apexpages.currentpage().getparameters().get('id');
        accData = [Select Id,Name,Company_Street__c,Company_City__c,Company_State__c,Company_Zip__c,County__c,Billing_County__c,Billing_Street__c,Billing_City__c,Billing_State__c,Billing_PostalCode__c,BillingAddress,Mailing_City__c,Mailing_County__c,Mailing_State__c,Mailing_Street__c,Mailing_Zip__c,Rating_Area__c,Website  from Account Where Id =:accountId ];
        readMode =true;
    }
 
     public void populateCountyList12(){
          System.debug('options selectedCounty::>'+selectedPhyCountyVar);
          System.debug('current County WRP is :>'+wrp);
          wrp.billingCounty= wrp.physicalCounty;
          wrp.billCountyErr = wrp.phyCountyErr;
          accData.Billing_County__c=selectedPhyCountyVar;
		  wrp.billCountyNotSelectedErr = wrp.phyCountyNotSelectedErr;
    }
    
    public void setMode(){
         populateCountyList();
         readMode = false;
    }
     public void clearBilling(){
         wrp.billCountyErr = null;
        // accData.Billing_County__c =null;
         wrp.billingCounty =null;
		 wrp.billCountyNotSelectedErr=null;
    }    
     public Pagereference cancelEdit(){
         accData = [Select Id,Name,Company_Street__c,Company_City__c,Company_State__c,Company_Zip__c,County__c,Billing_County__c,Billing_Street__c,Billing_City__c,Billing_State__c,Billing_PostalCode__c,BillingAddress,Mailing_City__c,Mailing_County__c,Mailing_State__c,Mailing_Street__c,Mailing_Zip__c,Rating_Area__c,Website  from Account Where Id =:accountId ];
        
         readMode = true;
         return NULL;       
    }
    
     public PageReference saveAddressInfo() {
         acListToUpdate = new List<Account>();
         boolean phyBillCountyNtSelectedErr = false;
		 // Physical address Validation
         if(String.isNotBlank(accData.Company_Zip__c) && String.isNotBlank(wrp.phyCountyErr) ) {
                wrp.phyCountyErr = INVALID_ZIP_ERR;
				 readMode = false;
                 return NULL;
               
         }else{
			 wrp.phyCountyErr = NULL;
			 }
		if(String.isNotBlank(accData.Company_Zip__c) && String.isBlank(wrp.phyCountyErr) && String.isBlank(accData.County__c) || accData.County__c =='' ){
				wrp.phyCountyNotSelectedErr = BLANK_COUNTY_ERR + ' from available Physical County';
                  phyBillCountyNtSelectedErr = true;
		}else{
			 wrp.phyCountyNotSelectedErr = NULL;
			 }
		// Billing address Validation
		if(String.isNotBlank(accData.Billing_PostalCode__c) && String.isNotBlank(wrp.billCountyErr) ) {
                wrp.billCountyErr = INVALID_ZIP_ERR; //update the error msg
			       readMode = false;
                   return NULL;
         }else{
			 wrp.billCountyErr = NULL;
			 }
			 
		if(String.isNotBlank(accData.Billing_PostalCode__c) && String.isBlank(wrp.billCountyErr) && String.isBlank(accData.Billing_County__c) || accData.Billing_County__c =='' ){
				wrp.billCountyNotSelectedErr = BLANK_COUNTY_ERR + ' from available Billing County';
                phyBillCountyNtSelectedErr = true;  
		}else{
			 wrp.billCountyNotSelectedErr = NULL;
			 }
         
         if(phyBillCountyNtSelectedErr == true){
             readMode = false;
             return NULL;
         }
         System.debug('Changed Account Info in SAVE'+accData);
         System.debug('Changed zipCodeMap IN SAVE'+zipCodeMap);
      // perform DML         
         if( countyErrMap.get(PHY) == NULL && wrp.phyCountyErr == NULL && wrp.phyCountyNotSelectedErr ==NULL && wrp.billCountyNotSelectedErr==NULL ){
		 if(String.isBlank(wrp.billCountyErr) && String.isBlank(wrp.phyCountyErr) && String.isBlank(wrp.phyCountyNotSelectedErr) && String.isBlank(wrp.billCountyNotSelectedErr)){
			
			boolean phySideChek;
			boolean billSideChek;
			if(accData.Company_Zip__c != NULL && accData.County__c != NULL){
				phySideChek=checkValidZipCountyPair(accData.Company_Zip__c);
				if(phySideChek == false){
					wrp.phyCountyErr = INVALID_ZIP_ERR;
					readMode = false;
					wrp.physicalCounty = null;
			        return NULL;
				}else{
					wrp.phyCountyErr = '';
					changedZipCodeType='PHY';
					populateCountyListOnChange();
					if(wrp.physicalCounty.size() > 1 && String.isNotBlank(accData.County__c)){
						boolean checkSelected =false;
						String selectedPhy='';
						for(selectOption optSelected : wrp.physicalCounty ){
							if(optSelected.getValue() == accData.County__c ){
								checkSelected =true;
                                selectedPhy=accData.County__c;						
							}
						}
						if(checkSelected == true){
							// correct data entered
							System.debug('correct data entered'+  accData.County__c);
						}else{
							accData.County__c = selectedPhy;
							wrp.phyCountyNotSelectedErr = BLANK_COUNTY_ERR + ' from available Physical County';
						    return NULL;
						}
					}else{
						accData.County__c = wrp.physicalCounty.get(0).getValue();
					}
				}
			}
			if(accData.Billing_PostalCode__c != NULL && String.isNotBlank(accData.Billing_County__c)){
				billSideChek=checkValidZipCountyPair(accData.Billing_PostalCode__c);
				if(billSideChek == false){
					wrp.billCountyErr = INVALID_ZIP_ERR;
					readMode = false;
					wrp.billingCounty = null;
			        return NULL;
				}else{
					wrp.billCountyErr = '';
				    changedZipCodeType='BILL';
					populateCountyListOnChange();
					if(wrp.billingCounty.size() > 1 && String.isNotBlank(accData.Billing_County__c )){
						boolean checkBillelected =false;
						String selectedBill='';
						for(selectOption optSelected : wrp.billingCounty){
							if(optSelected.getValue() == accData.Billing_County__c){
								checkBillelected =true;	
								selectedBill=accData.Billing_County__c;
							}
						}
						if(checkBillelected == true){
							// correct data entered
							accData.Billing_County__c = selectedBill;
							System.debug('correct data entered'+  accData.Billing_PostalCode__c);
						}else{
							wrp.billCountyNotSelectedErr = BLANK_COUNTY_ERR + ' from available Billing County';
						    return NULL;
						}
					}else{
						accData.Billing_County__c=wrp.billingCounty.get(0).getValue();
					}
					
				}
			}
            acListToUpdate.add(accData);   
            Database.update(acListToUpdate);
            errOnZip_County =false;
            readMode = true;
			return NULL;
		  }
         }
         return NULL;
    }
    
    public static List<selectOption> getAvailableCounties(String zipCode,String county,String addressType) {
        List<selectOption> options = new List<selectOption>(); 
          countyErrMap = new Map<String,String>();
         Set<String> countySet= new Set<String>();
        List<Geographical_Info__c> geoInfo = new List<Geographical_Info__c>();
        if(String.isNotBlank(zipCode)){
            geoInfo=[SELECT Area__c,County__c,Id,Name,RatingArea__c,SearchField__c,State__c,Tech_Zipcode_State__c,Tech_Zip_State_County__c,Zip_Code__c FROM Geographical_Info__c Where Zip_Code__c=:zipCode] ;
        } 
        if(geoInfo !=NULL && !geoInfo.isEmpty()){
			if( geoInfo.size() == 1 && String.isNotBlank(geoInfo.get(0).County__c)){
				 options.add(new selectOption(geoInfo.get(0).County__c,geoInfo.get(0).County__c));
			}else{
				 options.add(new selectOption('', '- None -')); 
				  for(Geographical_Info__c gf:geoInfo){
                  if(String.isNotBlank(gf.County__c)){
                    options.add(new selectOption(gf.County__c,gf.County__c));
                }
              }
			}
        }else{
             countyErrMap.put(addressType,INVALID_ZIP_ERR);
           
        }
        System.debug('options Got::>'+options);
        System.debug('options countyErrMap::>'+countyErrMap);
        return options; //return the picklist options
    }
  
    
    public void populateCountyList(){
        wrp = new wrpCounties();
        zipCodeMap = new Map<String,String>();
        System.debug('Current Zip Provide'+accData.Company_Zip__c +' ' +accData.Billing_PostalCode__c);
           if(String.isNotBlank(accData.Company_Zip__c)){
               zipCodeMap.put(PHY,accData.Company_Zip__c);
             wrp.physicalCounty = getAvailableCounties(accData.Company_Zip__c,'',PHY);
           }
           if(String.isNotBlank(accData.Billing_PostalCode__c)){
              zipCodeMap.put(BILL,accData.Billing_PostalCode__c);
                wrp.billingCounty = getAvailableCounties(accData.Billing_PostalCode__c,'',BILL);
           }        
    }
    public void setSelectedErr(){
         System.debug('zipCodeMap in setSelectedErr UI::>'+zipCodeMap);
        System.debug('selectedPhyCountyValue in setSelectedErr UI::>'+selectedPhyCountyValue);
        if( String.isNotBlank(wrp.phyCountyNotSelectedErr) && String.isNotBlank(selectedPhyCountyValue)){
            wrp.phyCountyNotSelectedErr = null;
        }     
    }
     public void setBillSelectedErr(){
         System.debug('zipCodeMap in setSelectedErr UI::>'+zipCodeMap);
        System.debug('selectedPhyCountyValue in setSelectedErr UI::>'+selectedBillCountyValue);
        if( String.isNotBlank(wrp.billCountyNotSelectedErr) && String.isNotBlank(selectedBillCountyValue)){
            wrp.billCountyNotSelectedErr = null;
        }     
    }
    public void populateCountyListOnChange(){
        if(String.isNotBlank(accData.Company_Zip__c) && changedZipCodeType != NULL){
			wrp.phyCountyNotSelectedErr=''; 
            if(changedZipCodeType == PHY ){
                   //accData.County__c =null;
                 // wrp.physicalCounty =null;
            }
             zipCodeMap.put(PHY,accData.Company_Zip__c);
             wrp.physicalCounty = getAvailableCounties(accData.Company_Zip__c,'',PHY);
            if(!countyErrMap.isEmpty() && countyErrMap.containsKey(PHY) ){
                  wrp.phyCountyErr=countyErrMap.get(PHY);
                 
                  wrp.physicalCounty = null;
            }else{
				 wrp.phyCountyErr ='';       
			} 
           
        }else{
             zipCodeMap.put(PHY,'');
            wrp.physicalCounty = NULL;
        }
        if(String.isNotBlank(accData.Billing_PostalCode__c) && changedZipCodeType != NULL){
			   	wrp.billCountyNotSelectedErr=''; 
                zipCodeMap.put(BILL,accData.Billing_PostalCode__c);
                wrp.billingCounty = getAvailableCounties(accData.Billing_PostalCode__c,'',BILL);
                if(!countyErrMap.isEmpty() && countyErrMap.containsKey(BILL) ){
                  wrp.billCountyErr=countyErrMap.get(BILL);
                 }else{
				      wrp.billCountyErr ='';
			     }
           }else{
             zipCodeMap.put(BILL,'');
             wrp.billingCounty = NULL;
        }
           
    }
    public static boolean checkValidZipCountyPair(String zipRecv){
		List<Geographical_Info__c> gfCheck= new List<Geographical_Info__c>();
		boolean  validZip= false;
         System.debug('checkValidZipCountyPair::>'+zipRecv);
        if(String.isNotBlank(zipRecv)){
         gfCheck =[SELECT Area__c,County__c,Id,Name,RatingArea__c,SearchField__c,State__c,Tech_Zipcode_State__c,Tech_Zip_State_County__c,Zip_Code__c FROM Geographical_Info__c Where Zip_Code__c=:zipRecv] ;
		 if(!gfCheck.isEmpty()){
			 validZip = true; 
		 }
        }  
		 return validZip;
    }
    
    public Class wrpCounties {
        public List<selectOption> physicalCounty{get;set;}
        public List<selectOption> billingCounty{get;set;}
        public String phyCountyErr{get;set;}
        public String phyCountyNotSelectedErr{get;set;}
        public String billCountyNotSelectedErr{get;set;}
        public String billCountyErr{get;set;}
    }
   
}