/*******************************************************************************************
* Class Name  : SGA_AP32_UpdateCaseStageTechFields_test
* Created By  : IDC Offshore
* CreatedDate : 8/4/2017
* Description : This test class for SGA_AP32_UpdateCaseStageTechFieldsOnAcc
********************************************************************************************/
@isTest
private class SGA_AP32_UpdateCaseStageTechFields_test {
    private static final String STATE_VALUE = 'CO';
    private static final String STAGE_VALUE1 = 'Products Loaded';
    private static final String STAGE_SC_VALUE1 = 'Scrub Completed';
    private static final String STAGE_VALUE = 'Vendor Return';
    private static final String STAGE_SC_VALUE = 'Payment Processed';
    private static final String STATE_VALUE_NY = 'NY';
    private static final String STATE_VALUE_CA = 'CA';
    private static final String STATE_VALUE_CO = 'CO';
        
        
    /************************************************************************
    * Method Name : createAndUpdateCasePositiveTest
    * Description : To test the stage and state changes 
    *                and creation/updation of case
    ************************************************************************/
    private testMethod static void createAndUpdateCasePositiveTest(){
        User testUser = Util02_TestData.createUser();   

        Account testAccount = Util02_TestData.createGroupAccountForRelatedList(); 
        //Account record creation

        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, System.today() +5);    //Application record creation

        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();  
        //Business Custom Setting insertion

        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data(); 
        //Record Type Assignment Custom Setting insertion
        
        System.runAs(testUser){
        
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.Insert(testApplication);
        Database.insert(testAccount);
            
        Case testCase0 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id);
        Insert testCase0; 
            
        Account accountObj0 = [SELECT id, Tech_Case_Stage_Is_PL__c, Tech_Case_Stage_Is_SC__c FROM Account where id=:testAccount.Id];
            
        Case updateCase0 = [select id,status,stage__c from case where id=:testCase0.id];
        updateCase0.Stage__c = STAGE_VALUE;
        update updateCase0;
                        
        Case updateCase1 = [select id,status,stage__c from case where id=:testCase0.id];
        updateCase1.BR_State__c = STATE_VALUE;
        update updateCase1;
            

        Test.startTest();
        Case testCase1 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id); 
        //Inserting new case with Poducts Loaded and State CO
        testCase1.Stage__c = STAGE_VALUE;
        testCase1.BR_State__c = STATE_VALUE;
        Insert testCase1; 

        Case testCase2 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id); 
        //Inserting new case with Poducts Loaded and State NY
        testCase2.Stage__c = STAGE_VALUE;
        Insert testCase2;
            
        Case updateCase3 = [select id,status,stage__c from case where id=:testCase2.id];
        updateCase3.Stage__c = STAGE_SC_VALUE;
        updateCase3.BR_State__c = STATE_VALUE_NY;
        
        update updateCase3;
            
        Test.stopTest();
            }   
    }
        
    /************************************************************************
    * Method Name : createAndUpdateCaseNegativeTest
    * Description : To test the stage and state changes 
    *                and creation/updation of case
    * Change History: Created Method to satify Else COnditions with different State and Stage Value
    ************************************************************************/
    private testMethod static void createAndUpdateCaseNegativeTest(){
        User testUser = Util02_TestData.createUser();   

        Account testAccount = Util02_TestData.createGroupAccountForRelatedList(); 
        //Account record creation

        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, System.today() +5);    //Application record creation

        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();  
        //Business Custom Setting insertion

        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data(); 
        //Record Type Assignment Custom Setting insertion
        
        System.runAs(testUser){
        
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.Insert(testApplication);
        Database.insert(testAccount);
            
        Case testCase0 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id);
        Insert testCase0; 
            
        Account accountObj0 = [SELECT id, Tech_Case_Stage_Is_PL__c, Tech_Case_Stage_Is_SC__c FROM Account where id=:testAccount.Id];
            
        Case updateCase0 = [select id,status,stage__c from case where id=:testCase0.id];
        updateCase0.Stage__c = STAGE_VALUE;
        update updateCase0;
                        
        Case updateCase1 = [select id,status,stage__c from case where id=:testCase0.id];
        updateCase1.BR_State__c = STATE_VALUE;
        update updateCase1;
            

        Test.startTest();
        Case testCase1 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id);
            //Inserting new case with Poducts Loaded and State CO
        testCase1.Stage__c = STAGE_VALUE1;
        testCase1.BR_State__c = STATE_VALUE_CA;
        Insert testCase1; 

        Case testCase2 = Util02_TestData.insertSGACase(testAccount.Id,testApplication.Id); 
            //Inserting new case with Poducts Loaded and State NY
        testCase2.Stage__c = STAGE_VALUE1;
        Insert testCase2;
            
        Case updateCase3 = [select id,status,stage__c from case where id=:testCase2.id];
        updateCase3.Stage__c = STAGE_SC_VALUE1;
        updateCase3.BR_State__c = STATE_VALUE_CO;
        
        update updateCase3;
            
        Test.stopTest();
            }   
    }
  }