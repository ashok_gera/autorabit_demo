/********************************************************************************************************************
* Class Name  : SGA_AP58_LicenseAppt_AccUpdate
* Created By  : IDC Offshore
* Created Date: 12/11/2017
* Description : 1. Apex class updates Account's Tech Appointment effective date field with the License Appointment's
*                Start Date value
*********************************************************************************************************************/
public without sharing class SGA_AP58_LicenseAppt_AccUpdate{    
    public static final string CLS_NAME = 'SGA_AP58_LicenseAppt_AccUpdate';
    public static final string MTD1 = 'insertAccTechAppointment';
    public static final string MTD2 = 'updateAccTechAppointment';
    
     /******************************************************************************************
    * Method Name  : insertAccTechAppointment
    * Parameters  : List<License_Appointment__c> lapList
    * Return type : Void
    * Description  : Insert Scenaior of Liscence Appointment
    *******************************************************************************************/
    public static void insertAccTechAppointment(List<License_Appointment__c> lapList){
        set<id> accIdSet = new set<id>();
        date tempDate;
        map<id, date> accDateMap = new map<id, date>();
        try{
            for(License_Appointment__c lap: lapList){
                system.debug('lap.SGA_AgencyBrokerage__c   '+lap.SGA_AgencyBrokerage__c);
                system.debug('lap.BR_Start_Date__c   '+lap.BR_Start_Date__c);
                if(lap.SGA_AgencyBrokerage__c != NULL && lap.BR_Start_Date__c != NULL){
                    accDateMap.put(lap.SGA_AgencyBrokerage__c, lap.BR_Start_Date__c);
                }
            }
            if(!accDateMap.isEmpty()){
                executeMtd(accDateMap);
            }
        }
        catch(exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME, CLS_NAME, MTD1, SG01_Constants.BLANK,Logginglevel.ERROR);}    
    }
    /******************************************************************************************
    * Method Name  : executeMtd
    * Parameters  : map<id, date> accDateMap
    * Return type : Void
    * Description  : Method Updates the Account's Tech business field
    *******************************************************************************************/
    private static void executeMtd(map<id, date> accDateMap){
        List<Account> accList = new List<Account>();
        date tempDate;
        List<Account> accUpdateList = new List<Account>();
        
        accList = [Select id, Tech_Appointment_Effective_Date__c from Account where id IN: accDateMap.keyset() LIMIT 2000];
        
        for(Account acc: accList){
            tempDate = accDateMap.get(acc.id);
            acc.Tech_Appointment_Effective_Date__c = tempdate;
            accUpdateList.add(acc);
        }
        if(!accUpdateList.isEmpty()){
            database.update(accUpdateList, false);
        }
    }
}