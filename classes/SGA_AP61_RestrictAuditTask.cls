/*****************************************************************************
* Class Name  : SGA_AP61_RestrictAuditTask
* Created By  : IDC Offshore
* Description : This class is used for SGQUOTING Task
* 				 validation. This will be called from TaskBeforeInsert trigger
* ****************************************************************************/
public class SGA_AP61_RestrictAuditTask {
    private static final string CLOSE_SCLC_RECTYPE = 'Close - SG SCLC Inquiry';
    private static final string SGA_TASK = 'SGA Task';
    private static final String CLASS_NAME = 'SGA_AP61_RestrictAuditTask';
    private static final String METHOD_NAME = 'restrictAuditforOpenCase';
    /****************************************************************************
    * Method Name : restrictAuditforOpenCase
    * Parameters  : List<Task>
    * Description : This method will be called from TaskBeforeInsert trigger.
    * 				 to check Audit task is only creating for closed sclc cases.
    * 				 And SGA Task is not creating for closed case.
    * ************************************************************************/
    public static void restrictAuditforOpenCase(List<Task> taskList){
        try{
            Id auditRecTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(System.Label.SG126_AuditRecType).getRecordTypeId();
            Id sclcRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CLOSE_SCLC_RECTYPE).getRecordTypeId();        
            Id sgaTaskRecTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(SGA_TASK).getRecordTypeId();        
            Set<ID> caseIds = new Set<ID>();
            //to fetch the case ids
            for(Task taskObj : taskList){
                if(taskObj.whatId != NULL && String.valueOf(taskObj.whatId).startsWith(System.Label.SG125_CasePrefix)){
                    caseIds.add(taskObj.whatId);
                }
            }
            //to fetch the cases
            Map<ID,Case> caseMap = new Map<ID,Case>([SELECT ID,STATUS,RecordTypeId FROM CASE WHERE ID IN :caseIds]);
            
            for(Task taskObj : taskList){
                if(taskObj.whatId != NULL && String.valueOf(taskObj.whatId).startsWith(System.Label.SG125_CasePrefix))
				{
                    if(taskObj.RecordTypeId == auditRecTypeId && !System.Label.SG124_Closed.equalsIgnoreCase(caseMap.get(taskObj.whatId).Status))
					{
						taskObj.addError(System.Label.SG123_AuditTaskError);
					}else if(taskObj.RecordTypeId == auditRecTypeId && System.Label.SG124_Closed.equalsIgnoreCase(caseMap.get(taskObj.whatId).Status) && sclcRecTypeId != caseMap.get(taskObj.whatId).RecordTypeId)
					{
						taskObj.addError(System.Label.SG127_AduitTaskError_1);
					}else if(taskObj.RecordTypeId == sgaTaskRecTypeId && System.Label.SG124_Closed.equalsIgnoreCase(caseMap.get(taskObj.whatId).Status))
					{
						taskObj.addError(System.Label.SG128_ClosedCaseTaskError);
					}
                }
            }
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,CLASS_NAME, METHOD_NAME, SG01_Constants.BLANK, Logginglevel.ERROR); }
    }
}