global with sharing class SGA_AP63_FetchBrokerPAGAPickLists implements vlocity_ins.VlocityOpenInterface{

     public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;

        try{         
            if(methodName == 'getGeneralAgencyList') {
                getGeneralAgencyList(inputMap, outMap, options);
            }
            if(methodName == 'getPaidAgencyList') {
                getPaidAgencyList(inputMap, outMap, options);
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        return true;
        }
        
        public void getGeneralAgencyList(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options)

            {
            List<Map<String,String>> ancyoptions= new List<Map<String, String>>();
            
            for (Account gacy: [SELECT Id, Name FROM Account WHERE AgencyType__c = 'General Agency'])
            
            { 
                Map<String, String> tempMap = new Map<String, String>();
                
                tempMap.put('name', gacy.Id); // Language Independent
                tempMap.put('value', gacy.Name); // Displayed in Picklist UI
                //tempMap.put('GeneralAgencyId', gacy.Id);
                //tempMap.put('GeneralAgencyTIN', (String)gacy.get('BR_Encrypted_TIN__c'));
                //tempMap.put('GeneralAgencySearch', (String)gacy.get('AccountTypeAhead__c'));
                
                ancyoptions.add(tempMap);
            }
            outMap.put('options',ancyoptions);
            }
            
            public void getPaidAgencyList(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options)

            {
            List<Map<String,String>> ancyoptions= new List<Map<String, String>>();
            
            for (Account pacy: [ SELECT Id, Name FROM Account WHERE AgencyType__c = 'Paid Agency'])
            
            { 
                Map<String, String> tempMap = new Map<String, String>();
                
                tempMap.put('name', pacy.Id); // Language Independent
                tempMap.put('value', pacy.Name); // Displayed in Picklist UI
                //tempMap.put('PaidAgencyId', pacy.Id);
                //tempMap.put('PaidAgencyTIN', (String)pacy.get('BR_Encrypted_TIN__c'));
                //tempMap.put('PaidAgencySearch', (String)pacy.get('AccountTypeAhead__c'));
                
                ancyoptions.add(tempMap);
            }
            outMap.put('options',ancyoptions);
            }

/*
    @RemoteAction
    global static List<Account> getGeneralAgencyList(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        //String searchName = (String) options.get('searchString');
        //String searchETIN = '%'+searchName+'%';

        List<Map<String, String>> returnList = new List<Map<String,String>>();
        List<Account> accList = new List<Account>();
        Set<Account> accSet = new Set<Account>();

          List<Account> accFetchList = [SELECT Id, Name, AccountTypeAhead__c, BR_Encrypted_TIN__c, AgencyType__c FROM Account WHERE AgencyType__c = 'General Agency'];
          accSet.addAll(accFetchList);
          System.debug('The accFetchList is: ' + accFetchList);

          //accList = ((List<SObject>)accQueryList[0]);
          accList.addAll(accSet);
          if(accList != null && accList.size() > 0){
              for (Account a : accList){

                  if((String)a.get('AgencyType__c') == 'General Agency'){
                    Map<String,String> tempMap = new Map<String,String>();
                    tempMap.put('GeneralAgencyId', a.Id);
                    tempMap.put('GeneralAgency', (String)a.get('Name'));
                    tempMap.put('GeneralAgencyTIN', (String)a.get('BR_Encrypted_TIN__c'));
                    tempMap.put('GeneralAgencySearch', (String)a.get('AccountTypeAhead__c'));
                    returnList.add(tempMap);
                  }
              }
          }
          System.debug('The accMap is: ' + returnList);
          outMap.put('OBDRresp', returnList);       

        //accList.sort();
        return accList;
    }

    @RemoteAction
    global static List<Account> getPaidAgencyList(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
       
        List<Map<String, String>> returnList = new List<Map<String,String>>();
        List<Account> accList = new List<Account>();
        Set<Account> accSet = new Set<Account>();

         List<Account> accFetchList = [SELECT Id, Name, AccountTypeAhead__c, BR_Encrypted_TIN__c, AgencyType__c FROM Account WHERE AgencyType__c = 'Paid Agency'];
          accSet.addAll(accFetchList);
          System.debug('The accFetchList is: ' + accFetchList);

          //accList = ((List<SObject>)accQueryList[0]);
          accList.addAll(accSet);
          if(accList != null && accList.size() > 0){
              for (Account a : accList){

                  if((String)a.get('AgencyType__c') == 'Paid Agency'){
                    Map<String,String> tempMap = new Map<String,String>();
                    tempMap.put('PaidAgencyId', a.Id);
                    tempMap.put('PaidAgency', (String)a.get('Name'));
                    tempMap.put('PaidAgencyTIN', (String)a.get('BR_Encrypted_TIN__c'));
                    tempMap.put('PaidAgencySearch', (String)a.get('AccountTypeAhead__c'));
                    returnList.add(tempMap);
                  }

              }
          }
          System.debug('The accMap is: ' + returnList);
          outMap.put('OBDRresp', returnList);
        //accList.sort();
        return accList;
    }*/

}