@isTest(seeAllData = false)
public class ValidateSelectionServiceTest{
    public testmethod static void ValidateSelectionServiceTestMethod(){
        
        Map<String,Object> MedicalinputMap = new Map<String,Object>();
        Map<String,Object> DentalinputMap = new Map<String,Object>();
        Map<String,Object> VisioninputMap = new Map<String,Object>();
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        Map<String,Object> SelectMedicalinputMap = new Map<String,Object>();
        Map<String,Object> SelectedMedicalinputMap = new Map<String,Object>();
        Map<String,Object> SelectDentalinputMap = new Map<String,Object>();
        Map<String,Object> SelectVisioninputMap = new Map<String,Object>();
        Map<String,Object> ZipCodeinputMap = new Map<String,Object>();
        Map<String,Object> DuplicateAccountinputMap = new Map<String,Object>();
        Map<String,Object> MedicalEmptyinputMap = new Map<String,Object>();
        Map<String,Object> DentalEmptyinputMap = new Map<String,Object>();
        Map<String,Object> VisionEmptyinputMap = new Map<String,Object>();
        Map<String,Object> SelectMedicalEmptyinputMap = new Map<String,Object>();
        Map<String,Object> SelectDentalEmptyinputMap = new Map<String,Object>();
        Map<String,Object> SelectVisionEmptyinputMap = new Map<String,Object>();
        
        Product2 prod = new Product2(Name = 'Test Product');
        List<Product2> MedicalAccounts =  new List<Product2>();
        List<Product2> DentalAccounts =  new List<Product2>();
        List<Product2> VisionAccounts =  new List<Product2>();
        
        List<Product2> EmptyAccounts =  null;
        Boolean Rider29Prod = true;
        
        Map<String,Object> MedicalCoverage = new Map<String,Object>();
        Map<String,Object> DentalCoverage = new Map<String,Object>();
        Map<String,Object> VisionCoverage = new Map<String,Object>();
        Map<String,Object> SelectMedicalCoverage = new Map<String,Object>();
        Map<String,Object> SelectedMedicalCoverage = new Map<String,Object>();
        Map<String,Object> SelectDentalCoverage = new Map<String,Object>();
        Map<String,Object> SelectVisionCoverage = new Map<String,Object>();
        Map<String,Object> MedicalEmptyCoverage = new Map<String,Object>();
        Map<String,Object> DentalEmptyCoverage = new Map<String,Object>();
        Map<String,Object> VisionEmptyCoverage = new Map<String,Object>();
        Map<String,Object> SelectMedicalEmptyCoverage = new Map<String,Object>();
        Map<String,Object> SelectDentalEmptyCoverage = new Map<String,Object>();
        Map<String,Object> SelectVisionEmptyCoverage = new Map<String,Object>();
        
        MedicalAccounts.add(prod);
        MedicalAccounts.add(prod);
        MedicalAccounts.add(prod);
        MedicalAccounts.add(prod);
        MedicalCoverage.put('MedicalAccounts', MedicalAccounts);
        MedicalinputMap.put('MedicalCoverage', MedicalCoverage);
        SelectMedicalCoverage.put('output2', MedicalAccounts);
        SelectedMedicalCoverage.put('isConflictRider29Plans', Rider29Prod);
        SelectedMedicalinputMap.put('SelectMedicalCoverage', SelectedMedicalCoverage);
        SelectMedicalinputMap.put('SelectMedicalCoverage', SelectMedicalCoverage);
        MedicalEmptyCoverage.put('MedicalAccounts', EmptyAccounts);
        MedicalEmptyinputMap.put('MedicalCoverage', MedicalEmptyCoverage);
        SelectMedicalEmptyCoverage.put('output2', EmptyAccounts);
        SelectMedicalEmptyinputMap.put('SelectMedicalCoverage', SelectMedicalEmptyCoverage);
        
        DentalAccounts.add(prod);
        DentalAccounts.add(prod);
        DentalAccounts.add(prod);
        DentalCoverage.put('DentalAccounts', DentalAccounts);
        DentalinputMap.put('DentalCoverage', DentalCoverage);
        SelectDentalCoverage.put('output3', DentalAccounts);
        SelectDentalinputMap.put('SelectDentalPlans', SelectDentalCoverage);
        DentalEmptyCoverage.put('DentalAccounts', EmptyAccounts);
        DentalEmptyinputMap.put('DentalCoverage', DentalEmptyCoverage);
        SelectDentalEmptyCoverage.put('output3', EmptyAccounts);
        SelectDentalEmptyinputMap.put('SelectDentalPlans', SelectDentalEmptyCoverage);
        
        VisionAccounts.add(prod);
        VisionAccounts.add(prod);
        VisionCoverage.put('VisionAccounts', VisionAccounts);
        VisioninputMap.put('VisionCoverage', VisionCoverage);
        SelectVisionCoverage.put('output4', VisionAccounts);
        SelectVisioninputMap.put('SelectVisionPlans', SelectVisionCoverage);
        VisionEmptyCoverage.put('VisionAccounts', EmptyAccounts);
        VisionEmptyinputMap.put('VisionCoverage', VisionEmptyCoverage);
        SelectVisionEmptyCoverage.put('output4', EmptyAccounts);
        SelectVisionEmptyinputMap.put('SelectVisionPlans', SelectVisionEmptyCoverage);
        
        Map<String,Object> Step = new Map<String,Object>();
        Integer CheckZipCode = 2;
        Step.put('CheckZipCode', CheckZipCode);
        ZipCodeinputMap.put('Step', Step);
        
        test.startTest();
        ValidateSelectionService vss = new ValidateSelectionService();
        
        Boolean validateMedical = vss.invokeMethod('validateMedical', MedicalinputMap, outMap, options);
        System.assertEquals(validateMedical, true);
        
        Boolean validateSelectedMedical = vss.invokeMethod('validateSelectedMedical', MedicalinputMap, outMap, options);
        System.assertEquals(validateSelectedMedical, true);
        
        Boolean validateSelectedMedicalRider = vss.invokeMethod('validateSelectedMedical', SelectedMedicalinputMap, outMap, options);
        System.assertEquals(validateSelectedMedicalRider, true);
        
        Boolean validateDental = vss.invokeMethod('validateDental', DentalinputMap, outMap, options);
        System.assertEquals(validateDental, true);
        
        Boolean validateVision = vss.invokeMethod('validateVision', VisioninputMap, outMap, options);
        System.assertEquals(validateVision, true);
        
        Boolean validateQuoteMedical = vss.invokeMethod('validateQuoteMedical', SelectMedicalinputMap, outMap, options);
        System.assertEquals(validateQuoteMedical, true);
        
        Boolean validateQuoteDental = vss.invokeMethod('validateQuoteDental', SelectDentalinputMap, outMap, options);
        System.assertEquals(validateQuoteDental, true);
        
        Boolean validateQuoteVision = vss.invokeMethod('validateQuoteVision', SelectVisioninputMap, outMap, options);
        System.assertEquals(validateQuoteVision, true);
        
        Boolean validateZipCode = vss.invokeMethod('validateZipCode', ZipCodeinputMap, outMap, options);
        System.assertEquals(validateZipCode, true);
        
        Boolean validateMedicalEmpty = vss.invokeMethod('validateMedical', MedicalEmptyinputMap, outMap, options);
        System.assertEquals(validateMedicalEmpty, true);
        
        Boolean validateDentalEmpty = vss.invokeMethod('validateDental', DentalEmptyinputMap, outMap, options);
        System.assertEquals(validateDentalEmpty, true);
        
        Boolean validateVisionEmpty = vss.invokeMethod('validateVision', VisionEmptyinputMap, outMap, options);
        System.assertEquals(validateVisionEmpty, true);
        
        Boolean validateQuoteMedicalEmpty = vss.invokeMethod('validateQuoteMedical', SelectMedicalEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteMedicalEmpty, true);
        
        Boolean validateQuoteDentalEmpty = vss.invokeMethod('validateQuoteDental', SelectDentalEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteDentalEmpty, true);
        
        Boolean validateQuoteVisionEmpty = vss.invokeMethod('validateQuoteVision', SelectVisionEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteVisionEmpty, true);
        
        Boolean validateQuoteMedicalEmptyCACO = vss.invokeMethod('validateDuplicateMedicalCACO', SelectMedicalEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteMedicalEmptyCACO, true);
        
        Boolean validateQuoteDentalEmptyCACO = vss.invokeMethod('validateDuplicateDentalCACO', SelectDentalEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteDentalEmptyCACO, true);
        
        Boolean validateQuoteVisionEmptyCACO = vss.invokeMethod('validateDuplicateVisionCACO', SelectVisionEmptyinputMap, outMap, options);
        System.assertEquals(validateQuoteVisionEmptyCACO, true);
        
        test.stopTest();
    }
    
    public testmethod static void duplicateAccountTest(){
        User u = Util02_TestData.createUser();
        system.runAs(u){
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            insert cs001List; 
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
            insert cs002List;
            Id grpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
            Account a = new Account(Name='Test Account Name');
            a.RecordTypeId = grpRecordTypeId; 
            insert a;
            
            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
            insert c;
            
            User user = new User();
            user.ProfileID = [Select Id From Profile Where Name='Anthem SG Broker'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first';
            user.LastName = 'last';
            user.Username = 'test@testssss.com';   
            user.CommunityNickname = 'testUser123';
            user.Alias = 't1';
            user.Email = 'no@email.com';
            user.IsActive = true;
            user.ContactId = c.Id;
            
            insert user;
            
            Account acc = Util02_TestData.createGroupAccount();
            Insert acc;
            AccountTeamMember actTeam = new AccountTeamMember(AccountId=acc.Id,TeamMemberRole = 'Broker',UserId=user.Id);
            Insert actTeam;
            Map<String,Object> outMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            Map<String,Object> ZipCodeinputMap = new Map<String,Object>();
            Map<String,Object> TypeAheadBrokerBlock = new Map<String,Object>();
            Map<String,Object> Step = new Map<String,Object>();
            TypeAheadBrokerBlock.put('BrokerId', user.ContactId);
            Step.put('TypeAheadBroker-Block', TypeAheadBrokerBlock);
            Step.put('TaxID', '345234123');
            Integer CheckZipCode = 1;
            Step.put('CheckZipCode', CheckZipCode);
            ZipCodeinputMap.put('Step', Step);
            
            test.startTest();
            ValidateSelectionService vss = new ValidateSelectionService();
            Boolean validateZipCode = vss.invokeMethod('validateZipCode', ZipCodeinputMap, outMap, options);
            System.assertEquals(validateZipCode, true);
            test.stopTest();
        }
        
    }
    
    public testmethod static void medicalDentalVisionCACOTest(){
        
        Map<String,Object> MedicalinputMap = new Map<String,Object>();
        Map<String,Object> DentalinputMap = new Map<String,Object>();
        Map<String,Object> VisioninputMap = new Map<String,Object>();
        Map<String,Object> outMap = new Map<String,Object>();
        Map<String,Object> options = new Map<String,Object>();
        List<SGA_State_Networks__c> networkList = new List<SGA_State_Networks__c>();
        SGA_State_Networks__c nw1 = new SGA_State_Networks__c(name='CA_California Care HMO',Product_Family__c='HMO');
        SGA_State_Networks__c nw2 = new SGA_State_Networks__c(name='CA_Select HMO',Product_Family__c='HMO');
        SGA_State_Networks__c nw3 = new SGA_State_Networks__c(name='CA_Prudent Buyer PPO',Product_Family__c='PPO');
        SGA_State_Networks__c nw4 = new SGA_State_Networks__c(name='CA_Select PPO',Product_Family__c='PPO');
        networkList.add(nw1);
        networkList.add(nw3);
        networkList.add(nw2);
        networkList.add(nw4);
        Database.insert(networkList);
        Product2 prod = new Product2(Name = 'Test Product',network__c='Select HMO');
        Product2 prod1 = new Product2(Name = 'Test Product',network__c='California Care HMO');
        insert prod;
        insert prod1;
        Product2 prod2 = new Product2(Name = 'Test Product',network__c='Select PPO');
        Product2 prod3 = new Product2(Name = 'Test Product',network__c='Select PPO');
        insert prod2;
        insert prod3;
        Map<String,Object> MedicalCoverage = new Map<String,Object>();
        Map<String,Object> selectMedicalCoverage = new Map<String,Object>();
        Map<String,Object> DentalCoverage = new Map<String,Object>();
        Map<String,Object> selectDentalCoverage = new Map<String,Object>();
        Map<String,Object> VisionCoverage = new Map<String,Object>();
		Map<String,Object> selectVisionCoverage = new Map<String,Object>();
        
        MedicalinputMap.put('qState','CA');
		MedicalCoverage.put('MedicalPlansCount',2);
        DentalCoverage.put('DentalPlansCount',2);
        VisionCoverage.put('VisionPlansCount',2);
        List<Object> medObjList = new List<Object>();
        List<Object> denObjList = new List<Object>();
        List<Object> visObjList = new List<Object>();
        
        String innerSelectMed = '[{"InputContractCodeMedical": "302U","SelectProductCodeLookupMedical": "'+prod.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"}]';
        List<object> data = (List<Object>)(JSON.deserializeUntyped(innerSelectMed));
                                                         
        String innerSelectDen = '[{"InputContractCodeMedical": "302U","SelectProductCodeLookupMedical": "'+prod.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"}]';
        
		String innerSelectVis = '[{"InputContractCodeMedical": "302U","SelectProductCodeLookupMedical": "'+prod.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod1.Id+'"}]';
        
        List<object> dendata = (List<Object>)(JSON.deserializeUntyped(innerSelectDen));
        
        List<object> visdata = (List<Object>)(JSON.deserializeUntyped(innerSelectVis));
        
		String innerSelectMed1 = '[{"InputContractCodeMedical": "302U","SelectProductCodeLookupMedical": "'+prod2.Id+'"},'+
        '{ "InputContractCodeMedical": "3004","SelectProductCodeLookupMedical": "'+prod3.Id+'"}]';
        
        String innerSelectden1 = '[{"InputContractCodeDental": "302U","SelectProductCodeLookupDental": "'+prod2.Id+'"},'+
        '{ "InputContractCodeDental": "3004","SelectProductCodeLookupDental": "'+prod3.Id+'"}]';
        
        String innerSelectvis1 = '[{"InputContractCodeVision": "302U","SelectProductCodeLookupVision": "'+prod2.Id+'"},'+
        '{ "InputContractCodeVision": "3004","SelectProductCodeLookupVision": "'+prod3.Id+'"}]';
        
        List<object> data1 = (List<Object>)(JSON.deserializeUntyped(innerSelectMed1));
        List<object> dendata1 = (List<Object>)(JSON.deserializeUntyped(innerSelectden1));
        List<object> visdata1 = (List<Object>)(JSON.deserializeUntyped(innerSelectvis1));
        selectMedicalCoverage.put('InnerSelectMedical',data);
        MedicalCoverage.put('SelectMedical',selectMedicalCoverage);
        
        MedicalinputMap.put('MedicalCoverage', MedicalCoverage);

		selectDentalCoverage.put('InnerSelectDental',dendata);
        DentalCoverage.put('SelectDental',selectDentalCoverage);
        

        DentalinputMap.put('DentalCoverage', DentalCoverage);
   
        
		selectVisionCoverage.put('InnerSelectVision',visdata);
        VisionCoverage.put('SelectVision',selectVisionCoverage);
        VisioninputMap.put('VisionCoverage', VisionCoverage);
        
        
        test.startTest();
        ValidateSelectionService vss = new ValidateSelectionService();
        
        Boolean validateQuoteMedicalEmptyCACO = vss.invokeMethod('validateDuplicateMedicalCACO', MedicalinputMap, outMap, options);
        System.assertEquals(validateQuoteMedicalEmptyCACO, true);
        selectMedicalCoverage.put('InnerSelectMedical',data1);
        Boolean validateQuoteMedicalEmptyCACO1 = vss.invokeMethod('validateDuplicateMedicalCACO', MedicalinputMap, outMap, options);
        Boolean validateQuoteDentalEmptyCACO = vss.invokeMethod('validateDuplicateDentalCACO', DentalinputMap, outMap, options);
        System.assertEquals(validateQuoteDentalEmptyCACO, true);
        selectDentalCoverage.put('InnerSelectDental',dendata1);
        Boolean validateQuoteDentalEmptyCACO1 = vss.invokeMethod('validateDuplicateDentalCACO', DentalinputMap, outMap, options);
        
        Boolean validateQuoteVisionEmptyCACO = vss.invokeMethod('validateDuplicateVisionCACO', VisioninputMap, outMap, options);
        System.assertEquals(validateQuoteVisionEmptyCACO, true);
        selectVisionCoverage.put('InnerSelectVision',visdata1);
        Boolean validateQuoteVisionEmptyCACO1 = vss.invokeMethod('validateDuplicateVisionCACO', VisioninputMap, outMap, options);

        test.stopTest();
    }
    
    
}