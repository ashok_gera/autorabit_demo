/*@author :      Accenture IDC 
@date :          02/01/2018
@name :          SGA_AP63_BrokerRelatedAgencies
@description    : This class fetches the General and Paid agencies related to a Broker
*/
global without sharing class SGA_AP63_BrokerRelatedAgencies implements vlocity_ins.VlocityOpenInterface{
     private static final String ZIPCODE= 'ZipCode3';
     private static final String AGENCY_METHODNAME='getAgencyList';
     private static final string AGENCY_TYPE='AgencyType';
     private static final String GENERAL_AGENCY='General Agency';
     private static final String GA_ID='GeneralAgencyId';
     private static final String GACY='GeneralAgency';
     private static final String GA_TIN='GeneralAgencyTIN';
     private static final String GA_SEARCH='GeneralAgencySearch';
     private static final String PAID_AGENCY='Paid Agency';
     private static final String PA_ID='PaidAgencyId';
     private static final String PACY='PaidAgency';
     private static final String PA_TIN='PaidAgencyTIN';
     private static final String PA_SEARCH='PaidAgencySearch';
     private static final String OBDRESP='OBDRresp';
     private static final String STEP_NAME='Step';
     private static final String BROKER_TYPEAHEAD='TypeAheadBroker-Block';
     private static final String BROKERCONID='BrokerId';
     private static final String RELATIONSHIP_STATUS='Active for New Business';
     private static final String ERROR_CODE='error';
     private static final String ERROR_MSG='Please select a valid Broker/Zipcode to get the agencies';
     public static final String CLASSNAME = SGA_AP63_BrokerRelatedAgencies.class.getName();
     private static final String SELECT_GEOGRAPHIC = 'SELECT Id, Zip_Code__c, State__c FROM\n'+
                                                     'Geographical_Info__c where Zip_Code__c =:zipcode LIMIT 1';      
     private static final String AGENCY_SEARCH_QUERY='Select vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__c,\n'+
                                                     'vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.Name,\n'+
                                                     'vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.AccountTypeAhead__c,\n'+
                                                     'vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.BR_Encrypted_TIN__c,\n'+
                                                     'vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.AgencyType__c from \n'+
                                                     'vlocity_ins__PartyRelationship__c';
     private static final String AGENCY_WHERECLAUSE=SG01_Constants.SPACE+'WHERE vlocity_ins__RelationshipTypeId__r.vlocity_ins__TargetRole__c=:agencyType \n'+
                                                    'AND vlocity_ins__SourcePartyId__r.vlocity_ins__AccountId__c=:brokerAccId AND \n'+
                                                    ' SGA_Relationship_State__c=:geoState AND SGA_RelationshipStatus__c=:relationshipStatus';
     private static final String BROKERACC_SEARCH_QUERY='SELECT AccountId from Contact Where Id=:conId LIMIT 1';
     @testVisible
     private Map<string,Object> blockMap;
     /*Implementation of invokeMEthod from VlocityOpenInterface2 interface.*/
     global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {                          
            if(AGENCY_METHODNAME.equalsIgnoreCase(methodName)) {
                getAgencyList(inputMap, outMap, options);
            }                 
        return true;
     }
/************************************************************************************
    Method Name : getAgencyList
    Return type : List
    Date: 02/01/2018
    author: IDC Offshore
    Description : This methos is to fetches the general and Paid agencies of a broker.
*************************************************************************************/ 
    global void getAgencyList(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
       
        Geographical_Info__c  geoGraphicInfo;        
        List<Map<String, String>> returnList = new List<Map<String,String>>();        
        try{  
                String agencyType=(String)options.get(AGENCY_TYPE);
                Map<String,Object> stepMap = (Map<string,Object>)inputMap.get(STEP_NAME);       
                String zipcode1 = (String)inputMap.get(ZIPCODE);
                SGA_UTIL17_AccessGeographicData.zipcode = zipcode1;
                geoGraphicInfo = SGA_UTIL17_AccessGeographicData.queryGeoInfo(SELECT_GEOGRAPHIC); 
                if(stepMap != null && stepMap.containsKey(BROKER_TYPEAHEAD))        
                {            
                    blockMap = (Map<string,Object>)stepMap.get(BROKER_TYPEAHEAD);           
                }
                String brokerId = (String)blockMap.get(BROKERCONID);
                SGA_Util20_ContactDataAccessHelper.conId=brokerId;       
                Contact con=SGA_Util20_ContactDataAccessHelper.fetchAccountId(BROKERACC_SEARCH_QUERY);                    
                SGA_Util19_VlcPartyRelationshipDAHelper.agencyType=agencyType;
                SGA_Util19_VlcPartyRelationshipDAHelper.brokerAccId=con.AccountId;
                SGA_Util19_VlcPartyRelationshipDAHelper.relationshipStatus=RELATIONSHIP_STATUS;
                SGA_Util19_VlcPartyRelationshipDAHelper.geoState=geoGraphicInfo.state__c;
                Map<ID,vlocity_ins__PartyRelationship__c> agencyMap = SGA_Util19_VlcPartyRelationshipDAHelper.partyRelationshipsMap(AGENCY_SEARCH_QUERY,
                                                                                                         AGENCY_WHERECLAUSE, SG01_Constants.BLANK, SG01_Constants.LIMIT_100);         
                if(agencyMap != NULL && !agencyMap.isEmpty()){
                    for(vlocity_ins__PartyRelationship__c act : agencyMap.values()){               
                            Map<String,String> tempMap = new Map<String,String>();
                            tempMap.put(GENERAL_AGENCY.equalsIgnoreCase(agencyType)?GA_ID:PA_ID, act.vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__c);
                            tempMap.put(GENERAL_AGENCY.equalsIgnoreCase(agencyType)?GACY:PACY, act.vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.Name);
                            tempMap.put(GENERAL_AGENCY.equalsIgnoreCase(agencyType)?GA_TIN:PA_TIN, act.vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.BR_Encrypted_TIN__c);
                            tempMap.put(GENERAL_AGENCY.equalsIgnoreCase(agencyType)?GA_SEARCH:PA_SEARCH, act.vlocity_ins__TargetPartyId__r.vlocity_ins__AccountId__r.AccountTypeAhead__c);
                            returnList.add(tempMap);                
                    }
                }
                outMap.put(OBDRESP, returnList);                       
        }
        catch(Exception excn){
             UTIL_LoggingService.logHandledException(excn, null, null, 
             CLASSNAME,AGENCY_METHODNAME,null,LoggingLevel.ERROR); 
             outMap.put(ERROR_CODE, ERROR_MSG); 
        }          
    }       
}