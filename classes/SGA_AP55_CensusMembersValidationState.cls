/*
@author         Accenture offshore
@date           01/12/2017
@name           SGA_AP55_CensusMembersValidationState
@description    Used in vlocity omniscript to validate Next button.  
*/
global without sharing class SGA_AP55_CensusMembersValidationState implements vlocity_ins.VlocityOpenInterface2
{
    public static final String GETVALIDATIONSTATUS = 'getValidationStatus';
    public static final string NODENAME='validationStatus';
    public static final String CLASSNAME = SGA_AP55_CensusMembersValidationState.class.getName();
    public static final String STEPNAME='Census';
    private static final String CENSUSID= 'CensusId1';
    private static final String FAILED='Failed';
    private static final String FAILEDMSG='Please fix all errors on this page before proceeding.';
    private static final String PASSED='Passed';
    private static final String EMPTY='';
    private static final String ERROR='error';
    private static final string CENSUS_SELECT_QUERY = 'select Id,TECH_ValidationStatus__c from vlocity_ins__GroupCensus__c';
    private static final string CENSUS_QUERY_WHERE = SG01_Constants.SPACE+'Where Id = : censusId LIMIT 1';
    /*Implementation of invokeMEthod from VlocityOpenInterface2 interface.*/
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,
                                Object> outMap,Map<String,Object> optns) {
        if(GETVALIDATIONSTATUS.equalsIgnoreCase(methodName))
        {
            getValidationStatus(inputMap, outMap, optns);
        }
        
        return true;
    }
                                
    /*Adds a list of provider group values along with other parameters to output map*/
    global  void  getValidationStatus(Map<String,Object> input, Map<String, Object> outMap, Map<String, Object> optns)
    {
        string state = EMPTY;
        vlocity_ins__GroupCensus__c  valState;
        try{   
            Id censusId = (Id)input.get(CENSUSID);      
            SGA_Util14_CensusDataAccessHelper.censusId=censusId;
            valState = SGA_Util14_CensusDataAccessHelper.queryCensus(CENSUS_SELECT_QUERY,CENSUS_QUERY_WHERE);
            if(FAILED.equalsIgnoreCase(valState.TECH_ValidationStatus__c)){
             state=FAILEDMSG;
             outMap.put(ERROR,state);                    
            } 
            else{
               state=PASSED;
            }
                 
        }
        catch(Exception excn){
            UTIL_LoggingService.logHandledException(excn, null, null, 
                                                    CLASSNAME, GETVALIDATIONSTATUS ,null,LoggingLevel.ERROR);  
        }
    }
}