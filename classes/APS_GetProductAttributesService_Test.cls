@isTest 
public class APS_GetProductAttributesService_Test {
    
    public static testMethod void testMethod1() {
        product2 prod = new product2();
        prod.name= 'testprod';
        list<product2> lipro = new list<product2>();
        lipro.add(prod);
        insert lipro;
        string optionjson = '{  "productSearchCriteria": "vlocity_ins__Type__c = \'' +'Medical'+ '\'",   "timeStamp": "2018-01-11T13:50:20.987Z", "userId": "0051g000000EjhtAAC",  "userName": "rmannam@vlocity.com",  "userProfile": "System Administrator",  "module": "PlanBuilder",  "layout": "lightning",  "id": "a3Z1g0000005aOkEAI",  "vlcPersistentComponent": {},   "productSearchCriteriaa": {    "ReqEffDtFormula-Monthly": "0b513830-6da1-4c74-8f99-d4e174c15fbc",    "ReqEffDtFormula-Year": 6271,    "sicPage": 6062,    "planTypePage": 83032,    "getDisplayAttributes": 299,    "getMedicalPlans": 1571  }  }';
        string inpjson = '{  "ContextId": "",   "timeStamp": "2018-01-11T13:50:20.987Z", "userId": "0051g000000EjhtAAC",  "userName": "rmannam@vlocity.com",  "userProfile": "System Administrator",  "module": "PlanBuilder",  "layout": "lightning",  "id": "a3Z1g0000005aOkEAI",  "vlcPersistentComponent": {},   "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017",    "sicPage": 6062,    "planTypePage": 83032,    "getDisplayAttributes": 299,    "getMedicalPlans": 1571  }  }';
        string outjson = '{  "ContextId": "",   "timeStamp": "2018-01-11T13:50:20.987Z", "userId": "0051g000000EjhtAAC",  "userName": "rmannam@vlocity.com",  "userProfile": "System Administrator",  "module": "PlanBuilder",  "layout": "lightning",  "id": "a3Z1g0000005aOkEAI",  "vlcPersistentComponent": {},   "CategorySelectionField": {    "ReqEffDtFormula-Monthly": "0b513830-6da1-4c74-8f99-d4e174c15fbc",    "ReqEffDtFormula-Year": "",    "sicPage": 6062,    "planTypePage": 83032,    "getDisplayAttributes": 299,    "getMedicalPlans": 1571  }  }';
        String methodName;
        Map<String,Object> inputMap = new  Map<String,Object> ();
        Map<String,Object> outMap = new  Map<String,Object> ();
        Map<String,Object> options = new  Map<String,Object> ();
        inputMap = (Map<String, Object>) JSON.deserializeUntyped(inpjson);
        options = (Map<String, Object>) JSON.deserializeUntyped(optionjson);
        outMap= (Map<String, Object>) JSON.deserializeUntyped(inpjson);
        APS_GetProductAttributesService ag = new APS_GetProductAttributesService ();
       // methodname='calculate';        
       // ag.invokeMethod(methodName,inputMap,outMap,options);  
       // ag.productList = lipro;
        ag.setcon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Id, ProductCode  from Product2 limit 1000]));
        methodName = 'getProducts';        
        ag.invokeMethod(methodName,inputMap,outMap,options);
        ag.pgnnext(outMap);
        ag.pgnprevious(outMap);
        
        APS_GetProductAttributesService agg = new APS_GetProductAttributesService ();
        methodName = 'calculate';        
        ag.invokeMethod(methodName,inputMap,outMap,options);
    }
    
    
}