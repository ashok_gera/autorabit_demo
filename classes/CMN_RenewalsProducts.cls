global with sharing class CMN_RenewalsProducts implements vlocity_ins.VlocityOpenInterface{
    string name;
    
    public  Map<Id, Product2> productMap = new Map<Id, Product2>();

    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;

        try{
            if(methodName == 'getRenewalProducts') {
                getRenewalProducts(inputMap, outMap, options);
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        
        return success;
    }
    public void getRenewalProducts(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        String searchSpec = (String) options.get('productSearchCriteria');
        String prodquery = 'SELECT Id, ProductCode, Name, vlocity_ins__Type__c, FundingType__c ';
        prodquery +=' from Product2';
        
        if(searchSpec !=null && searchSpec!=''){
            List<String> prodIdList = new List<String>();
            
            Map<String, RenewalProducts> ProdList = (Map<String, RenewalProducts>) inputMap.get('Benefits');
            
            System.debug('The ProdList is' + ProdList);
            
            
            System.debug('The searchSpec is' + searchSpec);
            System.debug('The prodquery is' + prodquery);
        }

        prodquery +=' LIMIT 50000';

        try{
            productMap.putAll((List<Product2>)database.query(prodquery));
        }catch(Exception e){
            System.debug(Logginglevel.ERROR, +e);
            throw e;
        }
       
        system.debug(productmap);
            

        List<ProductAttributes> productAttributeList=new List<ProductAttributes>();
        if(productMap!=null&&productMap.size()>0)
        {
            System.debug(Logginglevel.ERROR,' product list '  +productMap);
            Map<Id,List<Attribute>> attributeMap=new Map<Id,List<Attribute>>();
            for(vlocity_ins__AttributeAssignment__c attr:[SELECT Id, Name, vlocity_ins__ObjectId__c,vlocity_ins__ValueDescription__c, vlocity_ins__Value__c, 
                CategoryName__c, AttributeDisplayName__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c,vlocity_ins__ValueDataType__c,
                vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c, vlocity_ins__AttributeId__r.IsKey__c from vlocity_ins__AttributeAssignment__c where 
                vlocity_ins__ObjectId__c IN :productMap.keySet() Order by vlocity_ins__ObjectId__c, vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c, 
                vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c,AttributeDisplayName__c LIMIT 50000]){
                
                if(attributeMap.containsKey(attr.vlocity_ins__ObjectId__c))
                    attributeMap.get(attr.vlocity_ins__ObjectId__c).add(new Attribute(attr));
                else
                    attributeMap.put(attr.vlocity_ins__ObjectId__c,new List<Attribute>{new Attribute(attr)});          
                
            }    
            for(Id prodId: productMap.keyset()){
                if(attributeMap.containsKey(prodId))
                    productAttributeList.add(new ProductAttributes(productMap.get(prodId), attributeMap.get(prodId)));
                else
                    productAttributeList.add(new ProductAttributes(productMap.get(prodId)));
            }
        }
        outMap.put('output', productAttributeList);
 
    }
    private class ProductAttributes{
        public string Name;
        public string ProductCode;
        public string FundingType;
        public string Type;
        public Id ProductId;
        public List<Attribute> Attributes;
        public ProductAttributes(Product2 prod,List<Attribute> attributes){
            this.name=prod.name;
            this.Type=prod.vlocity_ins__Type__c;
            this.productCode=prod.ProductCode;
            this.ProductId=prod.Id;
            this.FundingType=prod.FundingType__c;
            this.Attributes=attributes;
        }
        public ProductAttributes(Product2 prod){
            this.name=prod.name;
            this.productCode=prod.ProductCode;
            this.ProductId=prod.Id;
            this.FundingType=prod.FundingType__c;
        }
        
    }
    private class Attribute{
        public string Id;
        public string Description;
        public string Value;
        public string Category;
        public string Name;
        public Decimal CategoryDisplaySequence;
        public Decimal DisplaySequence;
        public string ValueDataType;
        public Attribute(vlocity_ins__AttributeAssignment__c attr){
            this.Id = attr.Name;
            this.Description = attr.vlocity_ins__ValueDescription__c;
            this.Value = attr.vlocity_ins__Value__c;
            this.Category = attr.CategoryName__c;
            this.Name = attr.AttributeDisplayName__c;
            this.CategoryDisplaySequence = attr.vlocity_ins__AttributeCategoryId__r.vlocity_ins__DisplaySequence__c;
            this.DisplaySequence = attr.vlocity_ins__AttributeId__r.vlocity_ins__DisplaySequence__c;
            this.ValueDataType = attr.vlocity_ins__ValueDataType__c;
        }
    }
    
    private class RenewalProducts{
        public string contractPlanCode;
        public RenewalProducts(String contractPlanCode){
            this.contractPlanCode = contractPlanCode;
        }
    }
}