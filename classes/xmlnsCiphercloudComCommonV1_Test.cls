@isTest
private with sharing class xmlnsCiphercloudComCommonV1_Test {
    
    static testmethod void testclass()
    {
        new xmlnsCiphercloudComCommonV1.NamedUrl();
        new xmlnsCiphercloudComCommonV1.SalesforceAuthentication();
        new xmlnsCiphercloudComCommonV1.Organization();
        new xmlnsCiphercloudComCommonV1.Address();
        new xmlnsCiphercloudComCommonV1.IpAddressAuthentication();
        new xmlnsCiphercloudComCommonV1.Factory();
        new xmlnsCiphercloudComCommonV1.NamedUrls();
        new xmlnsCiphercloudComCommonV1.ErrorMessages();
        new xmlnsCiphercloudComCommonV1.BaseResponse();
        new xmlnsCiphercloudComCommonV1.BaseRequest();
        new xmlnsCiphercloudComCommonV1.BasicAuthentication();
        new xmlnsCiphercloudComCommonV1.Parameter();
        new xmlnsCiphercloudComCommonV1.ContactInfo();
        new xmlnsCiphercloudComCommonV1.ErrorMessage();
        new xmlnsCiphercloudComCommonV1.Person();
        new xmlnsCiphercloudComCommonV1.Credentials();
        new xmlnsCiphercloudComCommonV1.OpenIdAuthentication();
    }
}