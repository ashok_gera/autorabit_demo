/********************************************************************************
Class Name :   AP_CensusDataUtils
Date Created : 18-February-2018 
Created By   : Muthu Rajagopalan
Description  : Creating the account data, census data, product data JSON for Integration Procedures.
Usage: 
Change History : 
Draft: Initial design
*************************************************************************************/

public class  AP_CensusDataUtils {
    private String groupId = '';
    private String censusHeaderId = '';
    
    private String calcReqId = '';
    private String ratingMethod = '';
    private String provideRatesOnly = '';
    private String includeFamilyDetails = '';
    private String allSpecialtyRates = '';
    private map<String, String> groupDetails = new map<String, String>();
    private map<String, List<CensusData>> subscriberDetails = new map<String, List<CensusData>>();
    //private map<String, List<ContractCode>> planContractCodesList = new map<String, List<ContractCode>>();
    
    /**********************************************************************************
    Constructor Method Name : AP_CensusDataUtils
    Description : Constructor method which takes Vlocity Open Interface's inputMap as input
    *************************************************************************************/
    public AP_CensusDataUtils (Map<String, Object> inputMap) {
        this.calcReqId = '**JMHSpecialtySMG';
        this.ratingMethod = 'DEFAULT';
        this.provideRatesOnly = '0';
        this.includeFamilyDetails = '1';
        this.allSpecialtyRates = '1';
        getGroupDetails(inputMap);
        if (String.isNotBlank(this.groupId)) {
            getCensusDetails(inputMap);
            if (String.isNotBlank(censusHeaderId)) {
                this.subscriberDetails.put('subscriberList', getCensusMembers());
            } else {
                this.subscriberDetails.put('subscriberList', null);
            }
        } else {
            this.subscriberDetails.put('subscriberList', null);
        }
        //this.planContractCodesList.put('planContractCode', getContractCodes(inputMap));
    }
    
    /**********************************************************************************
    Private Method Name : getCensusDetails
    Description : Gets Census Header Record Information
    *************************************************************************************/
    private void getCensusDetails(Map<String, Object> inputMap) {
        List <vlocity_ins__GroupCensus__c> lstCensusHeaderRec;
        vlocity_ins__GroupCensus__c cenHeaderRec;
        
        lstCensusHeaderRec = [ 
            Select Id, Name
            from vlocity_ins__GroupCensus__c 
            where vlocity_ins__GroupId__c = :this.groupId
        ];
        
        if (!lstCensusHeaderRec.isEmpty()) {
            cenHeaderRec = lstCensusHeaderRec.get(0);
            this.censusHeaderId = cenHeaderRec.Id;
        } else {
            this.censusHeaderId =  'test 03';
        }
        
    }
    
    /**********************************************************************************
    Private Method Name :   getContractCodesNotWorking
    Description : Gets Group (Account) Information
    *************************************************************************************/
    private void getGroupDetails(Map<String, Object> inputMap) {
        List<Account> lstAccountRec;
        Account accountRecord;
        this.groupId = (String) inputMap.get('groupId');
        if (String.isNotBlank(this.groupId)) {
            lstAccountRec = [
                Select Id, Name, Zip_Code__c, State__c, County__c, Association__c, Sic, Requested_Effective_Date__c
                from Account 
                where Id = :this.groupId
            ];
            if (!lstAccountRec.isEmpty()) {
                accountRecord = lstAccountRec.get(0);
                this.groupDetails.put('noOfEligibleLivesVision', '3'); // ??? Not available in Account
                this.groupDetails.put('noOfEligibleLivesDental', '3'); // ??? Not available in Account
                this.groupDetails.put('zipCode', accountRecord.Zip_Code__c); // Zip_Code__c '95221'
                this.groupDetails.put('state', accountRecord.State__c); // State__c 'CA'
                this.groupDetails.put('county', accountRecord.County__c); // County__c ??? how do we get the code? '009'
                this.groupDetails.put('association', accountRecord.Association__c); // Association__c '02' ??? The picklist values are yes/no
                this.groupDetails.put('sic', accountRecord.Sic); // Sic '09999'
                this.groupDetails.put('groupEffectiveDate', String.valueOf(accountRecord.Requested_Effective_Date__c)); // Requested_Effective_Date__c '2018-01-01'
            } else {
                this.groupId = (String) inputMap.get('groupId') + 'test 01';
            }
        } else {
            this.groupId = (String) inputMap.get('groupId') + 'test 02';
        }
    }
    
    /**********************************************************************************
    Private Method Name :   getContractCodes
    Description : Gets Product Contract Codes
    *************************************************************************************/
    //  TODO: This method has to be modified to incorporate the integration method changes
    //  ??? Note: It is hardcoded as the inputs are not yet defined
    private  List<ContractCode> getContractCodes(Map<String, Object> inputMap) {
        List <ContractCode> planContractCode = new List<ContractCode>();
        ContractCode cntractCode = new ContractCode('24ND');
        planContractCode.add(cntractCode);
        cntractCode = new ContractCode('25ND');
        planContractCode.add(cntractCode);
        cntractCode = new ContractCode('26ND');
        planContractCode.add(cntractCode);
        cntractCode = new ContractCode('27ND');
        planContractCode.add(cntractCode);
        return planContractCode;
    }     
    
    /**********************************************************************************
    Private Method Name :   getCensusMembers
    Description : Gets Census Member Information
    *************************************************************************************/
    private  List<CensusData> getCensusMembers() {
        //  public static void getMembers() {
        Map<Id,CensusData> membersMap = new Map<Id,CensusData>();
        for(vlocity_ins__GroupCensusMember__c gMember:[Select Id, vlocity_ins__AgeAsOfToday__c, MemberAge__c,
                                                       vlocity_ins__Birthdate__c, COBRA__c, DateofBirth__c, DentalCoverageType__c, EmployeeSpouseDependentIndicator__c,
                                                       vlocity_ins__IsFamily__c, MedicalCoverageType__c, vlocity_ins__MemberIdentifier__c, OptionalLifeAmount__c,
                                                       vlocity_ins__PartyId__c, vlocity_ins__IsPrimaryMember__c, vlocity_ins__RelatedCensusMemberId__c, 
                                                       vlocity_ins__PrimaryMemberIdentifier__c, Salary__c,Earnings__c, TobaccoCessation__c, TobaccoUse__c,
                                                       vlocity_ins__MemberType__c, VisionCoverageType__c , vlocity_ins__Gender__c
                                                       from vlocity_ins__GroupCensusMember__c 
                                                       where vlocity_ins__CensusId__c = :this.censusHeaderId
                                                       order by vlocity_ins__RelatedCensusMemberId__c]) {
                                                           if(gMember.vlocity_ins__RelatedCensusMemberId__c == null)
                                                               membersMap.put(gMember.Id, new CensusData(new Member(gMember, groupDetails)));
                                                           else {
                                                               if(membersMap.containsKey(gMember.vlocity_ins__RelatedCensusMemberId__c))
                                                                   membersMap.get(gMember.vlocity_ins__RelatedCensusMemberId__c).addDependent(new Dependent(gMember));
                                                           }  
                                                       }
        return membersMap.values();
    }
    
    
    /**********************************************************************************
    Inner Class Name :   PlanContractCode
    Description  : JSON request structure for product data.
    *************************************************************************************/
    private class ContractCode {
        private String contractCode;
        
        public ContractCode(String strContractCode) {
            this.contractCode = strContractCode;
        }
        
    }
    
    /**********************************************************************************
    Inner Class Name :   CensusData
    Description  : JSON request tructure for census data.
    *************************************************************************************/
    private class CensusData {
        private Member member;
        private Dependent[] dependent;
        
        public CensusData(Member m) {
            this.member=m;
        }
        
        public void addDependent(Dependent d) {
            if (dependent == null) {
                dependent = new Dependent[]{};
                    dependent.add(d);
            }
            else {
                dependent.add(d);
            }
        }
    }
    
    /**********************************************************************************
    Inner Class Name :   Member
    Description  : Member structure for JSON request
    *************************************************************************************/
    private class Member {
        private String memberUniqueId = '';
        private String birthDate = '';
        private String genderCode = '';
        private String isTobaccoUser = '';
        private String memberRelationshipTypeCode = '';
        private String dentalTier = '';
        private String visionTier = '';
        private String rateGuarMonths = '';
        private String isDentalPriorCoverage = '';
        private String isBundled = '';
        private map<String, String> groupDetails = new  map<String, String>(); // county, state, zipCode
        private String age = '';
        private String inTobaccoWellness = '';
        
        private member(vlocity_ins__GroupCensusMember__c gMember, map<String, String> mapGroupDetails) {
            this.memberUniqueId = gMember.Id;
            this.birthDate = String.valueOf(gMember.vlocity_ins__Birthdate__c);
            this.genderCode = gMember.vlocity_ins__Gender__c;
            this.isTobaccoUser = gMember.TobaccoUse__c;
            this.memberRelationshipTypeCode = gMember.EmployeeSpouseDependentIndicator__c;
            this.dentalTier = gMember.DentalCoverageType__c;
            this.visionTier = gMember.VisionCoverageType__c;
            this.rateGuarMonths = '12'; // ??? unknown
            this.isDentalPriorCoverage = '1'; // ??? unknown
            this.isBundled = '0'; // ??? unknown
            this.groupDetails.put('zipCode', mapGroupDetails.get('zipCode'));    
            this.groupDetails.put('state', mapGroupDetails.get('state'));    
            this.groupDetails.put('county', mapGroupDetails.get('county'));    
            
            this.age = String.valueOf(gMember.MemberAge__c);
            this.inTobaccoWellness = gMember.TobaccoCessation__c;
        }
    }
    
    /**********************************************************************************
    Inner Class Name :   Dependent
    Description  : Dependent structure for JSON request
    *************************************************************************************/
    private class Dependent {
        private String memberUniqueId = '';
        private String birthDate = '';
        private String isTobaccoUser = '';
        private String inTobaccoWellness = '';
        private String memberRelationshipTypeCode = '';
        private String age = '';
        public dependent(vlocity_ins__GroupCensusMember__c gMember) {
            this.memberUniqueId = gMember.Id;
            this.birthDate = String.valueOf(gMember.vlocity_ins__Birthdate__c);
            this.isTobaccoUser = gMember.TobaccoUse__c;
            this.inTobaccoWellness = gMember.TobaccoCessation__c;
            this.memberRelationshipTypeCode = gMember.EmployeeSpouseDependentIndicator__c;
            this.age = String.valueOf(gMember.MemberAge__c);
        }
    }
    
}