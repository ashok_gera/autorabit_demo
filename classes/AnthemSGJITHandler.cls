//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class AnthemSGJITHandler implements Auth.SamlJitHandler 
{
    private class JitException extends Exception{}
    
    global User createUser(Id samlSsoProviderId,Id communityId,Id portalId,String federationIdentifier,Map<String,String> attributes,String assertion) 
    {
        System.debug('Inside AnthemSGJITHandler - createUser');
        String id = '00536000001hx39';
        User user = [SELECT Id, FirstName, ContactId FROM User WHERE Id= :id];
        System.debug('found user');
        System.debug('Attributes - '+attributes);
        System.debug('FederationIdentifier- '+federationIdentifier);
        System.debug('samlSsoProviderId- '+samlSsoProviderId);
        System.debug('samlSsoProviderId- '+samlSsoProviderId);
        System.debug('communityId- '+communityId);
        System.debug('portalId- '+portalId);
        System.debug('assertion- '+assertion);
        System.debug('Attributes - '+attributes);
        System.debug('FederationIdentifier- '+federationIdentifier);
        System.debug('samlSsoProviderId- '+samlSsoProviderId);
        System.debug('samlSsoProviderId- '+samlSsoProviderId);
        System.debug('communityId- '+communityId);
        System.debug('portalId- '+portalId);
        System.debug('assertion- '+assertion);
        return user;
    }

    global void updateUser(Id userId,Id samlSsoProviderId,Id communityId,Id portalId,String federationIdentifier,Map<String, String> attributes,String assertion) 
    { 
        System.debug('Inside AnthemSGJITHandler - updateUser');      
        System.debug('userId- '+userId);
        try
        {
        
            User usr = [SELECT Id,IsActive,UserRoleId FROM User WHERE FederationIdentifier =:federationIdentifier];
            
            //  If user is inactive, activate the user      
            if (!usr.IsActive)
            {
                usr.IsActive = true;
                System.debug('updated user to active');
                   
                //  Update user's role - no role update - exception page
                List<UserRole> rlList = [SELECT Id FROM UserRole WHERE  Name like '%Executive'];
                usr.UserRoleId = rlList[0].Id;
                update usr;
            }
            
            //Add user to Vlocity package
            List<PackageLicense> pckgLicenseList = [SELECT Id, NamespacePrefix FROM PackageLicense WHERE NamespacePrefix = 'vlocity_ins'];
            //String userId = '00531000006wpgi';
            List<UserPackageLicense> upList = new List<UserPackageLicense>();
            upList = [select id,PackageLicenseid,Userid from UserPackageLicense where PackageLicenseid =:pckgLicenseList[0].Id AND userId =:usr.Id];
            system.debug('upList.size() = '+upList.size());
            if (upList.size() <= 0) // User is not in the managed package
            {
                UserPackageLicense upl = new UserPackageLicense (PackageLicenseid=pckgLicenseList[0].Id, userId=usr.Id);
                insert upl;
                system.debug('after adding user to the managed package');
            }
        }
        catch(Exception e)
        {
            System.debug('Error Activating user with FederationId:'+federationIdentifier+'. Error Message: '+e.getMessage());
        }
     
    }
    
    /*
    global void UserCreation(String federationIdentifier, Map<String, String> attributes,User user) 
    {     
        System.debug('Inside UserCreation');   
        user.FederationIdentifier = federationIdentifier;
        user.ProfileId = '00e36000001COfx';
        user.LanguageLocaleKey = 'en_US';
        user.LocaleSidKey = 'en_US';
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.Email = 'asa@gmail.com';
        user.EmailEncodingKey = 'UTF-8';
        //   user.Tech_Businesstrack__c = 'BROKER';
        user.IsActive= true;
        user.CommunityNickname = 'abc1234';
        user.ContactId = '00336000009ghZN'; 
        user.Username = 'WORKINGFINE@anthem.isg';
        user.Alias = 'WORKING';
        user.FirstName = 'Test User'; 
        user.LastName = 'Test User'; 
        insert user;
    }
    */
    
    /*
    global void handleUser(String federationIdentifier, Map<String, String> attributes,User user) 
    {  
        System.debug('Inside handleUser');      
        List<Account> accntLst = [SELECT Id,IsPartner  from Account where FederationId__c= :federationIdentifier LIMIT 1];
        List<Contact> contactLst = [SELECT Id,FirstName,LastName from Contact where FederationId__c = :federationIdentifier LIMIT 1];
        if (accntLst.size() >0)
        {
            Account accnt = accntLst.get(0);
            accnt.IsPartner = true;
            update accnt;
            if (contactLst.size() >0)
            {
                Contact contact = contactLst.get(0);                 
                user.FederationIdentifier = federationIdentifier;
                user.ProfileId = '00e36000001COfx';
                user.LanguageLocaleKey = 'en_US';
                user.LocaleSidKey = 'en_US';
                user.TimeZoneSidKey = 'America/Los_Angeles';
                user.Email = 'asa@gmail.com'; 
                user.EmailEncodingKey = 'UTF-8';
                //  user.Tech_Businesstrack__c = 'BROKER';
                user.IsActive= true;
                user.CommunityNickname = 'abc1234';
                user.ContactId = contact.Id; 
                user.Username = 'WORKINGFINE@anthem.isg';
                user.Alias = 'WORKING';
                user.FirstName = contact.FirstName; 
                user.LastName = contact.LastName;
                //  insert user;
                User usr = [SELECT Id FROM User WHERE FederationIdentifier =:federationIdentifier];
                List<UserRole> rlList = [SELECT Id FROM UserRole WHERE  Name like '%Executive' and PortalAccountId =:accnt.Id];
                if(rlList.size()>0)
                {
                    usr.UserRoleId = rlList.get(0).Id;                    
                    update usr;
                }
             }
        }
        
        if(attributes.containsKey('ProfileId')) {
            String profileId = attributes.get('ProfileId');
            Profile p = [SELECT Id FROM Profile WHERE Id=:profileId];
            Usr.ProfileId = p.Id;
        }
        
    }
    */
}