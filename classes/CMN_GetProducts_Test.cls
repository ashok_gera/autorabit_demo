@isTest
public class CMN_GetProducts_Test{
    
    public static testMethod void testGetProducts() {
        string dt = '2017-12-31';
        product2 pd = new product2();
        pd.name = 'TestProd';
        pd.vlocity_ins__Type__c = 'Medical';
        pd.vlocity_ins__EndDate__c = date.valueof(dt);
        product2 pd1 = new product2();
        pd1.name = 'TestProd';
        pd1.vlocity_ins__Type__c = 'Medical';
        pd1.vlocity_ins__EndDate__c = date.valueof(dt);
        list<product2> lstpd = new list<product2>();
        lstpd.add(pd);
        lstpd.add(pd1);
        insert lstpd;
        
        list<vlocity_ins__AttributeAssignment__c> listva = new list<vlocity_ins__AttributeAssignment__c>();
        vlocity_ins__AttributeAssignment__c va = new vlocity_ins__AttributeAssignment__c();
        va.name = 'test attribute';
        va.vlocity_ins__ObjectId__c = lstpd[0].id;
        vlocity_ins__AttributeAssignment__c va1 = new vlocity_ins__AttributeAssignment__c();
        va1.name = 'test attribute';
        va1.vlocity_ins__ObjectId__c = lstpd[0].id; 
        listva.add(va);
        listva.add(va1);
        insert listva;
        
        string optionjson = '{  "productSearchCriteria": "vlocity_ins__Type__c = \'' +'Medical'+ '\'" }';
        string inpjson = '{  "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": ""     }  }';
        string outjson = '{  "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017" }  }';
        String methodName;
        
        Map<String,Object> inputMap = new  Map<String,Object> ();
        Map<String,Object> outMap = new  Map<String,Object> ();
        Map<String,Object> options = new  Map<String,Object> ();
        inputMap = (Map<String, Object>) JSON.deserializeUntyped(inpjson);
        options = (Map<String, Object>) JSON.deserializeUntyped(optionjson);
        outMap= (Map<String, Object>) JSON.deserializeUntyped(inpjson);
        
        CMN_GetProducts1 ag = new CMN_GetProducts1 ();       
        methodName = 'getProducts';
        ag.invokeMethod(methodName,inputMap,outMap,options);
        
            
        string optionjsonelse = '{  "productSearchCriteria": "vlocity_ins__Type__c = \'' +'Medical'+ '\'" }  }';
        string inpjsonelse = '{      "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017"  }    }';
        string outjsonelse = '{    "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017"  }  }';
                
        Map<String,Object> inputMapel = new  Map<String,Object> ();
        Map<String,Object> outMapel = new  Map<String,Object> ();
        Map<String,Object> optionsel = new  Map<String,Object> ();
        inputMapel = (Map<String, Object>) JSON.deserializeUntyped(inpjsonelse);
        optionsel = (Map<String, Object>) JSON.deserializeUntyped(optionjsonelse);
        outMapel= (Map<String, Object>) JSON.deserializeUntyped(inpjsonelse);           
        
        CMN_GetProducts1 ag1 = new CMN_GetProducts1 ();       
        methodName = 'getProducts';        
        ag1.invokeMethod(methodName,inputMapel,outMapel,optionsel);
        Map<String, Object> assteql = (Map<String, Object>)outMapel.get('CategorySelection');
        string asstyear = (string)assteql.get('ReqEffDtFormula-Year');
        System.assertequals(asstyear,'2017_2017_2017' );
        
        string optionjsonelse2 = '{  "productSearchCriteria": "vlocity_ins__EndDate__c = 2017-12-31" }  }';
        string inpjsonelse2 = '{      "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017"  }    }';
        string outjsonelse2 = '{    "CategorySelection": {    "ReqEffDtFormula-Monthly": "2017_2017_2017",    "ReqEffDtFormula-Year": "2017_2017_2017"  }  }';
                
        Map<String,Object> inputMapel2 = new  Map<String,Object> ();
        Map<String,Object> outMapel2 = new  Map<String,Object> ();
        Map<String,Object> optionsel2 = new  Map<String,Object> ();
        inputMapel2 = (Map<String, Object>) JSON.deserializeUntyped(inpjsonelse2);
        optionsel2 = (Map<String, Object>) JSON.deserializeUntyped(optionjsonelse2);
        outMapel2 = (Map<String, Object>) JSON.deserializeUntyped(inpjsonelse2);            
        
        CMN_GetProducts1 ag2 = new CMN_GetProducts1 ();       
        methodName = 'getProducts'; 
        
        Boolean result=ag2.invokeMethod(methodName,inputMapel2,outMapel2,optionsel2);
        System.assertequals(result,true);
        
  }
    

}