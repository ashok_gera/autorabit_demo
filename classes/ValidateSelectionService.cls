global without sharing class ValidateSelectionService implements vlocity_ins.VlocityOpenInterface
{
    public static final string ERROR = 'error';
    public static final string MEDICAL_COVERAGE = 'MedicalCoverage';
    public static final string SELECT_MEDICAL = 'SelectMedical';
    public static final string MED_PLAN_COUNT = 'MedicalPlansCount';
    public static final string MED_INNER_BLOCK = 'InnerSelectMedical';
    public static final string CC_MEDICAL = 'InputContractCodeMedical';
    public static final string PROD_CODE_MED = 'SelectProductCodeLookupMedical';
    public static final string DUPLICATE_PLANS = 'Please do not select duplicate plans';
    public static final string BLANK = '';
    public static final string UNDERSCORE = '_';
    public static final string ONE_NETWORK = 'Only one network plan allowed for PPO';
    
    public static final string DENTAL_COVERAGE = 'DentalCoverage';
    public static final string SELECT_DENTAL = 'SelectDental';
    public static final string DEN_PLAN_COUNT = 'DentalPlansCount';
    public static final string DEN_INNER_BLOCK = 'InnerSelectDental';
    public static final string CC_DENTAL = 'InputContractCodeDental';
    public static final string PROD_CODE_DEN = 'SelectProductCodeLookupDental';
    
    public static final string VISION_COVERAGE = 'VisionCoverage';
    public static final string SELECT_VISION = 'SelectVision';
    public static final string VIS_PLAN_COUNT = 'VisionPlansCount';
    public static final string VIS_INNER_BLOCK = 'InnerSelectVision';
    public static final string CC_VISION = 'InputContractCodeVision';
    public static final string PROD_CODE_VIS = 'SelectProductCodeLookupVision';
    
    public static final string STATE_VAL = 'qState';
    public static final string CA_STATE = 'CA';
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        try{
            if (methodName.equalsIgnoreCase('validateMedical'))
            {
                return validateMedical(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateSelectedMedical'))
            {
                return validateSelectedMedical(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateDental'))
            {
                return validateDental(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateVision'))
            {
                return validateVision(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateQuoteMedical'))
            {
                return validateQuoteMedical(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateQuoteDental'))
            {
                return validateQuoteDental(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateQuoteVision'))
            {
                return validateQuoteVision(inputMap,outMap,options);
            }
            
            else if (methodName.equalsIgnoreCase('validateZipCode'))
            {
                return validateZipCode(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateCoverageDesc'))
            {
                return validateCoverageDesc(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateDisablingCondition'))
            {
                return validateDisablingCondition(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateDuplicateMedicalCACO'))
            {
                return validateDuplicateMedicalCACO(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateDuplicateDentalCACO'))
            {
                return validateDuplicateDentalCACO(inputMap,outMap,options);
            }
            else if (methodName.equalsIgnoreCase('validateDuplicateVisionCACO'))
            {
                return validateDuplicateVisionCACO(inputMap,outMap,options);
            }
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.INVOKEMETHOD, '', Logginglevel.ERROR);}
        return true;
    }
    
    private Boolean validateDisablingCondition(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Map<String,Object> calCOBRAStep = (Map<String,Object>) inputMap.get('CalCOBRACOBRAQuestionnaire');
        Integer calCobCount = Integer.valueOf(calCOBRAStep.get('CalCOBRAMemCount'));
        if(calCobCount == 1){
            Map<String,Object> cobraBlockMap = (Map<String,Object>)calCOBRAStep.get('CalCOBRACOBRAMemberDetails');
            String employeeVal = (String)cobraBlockMap.get('Employeevalidate');
            String disablingCondition = (String)cobraBlockMap.get('DisablingCondition');
            if('Yes'.equalsIgnoreCase(employeeVal) && String.isBlank(disablingCondition)){
                outMap.put('error','Please fix all the fields with errors');
            }
        }else if(calCobCount > 1){
            List<Object> cobraBlockList = (List<Object>)calCOBRAStep.get('CalCOBRACOBRAMemberDetails');
            for(Object obj : cobraBlockList){
                Map<String,Object> cobraBlockMap = (Map<String,Object>) obj;
                String employeeVal = (String)cobraBlockMap.get('Employeevalidate');
                String disablingCondition = (String)cobraBlockMap.get('DisablingCondition');
                if('Yes'.equalsIgnoreCase(employeeVal) && String.isBlank(disablingCondition)){
                    outMap.put('error','Please fix all the fields with errors');
                    Break;
                }
            }
        }
        return true;
        
    }
    private Boolean validateCoverageDesc(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Map<String,Object> coverageStep = (Map<String,Object>) inputMap.get('SelectedLAndDCoverage');
        Boolean lifeNone = (Boolean)coverageStep.get('LDPrdNone');
        Boolean disNone = (Boolean)coverageStep.get('DisabilityPrdNone');
        Boolean LDPrdBasicLife = (Boolean)coverageStep.get('LDPrdBasicLife');
        Boolean LDPrdBasicDependent = (Boolean)coverageStep.get('LDPrdBasicDependent');
        Boolean LDPrdOptionalLife = (Boolean)coverageStep.get('LDPrdOptionalLife');
        Boolean LDPrdOptionalDependent = (Boolean)coverageStep.get('LDPrdOptionalDependent');
        Boolean DisabilityPrdShort = (Boolean)coverageStep.get('DisabilityPrdShort');
        Boolean DisabilityPrdLong = (Boolean)coverageStep.get('DisabilityPrdLong');
        Boolean DisabilityPrdVoluntaryShort = (Boolean)coverageStep.get('DisabilityPrdVoluntaryShort');
        Boolean DisabilityPrdVoluntaryLong = (Boolean)coverageStep.get('DisabilityPrdVoluntaryLong');
        Set<String> landdproducts = new Set<String>();
        
        if(!lifeNone || !disNone){
            if(LDPrdBasicLife){
                landdproducts.add('LDPrdBasicLife');
            }
            if(LDPrdBasicDependent){
                landdproducts.add('LDPrdBasicDependent');
            }
            if(LDPrdOptionalLife){
                landdproducts.add('LDPrdOptionalLife');
            }
            if(LDPrdOptionalDependent){
                landdproducts.add('LDPrdOptionalDependent');
            }
            if(DisabilityPrdShort){
                landdproducts.add('DisabilityPrdShort');
            }
            if(DisabilityPrdLong){
                landdproducts.add('DisabilityPrdLong');
            }
            if(DisabilityPrdVoluntaryShort){
                landdproducts.add('DisabilityPrdVoluntaryShort');
            }
            if(DisabilityPrdVoluntaryLong){
                landdproducts.add('DisabilityPrdVoluntaryLong');
            }
            if(landdproducts.isEmpty()){
                outMap.put('error','Please select the Coverage Description from the selected Life & Disability products');
            }else{
                Map<String,Object> landdblock = (Map<String,Object>)coverageStep.get('LDLifeDisabilityWaitingPeriodBlock');
                Integer landdwaiveCount = Integer.valueOf(coverageStep.get('LDWaiveBlockCount'));
                System.debug('count::::'+landdwaiveCount );
                if(landdwaiveCount == 1){
                    Map<String,Object> disableBlockMap = (Map<String,Object>)landdblock.get('LDLifeDisabilityWaiveBlock');
                    String coverageDesc = (String)disableBlockMap.get('LDCoverageDescription');
                    if(!landdproducts.contains(coverageDesc)){
                        outMap.put('error','Please select the Coverage Description from the selected Life & Disability products');
                    }
                }else if(landdwaiveCount > 1){
                    List<Object> disabilityBlock = (List<Object>)landdblock.get('LDLifeDisabilityWaiveBlock');
                    for(Object obj : disabilityBlock){
                        Map<String,Object> coverageBlock = (Map<String,Object>) obj;
                        String coverageDesc = (String)coverageBlock.get('LDCoverageDescription');
                        System.debug('coverage description ::::'+coverageDesc);
                        if(!landdproducts.isEmpty() && !landdproducts.contains(coverageDesc)){
                            outMap.put('error','Please select the Coverage Description from the selected Life & Disability products');
                            break;
                        }
                    }
                }
            }
            
            
        }else if(lifeNone && disNone){
            Map<String,Object> landdblock = (Map<String,Object>)coverageStep.get('LDLifeDisabilityWaitingPeriodBlock');
            Integer landdwaiveCount = Integer.valueOf(coverageStep.get('LDWaiveBlockCount'));
            System.debug('count::::'+landdwaiveCount );
            if(landdwaiveCount == 1){
                Map<String,Object> disableBlockMap = (Map<String,Object>)landdblock.get('LDLifeDisabilityWaiveBlock');
                String coverageDesc = (String)disableBlockMap.get('LDCoverageDescription');
                if(String.isNotBlank(coverageDesc)){
                    outMap.put('error','Please select the Coverage Description from the selected Life & Disability products');
                }
            }else if(landdwaiveCount > 1){
                List<Object> disabilityBlock = (List<Object>)landdblock.get('LDLifeDisabilityWaiveBlock');
                for(Object obj : disabilityBlock){
                    Map<String,Object> coverageBlock = (Map<String,Object>) obj;
                    String coverageDesc = (String)coverageBlock.get('LDCoverageDescription');
                    System.debug('coverage description ::::'+coverageDesc);
                    if(String.isNotBlank(coverageDesc)){
                        outMap.put('error','Please select the Coverage Description from the selected Life & Disability products');
                        break;
                    }
                }
            }
        }
        return true;
    }
    
    private Boolean validateMedical(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('MedicalCoverage');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('MedicalAccounts');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not enroll in more than three Medical plans';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Medical plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>3)
                
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            {
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEDENTAL, '', Logginglevel.ERROR);}
        
        return true;
    }
    
    private Boolean validateSelectedMedical(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('MedicalCoverage');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('MedicalAccounts');
        Boolean isConflictRider29Plans = (Boolean) selectCoverageMap.get('isConflictRider29Plans');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollRider29ProdErrorMsg = 'During enrollment you must only pick plans with or without age 29, not both.';
        String quoteEnrollMultipleProdErrorMsg = 'You may not enroll in more than three Medical plans';
        Try{
            if(isConflictRider29Plans){
                outMap.put('error',quoteEnrollRider29ProdErrorMsg);
            }
            else if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>3)
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEDENTAL, '', Logginglevel.ERROR);}
        
        return true;
    }
    
    private Boolean validateDental(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('DentalCoverage');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('DentalAccounts');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not enroll in more than two Dental plans';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Dental plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>2)
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            { 
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEDENTAL, '', Logginglevel.ERROR);}
        return true;
    }
    
    private Boolean validateVision(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('VisionCoverage');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('VisionAccounts');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not enroll in more than one Vision plan';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Vision plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()!=1)
                
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            {
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEVISION, '', Logginglevel.ERROR);}
        return true;
    }
    
    private Boolean validateQuoteMedical(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('SelectMedicalCoverage');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('output2');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not quote more than ten Medical plans';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Medical plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>10)
                
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            {
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEDENTAL, '', Logginglevel.ERROR);}
        return true;
    }
    
    private Boolean validateQuoteDental(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('SelectDentalPlans');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('output3');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not quote more than ten Dental plans';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Dental plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>10)
                
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            {
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEDENTAL, '', Logginglevel.ERROR);}
        
        return true;
    }
    
    private Boolean validateQuoteVision(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options)
    {
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get('SelectVisionPlans');
        List<Object> selectCoverageOutputList = (List<Object>) selectCoverageMap.get('output4');
        Map<String, Object> validationErrJSON = new Map<String, Object>();
        String quoteEnrollMultipleProdErrorMsg = 'You may not quote more than ten Vision plans';
        String quoteEnrollNoProdErrorMsg = 'You must enroll in at least one Vision plan';
        Try{
            if (selectCoverageOutputList!=null && selectCoverageOutputList.size()>10)
                
            {
                outMap.put('error',quoteEnrollMultipleProdErrorMsg);
            }
            else if (selectCoverageOutputList == null)
            {
                outMap.put('error',quoteEnrollNoProdErrorMsg);
            }
            
            if (validationErrJSON.keySet().size() > 0)
                outMap.put('error', validationErrJSON);
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEVISION, '', Logginglevel.ERROR);}
        
        return true;
    }
    
    
    private Boolean validateZipCode(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Map<String,Object> zipCodeMap = (Map<String,Object>)inputMap.get('Step');
        String zipCodeErrorMsg = 'You may have entered Invalid ZipCode';
        System.debug('inside validate zip code method');
        Try
        {
            if(zipCodeMap.get('CheckZipCode') != 1){
                outMap.put('error',zipCodeErrorMsg);
            }else{
                //calling validateDuplicateAccount method as validateZipCode method is directly calling 
                //from GroupProfileStep to validate the account is dupliate or not
                validateDuplicateAccount(inputMap,outMap,options);
            }
            
            
        }Catch(Exception ex){UTIL_LoggingService.logHandledException(ex, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,SG01_Constants.CLS_VALIDATESELECTIONSERVICE, SG01_Constants.VALIDATEQUOTEDENTAL, SG01_Constants.BLANK, Logginglevel.ERROR);}
        
        return true;
    }
    /**
* Method Name : validateDuplicateAccount
* Parameters  : Map<String,Object>,Map<String,Object>,Map<String,Object>
* Return Type : Boolean
* Description : This method is calling from validateZipCode method, is used to validate the account is duplicate or not.
*/
    private Boolean validateDuplicateAccount(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        /***********************IDC Offshore Code Starts *********************************/
        Map<String,Object> stepMap = (Map<String,Object>)inputMap.get('Step');
        if(stepMap != NULL && !stepMap.isEmpty()){
            //this block of code is used to check and restrict duplicate account creation.
            Map<String,Object> typeAheadBrokerMap = (Map<String,Object>)stepMap.get(System.Label.SG40_TypeAheadBrokerBlock);
            String brokerId = (String)typeAheadBrokerMap.get(System.Label.SG37_BrokerId);
            String taxId = (String)stepMap.get(System.Label.SG38_TaxId);
            Map<String, Object> validationErrJSON = new Map<String, Object>();
            String duplicateAccountError = System.Label.SG36_DuplicateErrMessage;
            Id grpRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get(System.Label.SG_GroupRecordTypeName).getRecordTypeId();
            //retriving the user ids where the user entered broker id matches
            List<User> userList = [select id from User where contactid =:brokerId LIMIT 1];
            //checking the user is present in the user list and the same is exist in any of the account teams
            if(userList != NULL && !userList.isEmpty() && String.isNotBlank(taxId)){
                
                List<AccountTeamMember> accTeamMemList = [SELECT Id FROM AccountTeamMember 
                                                          where Account.Employer_EIN__c =:taxId AND Account.RecordTypeId =:grpRecordTypeId 
                                                          AND TeamMemberRole =:System.Label.SG72_AccountTeamRole and UserId =:userList[0].Id LIMIT 1];
                if(accTeamMemList != NULL && !accTeamMemList.isEmpty()){
                    outMap.put(System.Label.SG41_Error,duplicateAccountError);
                }
            }
        }
        /***********************IDC Offshore Code Ends *********************************/
        return true;
    }
    
    
    /**
* Method Name : validateDuplicateMedicalCACO
* Parameters  : Map<String,Object>,Map<String,Object>,Map<String,Object>
* Return Type : Boolean
* Description : This method is calling from medical coverage step in CA/CO enrollment omniscript.
*/
    private Boolean validateDuplicateMedicalCACO(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        String stateValue = (String)inputMap.get(STATE_VAL);
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get(MEDICAL_COVERAGE);
        
        Map<String,Object> selectMedicalMap = (Map<String,Object>)selectCoverageMap.get(SELECT_MEDICAL);
        Integer medicalPlanCount = Integer.valueOf(selectCoverageMap.get(MED_PLAN_COUNT));
        Map<String,String> prodIdProdNameMap = new Map<String,String>();
        
        if(medicalPlanCount > 1){
            List<Object> innerSelectMedicalList = (List<Object>) selectMedicalMap.get(MED_INNER_BLOCK);
            for(Object obj : innerSelectMedicalList){

                Map<String,Object> innerSelectMedicalMap = (Map<String,Object>) obj;
                String contractORProduct = (String)innerSelectMedicalMap.get(CC_MEDICAL);
                String productID = (String)innerSelectMedicalMap.get(PROD_CODE_MED);
                if(!prodIdProdNameMap.isEmpty() && prodIdProdNameMap.containsKey(productID) && String.isNotBlank(productID)){
                    outMap.put(ERROR,DUPLICATE_PLANS);
                    break;
                }else if(String.isNotBlank(productID)){
                    prodIdProdNameMap.put(productID,contractORProduct);
                }
            }
            if(CA_STATE.equalsIgnoreCase(stateValue)){
                List<Product2> productList = [select Id,Name,ProductCode,Network__c from Product2 Where ID IN:prodIdProdNameMap.keySet() AND Network__c != :BLANK];
                Map<string,SGA_State_Networks__c> stateNetworks = SGA_State_Networks__c.getAll();
                Map<String,String> productNetworkMap = new Map<String,String>();
                String keyParameter = stateValue+UNDERSCORE;
                
                for(String stateNetwork : stateNetworks.keySet()){
                    if(stateNetwork.startsWith(keyParameter) && String.isNotBlank(stateNetworks.get(stateNetwork).Product_Family__c)){
                        productNetworkMap.put(stateNetwork.substringAfter(UNDERSCORE),stateNetworks.get(stateNetwork).Product_Family__c);                    
                    }
                }
                Set<String> productFamilySet = new Set<String>();
                Map<String,String> productFamilyMap = new Map<String,String>();
                for(Product2 productObj : productList){
                    if(!productNetworkMap.isEmpty() && !productFamilyMap.containsKey(productObj.Network__c) && 
                       productNetworkMap.get(productObj.Network__c) != NULL){
                           if(!productFamilySet.contains(productNetworkMap.get(productObj.Network__c))){
                               productFamilySet.add(productNetworkMap.get(productObj.Network__c));
                               productFamilyMap.put(productObj.Network__c,productNetworkMap.get(productObj.Network__c));
                           }else{
                               outMap.put(ERROR,ONE_NETWORK);
                               break;
                           }   
                       }
                    
                }
            }
            
        }
        return true;
    }
    
    /**
* Method Name : validateDuplicateDentalCACO
* Parameters  : Map<String,Object>,Map<String,Object>,Map<String,Object>
* Return Type : Boolean
* Description : This method is calling from dental coverage step in CA/CO enrollment omniscript.
*/
    private Boolean validateDuplicateDentalCACO(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get(DENTAL_COVERAGE);
        Map<String,Object> selectDentalMap = (Map<String,Object>)selectCoverageMap.get(SELECT_DENTAL);
        Integer dentalPlanCount = Integer.valueOf(selectCoverageMap.get(DEN_PLAN_COUNT));
        
        Map<String,String> prodIdProdNameMap = new Map<String,String>();
        
        if(dentalPlanCount > 1){
            List<Object> innerSelectDentalList = (List<Object>) selectDentalMap.get(DEN_INNER_BLOCK);
            for(Object obj : innerSelectDentalList){
                Map<String,Object> innerSelectDentalMap = (Map<String,Object>) obj;
                String contractORProduct = (String)innerSelectDentalMap.get(CC_DENTAL);
                String productID = (String)innerSelectDentalMap.get(PROD_CODE_DEN);
                if(!prodIdProdNameMap.isEmpty() && prodIdProdNameMap.containsKey(productID) && String.isNotBlank(productID)){
                    outMap.put(ERROR,DUPLICATE_PLANS);
                    break;
                }else if(String.isNotBlank(productID)){
                    prodIdProdNameMap.put(productID,contractORProduct);
                }
            }
        }
        return true;
    }
    /**
* Method Name : validateDuplicateVisionCACO
* Parameters  : Map<String,Object>,Map<String,Object>,Map<String,Object>
* Return Type : Boolean
* Description : This method is calling from vision coverage step in CA/CO enrollment omniscript.
*/
    private Boolean validateDuplicateVisionCACO(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        Map<String,Object> selectCoverageMap = (Map<String,Object>)inputMap.get(VISION_COVERAGE);
        Map<String,Object> selectVisionMap = (Map<String,Object>)selectCoverageMap.get(SELECT_VISION);
        Integer visionPlanCount = Integer.valueOf(selectCoverageMap.get(VIS_PLAN_COUNT));
        Map<String,String> prodIdProdNameMap = new Map<String,String>();
        
        if(visionPlanCount > 1){
            List<Object> innerSelectVisionList = (List<Object>) selectVisionMap.get(VIS_INNER_BLOCK);
            for(Object obj : innerSelectVisionList){
                Map<String,Object> innerSelectVisionMap = (Map<String,Object>) obj;
                String contractORProduct = (String)innerSelectVisionMap.get(CC_VISION);
                String productID = (String)innerSelectVisionMap.get(PROD_CODE_VIS);
                if(!prodIdProdNameMap.isEmpty() && prodIdProdNameMap.containsKey(productID) && String.isNotBlank(productID)){
                    outMap.put(ERROR,DUPLICATE_PLANS);
                    break;
                }else if(String.isNotBlank(productID)){
                    prodIdProdNameMap.put(productID,contractORProduct);
                }
            }
        }
        return true;
    }
    
}