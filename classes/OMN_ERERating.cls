global with sharing class OMN_ERERating implements vlocity_ins.VlocityOpenInterface2{
    
    public Object invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        Boolean success = true;
        
        try{
            if(methodName == 'getRatesFromERE') {
                return getRatesFromERE(input, output, options);
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        
        return success;
    }
    
    public Object getRatesFromERE(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
        BRPSGCMakeRESTAPICallToSGC cls = new BRPSGCMakeRESTAPICallToSGC();
        System.debug('req' + inputMap.get('reqJSON'));
        HttpResponse resp = cls.GetRatesFromERE((String)inputMap.get('reqJSON'));
        System.debug('resp' + resp);
        outMap.put('resJSON', resp.getBody());
        return resp.getBody();
    }
}