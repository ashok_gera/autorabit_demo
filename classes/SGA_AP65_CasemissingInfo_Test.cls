/**********************************************************************************
Class Name :   SGA_AP65_CasemissingInfo_Test
Date Created : 02/8/2018
Created By   : IDC Offshore
Description  : 1. This class is the test class for  SGA_AP64_CasemissingInfo
*************************************************************************************/
@isTest
private class SGA_AP65_CasemissingInfo_Test {



/************************************************************************************
    Method Name : testevaluateMissingInfo
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for testevaluateMissingInfo Method
*************************************************************************************/
    private static testMethod void testevaluateMissingInfo(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
		Pricebook2 customPB=Util02_TestData.createPricebook2SGA();
		Account testAcc = Util02_TestData.createGroupAccount();
      
		List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
		List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
		vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
        testApplication.vlocity_ins__AccountId__c=testApplication.id;
		
        System.runAs(testUser){
    
        Database.insert(cs001List);
        Database.insert(cs002List);
        testAcc.Company_State__c=Null;   
        testAcc.Employer_EIN__c=Null;
        testAcc.Billing_Street__c =Null;
        testAcc.Billing_PostalCode__c=Null;
        testAcc.Billing_County__c=Null;
        Database.insert(testAcc);
         
         testApplication.vlocity_ins__AccountId__c=testAcc.Id;
          testApplication.vlocity_ins__Status__c = SG01_Constants.APP_STATUS_GROUP_SUBMITED;
      
         Database.insert(testApplication);
            //Creating Case Record
          Case testCase = Util02_TestData.insertSGACase(testAcc.Id,testApplication.Id);
           Test.startTest();             
            SGA_AP65_CaseMissingInfo.evaluateMissingInfo(testCase.Id);
            
            Test.stopTest();
          }

        }
    /************************************************************************************
    Method Name : testevaluateMissingInfo
    Parameters  : None
    Return type : void
    Description : This is Negative testmethod for testevaluateMissingInfo Method
*************************************************************************************/
    private static testMethod void testNegevaluateMissingInfo(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
		Pricebook2 customPB=Util02_TestData.createPricebook2SGA();
		Account testAcc = Util02_TestData.createGroupAccount();
      
		List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
		List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
		vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
        testApplication.vlocity_ins__AccountId__c=testApplication.id;
		
        System.runAs(testUser){
    
        Database.insert(cs001List);
        Database.insert(cs002List);
        testAcc.Company_State__c= 'NY';   
        testAcc.Employer_EIN__c='123456789';
        testAcc.Billing_Street__c ='Test Street';
        testAcc.Billing_PostalCode__c='10004';     
        testAcc.Billing_County__c='Test County';
        Database.insert(testAcc);
         
         testApplication.vlocity_ins__AccountId__c=testAcc.Id;
          testApplication.vlocity_ins__Status__c = SG01_Constants.APP_STATUS_GROUP_SUBMITED;
      
         Database.insert(testApplication);
            //Creating Case Record
          Case testCase = Util02_TestData.insertSGACase(testAcc.Id,testApplication.Id);
           Test.startTest();             
            SGA_AP65_CaseMissingInfo.evaluateMissingInfo(testCase.Id);
            
            Test.stopTest();
          }

        }
     }