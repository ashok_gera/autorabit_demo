public with sharing class Util03_HardCodingConstants {
    
    public static final String ID = 'id';
    public final static string TECHZIP_CODE = 'Tech_ZipCode__c';
    public final static string DOB = 'Date_of_Birth__c';
    public final static string CUSTOMER_SCORE = 'Customer_Score__c';
    public static boolean recursionOpportunity = false;

}