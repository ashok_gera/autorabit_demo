public class BRPSGCUploadDocumentHolder
    {
        public blob fileContent{get;set;}
        public boolean required{get;set;}
        public String fname { get; set; } 
        public String appId{ get; set; } 
        public String attachId{ get; set; } 
        public String fcategory { get; set; } 
        public String fstatus { get; set; } 
        public String contentType { get; set; } 
        public Integer fileSize { get; set; } 
        public String fileSendStatus { get; set; }    
        public String partyId{ get; set; }    
        public String documentType{ get; set; }    
        public String formType{ get; set; }  
        public String uploadType  { get; set; }  
        public String userId  { get; set; }  
        public String EIN { get; set; }  
        public String grpName { get; set; }  
        public String state { get; set; }  
        public boolean curStatus { get; set; }  
        public boolean cntExist { get; set; }  
        public String errMsg { get; set; }  
        public String AppIdForSGC { get; set; }          
        public String attachmentSFDCId  { get; set; }  
        
    }