/*@author       Accenture
@date           12/04/2017
@name           GeographyInfo
@description    This class fetches the county ,state details from Geographical Info based on the entered Zipcode.
*/
global with sharing class GeographyInfo {
    private static final string LICENSE_VAL='License'; 
    private static final string CONTRACT_VAL='Contract';
    private static final String CONTRACT_TYPE='Small Group'; 
    private static final string VALID_STATUS='Valid';
    private static final string INVALID_STATUS='Invalid';
    private static final string BUSINESSTRACK  ='SGQUOTING';
    private static final string NOACTIVELICENSES='No Active licenses present for the given zipcode.';   
    private static final String EMPTY='';
    private static final String SELECT_GEOGRAPHIC = 'SELECT Id, Zip_Code__c, State__c FROM\n'+
                                                     'Geographical_Info__c where Zip_Code__c =:zipcode LIMIT 1';
    private static final string LICENSE_SELECT_QUERY = 'SELECT Id,BR_Type__c,BR_Start_Date__c,BR_End_Date__c,BR_Status__c,SGA_ProdType__c FROM License_Appointment__c';
   // private static final string LICENSE_QUERY_WHERE = SG01_Constants.SPACE+'Where SGA_Provider__c=:contactId AND Tech_Businesstrack__c=:businessTrack AND BR_State__c =:state AND BR_State__c != null AND BR_Type__c IN :idSet LIMIT 100';    
    private static final string LICENSE_QUERY_WHERE = SG01_Constants.SPACE+'Where SGA_Provider__c=:contactId AND Tech_Businesstrack__c=:businessTrack AND BR_State__c =:state AND BR_State__c != null AND (BR_Type__c=:licenseVal OR (BR_Type__c=:contractVal AND SGA_ContractType__c=:contactTypeVal)) LIMIT 100';
    //public GeographyInfo() { }
    /*This method fetches the counties based on entered zipcode.*/     
    @RemoteAction
    global static List<Geographical_Info__c> getCounty(String ZipCode) {
     List<Geographical_Info__c> County= [SELECT Id,Name,Area__c,County__c,RatingArea__c,SearchField__c,Zip_Code__c,State__c FROM Geographical_Info__c WHERE (Zip_Code__c LIKE : ZipCode) ORDER BY Zip_Code__c LIMIT 100];
        return County;   
    }
    /*This method verifies the license of broker based on state.*/
    @RemoteAction
    global static String getStateLicenseStatus(String zipcode1,Id BrokerId) {
        String status=EMPTY;       
        List<License_Appointment__c> appLcns;
        Geographical_Info__c geoGraphicInfo;
        set<string> blSet=new Set<String>();
        //Set<String> type = new Set<String>{'License','Contract'};
        Set<String> type = new Set<String>{LICENSE_VAL,CONTRACT_VAL};
        SGA_UTIL17_AccessGeographicData.zipcode = zipcode1;
        geoGraphicInfo = SGA_UTIL17_AccessGeographicData.queryGeoInfo(SELECT_GEOGRAPHIC);
        SGA_Util18_LicenseDataAccessHelper.businessTrack=BUSINESSTRACK;
        SGA_Util18_LicenseDataAccessHelper.state=geoGraphicInfo.State__c;    
        SGA_Util18_LicenseDataAccessHelper.contactId = BrokerId; 
        SGA_Util18_LicenseDataAccessHelper.idSet = type; 
        SGA_Util18_LicenseDataAccessHelper.licenseVal=LICENSE_VAL;
        SGA_Util18_LicenseDataAccessHelper.contractVal=CONTRACT_VAL;
        SGA_Util18_LicenseDataAccessHelper.contactTypeVal=CONTRACT_TYPE;  
        appLcns = SGA_Util18_LicenseDataAccessHelper.fetchLicenseList(LICENSE_SELECT_QUERY,LICENSE_QUERY_WHERE);
        if(!appLcns.isEmpty()){
                for(License_Appointment__c lp:appLcns){
                    if(lp.BR_Start_Date__c<=Date.Today() && lp.BR_End_Date__c>=Date.Today())
                    {
                        blSet.add(lp.BR_Type__c);                        
                    } 
                 }
                 if(!blSet.isEmpty()){ 
                    if (blSet.contains(LICENSE_VAL) && blSet.contains(CONTRACT_VAL)){
                        status = VALID_STATUS;
                    }
                    else{          
                        status=INVALID_STATUS;                
                    } 
                 } 
                 else{          
                    status=INVALID_STATUS;               
                 } 
             }       
             else{                
                status=INVALID_STATUS;
            }       
        return status;     
    }
}