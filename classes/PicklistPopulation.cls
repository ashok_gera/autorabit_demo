global class PicklistPopulation implements vlocity_ins.VlocityOpenInterface
{
    public Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options)
    {
        if (methodName.equals('PopulatePicklist'))
        {
            System.debug('input map::::'+input);
            System.debug('outmap map::::'+outMap);
            System.debug('options map::::'+options);
            PopulatePicklist(input, outMap, options);
        }
        return true;
    }
    
    // Get All Relationship Types for Account when the Omniscript is compiled.
    
    public void PopulatePicklist(Map<String, Object> input, Map<String, Object> outMap, Map<String, Object> options)
        
    {
        System.debug('input map::::'+input);
        System.debug('outmap map::::'+outMap);
        System.debug('options map::::'+options);
        List<Map<String,String>> options1 = new List<Map<String, String>>();
        
        for (Account rel : [ Select type, Id FROM account where type != NULL limit 5])
            
        { 
            Map<String, String> tempMap = new Map<String, String>();
            
            tempMap.put('name', rel.Id); // Language Independent
            tempMap.put('value', rel.type); // Displayed in Picklist UI
            
            options1.add(tempMap);
        }
        outMap.put('options',options1);
    }
}