/*@author       Accenture
@date           12/04/2017
@name           SGA_AP55_CensusMembersValidationState_Test
@description    Test class to test the SGA_AP55_CensusMembersValidationState class.
*/
@isTest
private class SGA_AP55_CensusValidationState_Test {
  /************************************************************************************
    Method Name : validatePass
    Parameters  : None
    Return type : void
    Description : This is testmethod for positive snenario
*************************************************************************************/
	private static testMethod void validatePass(){
		User testUser = Util02_TestData.createUser();
		Account testAcc = Util02_TestData.createGroupAccount();
		List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
		vlocity_ins__GroupCensus__c grpCensus= Util02_TestData.CreateCensus();
		System.runAs(testUser){
			Database.insert(cs001List);
			Database.insert(testAcc);
			grpCensus.vlocity_ins__GroupId__c = testAcc.id;
			grpCensus.TECH_ValidationStatus__c = 'Failed';
			Database.insert(grpCensus);
			vlocity_ins__GroupCensusMember__c censusMem = Util02_TestData.CreateCensusMember();
			Map<String,Object> inputmap=new Map<String,Object>();
			Map<String,Object> outputmap=new Map<String,Object>();
			Map<String,Object> options=new Map<String,Object>();
			String methodName='getValidationStatus';
			outputmap.put('validationStatus','Failed');
			inputmap.put('CensusId1',grpCensus.id);
			options.put('useQueueableApexRemoting',true); 	
			SGA_AP55_CensusMembersValidationState actct=new SGA_AP55_CensusMembersValidationState();
			Boolean approvalRes=actct.invokeMethod(methodName,inputmap,outputmap,options); 
            system.assertEquals(true,approvalRes);
		}
	}
/************************************************************************************
Method Name : validateFail
Parameters  : None
Return type : void
Description : This is testmethod for negative snenario
*************************************************************************************/
	private static testMethod void validateFail(){
		User testUser = Util02_TestData.createUser();
		System.runAs(testUser){
			Map<String,Object> inputmap=new Map<String,Object>();
			Map<String,Object> outputmap=new Map<String,Object>();
			Map<String,Object> options=new Map<String,Object>();
			String methodName='getValidationStatus';           
			outputmap.put('validationStatus','Failed');
			options.put('useQueueableApexRemoting',true); 
			inputmap.put('CensusId1',null);
			SGA_AP55_CensusMembersValidationState actct=new SGA_AP55_CensusMembersValidationState();
			Boolean approvalRes=actct.invokeMethod(methodName,inputmap,outputmap,options); 
			system.assertEquals(true,approvalRes);				
		}
	}
}