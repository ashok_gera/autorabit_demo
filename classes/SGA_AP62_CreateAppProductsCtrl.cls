/************************************************************************
* Class Name  : SGA_AP62_CreateAppProductsCtrl
* Created By  : IDC Offshore
* Created Date: 01/23/2018
* Description : It is used to create/update Application Products from
*                Application Detail page.
* 
**********************************************************************/
public with sharing class SGA_AP62_CreateAppProductsCtrl {
    public static final string FRWD_SLASH = '/';
    public List<Application_Product__c> appProdRecListToCreate {get;set;} 
    public List<Product2> prdListforAccState {get;set;} 
    public string searchString{get;set;} // search keyword
    private Static final string CLS_SGA_AP62='SGA_AP62_CreateAppProductsCtrl';
    private Static final string SGA_AP62_Fetch_App ='fetchAppProdRec';
    public  String applicationAccState {get;set;}
    private static vlocity_ins__Application__c appData = new vlocity_ins__Application__c();
    public string selectedProductId{get;set;}  
    public Id appId{get;set;}
    public Boolean errPage{get;set;}
    public static String errMsg {get;set;}
    private static Final String ERR_MSG = 'There are no records of this type'; 
    private static Final String ERR_MSG_ACCSTATE = 'Account State is not present';
    
    /**************************************
    *Constructor for the class.
    ************************************/

    public SGA_AP62_CreateAppProductsCtrl(ApexPages.StandardSetController controller){
        appProdRecListToCreate = new List<Application_Product__c>();
        appId = apexpages.currentpage().getparameters().get('id');
        System.debug('appId is ::>>'+appId);
        appData = [Select Id,Name,Account_State__c,vlocity_ins__PrimaryPartyId__c from vlocity_ins__Application__c Where Id =:appId];
        applicationAccState= appData.Account_State__c;
        if(applicationAccState == null && String.isBlank(applicationAccState)){
            errPage = true;
            errMsg = ERR_MSG_ACCSTATE;
            System.debug('Account_State__c is ::>>'+appData.Account_State__c);
        }
        else{
             errPage = false;
        }
    }
    /*************************************************************************************
    * Method Name : fetchAppProdRec
    * Parameters  : None
    * Return Type : void
    * Description : The method will call when the page is loaded which is used
    *               to create 3 records by default. User will delete if they do not want.
    **************************************************************************************/
    public void fetchAppProdRec(){
        try
        {
			System.debug('Application State is:>>'+applicationAccState);
             prdListforAccState= performSearch(searchString); 
            
            System.debug('prdListforAccState is:>>'+prdListforAccState);
            if(prdListforAccState.isEmpty()){
                errPage =true;
                errMsg = ERR_MSG;
            }else{
                errPage =false;
            }
         
        }catch(Exception e){UTIL_LoggingService.logHandledException(e, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,CLS_SGA_AP62,SGA_AP62_Fetch_App, SG01_Constants.BLANK, Logginglevel.ERROR);}        
    }
    

   /*************************************************************************************
    * Method Name : search
    * Parameters  : None
    * Return Type : PageReference
    * Description : invokes performSearch method and performs the keyword search
    **************************************************************************************/
  public PageReference search() {
     prdListforAccState = performSearch(searchString);  
     if(!prdListforAccState.isEmpty()){
           errPage =false;
              
     }else{ 
         errPage =true;
         errMsg = ERR_MSG;  
         System.debug('Filtered Data err'+ERR_MSG);
          }
    return null;
  }
       
        
    
    /*************************************************************************************
    * Method Name : performSearch
    * Parameters  : string srStr
    * Return Type : List<Product2>
    * Description : performs the keyword search and returns list of Prouducts found based on Account State or Search String
    **************************************************************************************/
  private List<Product2> performSearch(string srStr) {

    String soql = 'Select Id,Name,ProductCode,Description,Family,vlocity_ins__EffectiveDate__c,vlocity_ins__SubType__c,FundingType__c,vlocity_ins__Availability__c,vlocity_ins__Status__c,vlocity_ins__Type__c FROM Product2';
    if(srStr != '' && srStr != null){
          soql = soql +  ' where (Name LIKE \'%'+ srStr +'%\''+ 'OR ProductCode LIKE \'%'+ srStr +'%\''+ 'OR vlocity_ins__SubType__c LIKE \'%'+ srStr +'%\')' + ' AND vlocity_ins__Availability__c =: applicationAccState' ;
          soql = soql + ' limit 200';
	  
    }
     else {
          soql = soql +  ' where vlocity_ins__Availability__c =: applicationAccState';
          soql = soql + ' limit 200';
      }
    System.debug('Executed Soql :'+soql);
    return database.query(soql); 

  }
  
    /*************************************************************************************
    * Method Name : saveAppPrd
    * Parameters  : None
    * Return Type : PageReference
    * Description : The method is used to upsert the Application_Product__c product records 
    *               and deletes the records.
    **************************************************************************************/
    public PageReference saveAppPrd() {
        System.debug('Action fn cl'+selectedProductId);
        System.debug('Parent App Id is:'+  appId);
        String pgerRefStr= NULL;
        try{
            if(String.isNotBlank(selectedProductId) && appId != NULL){
                appProdRecListToCreate.add(new Application_Product__c(Application_Name__c = appId ,Product__c=selectedProductId ));
                Database.SaveResult[] srList = Database.insert(appProdRecListToCreate, false);
               // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (sr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted Application Product with Id: ' + sr.getId());
                        pgerRefStr = FRWD_SLASH + appId;
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('App Product fields that affected this error: ' + err.getFields());
                        }
                    }
            }
                
        }  
        } catch(Exception e){UTIL_LoggingService.logHandledException(e, SG01_Constants.ORGID, SG01_Constants.APPLICATIONNAME,CLS_SGA_AP62,SGA_AP62_Fetch_App, SG01_Constants.BLANK, Logginglevel.ERROR); }  
        PageReference pageRef = new PageReference(pgerRefStr);
        return pageRef;
    }   
}