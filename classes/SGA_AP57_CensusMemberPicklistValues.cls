/************************************************************************
* Class Name  : SGA_AP57_CensusMemberPicklistValues
* Created By  : IDC Offshore
* Created Date: 12/08/2017
* Description : It is used to get the picklistvalues of vlocity_ins__GroupCensusMember__c into VF page. * 
**********************************************************************/
public with sharing class SGA_AP57_CensusMemberPicklistValues{   
    private static final string PICKLISTSTATENONE='--None--';
    private static final String EMPTY='';
     /*This method gets the gender picklist values.*/
    public static List<SelectOption> getgenderValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.vlocity_ins__Gender__c.getDescribe();
        List<Schema.PicklistEntry> pleGender = fieldResult.getPicklistValues();
        options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry gender : pleGender)
        {
         options.add(new SelectOption(gender.getLabel(), gender.getValue()));
        }       
        return options;
    }

    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getdependentIndicatorValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.EmployeeSpouseDependentIndicator__c.getDescribe();
        List<Schema.PicklistEntry> pleDI = fieldResult.getPicklistValues(); 
        options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry di : pleDI)
        {
            options.add(new SelectOption(di.getLabel(), di.getValue()));
        }       
        return options;
    }
    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getmedicalCoverageTypeValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.MedicalCoverageType__c.getDescribe();
        List<Schema.PicklistEntry> pleMc = fieldResult.getPicklistValues(); 
         options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry mc : pleMc)
        {
            options.add(new SelectOption(mc.getLabel(), mc.getValue()));
        }       
        return options;
    }
    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getdentalCoverageTypeValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.DentalCoverageType__c.getDescribe();
        List<Schema.PicklistEntry> pleDc = fieldResult.getPicklistValues();  
        options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry dc : pleDc)
        {
            options.add(new SelectOption(dc.getLabel(), dc.getValue()));
        }       
        return options;
    }
    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getvisionCoverageTypeValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.VisionCoverageType__c.getDescribe();
        List<Schema.PicklistEntry> pleVc = fieldResult.getPicklistValues(); 
          options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry vc : pleVc)
        {
            options.add(new SelectOption(vc.getLabel(), vc.getValue()));
        }       
        return options;
    }
    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getlifeClassValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.LifeClass__c.getDescribe();
        List<Schema.PicklistEntry> pleLc = fieldResult.getPicklistValues(); 
         options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry lc : pleLc)
        {
            options.add(new SelectOption(lc.getLabel(), lc.getValue()));
        }       
        return options;
    }
    /*This method gets the dependentIndicator picklist values.*/
    public static List<SelectOption> getcobraValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.COBRA__c.getDescribe();
        List<Schema.PicklistEntry> plecb = fieldResult.getPicklistValues(); 
         options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry cb : plecb)
        {
            options.add(new SelectOption(cb.getLabel(), cb.getValue()));
        }       
        return options;
    }
     /*This method gets the earnings/Salary Mode picklist values.*/
    public static List<SelectOption> getearningsValues()
    {
        List<SelectOption> options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult = vlocity_ins__GroupCensusMember__c.Earnings__c.getDescribe();
        List<Schema.PicklistEntry> pleern = fieldResult.getPicklistValues();  
          options.add(new SelectOption(EMPTY,PICKLISTSTATENONE));
        for(Schema.PicklistEntry ern : pleern)
        {
            options.add(new SelectOption(ern.getLabel(), ern.getValue()));
        }       
        return options;
    }
}