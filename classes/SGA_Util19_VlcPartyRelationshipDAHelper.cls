/*****************************************************************************************
Class Name   : SGA_Util19_VlcPartyRelationshipDAHelper 
Date Created : 2/05/2018
Created By   : IDC Offshore
Description  : 1. This is the util data access class for Vlocity Party Relationship. Which is used to fetch 
the details from this object based on some parameters
 
******************************************************************************************/
public without sharing class SGA_Util19_VlcPartyRelationshipDAHelper {
    //Below are the final variables to check the operation on the list of records.
    public static final String INSERT_OPERATION = 'Insert';
    public static final String UPDATE_OPERATION = 'Update';
    public static final String DELETE_OPERATION = 'Delete';
    public static final String UPSERT_OPERATION = 'Upsert';
    public static String agencyType;
    public static String brokerAccId;
    public static String relationshipStatus;
    public static String geoState;
    /****************************************************************************************************
    Method Name : partyRelationshipsMap
    Parameters  : String selectQuery,String whereClause,String orderByClause,String limitClause
    Return type : Map<Id,vlocity_ins__PartyRelationship__c>
    Description : This method is used to fetch the Party Relationship records based on parameters passed.
    It will return the Map<ID,vlocity_ins__PartyRelationship__c> if user wants the Map, they can perform the logic on 
    Map, else they can covert the map to list of party relationships. 
    ******************************************************************************************************/
    public static Map<ID,vlocity_ins__PartyRelationship__c> partyRelationshipsMap(String selectQuery,String whereClause,String orderByClause,String limitClause){
        String dynaQuery = SG01_Constants.BLANK;
        Map<ID,vlocity_ins__PartyRelationship__c> relationshipMap = NULL;
        
        dynaQuery = String.isNotBlank(selectQuery) ? selectQuery : SG01_Constants.BLANK;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(whereClause) ? dynaQuery+=whereClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(orderByClause) ? dynaQuery+=orderByClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(limitClause) ? dynaQuery+=limitClause : dynaQuery;
        if(String.isNotBlank(dynaQuery))
        {
            System.debug('dynaQuery:::'+dynaQuery);
            relationshipMap = new Map<ID,vlocity_ins__PartyRelationship__c>((List<vlocity_ins__PartyRelationship__c>)Database.query(dynaQuery));
        }
        return relationshipMap ;
    }      
}