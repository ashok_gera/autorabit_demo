/************************************************************************
 * Class Name   : SGA_VFCE01_AssignToCaseTeamCntrl_Test
 * Description  : Test class for SGA_VFCE01_AssignToCaseTeamController
 * Created By   : IDC Offshore
 * Created Date : 13/12/2017
 * **********************************************************************/
@isTest
private class SGA_VFCE01_AssignToCaseTeamCntrl_Test {
    private static final string PAGE_NAME = 'Page.SGA_VFP13_AssignToCaseTeam';
    private static final string CASE_REC = 'caseRecords';
    private static final string COMMA = ',';
    private static final string ROLE_ID = [SELECT ID FROM CaseTeamRole WHERE NAME != 'Cash Ops Rep' LIMIT 1].ID;
	/**************************************************************
	 * Method Name : assignCaseTeamNegTest
	 * Description : To test the case team assignment functionality
	 * ************************************************************/
    private static testMethod void assignCaseTeamNegTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            //Account data creation
            Account testAccount = Util02_TestData.createGroupAccount();
            //Application record creation
            vlocity_ins__Application__c testApplication = Util02_TestData.createApplication();
            //Business Custom Setting insertion
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            //Record Type Assignment Custom Setting insertion
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();       
            Database.insert(cs001List);
            Database.insert(cs002List);
            //database.insert(createInternalUserEmailData);
            database.insert(testAccount);
            database.insert(testApplication);
            
            Case testCaseIns = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            database.insert(testCaseIns);
            
            //Testing the negative scenarios
            Test.setCurrentPageReference(new PageReference(PAGE_NAME));
            System.currentPageReference().getParameters().put(CASE_REC, testCaseIns.id);
            SGA_VFCE01_AssignToCaseTeamController controller = new SGA_VFCE01_AssignToCaseTeamController();
            controller.displaySelRecOnPage();
            controller.assignToCaseTeam();
            controller.removeFromListId = testCaseIns.id;
            controller.remove();
            system.assertEquals(0, controller.caseTeamList.size());
            controller.caseTeamObj.AssignCaseTeamUser__c = UserInfo.getUserId();
            controller.assignToCaseTeam(); 
        }
    }
    
    /**************************************************************
	 * Method Name : assignCaseTeamPosTest
	 * Description : To test the case team assignment functionality
	 * ************************************************************/
    private static testMethod void assignCaseTeamPosTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            //Account data creation
            Account testAccount = Util02_TestData.createGroupAccount();
            //Application record creation
            vlocity_ins__Application__c testApplication = Util02_TestData.createApplication();
            //Business Custom Setting insertion
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            //Record Type Assignment Custom Setting insertion
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();       
            Database.insert(cs001List);
            Database.insert(cs002List);
            //database.insert(createInternalUserEmailData);
            database.insert(testAccount);
            database.insert(testApplication);
            
            List<Case> caseList = new List<Case>();
            Case caseInstObj1 = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            Case caseInstObj2 = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            caseList.add(caseInstObj1);
            caseList.add(caseInstObj2);
            database.insert(caseList);
            
            Test.setCurrentPageReference(new PageReference(PAGE_NAME));
            System.currentPageReference().getParameters().put(CASE_REC, caseInstObj1.id+COMMA+caseInstObj2.Id);
            SGA_VFCE01_AssignToCaseTeamController controller1 = new SGA_VFCE01_AssignToCaseTeamController();
            controller1.displaySelRecOnPage();
            System.assertEquals(2, controller1.caseTeamList.size());
            controller1.caseTeamObj.AssignCaseTeamUser__c = UserInfo.getUserId();
            controller1.assignToCaseTeam();
            controller1.assignToCaseTeam();
        }
    }
    
     /**************************************************************
	 * Method Name : checkMemberAlreadyExistTest
	 * Description : To test the case team assignment functionality
	 * ************************************************************/
    private static testMethod void checkMemberAlreadyExistTest(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            //Account data creation
            Account testAccount = Util02_TestData.createGroupAccount();
            //Application record creation
            vlocity_ins__Application__c testApplication = Util02_TestData.createApplication();
            //Business Custom Setting insertion
            List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
            //Record Type Assignment Custom Setting insertion
            List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();       
            Database.insert(cs001List);
            Database.insert(cs002List);
            //database.insert(createInternalUserEmailData);
            database.insert(testAccount);
            database.insert(testApplication);
            
            List<Case> caseList = new List<Case>();
            Case caseInstObj1 = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            Case caseInstObj2 = Util02_TestData.insertSGACase(testAccount.id, testApplication.id);
            caseList.add(caseInstObj1);
            caseList.add(caseInstObj2);
            database.insert(caseList);
            
            CaseTeamMember caseTeamMemObj = new CaseTeamMember();
            caseTeamMemObj.MemberId = UserInfo.getUserId();
            caseTeamMemObj.ParentId = caseInstObj1.id;
            caseTeamMemObj.TeamRoleId = ROLE_ID;
            Insert caseTeamMemObj;
            Test.setCurrentPageReference(new PageReference(PAGE_NAME));
            System.currentPageReference().getParameters().put(CASE_REC, caseInstObj1.id+COMMA+caseInstObj2.Id);
            SGA_VFCE01_AssignToCaseTeamController controller1 = new SGA_VFCE01_AssignToCaseTeamController();
            controller1.displaySelRecOnPage();
            controller1.caseTeamObj.AssignCaseTeamUser__c = UserInfo.getUserId();
            controller1.assignToCaseTeam();
        }
    }
}