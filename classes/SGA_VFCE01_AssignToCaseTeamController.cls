/*****************************************************************
* Class Name     : SGA_VFCE01_AssignToCaseTeamController
* Created By     : IDC Offshore
* Description 	  : This is the controller class for user assignment
*                  to case team.
* Change History : 
* ***************************************************************/
public with sharing class SGA_VFCE01_AssignToCaseTeamController {
    private static final string CASH_OP_REP = System.Label.SG122_CashOpsRepRole;
    private static final string APPLICATIONNAME = 'SGA Automation';
    private static final string SGA_VFCE01_ASSIGNCASETEAM = 'SGA_VFCE01_AssignToCaseTeamController';
    private static final string ASSIGNCASETEAM = 'assignToCaseTeam';
    private static final string CASERECORDS = 'caseRecords';
    private static final string SELECT_USER = System.Label.SG121_SelectUserToAssign;
    private static final string NO_CASE = System.Label.SG117_NoCasePresent;
    private static final string CANNOT_ASSIGN1 = System.Label.SG118_CanNotAssign_1;
    private static final string CANNOT_ASSIGN2 = System.Label.SG118_CanNotAssign_2;
    private static final string ADDED_TO_CASETEAM = System.Label.SG119_AddedToTeam;
    private static final string COMMA = ',';
    private static final string CASH_REP_EXISTS_ALL = System.Label.SG120_ExistsAllCases;
    public Case caseTeamObj {get;set;}
    public List<CaseTeamAssociationHelper> caseTeamList {get;set;}
    public string removeFromListId {get;set;}
    private List<String> selectedRecordList {get;set;}
    private static final string BLANK = '';

    private static ID caseTeamRoleId = [SELECT ID FROM CaseTeamRole WHERE NAME=:CASH_OP_REP LIMIT 1].ID;//cash ops rep team role id
    /***********************************************************************************
    * Method Name : displaySelRecOnPage
    * Parameters  : 
    * Return Type : void
    * Description : This method will at the time of page load and display the selected
    * 				records on the page.
    * *********************************************************************************/
    public void displaySelRecOnPage(){
        caseTeamObj = new Case();
        caseTeamList = new List<CaseTeamAssociationHelper>();
        selectedRecordList = new List<String>();
        String caseRecords = ApexPages.currentPage().getParameters().get(CASERECORDS);
        selectedRecordList = caseRecords.split(COMMA);
        for(Case caseObj : [SELECT ID,CASENUMBER,(SELECT ID,TeamRoleId FROM TEAMMEMBERS WHERE TeamRoleId=:caseTeamRoleId limit 1) FROM CASE WHERE ID IN :selectedRecordList LIMIT 1000]){
            List<CaseTeamMember> teamMemList = caseObj.TEAMMEMBERS;
            if(teamMemList != NULL && !teamMemList.isEmpty()){
                caseTeamList.add(createCaseTeam(caseObj.Id, caseObj.CaseNumber,true,BLANK));
            }else{
                caseTeamList.add(createCaseTeam(caseObj.Id, caseObj.CaseNumber,false,BLANK));
            }
        }
    }
    /***********************************************************************************
    * Method Name : assignToCaseTeam
    * Parameters  : 
    * Return Type : void
    * Description : This method will be called when user clicks on the save button
    * 				 1. Checking the user is selected the member to assign to case team
    * 				 2. Checking at least one case in the list
    * 				 3. Checking the user is already exists not as cash ops rep
    * 				 4. Adding the selected user into case teams.
    * *********************************************************************************/
    public void assignToCaseTeam(){
        try{
            List<CaseTeamMember> caseTeamMemberList = new List<CaseTeamMember>();
            //checking the user selected the member to assign
            if(caseTeamObj.AssignCaseTeamUser__c == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,SELECT_USER));
                return;
            }
            //checking the case list is empty or not
            if(caseTeamList.isEmpty()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,NO_CASE));
                return;
            }
            Map<String,String> memberExistCases = new Map<String,String>();
            for(CASETEAMMEMBER caseTeamMeber : [SELECT PARENTID,TeamRoleId,TeamRole.Name FROM CASETEAMMEMBER WHERE PARENTID IN :selectedRecordList AND 
                                                MEMBERID =:caseTeamObj.AssignCaseTeamUser__c LIMIT 10000])
            {
                memberExistCases.put(caseTeamMeber.ParentId,caseTeamMeber.TeamRole.Name);
            }
            Boolean existsForAll = true;//variable to check the cash ops rep is exists for all the cases
            for(CaseTeamAssociationHelper caseTeamHelperObj : caseTeamList){
                if(memberExistCases.containsKey(caseTeamHelperObj.caseId) && !caseTeamHelperObj.isUserExists){
                    caseTeamHelperObj.caseTeamMemberStatus = CANNOT_ASSIGN1 + memberExistCases.get(caseTeamHelperObj.caseId) + CANNOT_ASSIGN2;
                    existsForAll = false;
                }else if(!memberExistCases.containsKey(caseTeamHelperObj.caseId) && !caseTeamHelperObj.isUserExists){
                    CaseTeamMember caseTeamMemObj = new CaseTeamMember();
                    caseTeamMemObj.MemberId = caseTeamObj.AssignCaseTeamUser__c;
                    caseTeamMemObj.ParentId = caseTeamHelperObj.caseId;
                    caseTeamMemObj.TeamRoleId = caseTeamRoleId;
                    caseTeamMemberList.add(caseTeamMemObj);
                    caseTeamHelperObj.caseTeamMemberStatus = ADDED_TO_CASETEAM;
                    caseTeamHelperObj.isUserExists = true;
                    existsForAll = false;
                }else if(caseTeamHelperObj.isUserExists && caseTeamHelperObj.caseTeamMemberStatus == ADDED_TO_CASETEAM){
                    caseTeamHelperObj.caseTeamMemberStatus = BLANK;
                }
            }
            if(existsForAll){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,CASH_REP_EXISTS_ALL));
                return;
            }
            if(!caseTeamMemberList.isEmpty()){
                Database.Insert(caseTeamMemberList); 
            }
        }catch(Exception ex){UTIL_LoggingService.logHandledException(ex, UserInfo.getOrganizationId(), APPLICATIONNAME,SGA_VFCE01_ASSIGNCASETEAM,ASSIGNCASETEAM, BLANK, Logginglevel.ERROR); }     
    }
    /***********************************************************************************
    * Method Name : remove
    * Parameters  : 
    * Return Type : void
    * Description : This method will be called when user clicks on remove icon
    * 				 It removes from the record from the wrapper list.
    * *********************************************************************************/
    public void remove(){
        for(Integer i=0;i < caseTeamList.size() ; i++){
            if(String.isNotBlank(removeFromListId) && removeFromListId.equalsIgnoreCase(caseTeamList[i].caseId)){
                caseTeamList.remove(i);
            }
        }
    }
    /*********************************************************************
    * Class Name  : CaseTeamAssociationHelper
    * Description : Used to construct the case team member list 
    * 				 and display in the visualforce page
    * ******************************************************************/
    public without sharing class CaseTeamAssociationHelper
    {
        public string caseId{get;set;}
        public string caseNumber{get;set;}
        public Boolean isUserExists{get;set;}
        public string caseTeamMemberStatus{get;set;}
        /******************************************************************************************
        * Constructor for the class : constructing the case team member association wrapper list
        ******************************************************************************************/
        public CaseTeamAssociationHelper(String caseId, String caseNumber,Boolean isUserExists,String status){
            this.caseId = caseId;
            this.caseNumber = caseNumber;
            this.isUserExists = isUserExists;
            this.caseTeamMemberStatus = status;
        }
    }
    /***********************************************************************************
    * Method Name : createCaseTeam
    * Parameters  : String caseId,String caseNumber,Boolean isUserExists,String status
    * Return Type : CaseTeamAssociationHelper
    * Description : This method is used to create object for caseteamassociationhelper
    * *********************************************************************************/
    private CaseTeamAssociationHelper createCaseTeam(String caseId,String caseNumber,Boolean isUserExists,String status){
        return new CaseTeamAssociationHelper(caseId, caseNumber,isUserExists,status);
    }
}