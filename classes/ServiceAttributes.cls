global with sharing class ServiceAttributes implements vlocity_ins.VlocityOpenInterface{

     public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        
        Boolean success = true;
        
        system.debug(inputMap);
                system.debug(methodName);


        try{
        
        
          //  getAttributes(inputMap, outMap, options);
        
           // if(methodName == 'medicalAttributes') {
            if(methodName =='getAttributes'){
               getAttributes(inputMap, outMap, options);
                
            //}
               
              //getproducts('ap_Medical_NY');
               
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }

        try{
            if(methodName == 'dentalAttributes') {
                
               // getProducts('ap_Dental_NY');
                
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        try{
            if(methodName == 'visionAttributes') {
               
               //getProducts('ap_Vision_NY');
               
            }
        }catch(Exception e){
            System.debug(' exception '+e);
            success = false;
            throw e;
        }
        
        return success;

    }
        
    // public void getProducts(string action){
    public void getAttributes(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){  
        list<string> querystr = new list<string>();
             
        try {
            String StateField = (String)InputMap.get('MinState');
            string str = string.valueOf(InputMap.get('planTypePage'));
            string planType = string.valueOf(InputMap.get('planType'));
            
            if (planType == 'dental') {
                querystr.add('ap_Dental_NY');
            }
            
            if(planType == 'medical') {
                querystr.add('ap_Medical_NY');
            }
            
            if(planType == 'vision'){
                querystr.add('ap_Vision_NY');
            }

            list<APS_Attribute_Display__mdt> APSAttributes = [select id, DeveloperName , APS_Attribute_Display_Field__c from APS_Attribute_Display__mdt where developername in : querystr];
            //Map<String,Object> APS_Attribute_Display_Field = (Map<String,Object>) inputMap.get('getattributes');
            //system.debug('Json data'+attribute.APS_Attribute_Display_Field); // This is json data
            
            //APS_Attibute_Display_class.APS_Attibute_Display_classdata out = APS_Attibute_Display_class.parse(attribute .APS_Attribute_Display_Field__c ); // This is formated data
            outMap.put('APSAttributes', APSAttributes);
            
        } catch (exception e) {
            throw e;
        }

    }   

}