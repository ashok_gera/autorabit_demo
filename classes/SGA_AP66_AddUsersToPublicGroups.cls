public class SGA_AP66_AddUsersToPublicGroups {
    
    public static void AddToGroups(Set<Id> userIds)
    {
        system.debug('#############userid');
        System.debug('User Ids:'+userIds);
        //Get the groups that the user should be added to
        List<Group> gList=[select Id,Name from Group Where Type ='Regular' AND (Name = 'Anthem SG(Non Quoting)' OR Name ='Anthem SG(Quoting)')];
        Map<String,Id> groupMap = new Map<String,Id>();
        if(gList != null && gList.size()>0)
        {
            for(Group g : gList)
            {
                groupMap.put(g.name, g.id);
            }
            
            System.debug('Map Quoting Value:'+groupMap.get('Anthem SG(Quoting)'));
            
           List<User> users=[Select Id,Name,Enabled_to_Work__c,Profile.Name from user Where Id IN :userIds AND((Profile.Name = 'Anthem SG Broker')OR(Profile.Name ='Anthem SG Broker (Non Quoting)'))];           
            System.debug('User list:'+users.size());
            List<GroupMember>listGroupMember =new List<GroupMember>();
            if(users != null && users.size()>0)
            {
                //loop the users that have been created
                for (User user : users)
                {
                    GroupMember gm= new GroupMember(); 
                    if(User.Enabled_to_Work__c=='NY')
                    {
                        gm.GroupId=groupMap.get('Anthem SG(Quoting)');
                        gm.UserOrGroupId = user.id;     
                        listGroupMember.add(gm); 
                        system.debug('groupmember'+ listGroupMember.size());
                    }
                    if((User.Enabled_to_Work__c=='CO'||User.Enabled_to_Work__c=='CA'))
                    {
                        gm.GroupId=groupMap.get('Anthem SG(Non Quoting)');
                        gm.UserOrGroupId = user.id;     
                        listGroupMember.add(gm); 
                       // system.debug('groupmember'+ listGroupMember.add(gm));
                    }
                    
              
                }
                insert listGroupMember;
            }
        }
    }
  
public static void DeteleFromGroups(Set<Id> userIds)
{
     List<Group> gList=[select Id,Name from Group Where Type ='Regular' AND (Name = 'Anthem SG(Non Quoting)' OR Name ='Anthem SG(Quoting)')];
        Map<String,Id> groupMap = new Map<String,Id>();
        if(gList != null && gList.size()>0)
        {
            for(Group g : gList)
            {
                groupMap.put(g.name, g.id);
            }
            
    List<GroupMember> listGroupMember =new List<GroupMember>();        //Group member instance
    List<User> users=[Select Id, Name,Profile.Name from user Where Id IN :userIds]; //Fetching user
   
	Set<ID> setGroupId = new Set<Id>();
	Set<ID> setUserOrGroupId = new Set<Id>();
	
    for (User user : users)
	{
        //GroupMember gm= new GroupMember(); 
  if((user.Profile.Name != 'Anthem SG Broker') || (user.Profile.Name != 'Anthem SG Broker (Non Quoting)')) 		{
				setGroupId.add(groupMap.get('Anthem SG(Non Quoting)'));
                setGroupId.add(groupMap.get('Anthem SG(Quoting)'));
				setUserOrGroupId.add(user.id);
				system.debug('#############groupid');
                //gm.GroupId=g.id;
                //gm.UserOrGroupId = user.id;
               	//listGroupMember.add(gm);
        }
    }
    
    if( setGroupId.size() > 0 && setUserOrGroupId.size() > 0 )
	{
		List<GroupMember> ListGM  = [ Select Id from GroupMember where UserOrGroupID in :setUserOrGroupId and GroupId  in :setGroupId ];
        
            
        
		if(ListGM.size() > 0 )
		{
			delete ListGM;
		}
		
    }
	
}	
}
}