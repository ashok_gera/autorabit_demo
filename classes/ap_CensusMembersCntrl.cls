/*
** 
** JIRA #: 1148
** Sprint: Anthem Prime - Optimus - Sprint 5.5
** UI Page: https://6lgk0b.axshare.com/#g=1&p=mini_census_full
** Author: Muthu Rajagopalan
** Date: 1/2/2018
**
** Census Members Controller Class
** VF Page: ap_CensusMembers.vfp
**
** Design
** ======
** Operations:
**  1. Get Census Members; 
**  2. Add Employee / Dependent; 
**  3. Delete Employee / Dependent;
**  4. Edit Employee / Dependent;
**  5. Save Record
**
** Inputs:
** Census Header Record Id 
*/

global with sharing class ap_CensusMembersCntrl {

    private string strAccountId;
    private string strCensusHeaderId;
    private boolean bolDebugMsgFlag = true;
    public List<wrpCensusMembers> censusMembersList{set;get;}  
    public integer intCMListRecNum{set;get;}


    
    public class wrpCensusMembers {
        /*
        ** Wrapper class which includes
        **      a. Census Member Record
        **      b. Record Edit Flag to display the record in Edit Mode (true for new record and edit record)
        **      c. Selected Record Flag to indicate whether the record is selected or not (true if the record is selected)
        */
        public vlocity_ins__GroupCensusMember__c cmRecord {get;set;}
        public boolean cmRecEditFlag {get;set;}
        public boolean cmRecSelectFlag {get;set;}
        
        // Constructor method for the wrapper class to assign the values
        public wrpCensusMembers (vlocity_ins__GroupCensusMember__c cmRec,  boolean bolEdit, boolean bolSelect) {
            cmRecord = cmRec;
            cmRecEditFlag = bolEdit;
            cmRecSelectFlag = bolSelect;
        }
    }
    
    // Constructor method which calls 'cmLoadRecords' method to load the wrapper class
    public  ap_CensusMembersCntrl() {
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.ap_CensusMembersCntrl: Entering');
            strAccountId = ApexPages.currentPage().getParameters().get('accid');

            // need to remove from here to here
            strCensusHeaderId = ApexPages.currentPage().getParameters().get('census');
            if (strCensusHeaderId == null || strCensusHeaderId == '') {
                // Census Header Id is null. Do not proceed
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ap_CensusMembersCntrl: Census Header Id should be passed as URL parameter "census"'));
            } else if (strCensusHeaderId != null && strCensusHeaderId != '') {
                cmLoadRecords();
            } else 
            // need to remove from here to here

            if (strAccountId == null || strAccountId == '') {
                // Account Id is null. Do not proceed
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ap_CensusMembersCntrl: Account Id should be passed as URL parameter "accid"'));
            } else {
                setCensusHeaderId();
                if (strCensusHeaderId == null || strCensusHeaderId == '') {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ap_CensusMembersCntrl: Unable to get Census Header Record Id'));
                    // Census Header Id is null. Do not proceed
                } else {
                    cmLoadRecords();
                }
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.ap_CensusMembersCntrl: Exiting');   
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ap_CensusMembersCntrl: ' + e.getMessage()));
        }
    }
    
    private void cmLoadRecords() {
    // Loads Census Members records into 'cmRecord' list
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmLoadRecords: Entering');   
            // Check for missing Census Header Id
            if (strCensusHeaderId == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmLoadRecords: NULL Census Header Id - Unable to get Census Header Id'));
            } else if (strCensusHeaderId == '') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmLoadRecords: BLANK Census Header Id - Unable to get Census Header Id'));
            } else {

                vlocity_ins__GroupCensus__c censusRecord;
                list <vlocity_ins__GroupCensus__c> censusRecordList;
                censusRecordList = Database.query('select id, name, vlocity_ins__GroupId__c ' +
                                                                    ' from vlocity_ins__GroupCensus__c ' + 
                                                                    ' where id = \'' + strCensusHeaderId + 
                                                                    '\' Limit 1');
                if (censusRecordList.isEmpty()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmLoadRecords: Could not get Census Header for the Census Header Id (cenid) = "' + strCensusHeaderId + '"'));
                    return;
                }

                    // Query to load all Employee Records (i.e. Primary Member Flag is true)
                string strEmpQuery =  'SELECT Id, Name, vlocity_ins__FirstName__c, vlocity_ins__Birthdate__c, ' + 
                    'vlocity_ins__Gender__c, EmployeeSpouseDependentIndicator__c, '+
                    'vlocity_ins__IsPrimaryMember__c, vlocity_ins__RelatedCensusMemberId__c, vlocity_ins__CensusId__c ' + 
                    'FROM vlocity_ins__GroupCensusMember__c ' +  
                    'where vlocity_ins__CensusId__c = \'' + strCensusHeaderId + '\'  AND vlocity_ins__IsPrimaryMember__c = true ' +
                    'ORDER BY id ASC ' +
                    'LIMIT 100';
                
                // Query to load Dependent records  (i.e. Primary Member Flag is false)
                string strDepQuery = 'SELECT Id, Name, vlocity_ins__FirstName__c, vlocity_ins__Birthdate__c, ' + 
                    'vlocity_ins__Gender__c, EmployeeSpouseDependentIndicator__c, '+
                    'vlocity_ins__IsPrimaryMember__c, vlocity_ins__RelatedCensusMemberId__c, vlocity_ins__CensusId__c ' + 
                    'FROM vlocity_ins__GroupCensusMember__c ' + 
                    'where vlocity_ins__CensusId__c = \'' + strCensusHeaderId + '\'  AND vlocity_ins__IsPrimaryMember__c = false ' +
                    'ORDER BY vlocity_ins__RelatedCensusMemberId__c ASC ' +
                    'LIMIT 100'; 
                
                censusMembersList = new List<wrpCensusMembers>();
                
                List<vlocity_ins__GroupCensusMember__c> cmEmpList; // List for Employee Records
                List<vlocity_ins__GroupCensusMember__c> cmDepList; // List of Dependent Records
                vlocity_ins__GroupCensusMember__c cmEmpRec;
                vlocity_ins__GroupCensusMember__c cmDepRec;
                
                integer intEmp = 0;
                integer intDep = 0;
                
                integer intEmpCount;
                integer intDepCount;

                boolean bolDepRecAvl = true;
                
                // Execute the queries
                cmEmpList = Database.query(strEmpQuery);
                cmDepList = Database.query(strDepQuery);
                
                // Get the sizes of the Employee and Dependent Lists
                intEmpCount = cmEmpList.size();
                intDepCount = cmDepList.size();
                
                if (bolDebugMsgFlag) system.debug('intEmpCount: ' + intEmpCount);
                if (bolDebugMsgFlag) system.debug('intDepCount: ' + intDepCount);
                
                /*
                 * Logic to match Employee and Dependent records
                 * =============================================
                 * 1. Employee list is sorted by record id.
                 * 2. Dependent list is sorted by Employee record id. 
                 * 3. Employee record is added to the Census Members List.
                 * 4. If Dependent record is NOT part of the Employee record, Fetch Next Employee record and goto step 3
                 * 5. If Dependent record is part of the Employee record, it is added to the Census Members List.
                 * 6. Fetch Next Dependent record and goto step 4
                */
                while (intEmp < intEmpCount) {
                    cmEmpRec = cmEmpList.get(intEmp);
                    censusMembersList.add(new wrpCensusMembers(cmEmpRec, false, false));
                    while (intDep < intDepCount && bolDepRecAvl == true) {
                        cmDepRec = cmDepList.get(intDep);
                        if (cmEmpRec.id == cmDepRec.vlocity_ins__RelatedCensusMemberId__c) {
                            censusMembersList.add(new wrpCensusMembers(cmDepRec, false, false));
                            intDep++;
                        } else {
                            bolDepRecAvl = false;
                        }
                    }
                    intEmp++;
                    bolDepRecAvl = true;
                }
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmLoadRecords: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmLoadRecords: ' + e.getMessage()));
        }
    }
    
    public void cmAddNewRecord() {
        /*
         *  1. Adds a new list element to the Census Member list. No DML operations. 
         *  2. If the request is to add a dependent record, then
         *      a. the new list element is added afer the Employee list element.
         *      b. Primary flag is set as false
         *      c. 
         *  3. If the request is to add a new employee record, the new list item is added as the last element.
        */
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmAddNewRecord: Entering');
            if (strCensusHeaderId == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmAddNewRecord: NULL Census Header Id - Unable to get Census Header Id'));
            } else if (strCensusHeaderId == '') {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmAddNewRecord: BLANK Census Header Id - Unable to get Census Header Id'));
            } else {
                integer intEditCMRecCount=0;
                integer intSelectedCMRecCount=0;
                boolean bolChildRec = false;

                intEditCMRecCount = fnEditCMRecCount();

                if (intEditCMRecCount > 0)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record is being edited. Please save or cancel the changes before adding a new record'));
                } else {
                    intSelectedCMRecCount = fnSelectedCMRecCount();
                    if (intSelectedCMRecCount > 1)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Multiple checkboxes are selected. Please select only one checkbox before adding a new record'));
                    } else {
                            // if a check box is selected, then consider the request as to add a dependent record
                        for (integer i=0; i < censusMembersList.size() && bolChildRec == false; i++) {
                            if (censusMembersList.get(i).cmRecSelectFlag) {
                                intCMListRecNum = i; 
                                bolChildRec = true;
                            } // if
                        } // for
                        vlocity_ins__GroupCensusMember__c cmNewRec = new vlocity_ins__GroupCensusMember__c();
                        if (bolChildRec == true) {
                            // Dependent Record
                            cmNewRec.vlocity_ins__IsPrimaryMember__c = false;
                            cmNewRec.vlocity_ins__RelatedCensusMemberId__c = censusMembersList.get(intCMListRecNum).cmRecord.Id;
                            cmNewRec.vlocity_ins__CensusId__c = strCensusHeaderId;

                            // check whether the new dependent element is the last record in the list
                            if (censusMembersList.size() == (intCMListRecNum + 1)) {
                                censusMembersList.add(new wrpCensusMembers(cmNewRec, true, false));
                            } else {
                                censusMembersList.add(intCMListRecNum + 1, new wrpCensusMembers(cmNewRec, true, false));
                            }
                        } else {
                            // Employee Record
                            cmNewRec.EmployeeSpouseDependentIndicator__C = 'Employee';
                            cmNewRec.vlocity_ins__IsPrimaryMember__c = true;
                            cmNewRec.vlocity_ins__CensusId__c = strCensusHeaderId;
                            censusMembersList.add(new wrpCensusMembers(cmNewRec, true, false));
                        } // else employee record
                    } // intSelectedCMRecCount <= 1
                } // intEditCMRecCount <= 0
            } // Census Header Id is available
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmAddNewRecord: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmAddNewRecord: ' + e.getMessage()));
        }
    }
    
    public void cmSaveRecord() {
        /*
         * Saves the record into Salesforce and refreshes the Census Member list
         * Before Saving, the validations are done for: Required Fields, "DOB" field shouldn't be in future and 
         * "Relationship" field can't be "Employee" for dependents.
         * Records are reloaded into list after save
        */      
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmSaveRecord: Entering');
            boolean bolErrorFlag = false;
            if (intCMListRecNum == null) {
                if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmSaveRecord: Unable to get intCMListRecNum from CensusMembers.vfp');
            } else {
                vlocity_ins__GroupCensusMember__c cmRecToBeSaved = censusMembersList.get(intCMListRecNum).cmRecord;
                if (cmRecToBeSaved.Name == null || cmRecToBeSaved.vlocity_ins__FirstName__c == null || cmRecToBeSaved.vlocity_ins__Birthdate__c == null || cmRecToBeSaved.vlocity_ins__Gender__c == null || cmRecToBeSaved.EmployeeSpouseDependentIndicator__c == null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter values in all fields before saving a Census Member record'));
                    bolErrorFlag = true;
                } 
                if (cmRecToBeSaved.vlocity_ins__Birthdate__c != null && cmRecToBeSaved.vlocity_ins__Birthdate__c > Date.today()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Invalid "Date of Birth". "Date of Birth" should not be in future'));
                    bolErrorFlag = true;
                }
                if (cmRecToBeSaved.vlocity_ins__IsPrimaryMember__c == false && cmRecToBeSaved.EmployeeSpouseDependentIndicator__c == 'Employee'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Dependent record should not have "Employee" as Relationship"'));
                    bolErrorFlag = true;
                } 
                if (bolErrorFlag == false) {
                    upsert cmRecToBeSaved;
                    cmLoadRecords();
                }
                
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmSaveRecord: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmSaveRecord: ' + e.getMessage()));
        }
    }
    
    public void cmEditRecord() {
        /*
         * Sets the cmRecEditFlag of the list element to true. So the records will be shown in edit mode in the UI
        */
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmEditRecord: Entering');
            integer intEditCMRecCount=0;
            integer intSelectedCMRecCount=0;
            intEditCMRecCount = fnEditCMRecCount();
            if (intEditCMRecCount > 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record is being edited. Please save or cancel the changes before editing another record'));
            } else {
                intSelectedCMRecCount = fnSelectedCMRecCount();
                if (intSelectedCMRecCount > 1) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Multiple checkboxes are selected. Please deselect all checkboxes before editing record'));
                } else if (intSelectedCMRecCount == 1) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'One checkbox is selected. Please deselect checkbox before editing record'));
                } else {
                    if (intCMListRecNum==null) {
                        if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmEditRecord: Unable to get intCMListRecNum from CensusMembers.vfp');
                    } else {
                        vlocity_ins__GroupCensusMember__c cmEditRec = censusMembersList.get(intCMListRecNum).cmRecord;
                        censusMembersList.get(intCMListRecNum).cmRecEditFlag = true;
                    }
                }
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmEditRecord: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmEditRecord: ' + e.getMessage()));
        }
    }

    public void cmCancelChanges() {
        /*
         * The changes are canceled for the "Editable" record by reloading the list from backend
        */
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmCancelChanges: Entering');
            if (intCMListRecNum==null) {
                if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmCancelChanges: Unable to get intCMListRecNum from CensusMembers.vfp');
            } else {
                vlocity_ins__GroupCensusMember__c cmEditRec = censusMembersList.get(intCMListRecNum).cmRecord;
                censusMembersList.get(intCMListRecNum).cmRecEditFlag = false; // this step is not required. bust done just to understand the flow
                cmLoadRecords();
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmCancelChanges: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmCancelChanges: ' + e.getMessage()));
        }
        
    }

    public void cmDelRecord() {
        /*
         * Deletes one dependent record or deletes the employee and its dependent records.
         * Records are reloaded into list after deletion
        */    
        try {
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmDelRecord: Entering');
            integer intEditCMRecCount=0;
            integer intSelectedCMRecCount=0;
            intEditCMRecCount = fnEditCMRecCount();
            if (intEditCMRecCount > 0)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Record is being edited. Please save or cancel the changes before deleting a record'));
            } else {
                intSelectedCMRecCount = fnSelectedCMRecCount();
                if (intSelectedCMRecCount > 1) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Multiple checkboxes are selected. Please deselect all checkboxes before deleting record'));
                } else if (intSelectedCMRecCount == 1) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'One checkbox is selected. Please deselect checkbox before deleting record'));
                } else {
                    if (intCMListRecNum==null) {
                        if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmDelRecord: Unable to get intCMListRecNum from CensusMembers.vfp');
                    } else {
                        vlocity_ins__GroupCensusMember__c cmDelRec = censusMembersList.get(intCMListRecNum).cmRecord;
                        List <vlocity_ins__GroupCensusMember__c> cmDelDepRec = 
                            Database.query('Select Id from vlocity_ins__GroupCensusMember__c where vlocity_ins__RelatedCensusMemberId__c= \'' + cmDelRec.id + '\'');
                        delete cmDelDepRec; // Delete the dependent records if found
                        delete cmDelRec; // Delete the record for which delete button is clicked. The record can be employee or dependent
                        cmLoadRecords();
                    }
                }
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.cmDelRecord: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'cmDelRecord: ' + e.getMessage()));
        }
        
    }
    
    private void setCensusHeaderId(){
        try {
            List<vlocity_ins__GroupCensus__c> censusRecordList;
            vlocity_ins__GroupCensus__c censusRecord;

            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.setCensusHeaderId: Entering');
            Account accRecord = Database.query('select id, name from Account where id = \''
                                               + strAccountId + '\'  Limit 1');
            if (accRecord == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'getCensusHeaderId: Could not get Account for the Account Id = "' + strAccountId + '"'));
                strCensusHeaderId = '';
            } else {
                if (bolDebugMsgFlag) system.debug('setCensusHeaderId 1');
                censusRecordList = Database.query('select id, name, vlocity_ins__GroupId__c ' +
                                                                    ' from vlocity_ins__GroupCensus__c ' + 
                                                                    ' where vlocity_ins__GroupId__c = \'' + strAccountId + 
                                                                    '\' Limit 1');
                if (bolDebugMsgFlag) system.debug('setCensusHeaderId 2');
                if (censusRecordList.isEmpty()) {
                    if (bolDebugMsgFlag) system.debug('setCensusHeaderId 3');
                    // Create a new Census Record and get the Census Id
                    censusRecord = new vlocity_ins__GroupCensus__c();
                    censusRecord.Name = 'New Census Record - ' + String.valueOf(Date.today());
                    censusRecord.vlocity_ins__GroupId__c = strAccountId;
                    // censusRecord.vlocity_ins__CensusStatus__c = 'Draft';
                    try {
                        insert censusRecord;
                    } catch (Exception es) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'setCensusHeaderId: Unable to create Census Header Record: ' + es.getMessage()));
                        strCensusHeaderId = '';
                        return;
                    }
                        
                    if (bolDebugMsgFlag) system.debug('setCensusHeaderId 4');
                    
                } else {
                    if (bolDebugMsgFlag) system.debug('setCensusHeaderId 5');
                    censusRecord = censusRecordList.get(0);
                    if (bolDebugMsgFlag) system.debug('setCensusHeaderId 6');
                }
                if (bolDebugMsgFlag) system.debug('setCensusHeaderId 7');
                strCensusHeaderId = censusRecord.id;
                if (bolDebugMsgFlag) system.debug('setCensusHeaderId 8');
            }
            if (bolDebugMsgFlag) system.debug('ap_CensusMembersCntrl.setCensusHeaderId: Exiting');
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'setCensusHeaderId: ' + e.getMessage()));
            strCensusHeaderId = '';
        }

    }
    
    private integer fnSelectedCMRecCount()    {
        /*
         * Returns number of selected (Checkbox) records in the UI.
         * It can be either 0 or 1. 
       */
        try {
            integer intNumSelRec = 0;
            for (integer i=0; i < censusMembersList.size(); i++) {
                if (censusMembersList.get(i).cmRecSelectFlag) {
                    intNumSelRec++;
                }
            }
            return intNumSelRec;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'fnSelectedCMRecCount: ' + e.getMessage()));
            return 9999;
        }
        
    }
    
    private integer fnEditCMRecCount()    {
        /*
         * Returns number of records being edited in the UI
         * It can be either 0 or 1. 
        */
        try {
            integer intNumEditRec = 0;
            for (integer i=0; i < censusMembersList.size(); i++) {
                if (censusMembersList.get(i).cmRecEditFlag) {
                    intNumEditRec++;
                }
            }
            return intNumEditRec;
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'fnEditCMRecCount: ' + e.getMessage()));
            return 9999;
        }
    }
    
}