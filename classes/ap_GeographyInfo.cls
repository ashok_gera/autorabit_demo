/**********************************************************************************
Class Name :   ap_GeographyInfo
Date Created : 01-January-2018 
Created By   : Pilot user
Description : 1. This class gets geography info.
*************************************************************************************/
global with sharing class ap_GeographyInfo {
    private static final String BROKERPROFILE='Anthem SG Broker';
    private static final String EMPTY='';    
    //public GeographyInfo() { }
/****************************************************************************************************
    Method Name : getCounty
    Parameters  : String zipcode
    Return type : String
    Description : This method queries the list of Geographical_Info__c object.
******************************************************************************************************/	
    @RemoteAction
    global static List<Geographical_Info__c> getCounty(String ZipCode) {
     List<Geographical_Info__c> County= [SELECT Id,Name,Area__c,County__c,RatingArea__c,SearchField__c,Zip_Code__c,State__c FROM Geographical_Info__c WHERE (Zip_Code__c LIKE : ZipCode AND State__c='NY') ORDER BY Zip_Code__c];
        return County;   
    }   
/****************************************************************************************************
    Method Name : getStateLicenseStatus
    Parameters  : String zipcode1
    Return type : String
    Description : This method verifies the license of broker based on state.
******************************************************************************************************/
    @RemoteAction
    global static String getStateLicenseStatus(String zipcode1) {
        String status=EMPTY;
        User usr;
        List<License_Appointment__c> appLcns;
        usr = [SELECT ContactId, AccountId,Profile.name FROM User WHERE Id = :UserInfo.getUserId()];
        if(BROKERPROFILE.equalsIgnoreCase(usr.Profile.name)){
            Geographical_Info__c geoGraphicInfo = [SELECT Id, Zip_Code__c, State__c from Geographical_Info__c where Zip_Code__c =:zipcode1 LIMIT 1];
        }
        else{
            status=EMPTY;
        }
        return status;     
    }
}