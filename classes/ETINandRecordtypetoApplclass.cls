public class ETINandRecordtypetoApplclass{
 public void etinandrecordtype(set<id> accid,list<vlocity_ins__Application__c> liappl){
     Map<ID, Account> accmap = new Map<ID, Account>([SELECT Id, Name,Company_State__c, BR_Encrypted_TIN__c FROM Account where id in : accid]);
       list<recordtype> applrecrdtyp = [select Id,Name from RecordType where SobjectType = 'vlocity_ins__Application__c'];
        map<string,id> accRcrdtypmap = new map<string,id>();
         for(recordtype aa: applrecrdtyp ){
         accrcrdtypmap.put(aa.name,aa.id);
         }
         for(vlocity_ins__Application__c ap: liappl){
        if(ap.Broker_Agent_Name_1__c != null && accmap.get(ap.Broker_Agent_Name_1__c).BR_Encrypted_TIN__c != null){
             ap.WritingAgentETIN__c = accmap.get(ap.Broker_Agent_Name_1__c).BR_Encrypted_TIN__c ;
             }else{
             ap.WritingAgentETIN__c = '';
             }
            if(ap.General_Agency_Name1__c != null && accmap.get(ap.General_Agency_Name1__c).BR_Encrypted_TIN__c != null){
             ap.Parent_Agency_ETIN__c = accmap.get(ap.General_Agency_Name1__c).BR_Encrypted_TIN__c;
             }else{
             ap.Parent_Agency_ETIN__c = '';
             }
              if(ap.Paid_Agency_Name1__c != null && accmap.get(ap.Paid_Agency_Name1__c).BR_Encrypted_TIN__c != null){
              ap.Paid_Agency_ETIN__c = accmap.get(ap.Paid_Agency_Name1__c).BR_Encrypted_TIN__c ; 
              }else{
             ap.Paid_Agency_ETIN__c = '';
             }
             if(ap.vlocity_ins__AccountId__c != null && accmap.get(ap.vlocity_ins__AccountId__c).Company_State__c != null){
             string recordtypename = accmap.get(ap.vlocity_ins__AccountId__c).Company_State__c ;
                 if(ap.vlocity_ins__Type__c == 'Paper'){
            ap.recordtypeid = accrcrdtypmap.get(recordtypename);
                         }else{
                    ap.recordtypeid = null; 
                     }
             }
           }    
      }
}