/**********************************************************************************
Class Name :   SGA_AP53_CensusMemberCtrl_Test
Date Created : 29-November-2017
Created By   : IDC Offshore
Description  : 1. This class is the test class for SGA_AP53_CensusMemberCtrl
*************************************************************************************/
@isTest
private class SGA_AP53_CensusMemberCtrl_Test { 
/************************************************************************************
    Method Name : testCallPicklistvaluesMethods
    Parameters  : None
    Return type : void
    Description : This is  testmethod for calling picklistvalues of fields
*************************************************************************************/
    private static testMethod void testCallPicklistvaluesMethods(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c gpcns=Util02_TestData.createCensusData();                     
            ApexPages.StandardController scr = new ApexPages.StandardController(gpcns);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            List<SelectOption> opts1=censusCtrl.getgenderValues();
            List<SelectOption> opts2=censusCtrl.getdependentIndicatorValues();
            List<SelectOption> opts3=censusCtrl.getdependentIndicatorEmp();
            List<SelectOption> opts4=censusCtrl.getchilddependentIndicatorValues();
            List<SelectOption> opts5=censusCtrl.getmedicalCoverageTypeValues();
            List<SelectOption> opts6=censusCtrl.getchildCoverageTypeValues();
            List<SelectOption> opts7=censusCtrl.getdentalCoverageTypeValues();
            List<SelectOption> opts8=censusCtrl.getvisionCoverageTypeValues();
            List<SelectOption> opts9=censusCtrl.getlifeClassValues();
            List<SelectOption> opts10=censusCtrl.getcobraValues();
            List<SelectOption> opts11=censusCtrl.getearningsValues();
        } 
    }
/*This method creates a single census wrapper record.*/
    public static SGA_AP53_CensusMemberCtrl.censusWrapper createCensusEmployeeWrapper(Id censusId){
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp ;
		List<SGA_AP53_CensusMemberCtrl.censusWrapper> childList=new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
        //cnemp.childList = new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
        cnemp = new SGA_AP53_CensusMemberCtrl.censusWrapper(1,childList,censusId,null,'TestCensusMember','Employee',Date.ValueOf('1985-07-01'),0,'Male','No','Employee+Child','Employee+Child','Employee+Child','1',12000,'Annually');                    
        cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(2,null,censusId,null,'TestCensusDep','Child',null,15,'Female','No','Employee','Employee','Employee',null,0,null));
        return cnemp;
	}
/*This method creates a single census wrapper with multiple child records.*/
    public static SGA_AP53_CensusMemberCtrl.censusWrapper createCensusEmployeeDepWrapper(Id censusId){
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp ;
		List<SGA_AP53_CensusMemberCtrl.censusWrapper> childList=new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
        //cnemp.childList = new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();
        cnemp = new SGA_AP53_CensusMemberCtrl.censusWrapper(1,childList,censusId,null,'TestCensusMember','Employee',Date.ValueOf('1985-07-01'),0,'Male','No','Employee+Child','Employee+Child','Employee+Child','1',12000,'Annually');                    
        cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(2,null,censusId,null,'TestCensusDep','Child',null,15,'Female','No','Employee','Employee','Employee',null,0,null));
        cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(3,null,censusId,null,'TestCensusDep','Spouse',null,20,'Female','No','Employee+Spouse','Employee+Spouse','Employee',null,0,null));
        cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(4,null,censusId,null,'TestCensusDep','Child',null,15,'Female','No','Employee+Child','Employee+Spouse','Employee',null,0,null));
        return cnemp;
        
	}    
/************************************************************************************
    Method Name : testfetchCensusMemberRec_Excp
    Parameters  : None
    Return type : void
    Description : This is  testmethod for fetchCensusMemberRec Method with exception scenario
*************************************************************************************/
    private static testMethod void testfetchCensusMemberRec_Excp(){
        User testUser = Util02_TestData.createUser();    
        vlocity_ins__GroupCensusMember__c censusMem = Util02_TestData.CreateCensusMember(); 
        vlocity_ins__GroupCensusMember__c censusDep; 
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c gpcns=Util02_TestData.createCensusData();
            censusMem.vlocity_ins__CensusId__c = gpcns.Id;
            Database.insert(censusMem);
            censusDep=Util02_TestData.createCensusDependents(censusMem.id);
            censusDep.vlocity_ins__CensusId__c = gpcns.Id;
            Database.insert(censusDep);
            ApexPages.StandardController scr = new ApexPages.StandardController(gpcns);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            Test.startTest();
            censusCtrl.fetchCensusMemberRec();
            Test.stopTest();
        }
     }
   
/************************************************************************************
    Method Name : testfetchCensusMemberRec
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchCensusMemberRec Method
**************************************************************************************/
    private static testMethod void testfetchCensusMemberRec(){
        User testUser = Util02_TestData.createUser();       
        vlocity_ins__GroupCensusMember__c censusMem = Util02_TestData.CreateCensusMember(); 
        vlocity_ins__GroupCensusMember__c censusDep;        
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData(); 
            censusMem.vlocity_ins__CensusId__c = grpCensus.Id;
            Database.insert(censusMem);
            censusDep=Util02_TestData.createCensusDependents(censusMem.id);
            censusDep.vlocity_ins__CensusId__c = grpCensus.Id;
            Database.insert(censusDep);
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            Test.startTest();
            censusCtrl.fetchCensusMemberRec();
            Test.stopTest();
        }
    }
/************************************************************************************
    Method Name : testSaveMethod
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchCensusMemberRec Method
**************************************************************************************/
    private static testMethod void testSaveMethod(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            Test.stopTest();
        }
    }    
/************************************************************************************
    Method Name : testdeleteAll
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for deleteAll Method & ConfirmDel
************************************************************************************/
    private static testMethod void testdeleteRow(){
       User testUser = Util02_TestData.createUser();       
       SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;       
       System.runAs(testUser){
           vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();        
           //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
           cnemp=createCensusEmployeeWrapper(grpCensus.Id);
           ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
           SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
           censusCtrl.dateString='{"1-0":"1985-06-07"}';
           censusCtrl.censusWrapperMap.put(1,cnemp);
           Test.startTest();
           censusCtrl.save();
           censusCtrl.parentKey=1;
           censusCtrl.childKey=0;
           censusCtrl.deleteRow();         
           censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
           censusCtrl.confirmRowDel();
           Test.stopTest();
        }
     }
/************************************************************************************
    Method Name : testdeleteRow1Dep
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for deleterow Method & ConfirmDelrow
************************************************************************************/
    private static testMethod void testdeleteRow1Dep(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id); 
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);            
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.deleteRow();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.parentKey=1;
            censusCtrl.childKey=2;
            censusCtrl.confirmRowDel();
            Test.stopTest();
        }
     }
/************************************************************************************
    Method Name : testdeleteRow3Deps
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for deleterow Method & ConfirmDelrow
************************************************************************************/
    private static testMethod void testdeleteRow3Deps(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeDepWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeDepWrapper(grpCensus.Id);            
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.deleteRow();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.parentKey=1;
            censusCtrl.childKey=2;
            censusCtrl.confirmRowDel();
            Test.stopTest();
        }
     }
/************************************************************************************
    Method Name : testdeleteRow2Deps
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for deleterow Method & ConfirmDelrow
************************************************************************************/
    private static testMethod void testdeleteRow2Deps(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);
            cnemp.childList.add(new SGA_AP53_CensusMemberCtrl.censusWrapper(3,null,grpCensus.Id,null,'TestCensusDep','Spouse',null,
                                                                            20,'Female','No','Employee+Spouse','Employee+Spouse','Employee',
                                                                            null,0,null));            
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.deleteRow();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.parentKey=1;
            censusCtrl.childKey=2;
            censusCtrl.confirmRowDel();
            Test.stopTest();
        }
     }     
/************************************************************************************
    Method Name : testAddRow
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchCensusMemberRec Method
**************************************************************************************/
    private static testMethod void testAddRow(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.addRow();
            Test.stopTest();
        }
     }
/************************************************************************************
    Method Name : testAddDependentRow
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchCensusMemberRec Method
**************************************************************************************/
    private static testMethod void testAddDependentRow(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.parentKey=1;
            censusCtrl.addDependent();
            Test.stopTest();
        }
    }
    
    /************************************************************************************
    Method Name : testAddDependentRow1
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchCensusMemberRec Method
**************************************************************************************/
    private static testMethod void testAddDependentRow1(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            cnemp = new SGA_AP53_CensusMemberCtrl.censusWrapper(1,null,grpCensus.Id,null,'TestCensusMember','Employee',
                                                                Date.ValueOf('1985-07-01'),0,'Male','No','Employee+Child',
                                                                'Employee+Child','Employee+Child','1',12000,'Annually');            
            cnemp.childList = new List<SGA_AP53_CensusMemberCtrl.censusWrapper>();          
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();
            censusCtrl.rowIndex = 2;
            censusCtrl.censusWrapperMap.put(censusCtrl.rowIndex,cnemp);
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.parentKey=1;
            censusCtrl.addDependent();
            Test.stopTest();
        }
    }
/************************************************************************************
    Method Name : testdeleteAll
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for deleteAll Method & ConfirmDel
************************************************************************************/
    private static testMethod void testdeleteAll(){
        User testUser = Util02_TestData.createUser();       
        SGA_AP53_CensusMemberCtrl.censusWrapper cnemp;
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            //cnemp=Util02_TestData.createCensusEmployeeWrapper(grpCensus.Id);
            cnemp=createCensusEmployeeWrapper(grpCensus.Id);
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
            censusCtrl.dateString='{"1-0":"1985-06-07"}';
            censusCtrl.censusWrapperMap.put(1,cnemp);
            Test.startTest();
            censusCtrl.save();  
            censusCtrl.deleteAll();
            censusCtrl.keyList = new List<Integer>(censusCtrl.censusWrapperMap.KeySet());
            censusCtrl.confirmDel();
            Test.stopTest();
        }
    }
/************************************************************************************
    Method Name : testgetCensusMemberCS
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for getCensusMemberCS Method 
************************************************************************************/
    private static testMethod void testgetCensusMemberCS(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            vlocity_ins__GroupCensus__c grpCensus=Util02_TestData.createCensusData();
            ApexPages.StandardController scr = new ApexPages.StandardController(grpCensus);
            SGA_AP53_CensusMemberCtrl censusCtrl= new SGA_AP53_CensusMemberCtrl(scr);
        	censusCtrl.getCensusMemberCS();
            censusCtrl.delCancel();
            censusCtrl.closePopup();
        }
    }
}