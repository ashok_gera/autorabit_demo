global class GetProductNames {
    @RemoteAction
    global static List<Product2> getProducts(String contractCode) {
        String contractCodeLike = '%'+contractCode+'%';
        List<Product2> products= [SELECT Id,Name from Product2 where ProductCode like :contractCodeLike LIMIT 100];
        return products;   
    }
}