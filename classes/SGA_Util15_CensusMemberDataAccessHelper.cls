/*****************************************************************************************
Class Name   : SGA_Util15_CensusMemberDataAccessHelper
Date Created : 11/28/2017
Created By   : IDC Offshore
Description  : 1. This is the util data access class for vlocity_ins__GroupCensusMember__c. Which is used to fetch 
the details from vlocity_ins__GroupCensusMember__c based on some parameters
2. This is used for DML Operations.
Change History : 
******************************************************************************************/
public without sharing class SGA_Util15_CensusMemberDataAccessHelper {
    
    //Below are the final variables to check the operation on the list of records.
    public static final String INSERT_OPERATION = 'Insert';
    public static final String UPDATE_OPERATION = 'Update';
    public static final String DELETE_OPERATION = 'Delete';
    public static final String UPSERT_OPERATION = 'Upsert';
    public static set<id> censusIDSet{get;set;}
    public static set<id> accIdSet{get;set;}
    public static id censusId{get;set;}
    /****************************************************************************************************
    Method Name : fetchCaseMap
    Parameters  : String selectQuery,String whereClause,String orderByClause,String limitClause
    Return type : Map<Id,vlocity_ins__GroupCensusMember__c>
    Description : This method is used to fetch the Account record based on parameters passed.
    It will return the Map<ID,Account> if user wants the Map, they can perform the logic on 
    Map, else they can covert the map to list of accounts.
    ******************************************************************************************************/
    public static Map<ID,vlocity_ins__GroupCensusMember__c> fetchCensusMemberMap(String selectQuery,String whereClause,
                                                                                 String orderByClause,String limitClause){
        String dynaQuery = SG01_Constants.BLANK;
        Map<ID,vlocity_ins__GroupCensusMember__c> accountMap = NULL;
        
        dynaQuery = String.isNotBlank(selectQuery) ? selectQuery : SG01_Constants.BLANK;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(whereClause) ? dynaQuery+=whereClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(orderByClause) ? dynaQuery+=orderByClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(limitClause) ? dynaQuery+=limitClause : dynaQuery;
        if(String.isNotBlank(dynaQuery))
        {
            accountMap = new Map<ID,vlocity_ins__GroupCensusMember__c>((List<vlocity_ins__GroupCensusMember__c>)
                                                                       Database.query(dynaQuery));
        }
        return accountMap;
    }
    
    /****************************************************************************************************
    Method Name : dmlOnCase
    Parameters  : List<vlocity_ins__GroupCensusMember__c> accList, String operation
    Return type : List<vlocity_ins__GroupCensusMember__c>
    Description : This method is used to perform the DML operations on Census.
    Operation value need to pass to perform the respective dml operation.
    Operation name should be passed from SG01_Constants class.
    ******************************************************************************************************/
    public static List<vlocity_ins__GroupCensusMember__c> dmlOnCensusMember(List<vlocity_ins__GroupCensusMember__c> censusList, 
                                                                            String operation){
        
        if(!censusList.isEmpty())
        {
            for(vlocity_ins__GroupCensusMember__c censusMember : censusList) {
                censusMember.Name = censusMember.EmployeeName__c;
            }
            
            if(String.isNotBlank(operation) && INSERT_OPERATION.equalsIgnoreCase(operation)){
                Database.Insert(censusList);
            }else if(String.isNotBlank(operation) && UPDATE_OPERATION.equalsIgnoreCase(operation)){
                Database.Update(censusList);
            }else if(String.isNotBlank(operation) && DELETE_OPERATION.equalsIgnoreCase(operation)){
                Database.Delete(censusList);
            }else if(String.isNotBlank(operation) && UPSERT_OPERATION.equalsIgnoreCase(operation)){
                Database.Upsert(censusList);
            }
            else{}
        }
        return censusList;
    }
}