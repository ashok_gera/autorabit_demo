/*****************************************************************************************
Class Name   : SGA_Util14_CensusDataAccessHelper
Date Created : 11/28/2017
Created By   : IDC Offshore
Description  : 1. This is the util data access class for vlocity_ins__GroupCensus__c. Which is used to fetch 
the details from vlocity_ins__GroupCensus__c based on some parameters
2. This is used for DML Operations.
Change History : 
******************************************************************************************/
public without sharing class SGA_Util14_CensusDataAccessHelper {
    
    //Below are the final variables to check the operation on the list of records.
    public static final String INSERT_OPERATION = 'Insert';
    public static final String UPDATE_OPERATION = 'Update';
    public static final String DELETE_OPERATION = 'Delete';
    public static final String UPSERT_OPERATION = 'Upsert';
    public static set<id> censusIDSet{get;set;}
    public static set<id> accIdSet{get;set;}
  public static Id censusId {get;set;}
    /****************************************************************************************************
    Method Name : fetchCaseMap
    Parameters  : String selectQuery,String whereClause,String orderByClause,String limitClause
    Return type : Map<Id,vlocity_ins__GroupCensus__c>
    Description : This method is used to fetch the Account record based on parameters passed.
    It will return the Map<ID,Account> if user wants the Map, they can perform the logic on 
    Map, else they can covert the map to list of accounts.
    ******************************************************************************************************/
    public static Map<ID,vlocity_ins__GroupCensus__c> fetchCensusMap(String selectQuery,String whereClause,
                                                                     String orderByClause,String limitClause){
        String dynaQuery = SG01_Constants.BLANK;
        Map<ID,vlocity_ins__GroupCensus__c> accountMap = NULL;
        
        dynaQuery = String.isNotBlank(selectQuery) ? selectQuery : SG01_Constants.BLANK;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(whereClause) ? dynaQuery+=whereClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(orderByClause) ? dynaQuery+=orderByClause : dynaQuery;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(limitClause) ? dynaQuery+=limitClause : dynaQuery;
        if(String.isNotBlank(dynaQuery))
        {
            accountMap = new Map<ID,vlocity_ins__GroupCensus__c>((List<vlocity_ins__GroupCensus__c>)Database.query(dynaQuery));
        }
        return accountMap;
    }
    /****************************************************************************************************
    Method Name : queryCensus
    Parameters  : vlocity_ins__GroupCensus__c censusRec
    Return type : void
    Description : This method is used to query Census record.
    *****************************************************************************************************/
    public static vlocity_ins__GroupCensus__c queryCensus(String selectQuery,String whereClause){ 
        vlocity_ins__GroupCensus__c cenRcd=NULL;
        String dynaQuery = SG01_Constants.BLANK;       
        dynaQuery = String.isNotBlank(selectQuery) ? selectQuery : SG01_Constants.BLANK;
        dynaQuery = String.isNotBlank(dynaQuery) && String.isNotBlank(whereClause) ? dynaQuery+=whereClause : dynaQuery;
        if(String.isNotBlank(dynaQuery))
        {
            cenRcd = Database.query(dynaQuery);
        }
        return cenRcd;
    } 
    /****************************************************************************************************
    Method Name : dmlOnCensus
    Parameters  : vlocity_ins__GroupCensus__c censusRec
    Return type : void
    Description : This method is used to perform the DML operations on Census.
    TECH_ValidationStatus__c field must be updated on census record.
    *****************************************************************************************************/
    public static void dmlOnCensus(vlocity_ins__GroupCensus__c censusRec){        
        if(censusRec != NULL)
        {
            System.debug('## Before update');
            Database.update(censusRec);
            System.debug('## Afterupdate');            
        }
    } 
}