/*******************************************************
Class Name  :  CombineHouseholdException
Created By  :  Bhaskar
Description :  Class used for Exxception Handling which extends the standard Exception Class. 
Change History :   
*******************************************************/
public without sharing class CombineHouseholdException extends Exception{
}