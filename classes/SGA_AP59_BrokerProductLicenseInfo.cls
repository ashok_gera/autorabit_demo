/**********************************************************************************
Class Name :   SGA_AP59_BrokerProductLicenseInfo
Date Created : 11-Dec-2017
Created By   : IDC Offshore
Description  : This class is the to check the license of products in a particular state*/
global with sharing class SGA_AP59_BrokerProductLicenseInfo implements vlocity_ins.VlocityOpenInterface2{
    private static final String BROKERCONID= 'BrokerContactId';
    private static final String BUSINESSTRACK= 'SGQUOTING';
    private static final String HEALTHLCNS= 'Health';
    private static final String LIFELCNS= 'Life';
    private static final String HEALTHLIFE= 'Health & Life';
    private static final String ZIPCODE= 'ZipCode3';
    private static final string LICENSE_VAL='License'; 
    public static final String CLASSNAME = SGA_AP59_BrokerProductLicenseInfo.class.getName();
    public static final string NODENAME='ProductLicense';
    public static final String GETSTATEPRODUCTLICENSE = 'getStateProductLicense';
    private static final String NOPRODLICENSE='NoProdLicense';
    private static final String EMPTY='';
    private static final String SELECT_GEOGRAPHIC = 'SELECT Id, Zip_Code__c, State__c FROM\n'+
                                                     'Geographical_Info__c where Zip_Code__c =:zipcode LIMIT 1';
    private static final string LICENSE_SELECT_QUERY = 'SELECT Id,BR_Type__c,BR_Start_Date__c,BR_End_Date__c,BR_Status__c,SGA_ProdType__c FROM License_Appointment__c';
    private static final string LICENSE_QUERY_WHERE = SG01_Constants.SPACE+'Where SGA_Provider__c=:contactId AND Tech_Businesstrack__c=:businessTrack AND BR_State__c =:state AND BR_Type__c=:licenseVal AND SGA_ProdType__c IN :idSet LIMIT 100';
    /*Implementation of invokeMEthod from VlocityOpenInterface2 interface.*/
    global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,
                                Object> outMap,Map<String,Object> optns) {
        if(GETSTATEPRODUCTLICENSE.equalsIgnoreCase(methodName))
        {
            getStateProductLicense(inputMap, outMap, optns);
        }
        return true;
    }
    /*This method validates the license of products in state for the selected broker.*/       
    global void getStateProductLicense(Map<String,Object> input, Map<String, Object> outMap, Map<String, Object> optns) {
        String licenseStatus=EMPTY;
        try{
            Id contactId = (Id)input.get(BROKERCONID); 
            String zipcode1 = (String)input.get(ZIPCODE);
            /*SGA_UTIL17_AccessGeographicData.zipcode = zipcode1;
            geoGraphicInfo = SGA_UTIL17_AccessGeographicData.queryGeoInfo(SELECT_GEOGRAPHIC);          
            SGA_Util18_LicenseDataAccessHelper.businessTrack=BUSINESSTRACK;
            SGA_Util18_LicenseDataAccessHelper.state=geoGraphicInfo.State__c;
            SGA_Util18_LicenseDataAccessHelper.licenseVal=LICENSE_VAL;
            SGA_Util18_LicenseDataAccessHelper.contactId = contactId;
            SGA_Util18_LicenseDataAccessHelper.idSet = productType;            
            productLcns = SGA_Util18_LicenseDataAccessHelper.fetchLicenseList(LICENSE_SELECT_QUERY,LICENSE_QUERY_WHERE);
            if(!productLcns.isEmpty()){
                for(License_Appointment__c pl:productLcns){
                    if(pl.BR_Start_Date__c<=Date.Today() && pl.BR_End_Date__c>=Date.Today()){
                        plSet.add(pl.SGA_ProdType__c);
                    }
                }
                if(!plSet.isEmpty()){            
                    if (plSet.contains(HEALTHLCNS) && !(plSet.contains(LIFELCNS))){
                        license = HEALTHLCNS;                   
                    }
                    else if (plSet.contains(HEALTHLCNS) && plSet.contains(LIFELCNS)){
                        license = HEALTHLIFE;
                    }
                    else if (plSet.contains(LIFELCNS) &&!(plSet.contains(HEALTHLCNS))){
                        license=LIFELCNS;
                    } 
                    else{}
                } 
                else{          
                    license=NOPRODLICENSE;               
                }  
            }
            else{
                license= NOPRODLICENSE; 
            }*/
            licenseStatus=validateProdLicense(zipcode1,contactId);
            outMap.put(NODENAME,licenseStatus); 
        }
        catch(Exception excn){
             UTIL_LoggingService.logHandledException(excn, null, null, 
             CLASSNAME, GETSTATEPRODUCTLICENSE,null,LoggingLevel.ERROR); 
        }                                  
    }
    /*This is a generic method for validating the product licenses*/
    public static String validateProdLicense(String zipcode1,Id contactId){
        String license=EMPTY;
        List<License_Appointment__c> productLcns=new List<License_Appointment__c>();
        Geographical_Info__c  geoGraphicInfo;
        set<string> plSet = new set<string>();
        Set<String> productType = new Set<String>{HEALTHLCNS,LIFELCNS};
        SGA_UTIL17_AccessGeographicData.zipcode = zipcode1;
        geoGraphicInfo = SGA_UTIL17_AccessGeographicData.queryGeoInfo(SELECT_GEOGRAPHIC);          
        SGA_Util18_LicenseDataAccessHelper.businessTrack=BUSINESSTRACK;
        SGA_Util18_LicenseDataAccessHelper.state=geoGraphicInfo.State__c;
        SGA_Util18_LicenseDataAccessHelper.licenseVal=LICENSE_VAL;
        SGA_Util18_LicenseDataAccessHelper.contactId = contactId;
        SGA_Util18_LicenseDataAccessHelper.idSet = productType;            
        productLcns = SGA_Util18_LicenseDataAccessHelper.fetchLicenseList(LICENSE_SELECT_QUERY,LICENSE_QUERY_WHERE);
        if(!productLcns.isEmpty()){
            for(License_Appointment__c pl:productLcns){
                if(pl.BR_Start_Date__c<=Date.Today() && pl.BR_End_Date__c>=Date.Today()){
                    plSet.add(pl.SGA_ProdType__c);
                }
            }
            if(!plSet.isEmpty()){            
                if (plSet.contains(HEALTHLCNS) && !(plSet.contains(LIFELCNS))){
                    license = HEALTHLCNS;                   
                }
                else if (plSet.contains(HEALTHLCNS) && plSet.contains(LIFELCNS)){
                    license = HEALTHLIFE;
                }
                else if (plSet.contains(LIFELCNS) &&!(plSet.contains(HEALTHLCNS))){
                    license=LIFELCNS;
                } 
                else{}
            } 
            else{          
                license=NOPRODLICENSE;               
            }  
        }
        else{
            license= NOPRODLICENSE; 
        }
        return license;
    }
}