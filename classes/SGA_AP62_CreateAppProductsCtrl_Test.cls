/**********************************************************************************
Class Name :   SGA_AP62_CreateAppProductsCtrl_Test
Date Created : 01/30/2018
Created By   : IDC Offshore
Description  : 1. This class is the test class for SGA_AP62_CreateAppProductsCtrl
*************************************************************************************/
@isTest
private class SGA_AP62_CreateAppProductsCtrl_Test {
/************************************************************************************
    Method Name : testfetchAppProdRec
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for fetchAppProdRec Method
*************************************************************************************/
    private static testMethod void testfetchAppProdRec(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
        Account testAcc = Util02_TestData.createGroupAccount();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
        List<Application_Product__c> apPrdList= new List<Application_Product__c>();
        Application_Product__c  apPrd= new Application_Product__c();
        apPrd.Application_Name__c = NULL;
       vlocity_ins__Party__c  partyToInsert= Util02_TestData.createPartyforAccount(NULL);
        System.runAs(testUser){
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc); 
        prod1.vlocity_ins__Availability__c = testAcc.Company_State__c;      
        Database.insert(prod1);
        partyToInsert.vlocity_ins__AccountId__c = testAcc.Id;
        Database.insert(partyToInsert);
        testApplication.vlocity_ins__AccountId__c=testAcc.Id;
        Database.insert(testApplication);
           apPrd.Application_Name__c = testApplication.Id;
           apPrdList.add(apPrd);
           Database.insert(apPrdList);            
        Test.startTest();
         Test.setCurrentPageReference(new PageReference('Page.SGA_VFP14_CreateAppProducts')); 
         System.currentPageReference().getParameters().put('id', testApplication.Id); 
         System.debug('inserted app in Test::'+testApplication);
         ApexPages.StandardSetController  stdSetController = new ApexPages.StandardSetController(apPrdList);
         SGA_AP62_CreateAppProductsCtrl apPrdCtrl= new SGA_AP62_CreateAppProductsCtrl(stdSetController);
     
         apPrdCtrl.fetchAppProdRec();
         Test.stopTest();
         System.assertEquals(1, apPrdCtrl.prdListforAccState.size());
        }
     }
   
/************************************************************************************
    Method Name : testfetchAppProdRecNeg
    Parameters  : None
    Return type : void
    Description : This is the Negative test for fetchAppProdRec Method
***************************************************************************************/
    private static testMethod void testfetchAppProdRecNeg(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
        Account testAcc = Util02_TestData.createGroupAccount();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
        List<Application_Product__c> apPrdList= new List<Application_Product__c>();
        Application_Product__c  apPrd= new Application_Product__c();
        apPrd.Application_Name__c = NULL;
        vlocity_ins__Party__c  partyToInsert= Util02_TestData.createPartyforAccount(NULL);
        System.runAs(testUser){
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc); 
        prod1.vlocity_ins__Availability__c = 'Test';      
        Database.insert(prod1);
        partyToInsert.vlocity_ins__AccountId__c = testAcc.Id;
        Database.insert(partyToInsert);
        testApplication.vlocity_ins__AccountId__c=testAcc.Id;
        Database.insert(testApplication);
           apPrd.Application_Name__c = testApplication.Id;
           apPrdList.add(apPrd);
           Database.insert(apPrdList);            
        // attach application to Account 
        testApplication.vlocity_ins__AccountId__c=testAcc.Id;
        Database.update(testApplication);
        
         Test.startTest();
         Test.setCurrentPageReference(new PageReference('Page.SGA_VFP14_CreateAppProducts')); 
         System.currentPageReference().getParameters().put('id', testApplication.Id); 
         ApexPages.StandardSetController  stdSetController = new ApexPages.StandardSetController(apPrdList);
         SGA_AP62_CreateAppProductsCtrl apPrdCtrl= new SGA_AP62_CreateAppProductsCtrl(stdSetController);
         apPrdCtrl.fetchAppProdRec();
         Test.stopTest();
         System.assertEquals(0, apPrdCtrl.prdListforAccState.size());
        }
     }
     
    /************************************************************************************
    Method Name : testSearch
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for Search Method
    ************************************************************************************/
    private static testMethod void testSearch(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
        Account testAcc = Util02_TestData.createGroupAccount();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
		
        List<Application_Product__c> apPrdList= new List<Application_Product__c>();
        Application_Product__c  apPrd= new Application_Product__c();
        apPrd.Application_Name__c = NULL;
		vlocity_ins__Party__c  partyToInsert= Util02_TestData.createPartyforAccount(NULL);
       
        System.runAs(testUser){
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc); 
        prod1.vlocity_ins__Availability__c = testAcc.Company_State__c;      
        Database.insert(prod1);
         partyToInsert.vlocity_ins__AccountId__c = testAcc.Id;
        Database.insert(partyToInsert);
        testApplication.vlocity_ins__AccountId__c=testAcc.Id;
        Database.insert(testApplication);
           apPrd.Application_Name__c = testApplication.Id;
           apPrdList.add(apPrd);
           Database.insert(apPrdList);            
        // attach application to Account 
         Test.startTest();
         Test.setCurrentPageReference(new PageReference('Page.SGA_VFP14_CreateAppProducts')); 
         System.currentPageReference().getParameters().put('id', testApplication.Id); 
         ApexPages.StandardSetController  stdSetController = new ApexPages.StandardSetController(apPrdList);
         SGA_AP62_CreateAppProductsCtrl apPrdCtrl= new SGA_AP62_CreateAppProductsCtrl(stdSetController);
         apPrdCtrl.fetchAppProdRec();
         apPrdCtrl.searchString = 'Testing Product';
         apPrdCtrl.search();
         Test.stopTest();
         System.assertEquals('Testing Product', apPrdCtrl.prdListforAccState[0].Name);
        }
     }
    /************************************************************************************
    Method Name : testsaveAppPrd
    Parameters  : None
    Return type : void
    Description : This is positive testmethod for testsaveAppPrd Method
    ************************************************************************************/
    private static testMethod void testsaveAppPrd(){
        User testUser = Util02_TestData.createUser();
        Id standardPBId = Test.getStandardPricebookId();
        Product2 prod1 = Util02_TestData.createProduct();
        Account testAcc = Util02_TestData.createGroupAccount();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        vlocity_ins__Application__c testApplication = Util02_TestData.createApplicationForRelatedList(SG01_Constants.STATUSINENROLLMENTCOMPLETED, NULL);
		
        List<Application_Product__c> apPrdList= new List<Application_Product__c>();
        Application_Product__c  apPrd= new Application_Product__c();
        apPrd.Application_Name__c = NULL;
		vlocity_ins__Party__c  partyToInsert= Util02_TestData.createPartyforAccount(NULL);
       
        System.runAs(testUser){
        Database.insert(cs001List);
        Database.insert(cs002List);
        Database.insert(testAcc); 
        prod1.vlocity_ins__Availability__c = testAcc.Company_State__c; 
        prod1.ProductCode='2XFC';
        Database.insert(prod1);
         partyToInsert.vlocity_ins__AccountId__c = testAcc.Id;
        Database.insert(partyToInsert);
        testApplication.vlocity_ins__AccountId__c=testAcc.Id;
        Database.insert(testApplication);
           apPrd.Application_Name__c = testApplication.Id;
           apPrdList.add(apPrd);
           Database.insert(apPrdList);            
        // attach application to Account 
         Test.startTest();
         Test.setCurrentPageReference(new PageReference('Page.SGA_VFP14_CreateAppProducts')); 
         System.currentPageReference().getParameters().put('id', testApplication.Id); 
         ApexPages.StandardSetController  stdSetController = new ApexPages.StandardSetController(apPrdList);
         SGA_AP62_CreateAppProductsCtrl apPrdCtrl= new SGA_AP62_CreateAppProductsCtrl(stdSetController);
         apPrdCtrl.fetchAppProdRec();
         apPrdCtrl.searchString = '2XFC';
         apPrdCtrl.search();
         apPrdCtrl.selectedProductId =apPrdCtrl.prdListforAccState[0].Id;
           System.debug('prdListforAccState'+apPrdCtrl.prdListforAccState);
         apPrdCtrl.saveAppPrd();
         Test.stopTest();
         System.assertEquals('2XFC', apPrdCtrl.prdListforAccState[0].ProductCode);
        }
     }
  
}