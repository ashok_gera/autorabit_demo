public class ap_ViewPdf {
    public String inputValue{get;set;}
    
    public String fileName {
        					get {return fileName;}
                            set {
                                	if(value.containsIgnoreCase('.pdf')) {
                                		fileName = value;
                                	} else {
                                		fileName = value + '.pdf';
                                	}
                                }
    }

    public String CreatePDF {get;set;}
    
    public void RefreshPdf(){
        Blob pdfBLOB;
        if (inputValue==null){ //default call URL
            inputValue='https://jsonblob.com/api/jsonBlob/f5274913-0164-11e8-82bd-e5afb89961e0';
            system.debug('CreatePDF2.ServiceURL (Fixed): '+inputValue);
        }
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(inputValue);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
        if (response.getStatusCode() == 200) {
            pdfRespDesClass pdfDes =  (pdfRespDesClass)JSON.deserialize(response.getBody(), pdfRespDesClass.class);
            system.debug(pdfDes.data);
            fileName =pdfDes.name;
            CreatePDF = EncodingUtil.base64Encode(pdfDes.data);
        }
    }

    public class pdfRespDesClass {
        public Blob data;
        public String name;
    }
     
    
    public ap_ViewPdf (){
        RefreshPdf();
    }
        
}