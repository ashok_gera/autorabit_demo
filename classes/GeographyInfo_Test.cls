/****************************************************************************
Class Name  :  GeographyInfo_Test
Created By  :  Vlocity
Description : Test class for GeographyInfo
****************************************************************************/
@isTest
private class GeographyInfo_Test{ 
/*This method is for fetching geographical info and related counties*/  
    private static testMethod void GeographyInfoTestMethod(){ 
      User tstUser = Util02_TestData.insertUser();
        System.runAs(tstUser){  
           Geographical_Info__c gInfo = new Geographical_Info__c(Name='EBC',Area__c='120',County__c='SCHENECTADY',
                                                                 RatingArea__c=1,Zip_Code__c='100',State__c='NY');
           Database.insert(gInfo);
         GeographyInfo.getCounty('100');
         }
    } 
/************************************************************************************
    Method Name : validStatus
    Parameters  : None
    Return type : void
    Description : This is testmethod for positive snenario
*************************************************************************************/
    private static testMethod void validStatus(){
		User testUser = Util02_TestData.createUser();
		System.runAs(testUser){		
			License_Appointment__c license = Util02_TestData.createLicenseData();
			license.BR_Start_Date__c = system.Today();
			license.BR_End_Date__c = system.Today();
			license.BR_Type__c = 'License';
			Database.insert(license);
			Profile portalProfile = [SELECT Id FROM Profile where name =: SG01_Constants.PNAME_TEST Limit 1];
			User user1 = new User(Username = System.now().millisecond() + SG01_Constants.USERNAME_TEST,
										  ContactId = license.SGA_Provider__c, ProfileId = portalProfile.Id,
										  Alias = SG01_Constants.ALIAS_TEST, Email = SG01_Constants.USERNAME_TEST,
										  EmailEncodingKey = SG01_Constants.ENCODING_TEST,
										  LastName = SG01_Constants.LNAME_TEST, CommunityNickname = SG01_Constants.NICKNAME_TEST,
										  TimeZoneSidKey = SG01_Constants.TIMEZONE_TEST, LocaleSidKey = SG01_Constants.LOCALE_TEST,
										  LanguageLocaleKey = SG01_Constants.LANG_TEST);
			Database.insert(user1);
			System.runAs(user1){
				Test.startTest();
				GeographyInfo.getStateLicenseStatus('12766',license.SGA_Provider__c);
				Test.stopTest();
			}
		}
    }
/************************************************************************************
    Method Name : invalidStatus1
    Parameters  : None
    Return type : void
    Description : This is testmethod for positive snenario
*************************************************************************************/
    private static testMethod void invalidStatus1(){
		User testUser = Util02_TestData.createUser();
		System.runAs(testUser){
			License_Appointment__c license = Util02_TestData.createLicenseData();
			license.BR_Type__c = 'License';
			Database.insert(license);
			Profile portalProfile = [SELECT Id FROM Profile where name =: SG01_Constants.PNAME_TEST Limit 1];
			User user2 = new User(Username = System.now().millisecond() + SG01_Constants.USERNAME_TEST,
										  ContactId = license.SGA_Provider__c, ProfileId = portalProfile.Id,
										  Alias = SG01_Constants.ALIAS_TEST, Email = SG01_Constants.USERNAME_TEST,
										  EmailEncodingKey = SG01_Constants.ENCODING_TEST,
										  LastName = SG01_Constants.LNAME_TEST, CommunityNickname = SG01_Constants.NICKNAME_TEST,
										  TimeZoneSidKey = SG01_Constants.TIMEZONE_TEST, LocaleSidKey = SG01_Constants.LOCALE_TEST,
										  LanguageLocaleKey = SG01_Constants.LANG_TEST);
			Database.insert(user2);
			System.runAs(user2){
				Test.startTest();
				GeographyInfo.getStateLicenseStatus('12766',license.SGA_Provider__c);
				Test.stopTest();
			}
		}
    }
/************************************************************************************
    Method Name : invalidStatus2
    Parameters  : None
    Return type : void
    Description : This is testmethod for positive snenario
*************************************************************************************/
    private static testMethod void invalidStatus2(){
		User testUser = Util02_TestData.createUser();
		System.runAs(testUser){
			License_Appointment__c license = Util02_TestData.createLicenseData();
			Database.insert(license);
			Profile portalProfile = [SELECT Id FROM Profile where name =: SG01_Constants.PNAME_TEST Limit 1];
			User user3 = new User(Username = System.now().millisecond() + SG01_Constants.USERNAME_TEST,
										  ContactId = license.SGA_Provider__c, ProfileId = portalProfile.Id,
										  Alias = SG01_Constants.ALIAS_TEST, Email = SG01_Constants.USERNAME_TEST,
										  EmailEncodingKey = SG01_Constants.ENCODING_TEST,
										  LastName = SG01_Constants.LNAME_TEST, CommunityNickname = SG01_Constants.NICKNAME_TEST,
										  TimeZoneSidKey = SG01_Constants.TIMEZONE_TEST, LocaleSidKey = SG01_Constants.LOCALE_TEST,
										  LanguageLocaleKey = SG01_Constants.LANG_TEST);
			Database.insert(user3);
			System.runAs(user3){
				Test.startTest();
				GeographyInfo.getStateLicenseStatus('12766',license.SGA_Provider__c);
				Test.stopTest();
			}
		}
    }
}