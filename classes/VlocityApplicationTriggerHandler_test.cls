@isTest(SeeAllData=false)

public class VlocityApplicationTriggerHandler_test{

    public static testmethod void mymethod(){
        
     
        List<CS001_RecordTypeBusinessTrack__c> rbtList = new List<CS001_RecordTypeBusinessTrack__c>();
        CS001_RecordTypeBusinessTrack__c rbt1 = new CS001_RecordTypeBusinessTrack__c();
        rbt1.Name = 'Account_Agency';
        rbt1.BusinessTrackName__c = 'BROKER';
        rbtList.add(rbt1);
         
        CS001_RecordTypeBusinessTrack__c rbt2 = new CS001_RecordTypeBusinessTrack__c();
        rbt2.Name = 'Account_Anthem Opps';
        rbt2.BusinessTrackName__c = 'ANTHEMOPPS';
        rbtList.add(rbt2);
        
        CS001_RecordTypeBusinessTrack__c rbt3 = new CS001_RecordTypeBusinessTrack__c();
        rbt3.Name = 'Account_Agency/Brokerage';
        rbt3.BusinessTrackName__c = 'SGQUOTING';
        rbtList.add(rbt3);
        
        CS001_RecordTypeBusinessTrack__c rbt4 = new CS001_RecordTypeBusinessTrack__c();
        rbt4.Name = 'Account_Broker';
        rbt4.BusinessTrackName__c = 'BROKER';
        rbtList.add(rbt4);
        
        CS001_RecordTypeBusinessTrack__c rbt5 = new CS001_RecordTypeBusinessTrack__c();
        rbt5.Name = 'Account_Group';
        rbt5.BusinessTrackName__c = 'SGQUOTING';
        rbtList.add(rbt5);
        
        CS001_RecordTypeBusinessTrack__c rbt6 = new CS001_RecordTypeBusinessTrack__c();
        rbt6.Name = 'Account_Person Account';
        rbt6.BusinessTrackName__c = 'TELESALES';
        rbtList.add(rbt6);
        
        CS001_RecordTypeBusinessTrack__c rbt7 = new CS001_RecordTypeBusinessTrack__c();
        rbt7.Name = 'TELESALES';
        rbt7.BusinessTrackName__c = 'SGDIRECT';
        rbtList.add(rbt7);
        
        insert rbtList;
        
        List<Account> accList = new List<Account>();
        Account acc = new Account();
        acc.Name = 'Test acc';
        acc.AgencyType__c = 'Brokerage';
        acc.Company_State__c = 'PO';
        acc.BR_Encrypted_TIN__c = 'test1';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency/Brokerage').getRecordTypeId();
        accList.add(acc);
        
        Account acc1 = new Account();
        acc1.Name = 'Test acc1';
        acc1.AgencyType__c = 'Paid Agency';
        acc1.Company_State__c = 'PO';
        acc1.BR_Encrypted_TIN__c = 'test2';
        acc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency/Brokerage').getRecordTypeId();
        accList.add(acc1);
        
        Account acc2 = new Account();
        acc2.Name = 'Test acc2';
        acc2.AgencyType__c = 'General Agency';
        acc2.Company_State__c = 'PO';
        acc2.BR_Encrypted_TIN__c = 'test3';
        acc2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Agency/Brokerage').getRecordTypeId();
        accList.add(acc2);
        
        insert accList ;
        
        
        
        list<vlocity_ins__Application__c> livloappl = new list<vlocity_ins__Application__c>();
        
        vlocity_ins__Application__c vloappl = new vlocity_ins__Application__c ();        
        vloappl.name = 'testname';        
        vloappl.vlocity_ins__AccountId__c = acc.id;             
        vloappl.Broker_Agent_Name_1__c = acc.id;
        vloappl.General_Agency_Name1__c = acc2.id;
        vloappl.Paid_Agency_Name1__c = acc1.id;        
        vloappl.Group_Coverage_Date__c = system.today()-1;
        
        livloappl.add(vloappl);        
        insert livloappl;
        
        accList[0].BR_Encrypted_TIN__c = '';
        accList[1].BR_Encrypted_TIN__c = '';
        accList[2].BR_Encrypted_TIN__c = '';
        update accList;
        vlocity_ins__Application__c vloapplupd = new vlocity_ins__Application__c ();        
        vloapplupd.id = vloappl.id;               
        vloapplupd.Group_Coverage_Date__c = system.today();        
        update vloapplupd ;
        
        map<id,vlocity_ins__Application__c> oldmap =  new  map<id,vlocity_ins__Application__c> ();
        oldmap.put(vloappl.id,vloappl);
        oldmap.put(vloapplupd.id,vloapplupd);
        
        case ca = new case();
        ca.Origin = 'phone';
        ca.Priority = 'Normal';
        ca.Stage__c = 'Group Submitted, Under Review';
        ca.BR_Category__c = 'Anthem Opps';
        ca.Status = 'new';
        ca.Paperwork_Received__c = system.today();
        ca.Application_Name__c = vloappl.id;        
        insert ca;
        
       
        list<vlocity_ins__Application__c> livloapplel = new list<vlocity_ins__Application__c>();
        
        vlocity_ins__Application__c vloapplel = new vlocity_ins__Application__c ();        
        vloapplel.name = 'testname';        
        vloapplel.vlocity_ins__AccountId__c = acc.id;             
        vloapplel.Broker_Agent_Name_1__c = acc.id;
        vloapplel.General_Agency_Name1__c = acc2.id;
        vloapplel.Paid_Agency_Name1__c = acc1.id;        
        vloapplel.Group_Coverage_Date__c = system.today()-1;
        
        livloapplel.add(vloapplel);        
        insert livloapplel;
        
        
        VlocityApplicationTriggerHandler vh = new VlocityApplicationTriggerHandler();        
        vh.effectivemethod(livloappl,oldmap);
        VlocityApplicationTriggerHandler.ETIN_Recordtype_isinsert(livloapplel);
        
   
    
    }

}