/************************************************************************
Class Name   : SGA_AP58_LicenseAppt_Test
Date Created : 21-Sept-2017
Created By   : IDC Offshore
Description  : 1. This is a test class for SGA_AP58_LicenseAppt_AccUpdate
**************************************************************************/
@isTest
private class SGA_AP58_LicenseAppt_Test
{
    /****************************************************************************************************
    Method Name : insertPositiveTest
    Parameters  : None
    Return type : void
    Description : This is the testmethod for Account team criteria with positive values
    *****************************************************************************************************/
    private static testMethod void insertPositiveTest()
    {   
        User testUser = Util02_TestData.createUser();
        Account testAcc = Util02_TestData.createBrokerAgentAccount();
        List<CS001_RecordTypeBusinessTrack__c> cs001List = Util02_TestData.createCS001Data();
        List<CS002_RTypeAssignOnLeadConvert__c> cs002List = Util02_TestData.createCS002Data();
        System.runAs(testUser)
        {
            Database.insert(cs001List);
            Database.insert(cs002List); 
            Database.insert(testAcc);
            
            License_Appointment__c lap = Util02_TestData.createTestLicenseData();
            lap.SGA_AgencyBrokerage__c = testAcc.id;
            lap.BR_Start_Date__c = system.Today();
            Test.StartTest();
                database.Insert(lap, false);
            Test.StopTest();
            //System.AssertEquals(lap.BR_Start_Date__c , testAcc.Tech_Appointment_Effective_Date__c);
        }
    }
    
    
}