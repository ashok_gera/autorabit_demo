public class EffectiveDateonCasehelper {

    public void effectivemethod (list<vlocity_ins__Application__c> listapp){
    
        map<id, vlocity_ins__Application__c > appid = new map<id,vlocity_ins__Application__c >();
    
        for(vlocity_ins__Application__c via: listapp){
    
            appid.put(via.id,via);
    
        }
        
        if(appid.size() > 0){
        
            EffectiveDateonCaseclass ec = new EffectiveDateonCaseclass ();
            
            ec.classmethod(appid);
        
        }
    
    }

}