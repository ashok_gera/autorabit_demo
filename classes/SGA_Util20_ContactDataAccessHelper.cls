/*****************************************************************************************
Class Name   : SGA_Util20_ContactDataAccessHelper 
Date Created : 02/05/2018
Created By   : IDC Offshore
Description  : 1. This is the util data access class for Contact. Which is used to fetch 
the details from Contact based on some parameters. 
******************************************************************************************/
public with sharing class SGA_Util20_ContactDataAccessHelper {
    //Below are the final variables to check the operation on the list of records.
    public static final String INSERT_OPERATION = 'Insert';
    public static final String UPDATE_OPERATION = 'Update';
    public static final String DELETE_OPERATION = 'Delete';
    public static final String UPSERT_OPERATION = 'Upsert';
    public static String conId;
/*
@Author : Accenture Offshore Dev team
@name : fetchAccountId
@CreateDate :06-02-2018
@Description : This class queries the Accountid from Contact.
*/
    public static Contact fetchAccountId(string selectConQuery){
         return Database.query(selectConQuery);        
    }
    
}