/*@author       Accenture
@date           12/13/2017
@name           SGA_AP59_BrokerProductLicenseInfo_Test
@description    Test class to test the SGA_AP59_BrokerProductLicenseInfo class.
*/
@isTest
private class SGA_AP59_BrokerProductLicenseInfo_Test {
private static final String HEALTH = 'Health';
private static final String LIFE = 'Life';
  /************************************************************************************
    Method Name : validStatus
    Parameters  : None
    Return type : void
    Description : This is testmethod for positive snenario
*************************************************************************************/
    private static testMethod void validStatus(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){            
            License_Appointment__c license = Util02_TestData.createTestLicenseData();
			license.BR_Start_Date__c = system.Today();    
            license.BR_End_Date__c = system.Today();
            license.SGA_ProdType__c  = HEALTH;
            Database.insert(license);
            User user1 = Util02_TestData.createPortalUser();
            System.runAs(user1){
                Map<String,Object> inputmap=new Map<String,Object>();
                Map<String,Object> outputmap=new Map<String,Object>();
                Map<String,Object> options=new Map<String,Object>();
                String methodName='getStateProductLicense';
                outputmap.put('ProductLicense','license');
                inputmap.put('BrokerContactId',license.SGA_Provider__c);
                inputmap.put('ZipCode3','12766');
                options.put('useQueueableApexRemoting',true);				
                SGA_AP59_BrokerProductLicenseInfo actct=new SGA_AP59_BrokerProductLicenseInfo();
                Boolean approvalRes= actct.invokeMethod(methodName,inputmap,outputmap,options);				
                system.assertEquals(true,approvalRes); 
            }
        }
    }
/************************************************************************************
Method Name : invalidStatus1
Parameters  : None
Return type : void
Description : This is testmethod for negative snenario
*************************************************************************************/
    private static testMethod void invalidStatus1(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            License_Appointment__c license = Util02_TestData.createTestLicenseData();
            license.BR_Start_Date__c = system.Today();    
            license.BR_End_Date__c = system.Today();
			license.SGA_ProdType__c  = LIFE;
            Database.insert(license);
            User user1 = Util02_TestData.createPortalUser();
            System.runAs(user1){
                Map<String,Object> inputmap=new Map<String,Object>();
                Map<String,Object> outputmap=new Map<String,Object>();
                Map<String,Object> options=new Map<String,Object>();
                String methodName='getStateProductLicense';
                outputmap.put('ProductLicense','license');
                inputmap.put('BrokerContactId',license.SGA_Provider__c);
                inputmap.put('ZipCode3','12766');    
                options.put('useQueueableApexRemoting',true); 
                SGA_AP59_BrokerProductLicenseInfo actct=new SGA_AP59_BrokerProductLicenseInfo();
                Boolean approvalRes= actct.invokeMethod(methodName,inputmap,outputmap,options);
                system.assertEquals(true,approvalRes); 
            }
        }
    }
/************************************************************************************
Method Name : invalidStatus3
Parameters  : None
Return type : void
Description : This is testmethod for negative snenario
*************************************************************************************/
    private static testMethod void invalidStatus3(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            License_Appointment__c license = Util02_TestData.createTestLicenseData();           
			license.SGA_ProdType__c  = HEALTH;			
            Database.insert(license);
            User user1 = Util02_TestData.createPortalUser();
            System.runAs(user1){
                Map<String,Object> inputmap=new Map<String,Object>();
                Map<String,Object> outputmap=new Map<String,Object>();
                Map<String,Object> options=new Map<String,Object>();
                String methodName='getStateProductLicense';
                outputmap.put('ProductLicense','license');
                inputmap.put('BrokerContactId',license.SGA_Provider__c);
                inputmap.put('ZipCode3','12766');    
                options.put('useQueueableApexRemoting',true);
                SGA_AP59_BrokerProductLicenseInfo actct=new SGA_AP59_BrokerProductLicenseInfo();
                Boolean approvalRes= actct.invokeMethod(methodName,inputmap,outputmap,options);  
				system.assertEquals(true,approvalRes);  
            }
        }
    }	
/************************************************************************************
Method Name : invalidStatus4
Parameters  : None
Return type : void
Description : This is testmethod for negative snenario
*************************************************************************************/
    private static testMethod void invalidStatus4(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            Contact contactProd = Util02_TestData.createAccData();
            Geographical_Info__c geo = Util02_TestData.createGeoRecord();
            Database.insert(geo);    
            License_Appointment__c license = Util02_TestData.Licence();
            license.BR_Type__c = 'License';
            Database.insert(license);
            User user1 = Util02_TestData.createPortalUser();
            System.runAs(user1){
                Map<String,Object> inputmap=new Map<String,Object>();
                Map<String,Object> outputmap=new Map<String,Object>();
                Map<String,Object> options=new Map<String,Object>();
                String methodName='getStateProductLicense';
                outputmap.put('ProductLicense','license');
                inputmap.put('BrokerContactId',contactProd.id);
                inputmap.put('ZipCode3','12766');    
                options.put('useQueueableApexRemoting',true);
                SGA_AP59_BrokerProductLicenseInfo actct=new SGA_AP59_BrokerProductLicenseInfo();
                Boolean approvalRes= actct.invokeMethod(methodName,inputmap,outputmap,options);  
				system.assertEquals(true,approvalRes);  
            }
        }
    }
    /************************************************************************************
Method Name : testException
Parameters  : None
Return type : void
Description : This is testmethod for negative snenario
*************************************************************************************/
    private static testMethod void testException(){
        User testUser = Util02_TestData.createUser();
        System.runAs(testUser){
            Contact contactProd = Util02_TestData.createAccData();
            Geographical_Info__c geo = Util02_TestData.createGeoRecord();
            Database.insert(geo);    
            License_Appointment__c license = Util02_TestData.Licence();
            license.BR_Type__c = 'License';
            Database.insert(license);
            User user1 = Util02_TestData.createPortalUser();
            System.runAs(user1){
                Map<String,Object> inputmap=new Map<String,Object>();
                Map<String,Object> outputmap=new Map<String,Object>();
                Map<String,Object> options=new Map<String,Object>();
                String methodName='getStateProductLicense';
                outputmap.put('ProductLicense','license');
                inputmap.put('BrokerContactId','test');
                inputmap.put('ZipCode3',null);    
                options.put('useQueueableApexRemoting',true);
                SGA_AP59_BrokerProductLicenseInfo actct=new SGA_AP59_BrokerProductLicenseInfo();
                Boolean approvalRes= actct.invokeMethod(methodName,inputmap,outputmap,options);  
				system.assertEquals(true,approvalRes);  
            }
        }
    }
 }