public class ETINandRecordtypetoApplHandler{

    public void etinandrecordtype(list<vlocity_ins__Application__c > listappl){
    
        set<id> accntids = new set<id>();
    
        for(vlocity_ins__Application__c  vi: listappl){  
            
        
            if(vi.Broker_Agent_Name_1__c != null ){
        
                accntids.add(vi.Broker_Agent_Name_1__c);
                
            }
            
            if(vi.General_Agency_Name1__c != null ){
            
                accntids.add(vi.General_Agency_Name1__c);
            
            }
            
            if(vi.Paid_Agency_Name1__c != null ){
            
                accntids.add(vi.Paid_Agency_Name1__c);
                
            }
            
            if(vi.vlocity_ins__AccountId__c != null){
            
                accntids.add(vi.vlocity_ins__AccountId__c);
                
            }       
            
        
        } 
    
            if(accntids.size()>0){
            
                ETINandRecordtypetoApplclass etinvar = new ETINandRecordtypetoApplclass();
                
                etinvar.etinandrecordtype(accntids, listappl);
            
            }    
    
    }

}