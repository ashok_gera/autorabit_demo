/**********************************************************************************
Class Name :   CMN_CensusDataForRating implements vlocity_ins.VlocityOpenInterface
Date Created : 18-February-2018 
Created By   : Sumeet Bath
Description  : Creating the census data JSON for Integration Procedure.
Usage: Please provide name of the Page or Script Refering this class.
Change History : 
*************************************************************************************/
//To do: Make class implementing open interface
//To do: add mapping to contructor of Member and Dependent class
public with sharing class CMN_CensusDataForRating {
	// Class to get census data liast and preare request JSON
	// To do: Apply fileter to Query
	public static List<CensusData> getMembers() {
		Map<Id,CensusData> membersMap = new Map<Id,CensusData>();
		for(vlocity_ins__GroupCensusMember__c gMember:[Select Id,vlocity_ins__AgeAsOfToday__c,MemberAge__c,
			vlocity_ins__Birthdate__c,COBRA__c,DateofBirth__c,DentalCoverageType__c,EmployeeSpouseDependentIndicator__c,
			vlocity_ins__IsFamily__c,MedicalCoverageType__c,vlocity_ins__MemberIdentifier__c,OptionalLifeAmount__c,
			vlocity_ins__PartyId__c,vlocity_ins__IsPrimaryMember__c,vlocity_ins__RelatedCensusMemberId__c,
			vlocity_ins__PrimaryMemberIdentifier__c,Salary__c,Earnings__c,TobaccoCessation__c,TobaccoUse__c,
			vlocity_ins__MemberType__c,VisionCoverageType__c from vlocity_ins__GroupCensusMember__c order by 
			vlocity_ins__RelatedCensusMemberId__c]) {
			if(gMember.vlocity_ins__RelatedCensusMemberId__c == null)
				membersMap.put(gMember.Id, new CensusData(new Member(gMember)));
			else {
				if(membersMap.containsKey(gMember.vlocity_ins__RelatedCensusMemberId__c))
					membersMap.get(gMember.vlocity_ins__RelatedCensusMemberId__c).addDependent(new Dependent(gMember));
			}	
		}
		system.debug('Member List' + JSON.serialize(membersMap.values()));
		return membersMap.values();
	}
	/**********************************************************************************
	Inner Class Name :   CensusData
	Description  : JSON request tructure for census data.
	*************************************************************************************/
	public class CensusData {
		private Member member;
    	private Dependent[] dependent;
    	public CensusData(Member m) {
    		this.member=m;
    	}
    	public void addDependent(Dependent d)
	    {
	    	if(dependent == null) {
	    		dependent = new Dependent[]{};
	    		dependent.add(d);
	    	}
	    	else {
	    		dependent.add(d);
	    	}
	    }
	}
	/**********************************************************************************
	Inner Class Name :   Member
	Description  : Member structure for JSON request
	*************************************************************************************/
	public class Member {
	    private String dentalTier;
	    private String isTobaccoUser;
	    private String inTobaccoWellness;
	    private String isDentalPriorCoverage;
	    private String age;
	    private String isBundled;
	    private String memberRelationshipTypeCode;
	    private String enrollmentTypeCode;
	    private String isCobra;
	    private String genderCode;
	    private String memberUniqueId;
	    public member(vlocity_ins__GroupCensusMember__c gMember) {
	    	this.dentalTier = gMember.DentalCoverageType__c;
	    	if(gMember.TobaccoUse__c.equalsIgnoreCase('yes'))
	    		this.isTobaccoUser = '1';
	    	else
	    		this.isTobaccoUser = '0';	
	    }
	}
	/**********************************************************************************
	Inner Class Name :   Dependent
	Description  : Dependent structure for JSON request
	*************************************************************************************/
	public class Dependent {
	    private String isTobaccoUser;
	    private String inTobaccoWellness;
	    private String age;
	    private String memberRelationshipTypeCode;
	    private String memberUniqueId;
	    public dependent(vlocity_ins__GroupCensusMember__c gMember) {
	    	if(gMember.TobaccoUse__c.equalsIgnoreCase('yes'))
	    		this.isTobaccoUser = '1';
	    	else
	    		this.isTobaccoUser = '0';
	    	if(gMember.MemberAge__c != null)
	    	this.age = String.valueOf(gMember.MemberAge__c);
	    }
	}
    
}