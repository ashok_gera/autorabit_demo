/*
@Author : Accenture Offshore Dev team
@name : SGA_UTIL17_AccessGeographicData
@CreateDate :01-02-2018
@Description : This class queries the Geographic Info records.
*/
public with sharing class SGA_UTIL17_AccessGeographicData{
public static String zipcode;
/* This method queries the Geographical_Info__c records for the Zipcode. */
    public static Geographical_Info__c queryGeoInfo(string selectGeoQuery){
         return Database.query(selectGeoQuery);        
    }
}