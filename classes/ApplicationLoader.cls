/**********************************************************************************
Class Name :   ApplicationLoader implements vlocity_ins.VlocityOpenInterface
Date Created : 10-December-2017 
Created By   : Pilot User
Description  : This class is implementing the interface for multiple classes wherein the interface is VlocityOpenInterface.
Change History : 
*************************************************************************************/
 global with sharing class ApplicationLoader implements vlocity_ins.VlocityOpenInterface{
     public static final string LOAD_DATA='loadData';
     public static final string CONTEXTID='ContextId';
     public static final string OMNIATTACHMNT='OmniScript Attachment';
     public static final string OMNIDATAJSON='OmniScriptDataJSON.json';
     public static final string COMPANY_INFO='CompanyInformation';
     public static final string APPTYPE='ApplicationType';
     public static final string MED_COV='MedicalCoverage';
     public static final string DENT_COV='DentalCoverage';
     public static final string AGNT_PROD='Agent/Producer/BrokerCert';
     public static final string VIS_COV='VisionCoverage';
     public static final string ELIGIBILITY='Eligibility';
     public static final string MAPPED_CODE='MappedCode';
     public static final string RATNG_AREA='RatingArea';
     public static final string BRAND='Brand';
     public static final string YEAR='YEAR';
     public static final string AGNCY_NAME='AgencyName';
     public static final string APP_ID='Application Id';
     public static final string BROKER_ID='BrokerId';
     public static final string BROKER_NAME='BrokerName';
     public static final string NEW_ENROLL='NewEnrollment';
     public static final string REQ_EFFECTDATE='RequestedEffectiveDateEnText';
     public static final string CLASS2_CHILD='Class2NumberofEE+Children';
     public static final string CLASS2_SPOUSE='Class2NumberofEE+Spouse';
     public static final string CLASS2_FAMILY='Class2NumberofFamily';
     public static final string CLASS2_SINGLE='Class2NumberofSingles(EE)';
     public static final string CLASS3_CHILD='Class3NumberofEE+Children';
     public static final string CLASS3_SPOUSE='Class3NumberofEE+Spouse';
     public static final string CLASS3_FAMILY='Class3NumberofFamily';
     public static final string CLASS3_SINGLE='Class3NumberofSingles (EE)';
     public static final string GROUPNAME='GroupName';
     public static final string MIN_INFO='MinInfo';
     public static final string MIN_STATE='MinState';
     public static final string NODEN_COV='NoDentalCoverage';
     public static final string PARTY_ACCID='PartyAccountId';
     public static final string QUOTE_PREP='QuotePreparedFor';
     public static final string SIC_CODE='SICCode';
     public static final string SIC='SIC';
     public static final string SELECT_MEDCOV='SelectMedicalCoverage';
     public static final string VIS_OPTNS='VisionOptions';
     public static final string ZIP='ZipCode';
     public static final string ERROR='error';
     public static final string ID='id';
     public static final string PLAN='plan';
     public static final string OPPO_NAME='qOpportunityName';
     public static final string QUOTE_NAME='qQuote Name';
     public static final string INSTANCE_ID='sInstanceId';
     public static final string TEXT_NODE='textNode';
     public static final string TEXT_NODE2='textNode2';
     public static final string TEXT_NODE3='textNode3';
     public static final string TIME_STAMP='timeStamp';
     public static final string USERNAME='userName';
     public static final string USER_PROFILE='userProfile';
     public static final string PERST_COMP='vlcPersistentComponent';
     public static final string APP_EFFECDATE='AppEffectiveDateYear';
     
/****************************************************************************************************
    Method Name : invokeMethod
    Parameters  : String methodName, Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> options
    Return type : Boolean
    Description : This method is used to invoke method name.
******************************************************************************************************/

 public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outputMap, Map<String,Object> options)
    {
        if (methodName == LOAD_DATA){
            return loadData(inputMap, outputMap) ;
        }
        return false;
    }
    
/****************************************************************************************************
    Method Name : loadData
    Parameters  : Map<String,Object> tempinputMap, Map<String,Object> outputMap
    Return type : Boolean
    Description : 1. This method is fetches velocity JSON data from Vlocity application and attaches all required data to omniscript file
******************************************************************************************************/
  public Boolean loadData(Map<String,Object> tempinputMap, Map<String,Object> outputMap){
        Id applcationId = (Id)tempinputMap.get(CONTEXTID);
        String dataJSON = [SELECT vlocity_ins__JSONData__c FROM vlocity_ins__Application__c WHERE ID =: applcationId LIMIT 1].vlocity_ins__JSONData__c;
        
        System.debug('The dataJSON is: ' + dataJSON);
        if(dataJSON == OMNIATTACHMNT){
             Blob AttachedFile = [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :applcationId AND NAME = :OMNIDATAJSON LIMIT 1].body;
            System.debug('The AttachedFile is: ' + AttachedFile.toString());
            dataJSON = AttachedFile.toString();
        }
        
        Map<String,Object> inputMap = (Map<String, Object>) JSON.deserializeUntyped(dataJSON);
        if(inputMap.get(COMPANY_INFO) != null)
            outputMap.put(COMPANY_INFO, inputMap.get(COMPANY_INFO));
        if(inputMap.get(APPTYPE) != null)
            outputMap.put(APPTYPE, inputMap.get(APPTYPE));
        if(inputMap.get(MED_COV) != null)
            outputMap.put(MED_COV, inputMap.get(MED_COV));
        if(inputMap.get(DENT_COV) != null)
            outputMap.put(DENT_COV, inputMap.get(DENT_COV));
        if(inputMap.get(AGNT_PROD) != null)
            outputMap.put(AGNT_PROD, inputMap.get(AGNT_PROD));
        if(inputMap.get(VIS_COV) != null)
            outputMap.put(VIS_COV, inputMap.get(VIS_COV));
        if(inputMap.get(ELIGIBILITY) != null)
            outputMap.put(ELIGIBILITY, inputMap.get(ELIGIBILITY));
        if(inputMap.get(MAPPED_CODE) != null)
            outputMap.put(MAPPED_CODE, inputMap.get(MAPPED_CODE));
        if(inputMap.get(RATNG_AREA) != null)
            outputMap.put(RATNG_AREA, inputMap.get(RATNG_AREA));
        if(inputMap.get(BRAND) != null)
            outputMap.put(BRAND, inputMap.get(BRAND));
       
        Map<String,Object> CompanyInformationField = (Map<String,Object>) inputMap.get(COMPANY_INFO);
        String YEARField = (String)CompanyInformationField.get(APP_EFFECDATE);
        
        System.debug('The YEARField is: ' + YEARField);
        
        if(YEARField != null)
            outputMap.put(YEAR, YEARField);
        if(inputMap.get(AGNCY_NAME) != null)
            outputMap.put(AGNCY_NAME, inputMap.get(AGNCY_NAME));
        if(inputMap.get(APP_ID) != null)
            outputMap.put(APP_ID, inputMap.get(APP_ID));
        if(inputMap.get(BROKER_ID) != null)
            outputMap.put(BROKER_ID, inputMap.get(BROKER_ID));
        if(inputMap.get(BROKER_NAME) != null)
            outputMap.put(BROKER_NAME, inputMap.get(BROKER_NAME));

        if(inputMap.get(NEW_ENROLL) != null)
            outputMap.put(NEW_ENROLL, inputMap.get(NEW_ENROLL));
        if(inputMap.get(REQ_EFFECTDATE) != null)
            outputMap.put(REQ_EFFECTDATE, inputMap.get(REQ_EFFECTDATE));

        if(inputMap.get(CLASS2_CHILD) != null)
            outputMap.put(CLASS2_CHILD, inputMap.get(CLASS2_CHILD));
        if(inputMap.get(CLASS2_SPOUSE) != null)
            outputMap.put(CLASS2_SPOUSE, inputMap.get(CLASS2_SPOUSE));
        if(inputMap.get(CLASS2_FAMILY) != null)
            outputMap.put(CLASS2_FAMILY, inputMap.get(CLASS2_FAMILY));
        if(inputMap.get(CLASS2_SINGLE) != null)
            outputMap.put(CLASS2_SINGLE, inputMap.get(CLASS2_SINGLE));
        if(inputMap.get(CLASS3_CHILD) != null)
            outputMap.put(CLASS3_CHILD, inputMap.get(CLASS3_CHILD));
        if(inputMap.get(CLASS3_SPOUSE) != null)
            outputMap.put(CLASS3_SPOUSE, inputMap.get(CLASS3_SPOUSE));
        if(inputMap.get(CLASS3_FAMILY) != null)
            outputMap.put(CLASS3_FAMILY, inputMap.get(CLASS3_FAMILY));
        if(inputMap.get(CLASS3_SINGLE) != null)
            outputMap.put(CLASS3_SINGLE, inputMap.get(CLASS3_SINGLE));

        if(inputMap.get(GROUPNAME) != null)
            outputMap.put(GROUPNAME, inputMap.get(GROUPNAME));
        if(inputMap.get(MAPPED_CODE) != null)
            outputMap.put(MAPPED_CODE, inputMap.get(MAPPED_CODE));
        if(inputMap.get(MIN_INFO) != null)
            outputMap.put(MIN_INFO, inputMap.get(MIN_INFO));
        if(inputMap.get(MIN_STATE) != null)
            outputMap.put(MIN_STATE, inputMap.get(MIN_STATE));
        if(inputMap.get(NODEN_COV) != null)
            outputMap.put(NODEN_COV, inputMap.get(NODEN_COV));
        if(inputMap.get(PARTY_ACCID) != null)
            outputMap.put(PARTY_ACCID, inputMap.get(PARTY_ACCID));
        if(inputMap.get(QUOTE_PREP) != null)
            outputMap.put(QUOTE_PREP, inputMap.get(QUOTE_PREP));
      /*  if(inputMap.get('SIC') != null)
            outputMap.put('SIC', inputMap.get('SIC'));*/
        if(inputMap.get(SIC_CODE) != null)
            System.debug('The SICCode is: ' + inputMap.get(SIC_CODE));
            outputMap.put(SIC, inputMap.get(SIC));
        if(inputMap.get(SELECT_MEDCOV) != null)
            outputMap.put(SELECT_MEDCOV, inputMap.get(SELECT_MEDCOV));
        if(inputMap.get(VIS_OPTNS) != null)
            outputMap.put(VIS_OPTNS, inputMap.get(VIS_OPTNS));
        if(inputMap.get(ZIP) != null)
            outputMap.put(ZIP, inputMap.get(ZIP));
        if(inputMap.get(ERROR) != null)
            outputMap.put(ERROR, inputMap.get(ERROR));
        if(inputMap.get(ID) != null)
            outputMap.put(ID, inputMap.get(ID));
        if(inputMap.get(PLAN) != null)
            outputMap.put(PLAN, inputMap.get(PLAN));
        if(inputMap.get(OPPO_NAME) != null)
            outputMap.put(OPPO_NAME, inputMap.get(OPPO_NAME));
        if(inputMap.get(QUOTE_NAME) != null)
            outputMap.put(QUOTE_NAME, inputMap.get(QUOTE_NAME));
        if(inputMap.get(INSTANCE_ID) != null)
            outputMap.put(INSTANCE_ID, inputMap.get(INSTANCE_ID));
        if(inputMap.get(TEXT_NODE) != null)
            outputMap.put(TEXT_NODE, inputMap.get(TEXT_NODE));
        if(inputMap.get(TEXT_NODE2) != null)
            outputMap.put(TEXT_NODE2, inputMap.get(TEXT_NODE2));
        if(inputMap.get(TEXT_NODE3) != null)
            outputMap.put(TEXT_NODE3, inputMap.get(TEXT_NODE3));
        if(inputMap.get(TIME_STAMP) != null)
            outputMap.put(TIME_STAMP, inputMap.get(TIME_STAMP));
        if(inputMap.get(USERNAME) != null)
            outputMap.put(USERNAME, inputMap.get(USERNAME));
        if(inputMap.get(USER_PROFILE) != null)
            outputMap.put(USER_PROFILE, inputMap.get(USER_PROFILE));
        if(inputMap.get(PERST_COMP) != null)
            outputMap.put(PERST_COMP, inputMap.get(PERST_COMP));

         return true;
    }

}